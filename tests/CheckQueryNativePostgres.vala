/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/query/native-postgresql/select",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var cnc = new Vpg.Connection ();
        assert (cnc is Vpg.Connection);
        assert (cnc is Vda.Connection);
        var loop = new GLib.MainLoop (null);
        var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
        if (dbcon != "" && dbcon != null)
          cnc_string = dbcon;
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          message ("Connection sucesss: %s", cnc_string);
          if (!cnc.is_opened) {
            message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select.begin (cnc, loop, ()=>{
            message ("End Test Select Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native Postgres, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-postgresql/select-parameters",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var cnc = new Vpg.Connection ();
        assert (cnc is Vpg.Connection);
        assert (cnc is Vda.Connection);
        var loop = new GLib.MainLoop (null);
        var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
        if (dbcon != "" && dbcon != null)
          cnc_string = dbcon;
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select_parameters.begin (cnc, loop, ()=>{
            message ("End Test Select Query Parameters");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native Postgres, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-postgresql/data-object",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var cnc = new Vpg.Connection ();
        assert (cnc is Vpg.Connection);
        assert (cnc is Vda.Connection);
        var loop = new GLib.MainLoop (null);
        var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
        if (dbcon != "" && dbcon != null)
          cnc_string = dbcon;
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select_parameters.begin (cnc, loop, ()=>{
            message ("End Test Select Query Parameters");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native Postgres, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-postgresql/insert-command",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var cnc = new Vpg.Connection ();
        assert (cnc is Vpg.Connection);
        assert (cnc is Vda.Connection);
        var loop = new GLib.MainLoop (null);
        var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
        if (dbcon != "" && dbcon != null)
          cnc_string = dbcon;
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_insert_sql_command_to_query.begin (cnc, loop, ()=>{
            message ("End Test Insert Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native Postgres, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-postgresql/insert-command/parameters",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var cnc = new Vpg.Connection ();
        assert (cnc is Vpg.Connection);
        assert (cnc is Vda.Connection);
        var loop = new GLib.MainLoop (null);
        var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
        if (dbcon != "" && dbcon != null)
          cnc_string = dbcon;
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_insert_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
            message ("End Test Insert Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native Postgres, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-postgresql/select-command/simple-fields",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var cnc = new Vpg.Connection ();
        assert (cnc is Vpg.Connection);
        assert (cnc is Vda.Connection);
        var loop = new GLib.MainLoop (null);
        var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
        if (dbcon != "" && dbcon != null)
          cnc_string = dbcon;
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select_sql_command_to_query.begin (cnc, loop, ()=>{
            message ("End Test SELECT Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native Postgres, started");
        });
        loop.run ();
    });
    // Test.add_func ("/vda/query/native-postgresql/select-command/simple-fields/table-alias",
    // ()=>{
    //     var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
    //     var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
    //     var d = GLib.File.new_for_path (dir);
    //     assert (d.query_exists (null));
    //     var cnc = new Vpg.Connection ();
    //     assert (cnc is Vpg.Connection);
    //     assert (cnc is Vda.Connection);
    //     var loop = new GLib.MainLoop (null);
    //     var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
    //     if (dbcon != "" && dbcon != null)
    //       cnc_string = dbcon;
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_select_sql_command_to_query2.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native Postgres, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-postgresql/select-command/simple-fields/field-alias",
    // ()=>{
    //     var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
    //     var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
    //     var d = GLib.File.new_for_path (dir);
    //     assert (d.query_exists (null));
    //     var cnc = new Vpg.Connection ();
    //     assert (cnc is Vpg.Connection);
    //     assert (cnc is Vda.Connection);
    //     var loop = new GLib.MainLoop (null);
    //     var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
    //     if (dbcon != "" && dbcon != null)
    //       cnc_string = dbcon;
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_select_sql_command_to_query2.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native Postgres, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-postgresql/select-command/simple-condition",
    // ()=>{
    //     var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
    //     var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
    //     var d = GLib.File.new_for_path (dir);
    //     assert (d.query_exists (null));
    //     var cnc = new Vpg.Connection ();
    //     assert (cnc is Vpg.Connection);
    //     assert (cnc is Vda.Connection);
    //     var loop = new GLib.MainLoop (null);
    //     var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
    //     if (dbcon != "" && dbcon != null)
    //       cnc_string = dbcon;
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_select_sql_command_to_query_condition.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native Postgres, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-postgresql/select-command/cond-parameters",
    // ()=>{
    //     var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
    //     var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
    //     var d = GLib.File.new_for_path (dir);
    //     assert (d.query_exists (null));
    //     var cnc = new Vpg.Connection ();
    //     assert (cnc is Vpg.Connection);
    //     assert (cnc is Vda.Connection);
    //     var loop = new GLib.MainLoop (null);
    //     var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
    //     if (dbcon != "" && dbcon != null)
    //       cnc_string = dbcon;
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_select_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native Postgres, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-postgresql/delete-command/simple-condition",
    // ()=>{
    //     var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
    //     var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
    //     var d = GLib.File.new_for_path (dir);
    //     assert (d.query_exists (null));
    //     var cnc = new Vpg.Connection ();
    //     assert (cnc is Vpg.Connection);
    //     assert (cnc is Vda.Connection);
    //     var loop = new GLib.MainLoop (null);
    //     var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
    //     if (dbcon != "" && dbcon != null)
    //       cnc_string = dbcon;
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_delete_sql_command_to_query.begin (cnc, loop, ()=>{
    //         message ("End Test DELETE with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native Postgres, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-postgresql/delete-command/cond-parameters",
    // ()=>{
    //     var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
    //     var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
    //     var d = GLib.File.new_for_path (dir);
    //     assert (d.query_exists (null));
    //     var cnc = new Vpg.Connection ();
    //     assert (cnc is Vpg.Connection);
    //     assert (cnc is Vda.Connection);
    //     var loop = new GLib.MainLoop (null);
    //     var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
    //     if (dbcon != "" && dbcon != null)
    //       cnc_string = dbcon;
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_delete_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
    //         message ("End Test DELETE with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native Postgres, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-postgresql/update-command/simple-condition",
    // ()=>{
    //     var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
    //     var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
    //     var d = GLib.File.new_for_path (dir);
    //     assert (d.query_exists (null));
    //     var cnc = new Vpg.Connection ();
    //     assert (cnc is Vpg.Connection);
    //     assert (cnc is Vda.Connection);
    //     var loop = new GLib.MainLoop (null);
    //     var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
    //     if (dbcon != "" && dbcon != null)
    //       cnc_string = dbcon;
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_update_sql_command_to_query.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native Postgres, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-postgresql/update-command/parameters-cond",
    // ()=>{
    //     var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
    //     var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
    //     var d = GLib.File.new_for_path (dir);
    //     assert (d.query_exists (null));
    //     var cnc = new Vpg.Connection ();
    //     assert (cnc is Vpg.Connection);
    //     assert (cnc is Vda.Connection);
    //     var loop = new GLib.MainLoop (null);
    //     var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
    //     if (dbcon != "" && dbcon != null)
    //       cnc_string = dbcon;
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_update_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native Postgres, started");
    //     });
    //     loop.run ();
    // });

    Test.add_func ("/vda/query/native-postgresql/types/numeric/basic",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var cnc = new Vpg.Connection ();
        assert (cnc is Vpg.Connection);
        assert (cnc is Vda.Connection);
        var loop = new GLib.MainLoop (null);
        var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
        if (dbcon != "" && dbcon != null)
          cnc_string = dbcon;
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          message ("Connection sucesss: %s", cnc_string);
          if (!cnc.is_opened) {
            message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          UniformTests.test_database_types_numeric_basic.begin (cnc, loop, ()=>{
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native Postgres, started");
        });
        loop.run ();
    });
    return Test.run ();
  }
}

