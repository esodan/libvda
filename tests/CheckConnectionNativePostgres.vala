/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Vpg;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/connection/native-postgresql",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        assert (cnc.is_opened);
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.timeout.connect (()=>{
        if (!cnc.is_opened) {
          message ("Connection timeout");
          message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it");
          loop.quit ();
          return;
        }
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
        assert (cnc.status == Vda.Connection.Status.IN_PROGRESS);
      });
      loop.run ();
    });
    return Test.run ();
  }
}

