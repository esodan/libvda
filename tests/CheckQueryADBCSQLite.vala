/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2024 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/query/adbc/sqlite",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var rand = new Rand ();
      string dbname = "table%u.db".printf (rand.next_int ());
      var dbf = GLib.File.new_for_uri (d.get_uri ()+"/"+dbname+".db");
      string driver = "adbc_driver_sqlite";
      string cncstr = "DRIVER=%s;URI=%s".printf (driver, dbf.get_uri());
      var cnc = new VdaGadbc.Connection ();
      assert (cnc is VdaGadbc.Connection);
      assert (cnc is Vda.Connection);
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (dbf.query_exists (null));
        assert (cnc.is_opened);
        message ("Initializing Database ADBC SQLite: %s", dbf.get_uri ());
        UniformTests.initialize_database.begin (cnc, loop, (obj, res)=>{
          try {
            UniformTests.initialize_database.end (res);
          } catch (GLib.Error e) { warning ("%s", e.message); }
          loop.quit ();
        });
      });
      cnc.open_from_string.begin (cncstr);
      loop.run ();
    });
    return Test.run ();
  }
}

