/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Vda;

namespace UniformTests {
  public static async void initialize_database (Vda.Connection cnc, MainLoop loop) throws GLib.Error {
    try {
      message ("Database type: "+cnc.get_type().name());
      assert (cnc.is_opened);
      string sql = "CREATE TABLE IF NOT EXISTS table_test (id INTEGER PRIMARY KEY, name TEXT, number integer)";
      var q = cnc.parse_string (sql);
      assert (q != null);
      if (q is InvalidQuery) {
        warning ("Invalid Query: %s", ((InvalidQuery) q).message);
      }
      assert (!(q is InvalidQuery));
      assert (q.connection == cnc);

      message ("Create table. Database type: "+cnc.get_type().name());
      try {
        yield q.execute (null);
      } catch (GLib.Error e) {
        message ("Create table has an non faltal error: %s", e.message);
      }
      var rand = new GLib.Rand ();
      int id = (int) rand.next_int ();
      sql = "INSERT INTO table_test (id, name, number) VALUES ("+id.to_string ()+", 'test1', 3)";
      message ("SQL to INSERT: %s", sql);
      var q1 = cnc.parse_string (sql);
      assert (q1 != null);
      assert (!(q1 is InvalidQuery));
      assert (q1.connection == cnc);
      message ("Execute Insert. table. Database type: "+cnc.get_type().name());
      var r1 = yield q1.execute (null);
      assert (r1 != null);
      assert (r1 is Result);
      if (r1 is InvalidResult) {
          warning ("Error: %s", ((InvalidResult) r1).message);
      }
    } catch (GLib.Error e) {
      message ("For provider %s, Error: %s", cnc.get_type().name(), e.message);
      assert_not_reached ();
    }
  }
  public static async void test_database_types_numeric_basic (Vda.Connection cnc, MainLoop loop) throws GLib.Error {
    try {
      message ("Database type: "+cnc.get_type().name());
      assert (cnc.is_opened);
      string sql = "CREATE TABLE IF NOT EXISTS types_numeric_basic (id integer, val1 real)";
      var q = cnc.parse_string (sql);
      assert (q != null);
      if (q is InvalidQuery) {
        warning ("Invalid Query: %s", ((InvalidQuery) q).message);
      }
      assert (!(q is InvalidQuery));
      assert (q.connection == cnc);

      message ("Create table. Database type: "+cnc.get_type().name());
      try {
        yield q.execute (null);
      } catch (GLib.Error e) {
        message ("Create table has an non faltal error: %s", e.message);
      }
      sql = "INSERT INTO types_numeric_basic (id, val1) VALUES (1, 3.14157)";
      var q1 = cnc.parse_string (sql);
      assert (q1 != null);
      assert (!(q1 is InvalidQuery));
      assert (q1.connection == cnc);
      message ("Execute Insert. table. Database type: "+cnc.get_type().name());
      q1.execute.begin (null, (obj,res)=>{
        try {
          var r1 = q1.execute.end (res);
          assert (r1 != null);
          assert (r1 is Result);
          message ("Res Type: %s", r1.get_type ().name ());
          if (r1 is InvalidResult) {
            warning ("Invalid Result: error: %s", ((InvalidResult) r1).message);
          }
          var qt1 = cnc.parse_string ("SELECT id, val1 FROM types_numeric_basic");
          qt1.execute.begin (null, (obj, res)=>{
            try {
              var rt1 = qt1.execute.end (res);
              message ("Result Type: %s", rt1.get_type ().name ());
              RowModel row = null;
              if (rt1 is TableModel) {
                var tt1 = rt1 as TableModel;
                assert (tt1.get_n_items () == 1);
                row = ((RowModel) tt1.get_item (0));
              } else if (rt1 is TableModelSequential) {
                  assert (((TableModelSequential) rt1).next ());
                  row = ((TableModelSequential) rt1).current ();
              }
              assert (row != null);
              assert (row.n_columns == 2);
              message ("Column Type: %s", row.get_column_at (0).data_type.name ());
              message ("String Value: %s", row.get_string_at (0));
              var col = row.get_column_at (0);
              assert (col != null);
              assert (col.name == "id");
              assert ("1" == row.get_string_at (0));
              var stt1 = row.get_string_at (1);
              message (stt1);
              assert ("3.14157" in stt1);
              var eq = cnc.parse_string ("DROP TABLE types_numeric_basic");
              eq.execute.begin (null, ()=>{
                message ("Dropped: types_numeric_basic table");
                loop.quit ();
              });
            } catch (GLib.Error e) {
              warning ("For provider %s, Error: %s", cnc.get_type().name(), e.message);
              assert_not_reached ();
            }
          });
        } catch (GLib.Error e) {
          warning ("For provider %s, Error: %s", cnc.get_type().name(), e.message);
          assert_not_reached ();
        }
      });
    } catch (GLib.Error e) {
      warning ("For provider %s, Error: %s", cnc.get_type().name(), e.message);
    }
  }
  public static async void insert_data_select (Vda.Connection cnc, MainLoop loop) {
    var rand = new GLib.Rand ();
    int id = (int) rand.next_int ();
    string sql = "INSERT INTO table_test (id, name) VALUES ("+id.to_string ()+",'test1')";
    try {
      message (cnc.connection_string);
      var q1 = cnc.parse_string (sql);
      yield q1.execute (null);
    } catch (GLib.Error e) {
      message ("%s", e.message);
      assert_not_reached ();
    }
  }
  public static async void test_table_model (Vda.Connection cnc,
                                            string cncstr,
                                            MainLoop loop,
                                            bool skip) throws GLib.Error
  {
    assert (cnc.is_opened);
    assert (cnc.status == Vda.Connection.Status.CONNECTED);
    message ("TableModel: %s", cnc.connection_string);
    try {
      message ("First try, inserting data...");
      yield initialize_database (cnc, loop);
      yield insert_data_select (cnc, loop);
    } catch (GLib.Error e) {
      message ("Trying again. First try fails with error: %s", e.message);
      yield insert_data_select (cnc, loop);
    }
    var sql = "SELECT id, name, number FROM table_test";
    var q2 = cnc.parse_string (sql);
    assert (!(q2 is InvalidQuery));
    var r2 = yield q2.execute (null);
    assert (!(r2 is InvalidResult));
    assert (r2 is TableModel);
    assert (r2 is GLib.ListModel);
    message ("Items in TableModel: %u", ((GLib.ListModel) r2).get_n_items ());
    assert (((GLib.ListModel) r2).get_n_items () >= 1);
    assert (((GLib.ListModel) r2).get_item (0) is RowModel);
    var row = ((GLib.ListModel) r2).get_item (0) as RowModel;
    assert (row.get_column ("id") is ColumnModel);
    assert (row.get_column ("id").data_type == GLib.Type.INT);
    var col = row.get_column ("name");
    assert (col is ColumnModel);
    var t0 = row.get_value ("name");
    assert (t0 != null);
    var t1 = row.get_value_at (1);
    assert (t1 != null);
    var t2 = row.get_string ("name");
    assert (t2 != null);
    var t3 = row.get_string_at (1);
    assert (t3 != null);
  }
  public static async void test_table_model_sequential (Vda.Connection cnc,
                                            string cncstr,
                                            MainLoop loop,
                                            bool skip) throws GLib.Error
  {
    assert (cnc.is_opened);
    assert (cnc.status == Vda.Connection.Status.CONNECTED);
    message ("TableModelSequential: %s", cnc.connection_string);
    try {
      message ("First try, inserting data...");
      yield initialize_database (cnc, loop);
      yield insert_data_select (cnc, loop);
    } catch (GLib.Error e) {
      message ("Trying again. First try fails with error: %s", e.message);
      yield insert_data_select (cnc, loop);
    }
    var sql = "SELECT id, name, number FROM table_test";
    var q2 = cnc.parse_string (sql);
    assert (!(q2 is InvalidQuery));
    var r2 = yield q2.execute (null);
    assert (!(r2 is InvalidResult));
    assert (r2 is TableModelSequential);
    var seq = r2 as TableModelSequential;
    assert (seq != null);
    assert (seq.current () == null);
    assert (seq.next ());
    var row = seq.current ();
    assert (row != null);
    assert (row.n_columns == 3);
    var col = row.get_column ("id");
    assert (col != null);
    assert (col is ColumnModel);
    assert (col.data_type == GLib.Type.INT64);
    var col_name = row.get_column ("name");
    assert (col_name is ColumnModel);
    var t0 = row.get_value ("name");
    assert (t0 != null);
    var t1 = row.get_value_at (1);
    assert (t1 != null);
    var t2 = row.get_string ("name");
    assert (t2 != null);
    var t3 = row.get_string_at (1);
    assert (t3 != null);
  }
}
