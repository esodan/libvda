/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2020 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/query/native-sqlite/select",
    ()=>{
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME=:memory:";
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native Sqlite, started");
          message ("Connection sucesss: %s", cnc_string);
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select.begin (cnc, loop, ()=>{
            message ("End Test Select Query");
            loop.quit ();
          });
        });
        loop.run ();
    });
//    Test.add_func ("/vda/query/native-sqlite/select-parameters",
//    ()=>{
//        var cnc = new Vsqlite.Connection ();
//        assert (cnc is Vsqlite.Connection);
//        assert (cnc is Vda.Connection);
//        var cnc_string = "DB_NAME=:memory:";
//        var loop = new GLib.MainLoop (null);
//        message ("Test: /vda/query/native-sqlite/select-parameters");
//        cnc.canceled.connect ((msg)=>{
//          warning ("Connection opening was canceled: %s", msg);
//        });
//        cnc.opened.connect (()=>{
//          if (!cnc.is_opened) {
//            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
//            loop.quit ();
//            return;
//          }
//          
//          message ("Connection to Native in memory SQLite, stablished");
//          QueryTests.test_select_parameters.begin (cnc, loop, ()=>{
//            message ("End Test Select Query Parameters");
//            loop.quit ();
//          });
//        });
//        cnc.open_from_string.begin (cnc_string, ()=>{
//          message ("Connection to Native in memory SQLite, started");
//        });
//        loop.run ();
//    });
    Test.add_func ("/vda/query/native-sqlite/insert-command",
    ()=>{
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME=:memory:";
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_insert_sql_command_to_query.begin (cnc, loop, ()=>{
            message ("End Test Insert Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native in memory SQLite, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/insert-command/parameters",
    ()=>{
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME=:memory:";
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_insert_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
            message ("End Test Insert Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native in memory SQLite, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/select-command/simple-fields",
    ()=>{
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME=:memory:";
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select_sql_command_to_query.begin (cnc, loop, ()=>{
            message ("End Test SELECT Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native in memory SQLite, started");
        });
        loop.run ();
    });
    // Test.add_func ("/vda/query/native-sqlite/select-command/simple-fields/table-alias",
    // ()=>{
        // var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        // var d = GLib.File.new_for_path (dir);
        // assert (d.query_exists (null));
        // var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        // if (f.query_exists ()) {
        //     try {
        //       f.delete ();
        //     } catch (GLib.Error e) {
        //       message ("Error: %s", e.message);
        //     }
        // }
        // var cnc = new Vsqlite.Connection ();
        // assert (cnc is Vsqlite.Connection);
        // assert (cnc is Vda.Connection);
        // var cnc_string = "DB_NAME="+f.get_path ();
    //     var loop = new GLib.MainLoop (null);
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_select_sql_command_to_query2.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native in memory SQLite, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-sqlite/select-command/simple-fields/field-alias",
    // ()=>{
        // var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        // var d = GLib.File.new_for_path (dir);
        // assert (d.query_exists (null));
        // var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        // if (f.query_exists ()) {
        //     try {
        //       f.delete ();
        //     } catch (GLib.Error e) {
        //       message ("Error: %s", e.message);
        //     }
        // }
        // var cnc = new Vsqlite.Connection ();
        // assert (cnc is Vsqlite.Connection);
        // assert (cnc is Vda.Connection);
        // var cnc_string = "DB_NAME="+f.get_path ();
    //     var loop = new GLib.MainLoop (null);
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_select_sql_command_to_query2.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native in memory SQLite, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-sqlite/select-command/simple-condition",
    // ()=>{
        // var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        // var d = GLib.File.new_for_path (dir);
        // assert (d.query_exists (null));
        // var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        // if (f.query_exists ()) {
        //     try {
        //       f.delete ();
        //     } catch (GLib.Error e) {
        //       message ("Error: %s", e.message);
        //     }
        // }
        // var cnc = new Vsqlite.Connection ();
        // assert (cnc is Vsqlite.Connection);
        // assert (cnc is Vda.Connection);
        // var cnc_string = "DB_NAME="+f.get_path ();
    //     var loop = new GLib.MainLoop (null);
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_select_sql_command_to_query_condition.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native in memory SQLite, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-sqlite/select-command/cond-parameters",
    // ()=>{
        // var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        // var d = GLib.File.new_for_path (dir);
        // assert (d.query_exists (null));
        // var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        // if (f.query_exists ()) {
        //     try {
        //       f.delete ();
        //     } catch (GLib.Error e) {
        //       message ("Error: %s", e.message);
        //     }
        // }
        // var cnc = new Vsqlite.Connection ();
        // assert (cnc is Vsqlite.Connection);
        // assert (cnc is Vda.Connection);
        // var cnc_string = "DB_NAME="+f.get_path ();
    //     var loop = new GLib.MainLoop (null);
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_select_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native in memory SQLite, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-sqlite/delete-command/simple-condition",
    // ()=>{
        // var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        // var d = GLib.File.new_for_path (dir);
        // assert (d.query_exists (null));
        // var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        // if (f.query_exists ()) {
        //     try {
        //       f.delete ();
        //     } catch (GLib.Error e) {
        //       message ("Error: %s", e.message);
        //     }
        // }
        // var cnc = new Vsqlite.Connection ();
        // assert (cnc is Vsqlite.Connection);
        // assert (cnc is Vda.Connection);
        // var cnc_string = "DB_NAME="+f.get_path ();
    //     var loop = new GLib.MainLoop (null);
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_delete_sql_command_to_query.begin (cnc, loop, ()=>{
    //         message ("End Test DELETE with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native in memory SQLite, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-sqlite/delete-command/cond-parameters",
    // ()=>{
        // var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        // var d = GLib.File.new_for_path (dir);
        // assert (d.query_exists (null));
        // var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        // if (f.query_exists ()) {
        //     try {
        //       f.delete ();
        //     } catch (GLib.Error e) {
        //       message ("Error: %s", e.message);
        //     }
        // }
        // var cnc = new Vsqlite.Connection ();
        // assert (cnc is Vsqlite.Connection);
        // assert (cnc is Vda.Connection);
        // var cnc_string = "DB_NAME="+f.get_path ();
    //     var loop = new GLib.MainLoop (null);
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_delete_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
    //         message ("End Test DELETE with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native in memory SQLite, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-sqlite/update-command/simple-condition",
    // ()=>{
        // var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        // var d = GLib.File.new_for_path (dir);
        // assert (d.query_exists (null));
        // var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        // if (f.query_exists ()) {
        //     try {
        //       f.delete ();
        //     } catch (GLib.Error e) {
        //       message ("Error: %s", e.message);
        //     }
        // }
        // var cnc = new Vsqlite.Connection ();
        // assert (cnc is Vsqlite.Connection);
        // assert (cnc is Vda.Connection);
        // var cnc_string = "DB_NAME="+f.get_path ();
    //     var loop = new GLib.MainLoop (null);
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_update_sql_command_to_query.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native in memory SQLite, started");
    //     });
    //     loop.run ();
    // });
    // Test.add_func ("/vda/query/native-sqlite/update-command/parameters-cond",
    // ()=>{
        // var list = GLib.Environ.@get ();
    //     var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        // var d = GLib.File.new_for_path (dir);
        // assert (d.query_exists (null));
        // var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        // if (f.query_exists ()) {
        //     try {
        //       f.delete ();
        //     } catch (GLib.Error e) {
        //       message ("Error: %s", e.message);
        //     }
        // }
        // var cnc = new Vsqlite.Connection ();
        // assert (cnc is Vsqlite.Connection);
        // assert (cnc is Vda.Connection);
        // var cnc_string = "DB_NAME="+f.get_path ();
    //     var loop = new GLib.MainLoop (null);
    //       cnc_string = dbcon;
    //     cnc.canceled.connect ((msg)=>{
    //       warning ("Connection opening was canceled: %s", msg);
    //     });
    //     cnc.opened.connect (()=>{
    //       if (!cnc.is_opened) {
    //         message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
    //         loop.quit ();
    //         return;
    //       }
    //       QueryTests.test_update_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
    //         message ("End Test SELECT with Condition Query");
    //         loop.quit ();
    //       });
    //     });
    //     cnc.open_from_string.begin (cnc_string, ()=>{
    //       message ("Connection to Native in memory SQLite, started");
    //     });
    //     loop.run ();
    // });

//    Test.add_func ("/vda/query/native-sqlite/types/numeric/basic",
//    ()=>{
//        var cnc = new Vsqlite.Connection ();
//        assert (cnc is Vsqlite.Connection);
//        assert (cnc is Vda.Connection);
//        var cnc_string = "DB_NAME=:memory:";
//        var loop = new GLib.MainLoop (null);
//        cnc.canceled.connect ((msg)=>{
//          warning ("Connection opening was canceled: %s", msg);
//        });
//        cnc.opened.connect (()=>{
//          message ("Connection sucesss: %s", cnc_string);
//          if (!cnc.is_opened) {
//            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
//            loop.quit ();
//            return;
//          }
//          message ("Starting uniform tests for database basic numeric types");
//          UniformTests.test_database_types_numeric_basic.begin (cnc, loop, ()=>{
//            message ("Database Types numeric basic test done");
//          });
//        });
//        cnc.open_from_string.begin (cnc_string, ()=>{
//          message ("Connection to Native SQLite, started");
//        });
//        loop.run ();
//    });
    return Test.run ();
  }
}

