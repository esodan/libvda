/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Vpg;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/sql-parser/expression/parse/math",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "10";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionValue);
          assert (exp.to_string () == "10");
          str = "id = 10";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id = 10");
          str = "id = -10";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id = -10");
          str = "id = +10";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id = +10");
          str = "id = 10*3-4";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id = 10*3-4");
          str = "id = 10*3-4*delta";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id = 10*3-4*delta");
          str = "id*2 = 10*3-4*delta";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id*2 = 10*3-4*delta");
          str = "id*2 = alpha*10*3-4*delta";
          message (str);
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id*2 = alpha*10*3-4*delta");
          str = "id/2 = alpha/10*3-4*delta";
          message (str);
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id/2 = alpha/10*3-4*delta");
          str = "id+2 = alpha*10*3-4*delta";
          message (str);
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id+2 = alpha*10*3-4*delta");
          str = "id-2 = alpha-10*3-4*delta";
          message (str);
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id-2 = alpha-10*3-4*delta");
          str = "id*2*teta = alpha*10*omega*3-4*delta";
          message (str);
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id*2*teta = alpha*10*omega*3-4*delta");
          str = "id^2*teta = alpha10*omega*3-4*delta";
          message (str);
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id^2*teta = alpha10*omega*3-4*delta");
          str = "id = alpha/10";
          message (str);
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id = alpha/10");
          str = "id^2*teta/3^2 = alpha/10*omega*3^gama-4*delta";
          message (str);
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "id^2*teta/3^2 = alpha/10*omega*3^gama-4*delta");
          str = "(id^2*teta/3^2) = alpha/10*(omega*3^gama-4)*delta";
          message (str);
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          message (exp.to_string ());
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperator);
          assert (exp is SqlExpressionOperatorEq);
          assert (exp.to_string () == "(id^2*teta/3^2) = alpha/10*(omega*3^gama-4)*delta");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/parameters",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "##id::integer";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionValue);
          message (exp.to_string ());
          assert (exp.to_string () == "##id::gint");
          str = "id = ##id::integer";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorEq);
          message (exp.to_string ());
          assert (exp.to_string () == "id = ##id::gint");

          var @params = new Parameters ();
          str = "id = ##id::integer";
          exp = SqlExpression.parse (str, cnc, @params);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorEq);
          @params.set_value ("id", 10);
          message (exp.to_string ());
          assert (exp.to_string () == "id = 10");

          @params = new Parameters ();
          str = "id = ##id::integer*10";
          exp = SqlExpression.parse (str, cnc, @params);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorEq);
          message (exp.to_string ());
          assert (exp.to_string () == "id = $id*10");
          @params.set_value ("id", 10);
          message (exp.to_string ());
          assert (exp.to_string () == "id = 10*10");
          @params = new Parameters ();

          str = "#id::integer*3 = ##tid::integer*10";
          exp = SqlExpression.parse (str, cnc, @params);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorEq);
          message (exp.to_string ());
          assert (exp.to_string () == "$id*3 = $tid*10");
          @params.set_value ("id", 10);
          @params.set_value ("tid", 14);
          message (exp.to_string ());
          assert (exp.to_string () == "10*3 = 14*10");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/and",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "1 AND 0";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorAnd);
          message (exp.to_string ());
          assert (exp.to_string () == "1 AND 0");
          str = "TRUE AND FALSE";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorAnd);
          message (exp.to_string ());
          assert (exp.to_string () == "TRUE AND FALSE");
          str = "1 AND 0 AND 1";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorAnd);
          message (exp.to_string ());
          assert (exp.to_string () == "1 AND 0 AND 1");

          var @params = new Parameters ();
          str = "1 AND 0 AND ##id::integer";
          exp = SqlExpression.parse (str, cnc, @params);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorAnd);
          @params.set_value ("id", 10);
          message (exp.to_string ());
          assert (exp.to_string () == "1 AND 0 AND 10");@params = new Parameters ();
          str = "TRUE AND ##id::integer AND FALSE AND 1";
          exp = SqlExpression.parse (str, cnc, @params);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorAnd);
          @params.set_value ("id", 10);
          message (exp.to_string ());
          assert (exp.to_string () == "TRUE AND 10 AND FALSE AND 1");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/or",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "1 OR 0";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorOr);
          message (exp.to_string ());
          assert (exp.to_string () == "1 OR 0");
          str = "TRUE OR FALSE";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorOr);
          message (exp.to_string ());
          assert (exp.to_string () == "TRUE OR FALSE");
          str = "1 OR 0 OR 1";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorOr);
          message (exp.to_string ());
          assert (exp.to_string () == "1 OR 0 OR 1");

          var @params = new Parameters ();
          str = "1 OR 0 OR ##id::integer";
          exp = SqlExpression.parse (str, cnc, @params);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorOr);
          @params.set_value ("id", 10);
          message (exp.to_string ());
          assert (exp.to_string () == "1 OR 0 OR 10");@params = new Parameters ();
          str = "TRUE OR ##id::integer OR FALSE OR 1";
          exp = SqlExpression.parse (str, cnc, @params);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorOr);
          @params.set_value ("id", 10);
          message (exp.to_string ());
          assert (exp.to_string () == "TRUE OR 10 OR FALSE OR 1");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/and-or",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "1 OR 0 AND 0";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorAnd);
          message (exp.to_string ());
          assert (exp.to_string () == "1 OR 0 AND 0");
          str = "TRUE AND FALSE OR TRUE";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorOr);
          message (exp.to_string ());
          assert (exp.to_string () == "TRUE AND FALSE OR TRUE");
          str = "1 OR 0 AND 1";
          exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorAnd);
          message (exp.to_string ());
          assert (exp.to_string () == "1 OR 0 AND 1");

          var @params = new Parameters ();
          str = "1 OR 0 AND ##id::integer";
          exp = SqlExpression.parse (str, cnc, @params);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorAnd);
          @params.set_value ("id", 10);
          message (exp.to_string ());
          assert (exp.to_string () == "1 OR 0 AND 10");@params = new Parameters ();
          str = "TRUE AND ##id::integer OR FALSE AND 1";
          exp = SqlExpression.parse (str, cnc, @params);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorAnd);
          @params.set_value ("id", 10);
          message (exp.to_string ());
          assert (exp.to_string () == "TRUE AND 10 OR FALSE AND 1");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/not-equal",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "id != 32";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorNotEq);
          message (exp.to_string ());
          assert (exp.to_string () == "id != 32");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/less-than-or-equal",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "id <= 32";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorLeq);
          message (exp.to_string ());
          assert (exp.to_string () == "id <= 32");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/greather-than-or-equal",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "id >= 32";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorGeq);
          message (exp.to_string ());
          assert (exp.to_string () == "id >= 32");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-diferent-of",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "id <> 32";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is ExpressionOperatorDiff);
          message (exp.to_string ());
          assert (exp.to_string () == "id <> 32");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/greather-than",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "id > 32";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorGt);
          message (exp.to_string ());
          assert (exp.to_string () == "id > 32");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/less-than",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "id < 32";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorLt);
          message (exp.to_string ());
          assert (exp.to_string () == "id < 32");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/in",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "a in b";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorIn);
          message (exp.to_string ());
          assert (exp.to_string () == "a IN b");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/not-in",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "a not in b";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorNotIn);
          message (exp.to_string ());
          assert (exp.to_string () == "a NOT IN b");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/like",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "a LIKE '%l%'";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorLike);
          message (exp.to_string ());
          assert (exp.to_string () == "a LIKE '%l%'");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/not-like",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "a NOT LIKE '%l%'";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorNotLike);
          message (exp.to_string ());
          assert (exp.to_string () == "a NOT LIKE '%l%'");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/ilike",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "a ILIKE '%l%'";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorIlike);
          message (exp.to_string ());
          assert (exp.to_string () == "a ILIKE '%l%'");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/not-ilike",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "a NOT ILIKE '%l%'";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorNotIlike);
          message (exp.to_string ());
          assert (exp.to_string () == "a NOT ILIKE '%l%'");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/multipe/or/like",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "a LIKE '%l%' OR a LIKE '%p%' OR a LIKE '%J%'";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorLike);
          message (exp.to_string ());
          assert (exp.to_string () == "a LIKE '%l%' OR a LIKE '%p%' OR a LIKE '%J%'");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/multipe/or/like/parameters",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "t.a LIKE ##name::string OR t.a LIKE ##name2::string OR a LIKE ##name3::string";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorLike);
          message (exp.to_string ());
          assert (exp.to_string () == "t.a LIKE ##name::gchararray OR t.a LIKE ##name2::gchararray OR a LIKE ##name3::gchararray");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/not",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "NOT b";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorNot);
          message (exp.to_string ());
          assert (exp.to_string () == "NOT b");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-null",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b IS NULL";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          assert (exp is SqlExpressionOperatorIsNull);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS NULL");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-not-null",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b IS NOT NULL";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorIsNotNull);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS NOT NULL");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/similar-to",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b SIMILAR TO a";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorSimilarTo);
          message (exp.to_string ());
          assert (exp.to_string () == "b SIMILAR TO a");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-true",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b IS TRUE";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorIsTrue);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS TRUE");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-not-true",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b IS NOT TRUE";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorIsNotTrue);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS NOT TRUE");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-false",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b is false";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorIsFalse);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS FALSE");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-not-false",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b is not false";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorIsNotFalse);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS NOT FALSE");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-unknown",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b is unknown";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorIsUnknown);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS UNKNOWN");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-not-unknown",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b is not unknown";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorIsNotUnknown);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS NOT UNKNOWN");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-distinct",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b is distinct from a";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorIsDistinctFrom);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS DISTINCT FROM a");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/is-not-distinct",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b is not distinct from a";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorIsNotDistinctFrom);
          message (exp.to_string ());
          assert (exp.to_string () == "b IS NOT DISTINCT FROM a");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/between-and",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b between a and c";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorBetweenAnd);
          message (exp.to_string ());
          assert (exp.to_string () == "b BETWEEN a AND c");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/not-between-and",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b not between a and c";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorNotBetweenAnd);
          message (exp.to_string ());
          assert (exp.to_string () == "b NOT BETWEEN a AND c");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/between-symmetric-and",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b between symmetric a and c";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorBetweenSymmetricAnd);
          message (exp.to_string ());
          assert (exp.to_string () == "b BETWEEN SYMMETRIC a AND c");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/expression/parse/not-between-symmetric-and",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection");
        string str = "b not between symmetric a and c";
        try {
          var exp = SqlExpression.parse (str, cnc);
          assert (exp != null);
          assert (exp is SqlExpression);
          message (exp.get_type ().name ());
          assert (exp is SqlExpressionOperatorNotBetweenSymmetricAnd);
          message (exp.to_string ());
          assert (exp.to_string () == "b NOT BETWEEN SYMMETRIC a AND c");
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    return Test.run ();
  }
}

