/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * libgdadata Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2011 <esodan@gmail.com>
 * 
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gda;
using Vda;
using Gee;

class Tests : GLib.Object {
	public Vda.Connection connection { get; set; }
	public Tests (string dbname) throws GLib.Error {
		var f = GLib.File.new_for_path ("%s.db".printf (dbname));
		if (f.query_exists ()) {
			f.delete ();
		}
		stdout.printf("Creating Database...\n");
		this.connection = new Vgsl.Connection ();
		connection.open_from_string.begin ("DB_DIR=.;DB_NAME=dbdata");
		stdout.printf("Creating table 'user'...\n");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command("CREATE TABLE user (id integer PRIMARY KEY AUTOINCREMENT, name string, city string)");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO user (id, name, city) VALUES (1, \"Daniel\", \"Mexico\")");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO user (id, name, city) VALUES (2, \"Jhon\", \"USA\")");

		stdout.printf("Creating table 'company'...\n");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command("CREATE TABLE company (id int PRIMARY KEY, name string, responsability string)");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO company (id, name, responsability) VALUES (1, \"Telcsa\", \"Programing\")");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO company (id, name, responsability) VALUES (2, \"Viasa\", \"Accessories\")");
	}
	class Record : Vgda.GRecord
	{
		//public static string t = "user";
		
	}

	static int main (string[] args)
	{
		GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
		Test.init (ref args);
		Test.add_func ("/vda/dbdata/record/api",
		()=>{
			try {
				var t = new Tests ("dbdata");
				stdout.printf(">>> NEW TEST: GdaData.DbRecord API tests\n");
				int fails = 0;
				stdout.printf("Creating new record\n");
				var r = new Tests.Record ();
				stdout.printf("Setting connection\n");
				r.connection = t.connection;
				stdout.printf("Setting up DbTable\n");
				var tb = new Vgda.GTable ();
				stdout.printf("Setting DbTable name\n");
				tb.name = "user";
				stdout.printf("Setting DbTable connection\n");
				tb.connection = t.connection;
				stdout.printf(">>> Setting table to record\n");
				r.table = tb;
				stdout.printf(">>> Setting up Key 'id'\n");
				var k = new Vgda.GField ("id", Vda.MetaField.Attribute.NONE);
				stdout.printf("Setting record ID to 1...");
				try {
					k.set_value (1);
					r.set_key (k);
					foreach (MetaField kv in r.keys) {
						stdout.printf ("KEY: " + kv.name + " VALUE: " + Gda.value_stringify(kv.get_value ()) + "\n");
					}
					r.update ();
					foreach (MetaField kv in r.fields) {
						stdout.printf ("FIELD: " + kv.name + " VALUE: " + Gda.value_stringify(kv.get_value ()) + "\n");
					}
					stdout.printf("PASS\n");
				}
				catch (Error e) {
					fails++;
					stdout.printf ("FAIL: %i\nCouln't set ID...ERROR: %s\n", fails, e.message);
				}

				stdout.printf("DbRecord points to, in table "+ r.table.name + ":\n", r.table);
				stdout.printf("%s\n", r.to_string());

				stdout.printf("Getting ID value...");
				var i = (int) (r.get_key ("id")).get_value ();
				if (i != 1 ){
					fails++;
					stdout.printf("FAIL: %i\n", fails);
				}
				else
					stdout.printf("PASS\n");
				
				stdout.printf("Getting value at 'name'...");
				var vdb = (string) (r.get_field ("name")).get_value ();
				if (vdb == null ){
					fails++;
					stdout.printf("FAIL: %i\n", fails);
				}
				else
					if ( vdb != "Daniel"){
						fails++;
						stdout.printf("FAIL: %i\nERROR: Value not match. Expected 'Daniel' but value is %s:\n",
										fails, vdb);
					}
					else
						stdout.printf("PASS\n");

				stdout.printf("Setting value at 'name'...");
				try {
					var f = r.get_field ("name");
					f.set_value ("Daniel Espinosa");
					r.set_field (f);
					stdout.printf("dbdata points to in memory modified value, in table '%s':\n", r.table.name);
					stdout.printf("%s\n", r.to_string());
					stdout.printf("PASS\n");
				}
				catch (Error e) {
					fails++;
					stdout.printf ("FAIL: %i\nCouln't modify record...ERROR: %s\n", fails, e.message);
				}
				stdout.printf("Saving changes...");
				try {
					r.save();
					stdout.printf("dbdata points to modified value, in table '%s':\n", r.table.name);
					stdout.printf("%s\n", r.to_string());
					stdout.printf("PASS\n");
				}
				catch (Error e) {
					fails++;
					stdout.printf ("FAIL: %i\nCouln't SAVE...ERROR: %s\n", fails, e.message);
				}

				try {
					stdout.printf ("Simulating external database update...");
					(t.connection as Vgda.GProvider).cnc.execute_non_select_command("UPDATE user SET name = \"Jhon Strauss\", city =\"New Jersey\"");
					stdout.printf("PASS\n");
				}
				catch (Error e) {
					fails++;
					stdout.printf ("FAIL: %i\nCouln't manual update table '%s'...ERROR: %s\n",
									fails, r.table.name,  e.message);
				}

				stdout.printf("Updating values from database...");
				try {
					r.update();
					stdout.printf("dbdata points to actual stored values, in table '%s':\n", r.table.name);
					stdout.printf("%s\n", r.to_string());

				}
				catch (Error e) {
					fails++;
					stdout.printf ("FAIL: %i\nCouln't UPDATE...ERROR: %s\n", fails, e.message);
				}

				stdout.printf("Setting a new Table... \n");
				var t2 = new Vgda.GTable ();
				t2.name = "company";
				t2.connection = t.connection;
				r.table = t2;
				stdout.printf("Updating values from database using a new table '" + r.table.name + "'...");
				try {
					r.update();
					stdout.printf("dbdata points to actual stored values, in table '%s':\n", r.table.name);
					stdout.printf("%s\n", r.to_string());
					stdout.printf("PASS\n");
				}
				catch (Error e) {
					fails++;
					stdout.printf ("FAIL: %i\nCouln't UPDATE...ERROR: %s\n", fails, e.message);
				}

				stdout.printf("Setting ID to 2...");
				try {
					k.set_value(2);
					r.set_key (k);
					stdout.printf("dbdata points to actual stored values, in table '%s':\n", r.table.name);
					stdout.printf("%s\n", r.to_string());
					stdout.printf("PASS\n");
				}
				catch (Error e) {
					fails++;
					stdout.printf ("FAIL: %i\nCouln't set ID...ERROR: %s\n", fails, e.message);
				}

				stdout.printf ("Coping Record ...");
				var nr = new Tests.Record ();
				nr.copy (r);
				if (!nr.equal (r)) {
					fails++;
					stdout.printf ("FAIL: %i\n No copy or equal works correctly\n", fails);
					stdout.printf ("DUMP (from):\n");
					foreach (MetaField f in r.fields) {
						stdout.printf ("%s\n", f.to_string ());
					}
					stdout.printf ("DUMP (to):\n");
					foreach (MetaField f2 in nr.fields) {
						stdout.printf ("%s\n", f2.to_string ());
					}
				}
				else
					stdout.printf ("PASS\n");
			} catch (GLib.Error e) {
				warning ("Error: %s", e.message);
			}
		});
		Test.add_func ("/vda/dbdata/record/add-delete",
		()=>{
			int fails = 0;
			try {
				var t = new Tests ("dbdata");
				stdout.printf(">>> NEW TEST: Gda.DbRecord - Adding/Deleting objects to DB...\n");
				var n = new Tests.Record ();
				n.connection = t.connection;
				var tb = new Vgda.GTable ();
				tb.name = "user";
				tb.connection = t.connection;
				n.table = tb;
				var f = new HashMap<string,GLib.Value?> ();
				f.set ("id", 3);
				f.set ("name", "GdaDataNewName");
				f.set ("city","GdaDataNewCity");
				foreach (string k in f.keys) {
					n.set_field_value (k, f.get (k));
				}
				stdout.printf("DbRecord in memory values, to added to table '%s':\n", n.table.name);
				stdout.printf("%s\n", n.to_string());
				n.append ();
				var m = (n.connection as Vgda.GProvider).cnc.execute_select_command ("SELECT * FROM user");
				stdout.printf ("All records:\n" + m.dump_as_string () + "\n");
				if (m.get_n_rows () != 3) {
					fails++;
					stdout.printf("FAIL\n");
				}
				else
					stdout.printf("PASS\n");
				// Delete an object from database
				var r = new Tests.Record ();
				r.connection = t.connection;
				r.table = tb;
				r.set_key_value ("id", 3);
				r.drop (false);
				var m2 = (n.connection as Vgda.GProvider).cnc.execute_select_command ("SELECT * FROM user");
				stdout.printf ("All records:\n" + m2.dump_as_string () + "\n");
				if (m2.get_n_rows () != 2) {
					fails++;
					stdout.printf("FAIL\n");
				}
				else
					stdout.printf("PASS\n");
				
			}
			catch (Error e) {
				fails++;
				stdout.printf ("FAIL: %i\nCouln't set add new record...-ERROR: %s\n", fails, e.message);
			}
		});
		return Test.run ();
	}
}
