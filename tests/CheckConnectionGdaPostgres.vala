/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Vgda;
using Vgpg;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/connection-provider/gda-postgresql",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vgpg.Connection ();
      assert (cnc is Vgpg.Connection);
      assert (cnc is Vgda.GProvider);
      assert (cnc is Vda.Connection);
      var loop = new GLib.MainLoop (null);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      cnc.open_from_string.begin (cnc_string, ()=>{
        if (!cnc.is_opened) {
          message ("No connection to PostgreSQL provider. Skiping. If you think this is an error, please report it; %s", cnc_string);
        }
        loop.quit ();
      });
      loop.run ();
    });
    Test.add_func ("/vda/connection/parameters",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vgpg.Connection ();
      assert (cnc is Vgpg.Connection);
      assert (cnc is GProvider);
      assert (cnc is Vda.Connection);
      var loop = new GLib.MainLoop (null);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=t";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      assert (par.get ("DB_NAME") != null);
      assert (par.get ("DB_NAME") is ConnectionParameterDbName);
      assert (par.get ("HOST") != null);
      assert (par.get ("HOST") is ConnectionParameterHost);
      assert (par.get ("USERNAME") != null);
      assert (par.get ("USERNAME") is ConnectionParameterUserName);
      assert (par.get ("PASSWORD") != null);
      assert (par.get ("PASSWORD") is ConnectionParameterPassword);
      par.get("PASSWORD").value = "test1";
      message ("Parameters' connection string: %s", par.to_string ());
      cnc.parameters = par;
      message ("Connection's string: %s", cnc.connection_string);

      TimeoutSource time = new TimeoutSource (2000);
      time.set_callback (() => {
          print ("Timeout!\n");
          loop.quit ();
          return false;
      });
      time.attach (loop.get_context ());

      cnc.opened.connect (()=>{
        message ("Connected");
        loop.quit ();
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening PostgreSQL GDA connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/connection/role",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vgpg.Connection ();
      assert (cnc is Vgpg.Connection);
      assert (cnc is GProvider);
      assert (cnc is Vda.Connection);
      var loop = new GLib.MainLoop (null);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      // var user = par.get ("USERNAME");

      TimeoutSource time = new TimeoutSource (2000);
      time.set_callback (() => {
          print ("Timeout!\n");
          loop.quit ();
          return false;
      });
      time.attach (loop.get_context ());

      cnc.opened.connect (()=>{
        message ("Connected");
        // IMPLEMENT ROLE BASED CONNECTIONS
        // var u = cnc.current_user ();
        // assert (u != null);
        // u.name.begin ((obj, res)=>{
        //   try {
        //     string n = u.name.end (res);
        //     assert (n != null);
        //     message ("Current user: '%s'", n);
        //     assert (n == user.value);
        //     loop.quit ();
        //   } catch (GLib.Error e) {
        //     warning ("Error: %s", e.message);
        //   }
        // });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening PostgreSQL GDA connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    return Test.run ();
  }
}

