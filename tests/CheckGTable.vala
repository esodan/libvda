/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * libgdadata Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2012 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gda;
using Vda;
using Gee;

class Tests : GLib.Object {
	public Vda.Connection connection { get; set; }
	public Vda.MetaTable table { get; set; }
	public Tests (string dbname) {
		try {
			GLib.FileUtils.unlink("table.db");
			bool usepg = false;
			// try {
			// 	(this.connection as Vgda.GProvider).cnc = Connection.open_from_string ("PostgreSQL",
			// 						"DB_NAME=test", null,
			// 						Gda.ConnectionOptions.NONE);
			// 	if ((this.connection as Vgda.GProvider).cnc.is_opened ()) {
			// 		usepg = true;
			// 		stdout.printf ("Using PostgreSQL provider. "+
			// 						"Creating Database...\n");
			// 		init_pg ();
			// 	}
			// }
		 // 	catch (Error e) {
			// 	GLib.message ("Not using PostgreSQL provider. Message: "
			// 					+ e.message+"\n");
			// }
			if (!usepg) {
				this.connection = new Vgsl.Connection ();
				this.connection.open_from_string.begin ("DB_DIR=.;DB_NAME=table", (obj,res)=>{
					try {
						if (this.connection.is_opened) {
							stdout.printf("Using SQLite provider. "+
										"Creating Database...\n");
							Init_sqlite ();
						}
					} catch (Error e) {
						warning ("Error: %s", e.message);
					}
				});
			}
			table = new Vgda.GTable ();
			table.connection = this.connection;
			table.name = "customer";
			table.update_meta = true;
			table.update ();
		} catch (Error e) {
			GLib.warning ("Couln't initalize database. ERROR: "+e.message);
		}
	}
	// private void init_pg () throws Error
	// {
	// 	try {
	// 		try {
	// 			(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 					"DROP TABLE IF EXISTS company CASCADE");
	// 		}
	// 		catch (Error e) {
	// 			stdout.printf ("Error on dopping table company: "+
	// 							e.message+"\n");
	// 		}
	// 		stdout.printf("Creating table 'company'...\n");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"CREATE TABLE company (id serial PRIMARY KEY, " +
	// 				"name text, responsability text)");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"INSERT INTO company (name, responsability) " +
	// 				"VALUES (\'Telcsa\', \'Programing\')");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"INSERT INTO company (name, responsability) " +
	// 				"VALUES (\'Viasa\', \'Accessories\')");
	// 	}
	// 	catch (Error e) {
	// 		stdout.printf ("Error on Create company table: "
	// 						+ e.message+"\n");
	// 	}
	// 	try {
	// 		try {
	// 			(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"DROP TABLE IF EXISTS customer CASCADE");
	// 		}
	// 		catch (Error e) {
	// 			stdout.printf (
	// 				"Error on dopping table customer: "+e.message+"\n");
	// 		}
	// 		stdout.printf("Creating table 'customer'...\n");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 					"CREATE TABLE customer (id serial PRIMARY KEY, "+
	// 					"name text UNIQUE,"+
	// 					" city text DEFAULT \'New Yield\',"+
	// 					" company integer REFERENCES company (id) "+
	// 					"ON DELETE SET NULL ON UPDATE CASCADE)");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"INSERT INTO customer (name, city, company) " +
	// 				"VALUES (\'Daniel\', \'Mexico\', 1)");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"INSERT INTO customer (name, city) VALUES " +
	// 				"(\'Jhon\', \'Springfield\')");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"INSERT INTO customer (name) VALUES (\'Jack\')");
	// 	}
	// 	catch (Error e) {
	// 		stdout.printf ("Error on Create customer table: "
	// 			+ e.message + "\n");
	// 	}
	// 	try {
	// 		try {
	// 			(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 					"DROP TABLE IF EXISTS salary CASCADE");}
	// 		catch (Error e) {
	// 			stdout.printf ("Error on dopping table salary: "
	// 				+e.message+"\n");
	// 		}
	// 		stdout.printf("Creating table 'salary'...\n");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"CREATE TABLE salary (id serial PRIMARY KEY,"+
 //                   " customer integer REFERENCES customer (id) "+
 //                   " ON DELETE CASCADE ON UPDATE CASCADE,"+
 //                   " income float DEFAULT 10.0)");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"INSERT INTO salary (customer, income) VALUES " +
	// 				" (1,55.0)");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"INSERT INTO salary (customer, income) VALUES " +
	// 				" (2,65.0)");
	// 		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
	// 				"INSERT INTO salary (customer) VALUES (3)");
	// 	}
	// 	catch (Error e) {
	// 		stdout.printf (
	// 			"Error on Create company table: "+e.message+"\n");
	// 	}
	// }
	private void Init_sqlite () throws Error
	{
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"CREATE TABLE company (id int PRIMARY KEY, "+
				"name string, responsability string)");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"INSERT INTO company (id, name, responsability) "+
				"VALUES (1, \"Telcsa\", \"Programing\")");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"INSERT INTO company (id, name, responsability) "+
				"VALUES (2, \"Viasa\", \"Accessories\")");

		stdout.printf("Creating table 'customer'...\n");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"CREATE TABLE customer (id integer PRIMARY KEY AUTOINCREMENT,"+
			   " name string UNIQUE,"+
               " city string DEFAULT \'New Yield\',"+
               " company integer REFERENCES company (id) "+
               "ON DELETE SET NULL ON UPDATE CASCADE)");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"INSERT INTO customer (id, name, city, company) "+
				"VALUES (1, \"Daniel\", \"Mexico\", 1)");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"INSERT INTO customer (id, name, city) VALUES (2, "+
				"\"Jhon\", \"Springfield\")");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"INSERT INTO customer (id, name) VALUES (3, \"Jack\")");
		stdout.printf("Creating table 'salary'...\n");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"CREATE TABLE salary (id integer PRIMARY KEY AUTOINCREMENT,"+
               " customer integer REFERENCES customer (id)"+
               " ON DELETE CASCADE ON UPDATE CASCADE,"+
               " income float DEFAULT 10.0)");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"INSERT INTO salary (id, customer, income) "+
				"VALUES (1,1,55.0)");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"INSERT INTO salary (id, customer, income) "+
				"VALUES (2,2,65.0)");
		(this.connection as Vgda.GProvider).cnc.execute_non_select_command(
				"INSERT INTO salary (customer) VALUES (3)");
	}
	private void create_table_definition (Vda.MetaTable t) throws Error
	{
		var field = new Vgda.GFieldInfo ();
		field.name = "id";
		field.value_type = typeof (int);
		field.attributes = MetaFieldInfo.Attribute.PRIMARY_KEY |
							MetaFieldInfo.Attribute.AUTO_INCREMENT;
		t.set_field (field);

		var field1 = new Vgda.GFieldInfo ();
		field1.name = "name";
		field1.value_type = typeof (string);
		field1.attributes = MetaFieldInfo.Attribute.NONE;
		t.set_field (field1);

		var field2 = new Vgda.GFieldInfo ();
		field2.name = "company";
		field2.value_type = typeof (int);
		field2.default_value = 1;
		var fk = new MetaFieldInfo.ForeignKey ();
		var rt = new Vgda.GTable ();
		rt.name = "company";
		fk.reftable = rt;
		fk.refcol.add ("id");
		fk.update_rule = MetaFieldInfo.ForeignKey.Rule.CASCADE;
		fk.delete_rule = MetaFieldInfo.ForeignKey.Rule.SET_DEFAULT;
		field2.fkey = fk;
		t.set_field (field2);
	}
	static int main (string[] args)
	{
		GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
		Test.init (ref args);
		Test.add_func ("/vda/table/fields",
		()=>{
			var tt = new Tests ("table");
			stdout.printf("\n\n\n>>>>>>>>>>>>>>> NEW TEST: Gda.MetaTable - Fields...\n");
			var f = new Gee.HashMap<string,int> ();
			f.set ("id", 0);
			f.set ("name", 0);
			f.set ("city", 0);
			f.set ("company",0);
			foreach (MetaFieldInfo fi in tt.table.fields) {
				assert (f.keys.contains (fi.name));
			}
			var f2 = new Gee.HashMap<string,int> ();
			f2.set ("id",0);
			f2.set ("name",0);
			foreach (MetaFieldInfo fi2 in tt.table.primary_keys) {
				assert (f2.keys.contains (fi2.name));
			}

			var f3 = new Gee.HashMap<string,int> ();
			f3.set ("company",0);
			foreach (Vda.MetaTable t in tt.table.depends) {
				assert (f3.keys.contains (t.name));
			}

			var f4 = new Gee.HashMap<string,int> ();
			f4.set ("salary",0);
			foreach (Vda.MetaTable t2 in tt.table.referenced) {
				assert (f4.keys.contains (t2.name));
			}

			// Test for default values
			int found = 0;
			foreach (MetaFieldInfo fi3 in tt.table.fields) {
				if (MetaFieldInfo.Attribute.HAVE_DEFAULT in fi3.attributes
						&& fi3.name == "city") {
					found++;
					(tt.table.connection as Vgda.GProvider).cnc
								.get_provider ()
									.get_data_handler_g_type ((tt.table.connection as Vgda.GProvider).cnc, typeof (string));
					assert (GLib.strcmp (
							(string) fi3.default_value,
							"New Yield") == 0);
				}
			}

			// MetaFieldInfo
			var fl = new Vgda.GFieldInfo ();
			fl.name = "FieldName1";
			assert (GLib.strcmp (fl.name, "FieldName1") == 0);
			fl.name = "NewFieldName";
			assert (GLib.strcmp (fl.name, "NewFieldName") == 0);
			
			assert (found != 0);
		});
		Test.add_func ("/vda/table/records",
		()=>{
			var tt = new Tests ("table");
			stdout.printf("\n\n\n>>>>>>>>>>>>>>> NEW TEST: Gda.MetaTable - Records...\n");
			foreach (MetaRecord r in tt.table.records) {
				stdout.printf (r.to_string () + "\n");
			}
		});
		Test.add_func ("/vda/table/equivalent",
		()=>{
			try {
				var tt = new Tests ("table");
				stdout.printf("\n\n\n>>>>>>>>>>>>>>> NEW TEST: Gda.MetaTable - equivalent...\n");
				var t = new Vgda.GTable ();
				t.name = "created_table";
				tt.create_table_definition (t);
				var a = new Vgda.GTable ();
				a.name = "created_table";
				a.connection = tt.connection;
				a.update_meta = true;
				a.update ();
				var rs = a.records;
				if (rs != null) {
					stdout.printf (@"Records in DATABASE table: $(a.name)\n");
					foreach (MetaRecord r in rs) {
						stdout.printf (@"$(r)\n");
					}
					stdout.printf (@"Fields in DATABASE table: $(a.name)\n");
					foreach (MetaFieldInfo f2 in a.fields) {
						stdout.printf (f2.to_string () + "\n");
					}
				}
				// FIXME: This must fail- see equivalent implementation
				if (!t.equivalent (a))	{
					stdout.printf (@"Fields in PRE-DEFINED table: $(t.name)\n");
					foreach (MetaFieldInfo f in t.fields)
						stdout.printf (f.to_string () + "\n");
					stdout.printf (@"\nFields in DATABASE table: $(a.name)\n");
					foreach (MetaFieldInfo f2 in a.fields)
						stdout.printf (f2.to_string () + "\n");
				}
				stdout.printf (">>>>>>>> TEST PASS <<<<<<<<<<<\n");
			} catch (Error e) {
				warning ("Error: %s".printf (e.message));
			}
		});
		Test.add_func ("/vda/table/compatible",
		()=>{
			try {
				var tt = new Tests ("table");
				stdout.printf("\n\n\n>>>>>>>>>>>>>>> NEW TEST: Gda.MetaTable - compatible...\n");
				var t = new Vgda.GTable ();
				t.name = "created_table";
				tt.create_table_definition (t);
				var a = new Vgda.GTable ();
				a.name = "created_table";
				a.connection = tt.connection;
				a.update_meta = true;
				a.update ();
				var rs = a.records;
				if (rs != null) {
					stdout.printf (@"Records in DATABASE table: $(a.name)\n");
					foreach (MetaRecord r in rs) { stdout.printf (@"$(r)\n"); }
					stdout.printf (@"Fields in DATABASE table: $(a.name)\n");
					foreach (MetaFieldInfo f2 in a.fields)
						stdout.printf (f2.to_string () + "\n");
				}
				if (!t.compatible (a))	{
					stdout.printf (@"Fields in PRE-DEFINED table: $(t.name)\n");
					foreach (MetaFieldInfo f in t.fields)
						stdout.printf (f.to_string () + "\n");
					stdout.printf (@"\nFields in DATABASE table: $(a.name)\n");
					foreach (MetaFieldInfo f2 in a.fields)
						stdout.printf (f2.to_string () + "\n");
					assert_not_reached ();
				}
			} catch (Error e) {
				warning ("Error: %s".printf (e.message));
			}
		});
		Test.add_func ("/vda/table/append",
		()=>{
			var tt = new Tests ("table");
			stdout.printf("\n\n\n>>>>>>>>>>>>>>> NEW TEST: Gda.MetaTable - Append...\n");
			int fails = 0;
			
			var t = new Vgda.GTable ();
			t.name = "created_table";
			t.connection = tt.connection;

			try {
				stdout.printf ("If the table doesn't exists this will warn...\n");
				if (t.records == null) {
					stdout.printf ("Table exists and is not empty. Deleting it!!\n");
					t.drop (false);
				}
				else {
					stdout.printf ("Table doesn't exist continue...\n");
				}
			}
			catch (Error e) {
				warning ("Error on dropping table with error message: "
								+ e.message + "\n");
			}
			try {
				tt.create_table_definition (t);

				stdout.printf (@"Table NEW '$(t.name)' definition:\n");
				foreach (MetaFieldInfo f in t.fields) {
					stdout.printf ( f.to_string () + "\n");
				}
			} catch (Error e) {
				warning ("Error on create table definition with error message: "
								+ e.message + "\n");
			}
			bool f = false;
			try { t.append (); }
			catch (Error e) {
				stdout.printf (@"ERROR on APPEND: $(e.message)\n");
			}
			try {
				var m =	(tt.connection as Vgda.GProvider).cnc.execute_select_command ("SELECT * FROM created_table");
				stdout.printf ("Table was appended succeeded"+
								@"\nContents\n$(m.dump_as_string())\n");
				if (m.get_column_index ("id") != 0)
					f = true;
				if (m.get_column_index ("name") != 1)
					f = true;
				if (m.get_column_index ("company") != 2)
					f = true;
				if (f) {
					fails++;
					stdout.printf ("Check Ordinal position: FAILED\n");
				}
			}
			catch (Error e) {
				warning ("Error on calling SELECT query "+
								@"for new table $(t.name). "+
								@"ERROR: $(e.message)\n");
			}

			try {
				var r = new Vgda.GRecord ();
				r.connection = tt.connection;
				var nt = new Vgda.GTable ();
				nt.name = "created_table";
				r.table = nt;
				r.set_field_value ("name", "Nancy");
				r.append ();
			}
			catch (Error e) {
				warning ("ERROR on appending "+
											@"Values: $(e.message)\n");
			}

			try {
				var m2 = (tt.connection as Vgda.GProvider).cnc.execute_select_command ("SELECT * FROM created_table");
				bool f2 = false;
				if (m2.get_n_rows () != 1)
					f2 = true;
				int id = (int) m2.get_value_at (0,0);
				if (id != 1)
					f2 = true;
				string name = (string) m2.get_value_at (1,0);
				if (GLib.strcmp (name, "Nancy") != 0)
					f2 = true;
				int company = (int) m2.get_value_at (2,0);
				if (company != 1)
					f2 = true;
				assert (!f);
			}
			catch (Error e) {
				warning (@"ERROR on getting data "+
											@"form new table: $(e.message)\n");
			}
		});
		Test.add_func ("/vda/table/drop",
		()=>{
			var tt = new Tests ("table");
			stdout.printf("\n\n\n>>>>>>>>>>>>>>> NEW TEST: Gda.MetaTable - Drop...\n");
			var t = new Vgda.GTable ();
			t.name = "created_table";
			t.connection = tt.connection;
			try {
					t.drop (false);
			}
			catch (Error e) {
				warning ("Dropping table Fails: " + e.message + "\n");
			}
		});
		Test.add_func ("/vda/table/save",
		()=>{
			var tt = new Tests ("table");
			stdout.printf("\n\n\n>>>>>>>>>>>>>>> NEW TEST: Gda.MetaTable - Rename ...\n");
			
			var t = new Vgda.GTable ();
			t.name = "customer";
			t.connection = tt.connection;
			try {
				t.save ();
				assert_not_reached ();
			}
			catch {}

			try {
				t.name = "customer2";
				t.save ();
			}
			catch (Error e) {
				//FIXME: Implement save
				//warning ("Table rename fails. Message: "+e.message+"\n");
			}
			
			try {
				var m = (tt.connection as Vgda.GProvider).cnc.execute_select_command ("SELECT * FROM customer2");
				stdout.printf ("Data from customer2:\n" + m.dump_as_string ());
			}
			catch (GLib.Error e) {
				//FIXME
				//warning ("Table rename: FAIL: %s".printf (e.message));
			}
		});
		return Test.run ();
	}
}
