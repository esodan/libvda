/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Vpg;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/sql-parser/invalid",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for INSERT test");
        string sql = "INVALID";
        try {
          var command = new CommandInsert (cnc);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          command.parse (sql);
        } catch (GLib.Error e) {
          message ("Catched Parsing Error: %s", e.message);
          loop.quit ();
        }
        loop.quit ();
      });
    });
    Test.add_func ("/vda/sql-parser/parser",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for SqlParser test");
        string sql = "";
        SqlCommand command = null;
        Parser parser = new Vda.Parser ();
        try {
          sql = "INSERT INTO table (id) VALUES (10)";
          command = parser.parse (sql, cnc);
          assert (command != null);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          assert (command is SqlCommandInsert);
          sql = "SELECT * FROM table";
          command = parser.parse (sql, cnc);
          assert (command != null);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          assert (command is SqlCommandSelect);
          sql = "DELETE FROM table WHERE id = 0";
          command = parser.parse (sql, cnc);
          assert (command != null);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          assert (command is SqlCommandDelete);
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Catched Parsing Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/insert",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for INSERT test");
        string sql = "INSERT INTO table_test (id, name, number) VALUES (##id::gint, 'SqlParserInsert', -1)";
        try {
          var command = new CommandInsert (cnc);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          command.parse (sql);
          message (command.stringify ());
          assert (command.stringify () == "INSERT INTO table_test (id, name, number) VALUES (##id::gint, 'SqlParserInsert', -1)");
          command.parameters.set_value ("id", 10);
          message (command.stringify ());
          assert (command.stringify () == "INSERT INTO table_test (id, name, number) VALUES (10, 'SqlParserInsert', -1)");
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/update/all/values",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null) {
        cnc_string = dbcon;
      }
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for UPDATE test");
        string sql = "UPDATE table_test SET id = 10, name = 'SqlParserInsert', number = -1";
        try {
          var command = new CommandUpdate (cnc);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          command.parse (sql);
          message (command.stringify ());
          assert (command.stringify () == "UPDATE table_test SET id = 10, name = 'SqlParserInsert', number = -1");
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/update/all/params",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null) {
        cnc_string = dbcon;
      }
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for UPDATE test");
        string sql = "UPDATE table_test SET id = ##id::gint, name = 'SqlParserInsert', number = -1";
        try {
          var command = new CommandUpdate (cnc);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          command.parse (sql);
          message (command.stringify ());
          assert (command.stringify () == "UPDATE table_test SET id = ##id::gint, name = 'SqlParserInsert', number = -1");
          command.parameters.set_value ("id", 10);
          message (command.stringify ());
          assert (command.stringify () == "UPDATE table_test SET id = 10, name = 'SqlParserInsert', number = -1");
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/update/condition/single-param",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for UPDATE test");
        string sql = "UPDATE table_test SET id = ##id::gint, name = 'SqlParserInsert', number = -1 WHERE id = ##id::integer";
        try {
          var command = new CommandUpdate (cnc);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          command.parse (sql);
          message (command.stringify ());
          assert (command.stringify () == "UPDATE table_test SET id = ##id::gint, name = 'SqlParserInsert', number = -1 WHERE id = ##id::gint");
          command.parameters.set_value ("id", 10);
          message (command.stringify ());
          assert (command.stringify () == "UPDATE table_test SET id = 10, name = 'SqlParserInsert', number = -1 WHERE id = 10");
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/update/condition/multi-params/and",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for UPDATE test");
        string sql = "UPDATE table_test SET text = ##text::string, name = ##name::string, number = ##number::integer WHERE id = ##id::integer AND name = ##namev::string";
        try {
          var command = new CommandUpdate (cnc);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          command.parse (sql);
          message (command.stringify ());
          assert (command.stringify () == "UPDATE table_test SET text = ##text::gchararray, name = ##name::gchararray, number = ##number::gint WHERE id = ##id::gint AND name = ##namev::gchararray");
          command.parameters.set_value ("id", 10);
          command.parameters.set_value ("text", "TextVal");
          command.parameters.set_value ("name", "SqlParserInsert");
          command.parameters.set_value ("number", -1);
          command.parameters.set_value ("namev", "test");
          message (command.stringify ());
          assert (command.stringify () == "UPDATE table_test SET text = 'TextVal', name = 'SqlParserInsert', number = -1 WHERE id = 10 AND name = 'test'");
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/update/condition/multi-params/or",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for UPDATE test");
        string sql = "UPDATE table_test SET text = ##text::string, name = ##name::string, number = ##number::integer WHERE id = ##id::integer OR name = ##namev::string OR ##time::string = '10:00'";
        try {
          var command = new CommandUpdate (cnc);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          command.parse (sql);
          message (command.stringify ());
          assert (command.stringify () == "UPDATE table_test SET text = ##text::gchararray, name = ##name::gchararray, number = ##number::gint WHERE id = ##id::gint OR name = ##namev::gchararray OR ##time::gchararray = '10:00'");
          command.parameters.set_value ("id", 10);
          command.parameters.set_value ("text", "TextVal");
          command.parameters.set_value ("name", "SqlParserInsert");
          command.parameters.set_value ("number", -1);
          command.parameters.set_value ("namev", "test");
          command.parameters.set_value ("time", "11:00");
          message (command.stringify ());
          assert (command.stringify () == "UPDATE table_test SET text = 'TextVal', name = 'SqlParserInsert', number = -1 WHERE id = 10 OR name = 'test' OR '11:00' = '10:00'");
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/delete/all",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for DELETE test");
        string sql = "DELETE FROM table_test";
        try {
          var command = new CommandDelete (cnc);
          command.parse (sql);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          message (command.stringify ());
          assert (command.stringify () == "DELETE FROM table_test");
          command.parameters.set_value ("id", 10);
          message (command.stringify ());
          assert (command.stringify () == "DELETE FROM table_test");
          var rand = new GLib.Rand ();
          var idr = (int) rand.next_int ();
          command.parameters.set_value ("id", idr);
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/delete/condition",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for DELETE test");
        string sql = "DELETE FROM table_test WHERE id = ##id::integer";
        try {
          message (sql);
          var command = new CommandDelete (cnc);
          command.parse (sql);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          message (command.stringify ());
          assert (command.stringify () == "DELETE FROM table_test WHERE id = ##id::gint");
          command.parameters.set_value ("id", 10);
          message (command.stringify ());
          assert (command.stringify () == "DELETE FROM table_test WHERE id = 10");
          var rand = new GLib.Rand ();
          var idr = (int) rand.next_int ();
          command.parameters.set_value ("id", idr);
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/select/all",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for SELECT test");
        string sql = "SELECT * FROM table_test";
        try {
          var command = new CommandSelect (cnc);
          command.parse (sql);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          message (command.stringify ());
          assert (command.stringify () == "SELECT * FROM table_test");
          command.parameters.set_value ("id", 10);
          message (command.stringify ());
          assert (command.stringify () == "SELECT * FROM table_test");
          sql = "SELECT * FROM table_test WHERE id = ##id::string";
          command = new CommandSelect (cnc);
          command.parse (sql);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          message (command.stringify ());
          assert (command.stringify () == "SELECT * FROM table_test WHERE id = ##id::gchararray");
          command.parameters.set_value ("id", "NewText");
          message (command.stringify ());
          assert (command.stringify () == "SELECT * FROM table_test WHERE id = 'NewText'");
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    Test.add_func ("/vda/sql-parser/select/as",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var cnc = new Vpg.Connection ();
      assert (cnc is Vpg.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME=test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (cnc.is_opened);
        message ("Connected to Vda Native Postgresql connection for SELECT test");
        string sql = null;
        SqlCommandSelect command = null;
        try {
          sql = "SELECT t.id FROM table_test AS t";
          command = new CommandSelect (cnc);
          command.parse (sql);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id FROM table_test AS t");
          command.parameters.set_value ("id", 10);
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id FROM table_test AS t");
          sql = "SELECT t.id, t.name, t.number FROM table_test AS t";
          command = new CommandSelect (cnc);
          command.parse (sql);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id, t.name, t.number FROM table_test AS t");
          command.parameters.set_value ("id", 10);
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id, t.name, t.number FROM table_test AS t");
          sql = "SELECT t.id, t.name, t.number FROM table_test AS t WHERE name = ##name::string";
          command = new CommandSelect (cnc);
          command.parse (sql);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id, t.name, t.number FROM table_test AS t WHERE name = ##name::gchararray");
          command.parameters.set_value ("name", "NameTest");
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id, t.name, t.number FROM table_test AS t WHERE name = 'NameTest'");
          sql = "SELECT t.id, t.name, t.number FROM table_test AS t WHERE id = ##id::integer AND name = ##name::string";
          command = new CommandSelect (cnc);
          command.parse (sql);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id, t.name, t.number FROM table_test AS t WHERE id = ##id::gint AND name = ##name::gchararray");
          command.parameters.set_value ("name", "NameTest");
          command.parameters.set_value ("id", 10);
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id, t.name, t.number FROM table_test AS t WHERE id = 10 AND name = 'NameTest'");
          sql = "SELECT t.id, t.name, t.number FROM table_test AS t WHERE t.name LIKE ##name::string OR t.name LIKE ##name2::string";
          command = new CommandSelect (cnc);
          command.parse (sql);
          assert (command is SqlCommand);
          assert (command is SqlCommandParametrized);
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id, t.name, t.number FROM table_test AS t WHERE t.name LIKE ##name::gchararray OR t.name LIKE ##name2::gchararray");
          command.parameters.set_value ("name", "%t%");
          command.parameters.set_value ("name2", "%j%");
          message (command.stringify ());
          assert (command.stringify () == "SELECT t.id, t.name, t.number FROM table_test AS t WHERE t.name LIKE '%t%' OR t.name LIKE '%j%'");
          loop.quit ();
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        loop.quit ();
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 4);
        assert (cnc.parameters.get ("DB_NAME") != null);
        assert (cnc.parameters.get ("USERNAME") != null);
        assert (cnc.parameters.get ("PASSWORD") != null);
        assert (cnc.parameters.get ("HOST") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
      });
      loop.run ();
    });
    return Test.run ();
  }
}

