/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Vgda;
using Vgpg;

namespace DataObjectTests {
  public class Address : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::pkey::auto")]
    public int id { get; set; }
    [Description (nick="@street")]
    public string street { get; set; }
    [Description (nick="@client::id")]
    public int client { get; set; }
    construct {
      database_table_name = "address";
    }
  }
  public class AddressClient : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::id::auto")]
    public int id { get; set; }
    [Description (nick="@street")]
    public string street { get; set; }
    [Description (nick="@client::id")]
    public int client { get; set; }
    construct {
      database_table_name = "address";
    }
  }
  public class MultipleIdAddress : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::id::auto")]
    public int id { get; set; }
    [Description (nick="@street::id")]
    public string street { get; set; }
    [Description (nick="@client::id")]
    public int client { get; set; }
    construct {
      database_table_name = "address";
    }
  }
  public class Addresses : Object, Vda.DataCollection {
    string _parent_property;
    DataObject _parent;
    GLib.Type _object_type;
    string _ref_field;

    public Vda.Connection database_connection { get; set; }
    public string parent_property { get { return _parent_property; } }
    public DataObject parent { get { return _parent; } }
    public GLib.Type object_type { get { return _object_type; } }
    public string ref_field{ get { return _ref_field; } }
    public Cancellable cancellable { get; set; }

    construct  {
      _parent_property = "id";
      _ref_field = "client";
      _object_type = typeof (Address);
    }

    public Addresses (Client client) {
      _parent = client;
    }
  }
  public class Client : Object, Vda.DataObject {
    Vda.Connection _cnc = null;
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection {
      get {
        return _cnc;
      }
      set {
        _cnc = value;
        addresses.database_connection = _cnc;
      }
    }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::pkey::auto")]
    public int id { get; set; }
    [Description (nick="@name")]
    public string name { get; set; }
    [Description (nick="@description")]
    public string description { get; set; }
    [Description (nick="@phone::id")]
    public string phone { get; set; }
    public Addresses addresses { get; set; }

    construct {
      database_table_name = "clients";
      addresses = new Addresses (this);
    }

    public async void initialization () throws GLib.Error {
      var qdt = database_connection.parse_string ("DROP TABLE IF EXISTS clients");
      yield qdt.execute (null);
      var qdt2 = database_connection.parse_string ("DROP TABLE IF EXISTS address");
      yield qdt2.execute (null);
      var qct = database_connection.parse_string ("CREATE TABLE IF NOT EXISTS clients (id SERIAL PRIMARY KEY, name varchar(50), description varchar(50), phone varchar(50), country text default 'Mexico')");
      yield qct.execute (null);
      var qct2 = database_connection.parse_string ("CREATE TABLE IF NOT EXISTS address (id SERIAL PRIMARY KEY, street varchar(100), client integer)");
      yield qct2.execute (null);
    }
  }

  public class TypesCheckExtend : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::id")]
    public int id { get; set; }
    [Description (nick="@val1")]
    public float val1 { get; set; }
    [Description (nick="@val2")]
    public double val2 { get; set; }
    [Description (nick="@val3")]
    public SqlValue val3 { get; set; } // Autodetected value Timestamp without time zone
    [Description (nick="@val4")]
    public DateTime val4 { get; set; } // Value Timestamp with time zone
    [Description (nick="@val5")]
    public SqlValue val5 { get; set; } // Autodetected value Time without time zone
    [Description (nick="@val6")]
    public SqlValue val6 { get; set; } // Autodetected value Time with time zone
    [Description (nick="@val7")]
    public Date val7 { get; set; }
    [Description (nick="@val8")]
    public float val8 { get; set; }
    [Description (nick="@val9")]
    public string val9 { get; set; }
    construct {
      database_table_name = "types_test_extend";
    }
    public async void initialize_database () throws GLib.Error {
      var qdt = database_connection.parse_string ("DROP TABLE IF EXISTS types_test_extend");
      yield qdt.execute (null);
      var qct = database_connection.parse_string ("CREATE TABLE IF NOT EXISTS types_test_extend (id integer, val1 real, val2 double precision, val3 timestamp, val4 timestamp with time zone, val5 time, val6 time with time zone, val7 date, val8 money, val9 text)");
      try {
        yield qct.execute (null);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
      var qst = database_connection.parse_string ("SELECT * FROM types_test_extend");
      try {
        message ("QUERY: %s", qst.sql);
        var r = yield qst.execute (null);
        assert (r is TableModel);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    }
  }

  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/dataobject/select/individual/single-id",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c = new Client ();
        c.name = "TestClient";
        c.description = "Client Description";
        c.phone = "000000001";
        c.database_connection = cnc;
        c.initialization.begin ((obj, res)=>{
          try {
            c.initialization.end (res);
            c.insert_data_into_db.begin ((obj, res)=>{
              try {
                c.insert_data_into_db.end (res);
                var c1 = new Client ();
                c1.database_connection = cnc;
                c1.phone = "000000001";
                assert (c1.name == null);
                assert (c1.description == null);
                c1.update_data_from_db.begin (()=>{
                  assert (c1.name == "TestClient");
                  assert (c1.description == "Client Description");
                  assert (c1.phone == "000000001");
                  assert (c1.id != 0);
                  loop.quit ();
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/dataobject/select/all",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c1 = new Client ();
        c1.name = "TestClient";
        c1.description = "Client Description";
        c1.phone = "000000001";
        c1.database_connection = cnc;
        var c2 = new Client ();
        c2.name = "TestClient";
        c2.description = "Client Description";
        c2.phone = "000000002";
        c2.database_connection = cnc;
        var c3 = new Client ();
        c3.name = "TestClient";
        c3.description = "Client Description";
        c3.phone = "000000003";
        c3.database_connection = cnc;
        c1.insert_data_into_db.begin (()=>{
          c2.insert_data_into_db.begin (()=>{
            c3.insert_data_into_db.begin (()=>{
              try {
                var q = DataObject.create_select_all (c3).to_query ();
                q.execute.begin (null, (obj, res)=>{
                  try {
                    var qres = q.execute.end (res);
                    assert (qres is TableModel);
                    assert (((TableModel) qres).get_n_items () >= 3);
                    loop.quit ();
                  } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                  }
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          });
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/dataobject/select/individual/multiple-id",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c = new Client ();
        c.name = "TestClient";
        c.description = "Client Description";
        c.phone = "000000001";
        c.database_connection = cnc;
        c.initialization.begin ((obj, res)=>{
          try {
            c.initialization.end (res);
            c.insert_data_into_db.begin ((obj, res)=>{
              try {
                c.insert_data_into_db.end (res);
                var c1 = new Client ();
                c1.database_connection = cnc;
                c1.phone = "000000001";
                assert (c1.name == null);
                assert (c1.description == null);
                c1.update_data_from_db.begin (()=>{
                  assert (c1.name == "TestClient");
                  assert (c1.description == "Client Description");
                  assert (c1.phone == "000000001");
                  var a1 = new Address ();
                  a1.database_connection = cnc;
                  a1.street = "Multiple Str 45";
                  a1.client = c1.id;
                  a1.insert_data_into_db.begin (()=>{
                    var a2 = new AddressClient ();
                    a2.database_connection = cnc;
                    a2.client = c1.id;
                    a2.update_data_from_db.begin ((obj, res)=>{
                      try {
                        a2.update_data_from_db.end (res);
                        assert (a2.client == c1.id);
                        message ("Data: id=%d; street:'%s'; client=%d", a2.id, a2.street, a2.client);
                        message ("Getting multiple IDs from dataobject");
                        var a3 = new MultipleIdAddress ();
                        a3.database_connection = cnc;
                        a3.client = a2.client;
                        a3.street = a2.street;
                        a3.update_data_from_db.begin ((obj, res)=>{
                          try {
                            a3.update_data_from_db.end (res);
                            assert (a3.id == a2.id);
                            assert (a3.street == a2.street);
                            assert (a3.client == a2.client);
                            loop.quit ();
                          } catch (GLib.Error e) {
                            warning ("Error by getting multiple id from a data object: %s", e.message);
                          }
                        });
                      } catch (GLib.Error e) {
                        warning ("Error by getting multiple id from a data object: %s", e.message);
                      }
                    });
                  });
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/dataobject/select/individual/multiple-id/missing-params",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c = new Client ();
        c.name = "TestClient";
        c.description = "Client Description";
        c.phone = "000000001";
        c.database_connection = cnc;
        c.initialization.begin ((obj, res)=>{
          try {
            c.initialization.end (res);
            c.insert_data_into_db.begin ((obj, res)=>{
              try {
                c.insert_data_into_db.end (res);
                var c1 = new Client ();
                c1.database_connection = cnc;
                c1.phone = "000000001";
                assert (c1.name == null);
                assert (c1.description == null);
                c1.update_data_from_db.begin (()=>{
                  assert (c1.name == "TestClient");
                  assert (c1.description == "Client Description");
                  assert (c1.phone == "000000001");
                  var a1 = new Address ();
                  a1.database_connection = cnc;
                  a1.street = "Multiple Str 45";
                  a1.client = c1.id;
                  message ("Data: id=%d; street:'%s'; client=%d", a1.id, a1.street, a1.client);
                  a1.insert_data_into_db.begin (()=>{
                    var a2 = new AddressClient ();
                    a2.database_connection = cnc;
                    a2.client = c1.id;
                    a2.update_data_from_db.begin ((obj, res)=>{
                      try {
                        a2.update_data_from_db.end (res);
                        message ("Data: id=%d; street:'%s'; client=%d", a2.id, a2.street, a2.client);
                        message ("Getting multiple IDs from dataobject");
                        var a3 = new MultipleIdAddress ();
                        a3.database_connection = cnc;
                        a3.client = a2.client;
                        a3.update_data_from_db.begin ((obj, res)=>{
                          try {
                            a3.update_data_from_db.end (res);
                            assert_not_reached ();
                          } catch (GLib.Error e) {
                            message ("Catched Correctly Error: %s", e.message);
                            loop.quit ();
                          }
                        });
                      } catch (GLib.Error e) {
                        warning ("Error by getting multiple id from a data object: %s", e.message);
                      }
                    });
                  });
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/dataobject/update/ids",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c = new Client ();
        c.name = "TestClient";
        c.description = "Client Description";
        c.phone = "000000001";
        c.database_connection = cnc;
        c.initialization.begin ((obj, res)=>{
          try {
            c.initialization.end (res);
            var tq = cnc.parse_string ("SELECT * FROM clients");
            tq.execute.begin (null, (obj, res)=>{
              try {
                var r = tq.execute.end (res);
                assert (r is TableModel);
                assert (((TableModel) r).get_n_items () == 0);
                c.insert_data_into_db.begin ((obj, res)=>{
                  try {
                    c.insert_data_into_db.end (res);
                    var tq2 = cnc.parse_string ("SELECT * FROM clients");
                    tq2.execute.begin (null, (obj, res)=>{
                      try {
                        var r2 = tq.execute.end (res);
                        assert (r2 is TableModel);
                        assert (((TableModel) r2).get_n_items () == 1);
                        var c1 = new Client ();
                        c1.database_connection = cnc;
                        c1.phone = "000000001";
                        assert (c1.name == null);
                        assert (c1.description == null);
                        c1.update_data_from_db.begin (()=>{
                          assert (c1.name == "TestClient");
                          assert (c1.description == "Client Description");
                          assert (c1.phone == "000000001");
                          c1.name = "TestClient100";
                          c1.update_data_into_db.begin (()=>{
                            var c2 = new Client ();
                            c2.database_connection = cnc;
                            c2.phone = "000000001";
                            c2.update_data_from_db.begin (()=>{
                              message ("Name: %s", c2.name);
                              assert (c2.name == "TestClient100");
                              assert (c2.description == "Client Description");
                              assert (c2.phone == "000000001");
                              loop.quit ();
                            });
                          });
                        });
                      } catch (GLib.Error e) {
                        warning ("Error: %s", e.message);
                      }
                    });
                  } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                  }
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/dataobject/update/pkeys",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c = new Client ();
        c.name = "TestClient";
        c.description = "Client Description";
        c.phone = "00000000k";
        c.database_connection = cnc;
        c.initialization.begin ((obj, res)=>{
          try {
            c.initialization.end (res);
            var tq = cnc.parse_string ("SELECT * FROM clients");
            tq.execute.begin (null, (obj, res)=>{
              try {
                var r = tq.execute.end (res);
                assert (r is TableModel);
                assert (((TableModel) r).get_n_items () == 0);
                c.insert_data_into_db.begin ((obj, res)=>{
                  try {
                    c.insert_data_into_db.end (res);
                    var tq2 = cnc.parse_string ("SELECT * FROM clients");
                    tq2.execute.begin (null, (obj, res)=>{
                      try {
                        var r2 = tq.execute.end (res);
                        assert (r2 is TableModel);
                        assert (((TableModel) r2).get_n_items () == 1);
                        var c1 = new Client ();
                        c1.database_connection = cnc;
                        c1.phone = "00000000k";
                        assert (c1.name == null);
                        assert (c1.description == null);
                        c1.update_data_from_db.begin (()=>{
                          assert (c1.id != 0);
                          assert (c1.name == "TestClient");
                          assert (c1.description == "Client Description");
                          assert (c1.phone == "00000000k");
                          c1.phone = "00001k";
                          c1.update_data_into_db_pkey.begin ((obj, res)=>{
                            try {
                              c1.update_data_into_db_pkey.end (res);
                              var q = DataObject.create_update_query (c1, true);
                              message (q.stringify ());
                              assert (q.stringify () == "UPDATE clients SET name = ##name::gchararray, description = ##description::gchararray, phone = ##phone::gchararray WHERE id = ##id::gint");
                              var c2 = new Client ();
                              c2.id = c1.id;
                              c2.database_connection = cnc;
                              c2.update_data_from_db_pkey.begin ((obj, res)=>{
                                try {
                                  c2.update_data_from_db_pkey.end (res);
                                  message ("Name: %s", c2.name);
                                  assert (c2.id == c1.id);
                                  assert (c2.name == "TestClient");
                                  assert (c2.description == "Client Description");
                                  assert (c2.phone == "00001k");
                                  loop.quit ();
                                } catch (GLib.Error e) {
                                  warning ("Error: %s", e.message);
                                }
                              });
                            } catch (GLib.Error e) {
                              warning ("Error: %s", e.message);
                            }
                          });
                        });
                      } catch (GLib.Error e) {
                        warning ("Error: %s", e.message);
                      }
                    });
                  } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                  }
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/dataobject/insert",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c = new Client ();
        c.name = "TestClient";
        c.description = "Client Description";
        c.phone = "000000001";
        c.database_connection = cnc;
        c.initialization.begin ((obj, res)=>{
          try {
            c.initialization.end (res);
            c.insert_data_into_db.begin ((obj, res)=>{
              try {
                c.insert_data_into_db.end (res);
                var c1 = new Client ();
                c1.phone = "000000001";
                c1.database_connection = cnc;
                assert (c1.name == null);
                assert (c1.description == null);
                c1.update_data_from_db.begin ((obj, res)=>{
                  try {
                    c1.update_data_from_db.end (res);
                    assert (c1.name == "TestClient");
                    assert (c1.description == "Client Description");
                    assert (c1.phone == "000000001");
                    loop.quit ();
                  } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                  }
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/dataobject/delete/id",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c = new Client ();
        c.name = "TestClient";
        c.description = "Client Description";
        c.phone = "000000001";
        c.database_connection = cnc;
        c.initialization.begin ((obj, res)=>{
          try {
            c.initialization.end (res);
            c.insert_data_into_db.begin ((obj, res)=>{
              try {
                c.insert_data_into_db.end (res);
                var c1 = new Client ();
                c1.database_connection = cnc;
                c1.phone = "000000001";
                assert (c1.name == null);
                assert (c1.description == null);
                c1.update_data_from_db.begin (()=>{
                  assert (c1.name == "TestClient");
                  assert (c1.description == "Client Description");
                  assert (c1.phone == "000000001");
                  c1.delete_data_from_db.begin ((objd, resd)=>{
                    try {
                      c1.delete_data_from_db.end (resd);
                      var qs = cnc.parse_string ("SELECT * FROM clients WHERE id = "+c1.id.to_string ());
                      qs.execute.begin (null, (obj, res)=>{
                        try {
                          var qsr = qs.execute.end (res);
                          assert (qsr is TableModel);
                          assert (((TableModel) qsr).get_n_items () == 0);
                          loop.quit ();
                        } catch (GLib.Error e) {
                          warning ("Error: %s", e.message);
                        }
                      });
                    } catch (GLib.Error e) {
                      warning ("Error: %s", e.message);
                    }
                  });
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/dataobject/delete/pkey",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c = new Client ();
        c.name = "TestClient";
        c.description = "Client Description";
        c.phone = "000000001";
        c.database_connection = cnc;
        c.initialization.begin ((obj, res)=>{
          try {
            c.initialization.end (res);
            c.insert_data_into_db.begin ((obj, res)=>{
              try {
                c.insert_data_into_db.end (res);
                var c1 = new Client ();
                c1.database_connection = cnc;
                c1.phone = "000000001";
                assert (c1.name == null);
                assert (c1.description == null);
                c1.update_data_from_db.begin (()=>{
                  try {
                    assert (c1.id != 0);
                    assert (c1.name == "TestClient");
                    assert (c1.description == "Client Description");
                    assert (c1.phone == "000000001");
                    var q = DataObject.create_delete_query (c1, true);
                    message (q.stringify ());
                    assert (q.stringify () == "DELETE FROM clients WHERE id = ##id::gint");
                    c1.delete_data_from_db_pkey.begin ((objd, resd)=>{
                      try {
                        c1.delete_data_from_db_pkey.end (resd);
                        var qs = cnc.parse_string ("SELECT * FROM clients WHERE id = "+c1.id.to_string ());
                        qs.execute.begin (null, (obj, res)=>{
                          try {
                            var qsr = qs.execute.end (res);
                            assert (qsr is TableModel);
                            assert (((TableModel) qsr).get_n_items () == 0);
                            loop.quit ();
                          } catch (GLib.Error e) {
                            warning ("Error: %s", e.message);
                          }
                        });
                      } catch (GLib.Error e) {
                        warning ("Error: %s", e.message);
                      }
                    });
                  } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                  }
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/vda/dataobject/collection",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vgpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        message ("Connected");
        var c = new Client ();
        c.name = "TestClient";
        c.description = "Client Description";
        c.phone = "000000001";
        c.database_connection = cnc;
        c.initialization.begin ((obj, res)=>{
          try {
            c.initialization.end (res);
            c.insert_data_into_db.begin ((obj, res)=>{
              try {
                c.insert_data_into_db.end (res);
                var c1 = new Client ();
                c1.database_connection = cnc;
                c1.phone = "000000001";
                c1.update_data_from_db.begin ((obj, res)=>{
                  try {
                    c1.update_data_from_db.end (res);
                    assert (c1.id != 0);
                    var add1 = new Address ();
                    add1.database_connection = cnc;
                    add1.street = "Fortest Street 56";
                    add1.client = c1.id;
                    add1.insert_data_into_db.begin ((obj1, res1)=>{
                      try {
                        add1.insert_data_into_db.end (res1);
                        var add2 = new Address ();
                        add2.database_connection = cnc;
                        add2.street = "North 4";
                        add2.client = c1.id;
                        add2.insert_data_into_db.begin ((obj2, res2)=>{
                          try {
                            add2.insert_data_into_db.end (res2);
                            var add3 = new Address ();
                            add3.database_connection = cnc;
                            add3.street = "West 89";
                            add3.client = c1.id;
                            add3.insert_data_into_db.begin ((obj3, res3)=>{
                              try {
                                add3.insert_data_into_db.end (res3);
                                assert (c1.addresses != null);
                                c1.addresses.get_objects.begin ((obj4, res4)=>{
                                  try {
                                    message ("Getting all address for given client");
                                    var objs = c1.addresses.get_objects.end (res4);
                                    assert (objs is TableModel);
                                    assert (objs.get_n_items () == 3);
                                    var nadd = new Address ();
                                    nadd.update_from_row (objs, 0);
                                    assert (nadd.street != null);
                                    assert (nadd.client == c1.id);
                                    message ("Object Collection tested!");
                                    loop.quit ();
                                  } catch (GLib.Error e) {
                                    warning ("Error: %s", e.message);
                                  }
                                });
                                loop.quit ();
                              } catch (GLib.Error e) {
                                warning ("Error: %s", e.message);
                              }
                            });
                          } catch (GLib.Error e) {
                            warning ("Error: %s", e.message);
                          }
                        });
                      } catch (GLib.Error e) {
                        warning ("Error: %s", e.message);
                      }
                    });
                  } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                  }
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    return Test.run ();
  }
}
