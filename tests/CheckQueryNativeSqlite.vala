/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2020 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/query/native-sqlite/select",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native Sqlite, started");
          message ("Connection sucesss: %s", cnc_string);
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select.begin (cnc, loop, ()=>{
            message ("End Test Select Query");
            loop.quit ();
          });
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/select-parameters",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select_parameters.begin (cnc, loop, ()=>{
            message ("End Test Select Query Parameters");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native SQLite, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/data-object",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select_parameters.begin (cnc, loop, ()=>{
            message ("End Test Select Query Parameters");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native SQLite, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/insert-command",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_insert_sql_command_to_query.begin (cnc, loop, ()=>{
            message ("End Test Insert Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native SQLite, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/insert-command/parameters",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_insert_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
            message ("End Test Insert Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native SQLite, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/select-command/simple-fields",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          QueryTests.test_select_sql_command_to_query.begin (cnc, loop, ()=>{
            message ("End Test SELECT Query");
            loop.quit ();
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native SQLite, started");
        });
        loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/select-command/simple-fields/table-alias",
    ()=>{
        var list = GLib.Environ.@get ();
       var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
           try {
             f.delete ();
           } catch (GLib.Error e) {
             message ("Error: %s", e.message);
           }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
       var loop = new GLib.MainLoop (null);
       cnc.canceled.connect ((msg)=>{
         warning ("Connection opening was canceled: %s", msg);
       });
       cnc.opened.connect (()=>{
         if (!cnc.is_opened) {
           message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
           loop.quit ();
           return;
         }
         QueryTests.test_select_sql_command_to_query2.begin (cnc, loop, ()=>{
           message ("End Test SELECT Query");
           loop.quit ();
         });
       });
       cnc.open_from_string.begin (cnc_string, ()=>{
         message ("Connection to Native SQLite, started");
       });
       loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/select-command/simple-fields/field-alias",
    ()=>{
        var list = GLib.Environ.@get ();
       var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
           try {
             f.delete ();
           } catch (GLib.Error e) {
             message ("Error: %s", e.message);
           }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
       var loop = new GLib.MainLoop (null);
       cnc.canceled.connect ((msg)=>{
         warning ("Connection opening was canceled: %s", msg);
       });
       cnc.opened.connect (()=>{
         if (!cnc.is_opened) {
           message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
           loop.quit ();
           return;
         }
         QueryTests.test_select_sql_command_to_query3.begin (cnc, loop, ()=>{
           message ("End Test SELECT Query");
           loop.quit ();
         });
       });
       cnc.open_from_string.begin (cnc_string, ()=>{
         message ("Connection to Native SQLite, started");
       });
       loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/select-command/simple-condition",
    ()=>{
        var list = GLib.Environ.@get ();
       var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
           try {
             f.delete ();
           } catch (GLib.Error e) {
             message ("Error: %s", e.message);
           }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
       var loop = new GLib.MainLoop (null);
       cnc.canceled.connect ((msg)=>{
         warning ("Connection opening was canceled: %s", msg);
       });
       cnc.opened.connect (()=>{
         if (!cnc.is_opened) {
           message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
           loop.quit ();
           return;
         }
         QueryTests.test_select_sql_command_to_query_condition.begin (cnc, loop, ()=>{
           message ("End Test SELECT with Condition Query");
           loop.quit ();
         });
       });
       cnc.open_from_string.begin (cnc_string, ()=>{
         message ("Connection to Native SQLite, started");
       });
       loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/select-command/cond-parameters",
    ()=>{
        var list = GLib.Environ.@get ();
       var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
           try {
             f.delete ();
           } catch (GLib.Error e) {
             message ("Error: %s", e.message);
           }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
       var loop = new GLib.MainLoop (null);
       cnc.canceled.connect ((msg)=>{
         warning ("Connection opening was canceled: %s", msg);
       });
       cnc.opened.connect (()=>{
         if (!cnc.is_opened) {
           message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
           loop.quit ();
           return;
         }
         QueryTests.test_select_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
           message ("End Test SELECT Query");
           loop.quit ();
         });
       });
       cnc.open_from_string.begin (cnc_string, ()=>{
         message ("Connection to Native SQLite, started");
       });
       loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/delete-command/simple-condition",
    ()=>{
        var list = GLib.Environ.@get ();
       var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
           try {
             f.delete ();
           } catch (GLib.Error e) {
             message ("Error: %s", e.message);
           }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
       var loop = new GLib.MainLoop (null);
       cnc.canceled.connect ((msg)=>{
         warning ("Connection opening was canceled: %s", msg);
       });
       cnc.opened.connect (()=>{
         if (!cnc.is_opened) {
           message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
           loop.quit ();
           return;
         }
         QueryTests.test_delete_sql_command_to_query.begin (cnc, loop, ()=>{
           message ("End Test DELETE with Condition Query");
           loop.quit ();
         });
       });
       cnc.open_from_string.begin (cnc_string, ()=>{
         message ("Connection to Native SQLite, started");
       });
       loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/delete-command/cond-parameters",
    ()=>{
        var list = GLib.Environ.@get ();
       var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
           try {
             f.delete ();
           } catch (GLib.Error e) {
             message ("Error: %s", e.message);
           }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
       var loop = new GLib.MainLoop (null);
       cnc.canceled.connect ((msg)=>{
         warning ("Connection opening was canceled: %s", msg);
       });
       cnc.opened.connect (()=>{
         if (!cnc.is_opened) {
           message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
           loop.quit ();
           return;
         }
         QueryTests.test_delete_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
           message ("End Test DELETE with Condition Query");
           loop.quit ();
         });
       });
       cnc.open_from_string.begin (cnc_string, ()=>{
         message ("Connection to Native SQLite, started");
       });
       loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/update-command/simple-condition",
    ()=>{
        var list = GLib.Environ.@get ();
       var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
           try {
             f.delete ();
           } catch (GLib.Error e) {
             message ("Error: %s", e.message);
           }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
       var loop = new GLib.MainLoop (null);
       cnc.canceled.connect ((msg)=>{
         warning ("Connection opening was canceled: %s", msg);
       });
       cnc.opened.connect (()=>{
         if (!cnc.is_opened) {
           message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
           loop.quit ();
           return;
         }
         QueryTests.test_update_sql_command_to_query.begin (cnc, loop, ()=>{
           message ("End Test SELECT with Condition Query");
           loop.quit ();
         });
       });
       cnc.open_from_string.begin (cnc_string, ()=>{
         message ("Connection to Native SQLite, started");
       });
       loop.run ();
    });
    Test.add_func ("/vda/query/native-sqlite/update-command/parameters-cond",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
           try {
             f.delete ();
           } catch (GLib.Error e) {
             message ("Error: %s", e.message);
           }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
       var loop = new GLib.MainLoop (null);
       cnc.canceled.connect ((msg)=>{
         warning ("Connection opening was canceled: %s", msg);
       });
       cnc.opened.connect (()=>{
         if (!cnc.is_opened) {
           message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
           loop.quit ();
           return;
         }
         QueryTests.test_update_sql_command_to_query_parameters.begin (cnc, loop, ()=>{
           message ("End Test SELECT with Condition Query");
           loop.quit ();
         });
       });
       cnc.open_from_string.begin (cnc_string, ()=>{
         message ("Connection to Native SQLite, started");
       });
       loop.run ();
    });

    Test.add_func ("/vda/query/native-sqlite/types/numeric/basic",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          message ("Connection sucesss: %s", cnc_string);
          if (!cnc.is_opened) {
            message ("No connection to SQLite provider. Skiping. If you think this is an error, please report it: %s", cnc_string);
            loop.quit ();
            return;
          }
          UniformTests.test_database_types_numeric_basic.begin (cnc, loop, ()=>{
          });
        });
        cnc.open_from_string.begin (cnc_string, ()=>{
          message ("Connection to Native SQLite, started");
        });
        loop.run ();
    });
    

    Test.add_func ("/vda/query/native-sqlite/native-bind",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-query-test-select.db");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }
        var cnc = new Vsqlite.Connection ();
        assert (cnc is Vsqlite.Connection);
        assert (cnc is Vda.Connection);
        var cnc_string = "DB_NAME="+f.get_path ();
        var loop = new GLib.MainLoop (null);
        cnc.canceled.connect ((msg)=>{
          warning ("Connection opening was canceled: %s", msg);
        });
        cnc.opened.connect (()=>{
          message ("Connection is opened");
        });

        Idle.add (()=>{
          cnc.open_from_string.begin (cnc_string, ()=>{
            UniformTests.initialize_database.begin (cnc, loop, ()=>{
              message ("Connection to Native SQLite, done");
              if (!cnc.is_opened) {
                warning ("No connection to SQLite provider");
              } else {
                message ("Connection sucesss: %s", cnc_string);
              }

              try {
                string sql = "SELECT :idem";
                var q1 = cnc.parse_string_prepared (null, sql);
                assert (q1 is Vda.ParsedQuery);
                ((Vda.ParsedQuery) q1).parameters.set_value (":idem", 200);
                string r1 = q1.render_sql ();
                message ("SQL RENDERED: %s", r1);
                assert (r1 == "SELECT 200");
                sql = "SELECT @idem";
                var q2 = cnc.parse_string_prepared (null, sql);
                assert (q2 is Vda.ParsedQuery);
                ((Vda.ParsedQuery) q2).parameters.set_value ("@idem", 300);
                string r2 = q2.render_sql ();
                message ("SQL RENDERED: %s", r2);
                assert (r2 == "SELECT 300");
                sql = "SELECT $idem";
                var q3 = cnc.parse_string_prepared (null, sql);
                assert (q3 is Vda.ParsedQuery);
                ((Vda.ParsedQuery) q3).parameters.set_value ("$idem", 400);
                string r3 = q3.render_sql ();
                message ("SQL RENDERED: %s", r3);
                assert (r3 == "SELECT 400");
                sql = "SELECT 500 = $idem";
                var q4 = cnc.parse_string_prepared (null, sql);
                assert (q4 is Vda.ParsedQuery);
                ((Vda.ParsedQuery) q4).parameters.set_value ("$idem", "Text");
                string r4 = q4.render_sql ();
                message ("SQL RENDERED: %s", r4);
                assert (r4 == "SELECT 500 = 'Text'");
                
                // Switch back to GDA's like parameters definition
                sql = "SELECT id FROM table_test WHERE id = ##idem::integer";
                var q5 = cnc.parse_string_prepared (null, sql);
                assert (q5 is Vda.ParsedQuery);
                ((Vda.ParsedQuery) q5).parameters.set_value ("idem", 600);
                string r5 = q5.render_sql ();
                message ("SQL RENDERED: %s", r5);
                assert (r5 == "SELECT id FROM table_test WHERE id = 600");

                // Complex queries
                message ("Parsing complex queries");
                sql = "SELECT * FROM table_test WHERE id = $id";
                var q6 = cnc.parse_string_prepared (null, sql);
                assert (q6 is Vda.ParsedQuery);
                message ("SQL: %s", q6.sql);
                ((Vda.ParsedQuery) q6).parameters.set_value ("$id", 600);
                string r6 = q6.render_sql ();
                message ("SQL RENDERED: %s", r6);
                assert (r6 == "SELECT * FROM table_test WHERE id = 600");

                // Setup more complex table
                string table = """CREATE TABLE IF NOT EXISTS symbols (id INTEGER PRIMARY KEY AUTOINCREMENT, full_name TEXT, name TEXT, documentation TEXT, code TEXT, file TEXT, kind INTEGER, data_type TEXT, container_name TEXT, location_line_start INTEGER, location_character_start INTEGER, location_line_end INTEGER, location_character_end INTEGER, deprecated BOOLEAN, detail TEXT, selection_line_start INTEGER, selection_character_start INTEGER, selection_line_end INTEGER, selection_character_end INTEGER, UNIQUE (full_name, file))""";
                var ct = cnc.parse_string (table);
                ct.execute.begin (null, (o,r)=>{
                  try {
                    ct.execute.end (r);
                    sql = """SELECT * FROM symbols WHERE file = $uri AND full_name LIKE 'block::%' AND NOT ((location_line_start > $line OR location_line_end < $line) OR ($line = location_line_start AND $char < location_character_start) OR ($line = location_line_end AND $char > location_character_end))""";
                    var q7 = cnc.parse_string_prepared (null, sql);
                    assert (q7 is Vda.ParsedQuery);
                    message ("SQL: %s", q7.sql);
                    q7.parameters.set_value ("$line", 3);
                    q7.parameters.set_value ("$char", 1);
                    q7.parameters.set_value ("$uri", "file:///");
                    string r7 = q7.render_sql ();
                    message ("SQL RENDERED: %s", r7);
                    assert (r7 == """SELECT * FROM symbols WHERE file = 'file:///' AND full_name LIKE 'block::%' AND NOT ((location_line_start > 3 OR location_line_end < 3) OR (3 = location_line_start AND 1 < location_character_start) OR (3 = location_line_end AND 1 > location_character_end))""");
                    loop.quit ();
                  } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                  }
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          });
          return Source.REMOVE;
        });
        loop.run ();
    });
    return Test.run ();
  }
}

