/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/value/money",
    ()=>{
      Vda.ValueMoney v = new Vda.ValueMoney ();
      v.set_double (1000.128);
      message (v.locale ());
      message (v.int_locale ());
      v.set_double (10000.128);
      message (v.locale ());
      message (v.int_locale ());
      v.set_double (100000.12899);
      message (v.locale ());
      message (v.int_locale ());

      v.set_precision (3);
      v.set_int_precision (3);
      v.set_double (1000.128);
      message (v.locale ());
      message (v.int_locale ());
      v.set_double (10000.128);
      message (v.locale ());
      message (v.int_locale ());
      v.set_double (100000.12899);
      message (v.locale ());
      message (v.int_locale ());

      message (v.to_string ());
    });
    Test.add_func ("/vda/value/xml",
    ()=>{
      try {
        Vda.ValueXml v = new Vda.ValueXml ();
        v.parse ("<hello><child/></hello>");
        GXml.DomDocument doc = v.document;
        assert (doc != null);
        assert (doc.document_element != null);
        assert (doc.document_element.child_nodes.size == 1);
        assert (doc.document_element.child_nodes.item (0) != null);
        message (doc.document_element.child_nodes.item (0).node_name);
        assert (doc.document_element.child_nodes.item (0).node_name == "child");
        message (v.to_string ());
        assert ("<hello><child/></hello>" in v.to_string ());
        assert (doc.document_element.child_nodes.item (0) is GXml.DomElement);
        ((GXml.DomElement) doc.document_element.child_nodes.item (0)).set_attribute ("myatt", "value");
        message (doc.write_string ());
        assert ("""<hello><child myatt="value"/></hello>""" in v.to_string ());
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/vda/value/json",
    ()=>{
      Vda.ValueJson v = new Vda.ValueJson ();
      v.parse ("""{"prop1": "value"}""");
      message (v.to_string ());
      assert ("prop1" in v.to_string ());
      assert ("value" in v.to_string ());
      Json.Node doc = v.document;
      message (v.to_string ());
      assert ("prop1" in v.to_string ());
      assert ("value" in v.to_string ());
      message ("%s", doc.get_node_type ().to_string ());
      assert (doc.get_node_type () == Json.NodeType.OBJECT);
      var iter = Json.ObjectIter ();
      iter.init (doc.get_object ());
      Json.Node node = null;
      string name = null;
      assert (iter.next (out name, out node));
      assert (name == "prop1");
      assert (node != null);
      message (node.get_node_type ().to_string ());
      assert (node.get_node_type () == Json.NodeType.VALUE);
    });
    Test.add_func ("/vda/value/time",
    ()=>{
      Vda.SqlValueTime v = new Vda.ValueTime ();
      v.parse ("11:04:32-04");
      message (v.to_string ());
      assert (v.to_string () == "11:04:32-04");
      var dt = v.get_timestamp ();
      assert (dt.get_hour () == 11);
      assert (dt.get_minute () == 4);
      assert (dt.get_seconds () == 32);
      var tz = dt.get_timezone ();
      message ("TZ: %s", tz.get_identifier ());
      assert (tz.get_identifier () == "-04");
    });
    Test.add_func ("/vda/value/time_ntz",
    ()=>{
      Vda.SqlValueTimeNtz v = new Vda.ValueTimeNtz ();
      v.parse ("17:12:41");
      message (v.to_string ());
      assert (v.to_string () == "17:12:41");
      var dt = v.get_timestamp ();
      assert (dt.get_hour () == 17);
      assert (dt.get_minute () == 12);
      assert (dt.get_seconds () == 41);
    });
    return Test.run ();
  }
}

