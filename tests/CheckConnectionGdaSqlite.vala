/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Vgda;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/connection-provider/gda-sqlite",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var dbf = GLib.File.new_for_uri (d.get_uri ()+"/test_cnc.db");
      string cncstr = "DB_DIR=%s;DB_NAME=test_cnc".printf (d.get_path ());
      var cnc = new Vgsl.Connection ();
      assert (cnc is Vgsl.Connection);
      assert (cnc is GProvider);
      assert (cnc is Vda.Connection);
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        assert (dbf.query_exists (null));
        assert (cnc.is_opened);
        loop.quit ();
      });
      TimeoutSource time = new TimeoutSource (2000);
      time.set_callback (() => {
          print ("Timeout!\n");
          loop.quit ();
          return false;
      });
      time.attach (loop.get_context ());
      Idle.add (()=>{
        cnc.open_from_string.begin (cncstr, ()=>{
          message ("End opening operation...");
        });
        return Source.REMOVE;
      });
      loop.run ();
      try { dbf.@delete (); } catch (GLib.Error e) { warning ("%s", e.message); }
    });
    return Test.run ();
  }
}

