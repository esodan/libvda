/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

namespace QueryTests {
  public static async void test_select (Vda.Connection cnc, MainLoop loop) throws GLib.Error {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    string sql = "SELECT id, name, number FROM table_test";
    var q = cnc.parse_string (sql);
    try {
      var res = yield q.execute (null);
      Vda.RowModel rw = null;
      message ("Test SELECT");
      if (res is TableModel) {
        var t = res as TableModel;
        assert (t.get_n_items () > 0);
        rw = t.get_item (0) as RowModel;
        assert (rw != null);
        assert (rw is RowModel);
      }

      if (res is TableModelSequential) {
        var t = res as TableModelSequential;
        assert (t.next ());
        rw = t.current ();
        assert (rw != null);
        assert (rw is RowModel);
      }

      assert (rw != null);
      assert (rw.n_columns == 3);
      assert (rw.get_value_at (0) != null);
      GLib.Value vid = rw.get_value ("id").to_gvalue ();
      int id = 0;
      if (vid.holds (GLib.Type.INT)) {
        id = (int) vid;
      } else if (vid.holds (GLib.Type.INT64)) {
        id = (int) (int64) vid;
      }
      assert (id != 0);
      message ("ID: %s", id.to_string ());
      string name = rw.get_value ("name").to_string ();
      assert (name != null);
      message ("Name: '%s'", name);
      GLib.Value vnumber = rw.get_value ("number").to_gvalue ();
      int number = 0;
      if (vnumber.holds (GLib.Type.INT)) {
        number = (int) vnumber;
      } else if (vid.holds (GLib.Type.INT64)) {
        number = (int) (int64) vnumber;
      }
      assert (number != 0);
      assert (number == 3);
      assert (rw.n_columns == 3);
      GLib.Value vid2 = rw.get_value_at (0).to_gvalue ();
      int id2 = 0;
      if (vid2.holds (GLib.Type.INT)) {
        id2 = (int) vid2;
      } else if (vid2.holds (GLib.Type.INT64)) {
        id2 = (int) (int64) vid2;
      }
      assert (id2 != 0);
      string name2 = rw.get_value_at (1).to_string ();
      message ("ID: %s Name: %s / %s Number: %s", rw.get_value_at (0).to_string (), name2, rw.get_value_at (1).to_string (), rw.get_value_at (2).to_string ());
      assert (name2 != null);
      assert (name2 == "test1" || name2 == "NAME");
      GLib.Value vnumber2 = rw.get_value_at (2).to_gvalue ();
      int number2 = 0;
      if (vnumber2.holds (GLib.Type.INT)) {
        number2 = (int) vnumber2;
      } else if (vnumber2.holds (GLib.Type.INT64)) {
        number2 = (int) (int64) vnumber2;
      }
      message ("Number Value: %d", number2);
      assert (number2 != 0);
      assert (number2 == 3);
      // ColumnModel
      var c = rw.get_column ("id");
      assert (c != null);
      assert (c is ColumnModel);
      assert (c.name == "id");
      message ("DataType: %s", c.data_type.name ());
      assert (c.data_type == typeof(Vda.ValueInt4) || c.data_type == typeof(int) || c.data_type == typeof(int64));
      message ("End test select");
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_select_parameters (Vda.Connection cnc, MainLoop loop) throws GLib.Error {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var rand = new GLib.Rand ();
    int id = (int) Math.floor (rand.double_range (1, 10000));
    string sql = "INSERT INTO table_test (id, name, number) VALUES ("+id.to_string ()+", 'test1', 3)";
    message (sql);
    var qi = cnc.parse_string_prepared ("insert", sql);
    message (qi.sql);
    var irest = yield qi.execute (null);
    message ("Rest type: %s", irest.get_type ().name ());
    if (irest is InvalidResult) {
      warning ("Invalid Result to insert: %s", ((InvalidResult) irest).message);
    }
    assert (irest is AffectedRows);
    message ("Inserte Rows: %u",((AffectedRows) irest).number );
    sql = "SELECT id, name, number FROM table_test WHERE id = ##id::gint";
    var q = cnc.parse_string_prepared ("select", sql);
    assert (q is PreparedQuery);
    assert (q.parameters != null);
    message (q.sql);
    assert (q.sql == "SELECT id, name, number FROM table_test WHERE id = ##id::gint");
    message ("ID in use: %d", id);
    q.parameters.set_value ("id", id);
    message (q.sql);
    try {
      var res = yield q.execute (null);
      if (res is TableModel) {
        var t = res as TableModel;
        assert (t.get_n_items () > 0);
        var rw = t.get_item (0) as RowModel;
        assert (rw is RowModel);
        int id2 = (int) rw.get_value ("id").to_gvalue ();
        assert (id2 == id);
        string name = rw.get_value ("name").to_string ();
        assert (name != null);
        message ("Number");
        var cnumber = rw.get_column ("number");
        assert (cnumber != null);
        assert (cnumber.name == "number");
        int number = (int) rw.get_value ("number").to_gvalue ();
        assert (number != 0);
        var cid = rw.get_column ("id");
        assert (cid != null);
        var cname = rw.get_column ("name");
        assert (cname != null);
        assert (rw.n_columns == 3);
        int id3 = (int) rw.get_value_at (0).to_gvalue ();
        assert (id3 == id);
        string name2 = (string) rw.get_value_at (1).to_string ();
        assert (name2 != null);
        var vnumber = rw.get_value_at (2);
        message (vnumber.get_type ().name ());
        assert (vnumber.to_gvalue ().holds (typeof (int)));
        int number2 = (int) vnumber.to_gvalue ();
        message ("Number: %d", number2);
        assert (number2 == 3);
        // ColumnModel
        var c = rw.get_column ("id");
        assert (c != null);
        assert (c is ColumnModel);
        assert (c.name == "id");
        message (c.data_type.name ());
        assert (c.data_type == typeof(int) || c.data_type.is_a (typeof (Vda.SqlValueInt4)));
        message ("End test select");
      }
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_insert_sql_command_to_query (Vda.Connection cnc, MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var rand = new GLib.Rand ();
    int id = (int) rand.next_int ();
    var iq = new CommandInsert (cnc);
    iq.table = "table_test";
    iq.add_field_value ("id", id);
    iq.add_field_value ("name", "Vda's name");
    iq.add_field_value ("number", 100);
    var q = iq.to_query (null);
    assert (q is Query);
    assert (!(q is InvalidQuery));
    message (q.get_type ().name ());
    message (q.sql);
    try {
      var rq = yield q.execute (null);
      assert (rq != null);
      message ("Response Type: %s", rq.get_type ().name ());
      if (rq is InvalidResult)  {
        warning ("Invalid Response: %s", ((InvalidResult) rq).message);
      }
      assert (rq is AffectedRows);
      assert (((AffectedRows) rq).number == 1);
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_insert_sql_command_to_query_parameters (Vda.Connection cnc, MainLoop loop)  throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandInsert (cnc);
    iq.table = "table_test";
    iq.add_field_parameter_value ("id", "id", typeof (int));
    iq.add_field_parameter_value ("name", "name", typeof (string));
    iq.add_field_parameter_value ("number", "number", typeof (int));
    message (iq.to_string ());
    message ("Convert SQL INSERT with parameters");
    var q = iq.to_query ("select") as PreparedQuery;
    assert (q is Query);
    assert (q is PreparedQuery);
    assert (q.parameters != null);
    var rand = new GLib.Rand ();
    int id = (int) rand.next_int ();
    q.parameters.set_value ("id", id);
    q.parameters.set_value ("name", "NAME");
    q.parameters.set_value ("number", 3);
    try {
      var rq = yield q.execute (null);
      assert (rq is AffectedRows);
      assert (((AffectedRows) rq).number == 1);
      var qs = cnc.parse_string ("SELECT * FROM table_test WHERE Id = "+id.to_string ());
      var rqs = yield qs.execute (null);
      if (rqs is TableModel) {
        assert (((TableModel) rqs).get_n_items () == 1);
      } else if (rqs is TableModelSequential) {
          assert (((TableModelSequential) rqs).next ());
          var row = ((TableModelSequential) rqs).current ();
          assert (row != null);
          var v = row.get_value ("id");
          assert (v is Vda.SqlValueInt8);
          assert ((int64) v.to_gvalue () == id);
      }
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_select_sql_command_to_query (Vda.Connection cnc, MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandSelect (cnc);
    iq.add_field ("id", "table_test");
    iq.add_field ("name", "table_test");
    iq.add_field ("number", "table_test");
    var q = iq.to_query (null);
    assert (q is Query);
    message (q.sql);
    try {
      var rq = yield q.execute (null);
      if (rq is TableModel) {
        assert (((TableModel) rq).get_n_items () > 0);
      } else if (rq is TableModelSequential) {
          assert (((TableModelSequential) rq).next ());
          var row = ((TableModelSequential) rq).current ();
          assert (row != null);
          var v = row.get_value ("id");
          assert (v is Vda.SqlValueInt8);
          assert ((int64) v.to_gvalue () != 0);
      }
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_select_sql_command_to_query_parameters (Vda.Connection cnc,
                                                                        MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandSelect (cnc);
    iq.add_field ("id", "table_test");
    iq.add_field ("name", "table_test");
    iq.add_field ("number", "table_test");
    assert (iq.condition != null);
    assert (iq.condition is SqlExpressionOperator);
    var cod = iq.condition as SqlExpressionOperator;
    var fe = cod.create_field_expression ("number");
    var fp = cod.create_parameter_expression ("number", typeof (int));
    cod.add_eq_operator (fe, fp);
    message ("Condition: %s", cod.to_string ());
    assert (cod.get_n_items () == 1);
    var q = iq.to_query ("select-parameters") as PreparedQuery;
    assert (q is Query);
    try {
      assert (q.parameters != null);
      q.parameters.set_value ("number", 3);
      message (q.sql);
      var rq = yield q.execute (null);
      assert (rq is TableModel || rq is TableModelSequential);
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_select_sql_command_to_query2 (Vda.Connection cnc,
                                                              MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandSelect (cnc);
    iq.add_table ("table_test", "t");
    iq.add_field ("id", "t");
    iq.add_field ("name", "t");
    iq.add_field ("number", "t");
    var q = iq.to_query (null);
    assert (q is Query);
    message (q.sql);
    try {
      var rq = yield q.execute (null);
      assert (rq is TableModel || rq is TableModelSequential);
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_select_sql_command_to_query3 (Vda.Connection cnc,
                                                              MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandSelect (cnc);
    iq.add_field ("id", "table_test");
    iq.add_field ("name", "table_test", "NOMBRE");
    iq.add_field ("number", "table_test");
    var q = iq.to_query (null);
    assert (q is Query);
    message (q.sql);
    try {
      var rq = yield q.execute (null);
      assert (rq is TableModel || rq is TableModelSequential);
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_select_sql_command_to_query_condition (Vda.Connection cnc,
                                                                      MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandSelect (cnc);
    iq.add_field ("id", "table_test");
    iq.add_field ("name", "table_test", "NOMBRE");
    iq.add_field ("number", "table_test");
    assert (iq.condition != null);
    assert (iq.condition is SqlExpressionOperator);
    var cod = iq.condition as SqlExpressionOperator;
    var fe = cod.create_field_expression ("number");
    var fv = cod.create_value_expression (3, cnc);
    cod.add_eq_operator (fe, fv);
    assert (cod.get_n_items () == 1);
    var q = iq.to_query (null);
    assert (q is Query);
    message (q.sql);
    try {
      var rq = yield q.execute (null);
      assert (rq is TableModel || rq is TableModelSequential);
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_delete_sql_command_to_query (Vda.Connection cnc,
                                                            MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandInsert (cnc);
    iq.table = "table_test";
    var rand = new GLib.Rand ();
    int id = (int) rand.next_int ();
    iq.add_field_value ("id", id);
    iq.add_field_value ("name", "table_test");
    iq.add_field_value ("number", 5);
    try {
      var q1 = iq.to_query (null);
      var rq1 = yield q1.execute (null);
      assert (rq1 is AffectedRows);
      var dq = new CommandDelete (cnc);
      dq.table = "table_test";
      assert (dq.condition != null);
      assert (dq.condition is SqlExpressionOperator);
      var cod = dq.condition as SqlExpressionOperator;
      var fe = cod.create_field_expression ("id");
      var fv = cod.create_value_expression (id, cnc);
      cod.add_eq_operator (fe, fv);
      assert (cod.get_n_items () == 1);
      var q = dq.to_query (null);
      assert (q is Query);
      message (q.sql);
      var rq = yield q.execute (null);
      assert (rq is AffectedRows);
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_delete_sql_command_to_query_parameters (Vda.Connection cnc,
                                                                       MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandInsert (cnc);
    iq.table = "table_test";
    var rand = new GLib.Rand ();
    int id = (int) rand.next_int ();
    iq.add_field_value ("id", id);
    iq.add_field_value ("name", "table_test");
    iq.add_field_value ("number", 5);
    try {
      var q1 = iq.to_query (null);
      var rq1 = yield q1.execute (null);
      assert (rq1 is AffectedRows);
      var dq = new CommandDelete (cnc);
      dq.table = "table_test";
      assert (dq.condition != null);
      assert (dq.condition is SqlExpressionOperator);
      var cod = dq.condition as SqlExpressionOperator;
      var fe = cod.create_field_expression ("id");
      var fv = cod.create_parameter_expression ("id", typeof (int));
      cod.add_eq_operator (fe, fv);
      assert (cod.get_n_items () == 1);
      var q = dq.to_query ("delete-params") as PreparedQuery;
      assert (q is Query);
      q.parameters.set_value ("id", id);
      message (q.sql);
      var rq = yield q.execute (null);
      assert (rq is AffectedRows);
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_update_sql_command_to_query_parameters (Vda.Connection cnc,
                                                                        MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandInsert (cnc);
    iq.table = "table_test";
    var rand = new GLib.Rand ();
    int id = (int) rand.next_int ();
    iq.add_field_value ("id", id);
    iq.add_field_value ("name", "table_test");
    iq.add_field_value ("number", 5);
    try {
      var q1 = iq.to_query (null);
      var rq1 = yield q1.execute (null);
      assert (rq1 is AffectedRows);
      // Update
      var dq = new CommandUpdate (cnc);
      var rand2 = new GLib.Rand ();
      int id2 = (int) rand2.next_int ();
      dq.add_field_value ("id", id2);
      dq.add_field_value ("name", "table_test");
      dq.add_field_value ("number", 55);
      dq.table = "table_test";
      assert (dq.condition != null);
      assert (dq.condition is SqlExpressionOperator);
      var cod = dq.condition as SqlExpressionOperator;
      var fe = cod.create_field_expression ("id");
      var fv = cod.create_parameter_expression ("id", typeof (int));
      cod.add_eq_operator (fe, fv);
      assert (cod.get_n_items () == 1);
      var q = dq.to_query ("update-params") as PreparedQuery;
      assert (q is Query);
      q.parameters.set_value ("id", id2);
      message (q.sql);
      var rq = yield q.execute (null);
      assert (rq is AffectedRows);
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  public static async void test_update_sql_command_to_query (Vda.Connection cnc,
                                                            MainLoop loop) throws GLib.Error
  {
    assert (cnc.is_opened);
    yield UniformTests.initialize_database (cnc, loop);
    var iq = new CommandInsert (cnc);
    iq.table = "table_test";
    var rand = new GLib.Rand ();
    int id = (int) rand.next_int ();
    iq.add_field_value ("id", id);
    iq.add_field_value ("name", "table_test");
    iq.add_field_value ("number", 5);
    try {
      var q1 = iq.to_query (null);
      var rq1 = yield q1.execute (null);
      assert (rq1 is AffectedRows);
      // Update
      var dq = new CommandUpdate (cnc);
      int id2 = (int) rand.next_int ();
      dq.add_field_value ("id", id2);
      dq.add_field_value ("name", "table_test");
      dq.add_field_value ("number", 55);
      dq.table = "table_test";
      assert (dq.condition != null);
      assert (dq.condition is SqlExpressionOperator);
      var cod = dq.condition as SqlExpressionOperator;
      var fe = cod.create_field_expression ("id");
      var fv = cod.create_value_expression (id2, cnc);
      cod.add_eq_operator (fe, fv);
      assert (cod.get_n_items () == 1);
      var q = dq.to_query (null);
      assert (q is Query);
      message (q.sql);
      var rq = yield q.execute (null);
      assert (rq is AffectedRows);
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
}
