/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

namespace DataObjectTests {
  public class Address : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::pkey::auto")]
    public int id { get; set; }
    [Description (nick="@street")]
    public string street { get; set; }
    [Description (nick="@client::id")]
    public int client { get; set; }
    construct {
      database_table_name = "address";
    }
  }
  public class AddressClient : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::id::auto")]
    public int id { get; set; }
    [Description (nick="@street")]
    public string street { get; set; }
    [Description (nick="@client::id")]
    public int client { get; set; }
    construct {
      database_table_name = "address";
    }
  }
  public class MultipleIdAddress : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::id::auto")]
    public int id { get; set; }
    [Description (nick="@street::id")]
    public string street { get; set; }
    [Description (nick="@client::id")]
    public int client { get; set; }
    construct {
      database_table_name = "address";
    }
  }
  public class Addresses : Object, Vda.DataCollection {
    string _parent_property;
    DataObject _parent;
    GLib.Type _object_type;
    string _ref_field;

    public Vda.Connection database_connection { get; set; }
    public string parent_property { get { return _parent_property; } }
    public DataObject parent { get { return _parent; } }
    public GLib.Type object_type { get { return _object_type; } }
    public string ref_field{ get { return _ref_field; } }
    public Cancellable cancellable { get; set; }

    construct  {
      _parent_property = "id";
      _ref_field = "client";
      _object_type = typeof (Address);
    }

    public Addresses (Client client) {
      _parent = client;
    }
  }
  public class Client : Object, Vda.DataObject {
    Vda.Connection _cnc = null;
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection {
      get {
        return _cnc;
      }
      set {
        _cnc = value;
        addresses.database_connection = _cnc;
      }
    }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::pkey::auto")]
    public int id { get; set; }
    [Description (nick="@name")]
    public string name { get; set; }
    [Description (nick="@description")]
    public string description { get; set; }
    [Description (nick="@phone::id")]
    public string phone { get; set; }
    public Addresses addresses { get; set; }

    construct {
      database_table_name = "clients";
      addresses = new Addresses (this);
    }

    public async void initialization () throws GLib.Error {
      var qdt = database_connection.parse_string ("DROP TABLE IF EXISTS clients");
      yield qdt.execute (null);
      var qdt2 = database_connection.parse_string ("DROP TABLE IF EXISTS address");
      yield qdt2.execute (null);
      var qct = database_connection.parse_string ("CREATE TABLE IF NOT EXISTS clients (id SERIAL PRIMARY KEY, name varchar(50), description varchar(50), phone varchar(50), country text default 'Mexico')");
      yield qct.execute (null);
      var qct2 = database_connection.parse_string ("CREATE TABLE IF NOT EXISTS address (id SERIAL PRIMARY KEY, street varchar(100), client integer)");
      yield qct2.execute (null);
    }
  }

  public class TypesCheckExtend : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::id")]
    public int val_id { get; set; }
    [Description (nick="@val1")]
    public float val1 { get; set; }
    [Description (nick="@val2")]
    public double val2 { get; set; }
    [Description (nick="@val3")]
    public SqlValue val3 { get; set; } // Autodetected value Timestamp without time zone
    [Description (nick="@val4")]
    public DateTime val4 { get; set; } // Value Timestamp with time zone
    [Description (nick="@val5")]
    public SqlValue val5 { get; set; } // Autodetected value Time without time zone
    [Description (nick="@val6")]
    public SqlValue val6 { get; set; } // Autodetected value Time with time zone
    [Description (nick="@val7")]
    public Date val7 { get; set; }
    [Description (nick="@val8")]
    public float val8 { get; set; }
    [Description (nick="@val9")]
    public string val9 { get; set; }
    construct {
      database_table_name = "types_test_extend";
    }
    public async void initialize_database () throws GLib.Error {
      var qdt = database_connection.parse_string ("DROP TABLE IF EXISTS types_test_extend");
      yield qdt.execute (null);
      var qct = database_connection.parse_string ("CREATE TABLE IF NOT EXISTS types_test_extend (id integer, val1 real, val2 double precision, val3 timestamp, val4 timestamp with time zone, val5 time, val6 time with time zone, val7 date, val8 money, val9 text)");
      try {
        yield qct.execute (null);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
      var qst = database_connection.parse_string ("SELECT * FROM types_test_extend");
      try {
        message ("QUERY: %s", qst.sql);
        var r = yield qst.execute (null);
        assert (r is TableModel);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    }
  }

  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "en_US.utf8");
    Test.init (ref args);
    Test.add_func ("/vda/dataobject/native-postgres/types/extended",
    ()=>{
      var list = GLib.Environ.@get ();
      var dbcon = GLib.Environ.get_variable (list, "POSTGRESQL_CNC_PARAMS");
      var loop = new GLib.MainLoop (null);
      var cnc = new Vpg.Connection ();
      var cnc_string = "DB_NAME=vda_test;HOST=localhost;USERNAME=test;PASSWORD=test1";
      if (dbcon != "" && dbcon != null)
        cnc_string = dbcon;
      var par = new ConnectionParameters (cnc_string);
      cnc.parameters = par;
      cnc.opened.connect (()=>{
        var m = new Vda.ValueMoney ();
        m.parse ("$1,000.10");
        assert (m.get_double () == 1000.10);
        m.parse ("MXN$ 1 000,10");
        assert (m.get_double () == 1000.10);
        m.parse ("EUR€ 1.000,12");
        assert (m.get_double () == 1000.12);
        m.parse ("WRL฿ 1.000,1562");
        assert (m.get_double () == 1000.1562);
        m.parse ("USD$ 1,000.1562");
        assert (m.get_double () == 1000.1562);

        var o = new TypesCheckExtend ();
        o.database_connection = cnc;
        o.initialize_database.begin ((obj, res)=>{
          try {
            o.initialize_database.end (res);
            o.val_id = 1;
            o.val1 = 3.145f;
            o.val2 = 3.9009;
            o.val3 = new ValueTimestampNtz ();
            o.val4 = new DateTime.now_local ();
            o.val5 = new ValueTimeNtz ();
            o.val6 = new ValueTime ();
            o.val7 = new ValueDate ().get_date ();
            o.val7.set_dmy ((DateDay) 1, (DateMonth) 10, (DateYear) 2019);
            o.val8 = 1000.1f;
            o.val9 = null;
            var iq = DataObject.create_insert_query (o);
            message ("QUERY: %s", iq.stringify  ());
            o.insert_data_into_db.begin ((obj, res)=>{
              try {
                o.insert_data_into_db.end (res);
                var o2 = new TypesCheckExtend ();
                o2.database_connection = cnc;
                o2.val_id = 1;
                var qc = DataObject.create_select_all (o2);
                message (qc.stringify ());
                var q = qc.to_query ();
                q.execute.begin (null, (obj, res)=>{
                  try {
                    var r = q.execute.end (res);
                    assert (r is  TableModel);
                    var t = r as TableModel;
                    assert (t.get_n_items () == 1);
                    message ("Monetary Value: %s", ((RowModel) t.get_item (0)).get_string ("val8"));
                    o2.update_data_from_db_pkey.begin ((obj, res)=>{
                      try {
                        o2.update_data_from_db.end (res);
                        assert (o.val_id == o2.val_id);
                        assert (o.val1 == o2.val1);
                        message ("%g : %g".printf (o.val2, o2.val2));
                        assert (o.val2 == o2.val2);
                        assert (o2.val3 != null);
                        message ("%s : %s", o.val3.to_string (), o2.val3.to_string ());
                        assert (o.val3.to_string () == o2.val3.to_string ());
                        assert (o2.val4 != null);
                        message ("%s : %s", o.val4.to_string (), o2.val4.to_string ());
                        assert (o.val4.to_string () == o2.val4.to_string ());
                        assert (o2.val5 != null);
                        message ("%s : %s", o.val5.to_string (), o2.val5.to_string ());
                        assert (o.val5.to_string () == o2.val5.to_string ());
                        assert (o2.val6 != null);
                        message ("%s : %s", o.val6.to_string (), o2.val6.to_string ());
                        assert (o.val6.to_string () == o2.val6.to_string ());
                        message ("%d-%d-%d : %d-%d-%d".printf (o.val7.get_year (), o.val7.get_month (), o.val7.get_day (),
                                                              o2.val7.get_year (), o2.val7.get_month (), o2.val7.get_day ()));
                        assert (o.val7.get_year () == o.val7.get_year ());
                        assert (o.val7.get_month () == o.val7.get_month ());
                        assert (o.val7.get_day () == o.val7.get_day ());
                        message ("%g vs. %g".printf (o.val8, o2.val8));
                        assert (o.val8 == o2.val8);
                        assert (o.val9 == null);
                        loop.quit ();
                      } catch (GLib.Error e) {
                        warning ("Error: %s", e.message);
                      }
                    });
                  } catch (GLib.Error e) {
                    warning ("Error Query: %s", e.message);
                  }
                });
              } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
              }
            });
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });

      cnc.canceled.connect ((str)=>{
        warning ("Canceled: %s", str);
        loop.quit ();
      });

      Idle.add (()=>{
        cnc.open.begin ((obj, res)=>{
          try {
            message ("End Connection open...");
            cnc.open.end (res);
          } catch (GLib.Error e) {
            warning ("Error opening connection: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    });
    return Test.run ();
  }
}
