/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * libgdadata Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2011 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gda;
using Vda;
using Gee;

class Tests : GLib.Object {
	public Vda.Connection connection { get; set; }
	public MetaRecordCollection itermodel { get; set; }
	public GLib.File dbf { get; set; }

	public signal void ready ();

	public Tests (string dbname) {
		try {
		    var list = GLib.Environ.@get ();
		    var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
		    var d = GLib.File.new_for_path (dir);
		    assert (d.query_exists (null));
		    message (d.get_uri ());
			var dbf = GLib.File.new_for_uri ("%s/%s.db".printf (d.get_uri (), dbname));
			string ndbname = dbname;
			int i = 0;
			while (dbf.query_exists (null)) {
				i++;
				dbf = GLib.File.new_for_path ("%s/%s%i.db".printf (d.get_uri (), dbname, i));
				ndbname = "%s%i".printf (dbname,i);
			}
			stdout.printf("Creating Database...\n");
			connection = new Vgsl.Connection ();
			connection.open_from_string.begin ("DB_DIR=%s;DB_NAME=%s".printf (d.get_path (), ndbname));
			assert (connection.is_opened);
			stdout.printf("Creating table 'user'...\n");
			(connection as Vgda.GProvider).cnc.execute_non_select_command("CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, name string, city string)");
			(connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO user (name, city) VALUES (\"Daniel\", \"Mexico\")");
			(connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO user (name, city) VALUES (\"Jhon\", \"USA\")");
			(connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO user (name, city) VALUES (\"James\", \"Germany\")");
			(connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO user (name, city) VALUES (\"Jack\", \"United Kindom\")");
			(connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO user (name, city) VALUES (\"Elsy\", \"España\")");
			(connection as Vgda.GProvider).cnc.execute_non_select_command("INSERT INTO user (name, city) VALUES (\"Mayo\", \"Mexico\")");

			var model = (connection as Vgda.GProvider).cnc.execute_select_command ("SELECT * FROM user ORDER BY id");
			stdout.printf ("Setting up Table...");
			var table = new Vgda.GTable ();
			table.connection = this.connection;
			table.name = "user";
			stdout.printf ("Setting up MetaRecordCollection...");
			itermodel = new Vgda.GRecordCollection (model, table);
		} catch (GLib.Error e) { warning ("Fail at init: %s", e.message); }
	}
	~Tests () {
		if (dbf != null)
			if (dbf.query_exists (null))
				try { dbf.delete (); } catch {}
	}

	static int main (string[] args)
	{
		GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
		Test.init (ref args);
		Test.add_func ("/vda/iteration",
		()=>{
			var t = new Tests ("datamodeliterator");
			var loop = new MainLoop (null);
			Idle.add(()=> {
				int fails = 0;
				stdout.printf (">>> TESTING: Iterating over all Records in DataModel using foreach...\n");
				int i = 0;
				stdout.printf ("Iterating using a Gee.Iterator...\n");
				i = 0;
				var iter = t.itermodel.iterator ();
				if (iter == null)
					stdout.printf ("----- FAIL "+ (++fails).to_string () + "\n");
				while (iter.next ()) {
					string t2 = iter.get ().to_string ();
					if (t2 == null) {
						fails++;
						break;
					}
					else {
						stdout.printf (t2 + "\n");
						i++;
					}
				}
				if (fails > 0 || i != 6)
					stdout.printf ("----- FAIL "+ fails.to_string () + "\n");
				else
					stdout.printf ("+++++ PASS\n");
				stdout.printf ("Iterating using foreach instruction...\n");
				i = 0;
				foreach (MetaRecord r in t.itermodel) {
					string tx = r.to_string ();
					if (tx == null) {
						fails++;
						break;
					}
					i++;
					stdout.printf (tx + "\n");
				}
				if (fails > 0 || i != 6)
					stdout.printf ("----- FAIL "+ fails.to_string () + "\n");
				else
					stdout.printf ("+++++ PASS\n");
				loop.quit ();
				return GLib.Source.REMOVE;
			});
			loop.run ();
		});
		Test.add_func ("/vda/chopping",
		()=>{
			var t = new Tests ("datamodeliterator");
			var loop = new MainLoop (null);
			Idle.add(()=>{
			 try {
				int fails = 0;
				stdout.printf (">>> TESTING: Chopping...\n");
				stdout.printf (" to get from the 2nd MetaRecord to the 6th...\n");
				var iter = t.itermodel.chop (1);
				int i = 0;
				while(iter.has_next ()) {
					if (!iter.next ()) break;
					if (!iter.valid) break;
					stdout.printf (iter.get().to_string () + "\n");
					i++;
					var name = (string) iter.get().get_value ("name");
					switch (i) {
					case 1:
						assert (name == "Jhon");
						break;
					case 2:
						assert (name == "James");
						break;
					case 3:
						assert (name == "Jack");
						break;
					case 4:
						assert (name == "Elsy");
						break;
					case 5:
						assert (name == "Mayo");
						break;
					}
				}
				stdout.printf ("Choping to get the 4th to 5th MetaRecords...\n");
				var iter2 = t.itermodel.chop (3,2);
				i = 0;
				while (iter2.has_next ()) {
					iter2.next ();
					i++;
					stdout.printf (iter2.get ().to_string () + "\n");
					var name2 = (string) iter2.get().get_value ("name");
					if (i==1) {
						if (name2 != "Jack") {
							fails++;
							stdout.printf ("----- FAIL: " + (++fails).to_string () + "\n");
							break;
						}
					}
					if (i==2) {
						if (name2 != "Elsy") {
							fails++;
							stdout.printf ("----- FAIL: " + (++fails).to_string () + "\n");
							break;
						}
					}
				}
				if (i!=2) {
					fails++;
					stdout.printf ("----- FAIL: " + (++fails).to_string () + "\n");
				}
				else
				stdout.printf ("+++++ PASS\n");

				stdout.printf ("Choping offset = 7 must fail...\n");
				var iter3 = t.itermodel.chop (7);
				if (iter3.has_next ())
					stdout.printf ("----- FAIL: " + (++fails).to_string () + "\n");
				else {
					if (iter3.next ())
						stdout.printf ("----- FAIL: " + (++fails).to_string () + "\n");
					if (fails == 0)
						stdout.printf ("+++++ PASS\n");
				}

				stdout.printf ("Choping offset = 6 length = 0 must fail...\n");
				var iter4 = t.itermodel.chop (6,0);
				if (iter4.has_next ())
					stdout.printf ("----- FAIL: " + (++fails).to_string () + "\n");
				else {
					if (iter4.next ())
						stdout.printf ("----- FAIL: " + (++fails).to_string () + "\n");
					if (fails == 0)
						stdout.printf ("+++++ PASS\n");
				}

				stdout.printf ("Choping offset = 5 length = 1...\n");
				var iter5 = t.itermodel.chop (5,1);
				if (!iter5.next ())
					stdout.printf ("----- FAIL: " + (++fails).to_string () + "\n");
				else {
					if (iter5.next ())
						stdout.printf ("----- FAIL: " + (++fails).to_string () + "\n");
					if (fails == 0)
						stdout.printf ("+++++ PASS\n");
				}
				if (fails == 0)
					stdout.printf ("+++++ PASS: " + "\n");
			 } catch (GLib.Error e) {
			 	warning ("Error: %s", e.message);
			 }
			 loop.quit ();
			 return GLib.Source.REMOVE;
			});
			loop.run ();
		});
		Test.add_func ("/vda/filtering",
		()=>{
			var t = new Tests ("datamodeliterator");
			var loop = new MainLoop (null);
			Idle.add (()=>{
				int fails = 0;
				stdout.printf (">>> TESTING: Filtering Records: Any field type STRING with a letter 'J'...\n");

				var iter = t.itermodel.filter ((g) => {
					bool ret = false;
					foreach (MetaField fl in g.fields) {
						string tx = Gda.value_stringify (fl.get_value ());
						stdout.printf ("Value to check: " + tx);
						if (tx.contains ("J")) {
							ret = true;
							stdout.printf ("...SELECTED\n");
							break;
						}
						else {
							ret = false;
							stdout.printf ("...REJECTED\n");
						}
					}
					if (ret) stdout.printf ("SELECTED ROW: \n" + g.to_string () + "\n");
					return ret;
				});
				stdout.printf ("\nPrinting Filtered Values...\n");
				int i = 0;
				while (iter.next ()) {
					stdout.printf ("Row"+(++i).to_string()+":\n" + iter.get ().to_string () + "\n");
				}
				if(i != 3) {
					stdout.printf ("----- FAIL" + (++fails).to_string () + "\n");
				}
				else
					stdout.printf ("+++++ PASS\n");
				loop.quit ();
				return GLib.Source.REMOVE;
			});
			loop.run ();
		});
		Test.add_func ("/vda/streaming",
		()=>{
			var t = new Tests ("datamodeliterator");
			var loop = new MainLoop (null);
			Idle.add (()=>{
				int fails = 0;
				stdout.printf (">>> TESTING: Streaming Values: First MetaRecord's field type STRING will be YIELDED...\n");
				Lazy<string> lazy = null;
				var iter = t.itermodel.stream<string> ((state, g, out lazy) =>	{
					lazy = null;
					Gee.Traversable.Stream ret = Gee.Traversable.Stream.END;
					switch(state) {
						case Gee.Traversable.Stream.YIELD:
							lazy = null;
							ret = Gee.Traversable.Stream.CONTINUE;
							break;
						case Gee.Traversable.Stream.CONTINUE:
							var r = g.value;
							foreach (MetaField f in r.fields) {
								if (f.get_value ().type () == typeof (string)) {
									stdout.printf ("Field (%s) =  %s\n", f.name, Gda.value_stringify (f.get_value ()));
									string ts = Gda.value_stringify (f.get_value ());
									lazy = new Gee.Lazy<string> (() => {return ts.dup ();});
								}
							}
							ret = Gee.Traversable.Stream.YIELD;
							break;
					}
					assert (lazy == null || lazy != null);
					return ret;
				});
				assert (lazy == null || lazy != null);
				stdout.printf ("Printing Streamed Values...\n");
				var l = new Gee.ArrayList<string> ();
				while (iter.next ()) {
					string v = iter.get ();
					l.add (v);
					stdout.printf (v + "\n");
				}

				if (!l.contains ("Daniel"))
					++fails;
				if (!l.contains ("Jhon"))
					++fails;
				if (!l.contains ("Jack"))
					++fails;
				if (!l.contains ("Elsy"))
					++fails;
				if (!l.contains ("Mayo"))
					++fails;

				if (fails > 0) {
					stdout.printf ("----- FAIL" + (++fails).to_string () + "\n");
				}
				else
					stdout.printf ("+++++ PASS\n");
				loop.quit ();
				return GLib.Source.REMOVE;
			});
			loop.run ();
		});
		return Test.run ();
	}
}
