/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Vsqlite;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/vda/connection/native-sqlite",
    ()=>{
      var list = GLib.Environ.@get ();
      var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
      var d = GLib.File.new_for_path (dir);
      assert (d.query_exists (null));
      var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-test.db");
      if (f.query_exists ()) {
          try {
            f.delete ();
          } catch (GLib.Error e) {
            message ("Error: %s", e.message);
          }
      }
      var cnc = new Vsqlite.Connection ();
      assert (cnc is Vsqlite.Connection);
      assert (cnc is Vda.Connection);
      var cnc_string = "DB_NAME="+f.get_path ();
      var loop = new GLib.MainLoop (null);
      cnc.opened.connect (()=>{
        message ("Opened file");
      });
      cnc.canceled.connect ((msg)=>{
        message ("Parsed connection string: %s", cnc.connection_string);
        assert (cnc.parameters.size == 1);
        assert (cnc.parameters.get ("DB_NAME") != null);
        message ("Error: %s", msg);
        warning ("Connection Canceled!");
        loop.quit ();
      });
      cnc.timeout.connect (()=>{
        if (!cnc.is_opened) {
          message ("Connection timeout");
          message ("No connection to Sqlite provider. Skiping. If you think this is an error, please report it");
          loop.quit ();
          return;
        }
      });
      cnc.open_from_string.begin (cnc_string, ()=>{
        message ("Sent Request to open...");
        assert (cnc.is_opened);
        message ("Connected to Vda Native Sqlite connection");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }
        message ("Quiting");
        loop.quit ();
      });
      loop.run ();
    });
    return Test.run ();
  }
}

