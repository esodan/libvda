/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

namespace DataObjectTests {
  public class Address : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::pkey::auto")]
    public int id { get; set; }
    [Description (nick="@street")]
    public string street { get; set; }
    [Description (nick="@client::id")]
    public int client { get; set; }
    construct {
      database_table_name = "address";
    }
  }
  public class AddressClient : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::id::auto")]
    public int id { get; set; }
    [Description (nick="@street")]
    public string street { get; set; }
    [Description (nick="@client::id")]
    public int client { get; set; }
    construct {
      database_table_name = "address";
    }
  }
  public class MultipleIdAddress : Object, Vda.DataObject {
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection { get; set; }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::id::auto")]
    public int id { get; set; }
    [Description (nick="@street::id")]
    public string street { get; set; }
    [Description (nick="@client::id")]
    public int client { get; set; }
    construct {
      database_table_name = "address";
    }
  }
  public class Addresses : Object, Vda.DataCollection {
    string _parent_property;
    DataObject _parent;
    GLib.Type _object_type;
    string _ref_field;

    public Vda.Connection database_connection { get; set; }
    public string parent_property { get { return _parent_property; } }
    public DataObject parent { get { return _parent; } }
    public GLib.Type object_type { get { return _object_type; } }
    public string ref_field{ get { return _ref_field; } }
    public Cancellable cancellable { get; set; }

    construct  {
      _parent_property = "id";
      _ref_field = "client";
      _object_type = typeof (Address);
    }

    public Addresses (Client client) {
      _parent = client;
    }
  }
  public class Client : Object, Vda.DataObject {
    Vda.Connection _cnc = null;
    // DataObject
    public string database_table_name { get; construct set; }
    public Vda.Connection database_connection {
      get {
        return _cnc;
      }
      set {
        _cnc = value;
        addresses.database_connection = _cnc;
      }
    }
    public Cancellable cancellable { get; set; }

    // Database mapping
    [Description (nick="@id::pkey::auto")]
    public int id { get; set; }
    [Description (nick="@name")]
    public string name { get; set; }
    [Description (nick="@description")]
    public string description { get; set; }
    [Description (nick="@phone::id")]
    public string phone { get; set; }
    [Description (nick="@number")]
    public int number { get; set; }
    public Addresses addresses { get; set; }

    construct {
      database_table_name = "clients";
      addresses = new Addresses (this);
    }

    public async void initialization () throws GLib.Error {
      var qdt = database_connection.parse_string ("DROP TABLE IF EXISTS clients");
      yield qdt.execute (null);
      var qdt2 = database_connection.parse_string ("DROP TABLE IF EXISTS address");
      yield qdt2.execute (null);
      var qct = database_connection.parse_string ("CREATE TABLE IF NOT EXISTS clients (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, description TEXT, phone TEXT, country TEXT default 'Mexico', number INTEGER)");
      yield qct.execute (null);
      var qct2 = database_connection.parse_string ("CREATE TABLE IF NOT EXISTS address (id INTEGER PRIMARY KEY AUTOINCREMENT, street TEXT, client INTEGER)");
      yield qct2.execute (null);
    }
  }

  

  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "en_US.utf8");
    Test.init (ref args);
    Test.add_func ("/vda/dataobject/native-sqlite/types/extended",
    ()=>{
        var list = GLib.Environ.@get ();
        var dir = GLib.Environ.get_variable (list, "BUILD_DIR");
        var d = GLib.File.new_for_path (dir);
        assert (d.query_exists (null));
        var f = GLib.File.new_for_uri (d.get_uri () + "/sqlite-dataobject.db");
        if (f.query_exists ()) {
            try {
              f.delete ();
            } catch (GLib.Error e) {
              message ("Error: %s", e.message);
            }
        }

        var loop = new GLib.MainLoop (null);
        var cnc = new Vsqlite.Connection ();
        var cnc_string = "DB_NAME="+f.get_uri ();
        var par = new ConnectionParameters (cnc_string);
        cnc.parameters = par;
        cnc.opened.connect (()=>{
            var o = new Client ();
            o.database_connection = cnc;
            o.initialization.begin ((obj, res)=>{
            try {
                o.initialization.end (res);
                o.name = "Jack Sp";
                o.description = "No one";
                o.number = 3;
                o.insert_data_into_db.begin ((obj, res)=>{
                try {
                    o.insert_data_into_db.end (res);
                    var o2 = new Client ();
                    o2.database_connection = cnc;
                    o2.id = 1;
                    var qc = DataObject.create_select_all (o2);
                    message (qc.stringify ());
                    var q = qc.to_query ();
                    message ("Execute SELECT all data");
                    q.execute.begin (null, (obj, res)=>{
                    try {
                        var r = q.execute.end (res);
                        assert (r is  TableModelSequential);
                        var t = r as TableModelSequential;
                        assert (t.next ());
                        var row = t.current ();
                        assert (row != null);
                        message ("Name Value: %s", row.get_string ("name"));
                        message ("Number Value as String: %s", row.get_value ("number").to_string ());
                        GLib.Value iv = (int64) row.get_value ("number").to_gvalue ();
                        assert (iv.holds (GLib.Type.INT64));
                        int64 lv = (int64) iv;
                        message ("Number Value as INT64: %lld", lv);
                        assert (lv == 3l);
                        assert ("3" == "%lld".printf (lv));
                        o2.update_data_from_db_pkey.begin ((obj, res)=>{
                        try {
                            o2.update_data_from_db.end (res);
                            message ("NAME = %s : %s", o.name, o2.name);
                            message ("%s : %s".printf (o.description, o2.description));
                            message ("NUMBER %d : %d".printf (o.number, o2.number));
                            assert (o.name == o2.name);
                            assert (o.description == o2.description);
                            assert (o.number == o2.number);
                            loop.quit ();
                        } catch (GLib.Error e) {
                            warning ("Error: %s", e.message);
                        }
                        });
                    } catch (GLib.Error e) {
                        warning ("Error Query: %s", e.message);
                    }
                    });
                } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                }
                });
            } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
            }
            });
        });

        cnc.canceled.connect ((str)=>{
            warning ("Canceled: %s", str);
            loop.quit ();
        });

        Idle.add (()=>{
            cnc.open.begin ((obj, res)=>{
            try {
                message ("End Connection open...");
                cnc.open.end (res);
            } catch (GLib.Error e) {
                warning ("Error opening connection: %s", e.message);
            }
            });
            return Source.REMOVE;
        });
        loop.run ();
        });
        return Test.run ();
    }
}
