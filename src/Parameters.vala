/*
 * Parameters.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019-2020 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Set parameters for a prepared query.
 */
public class Vda.Parameters : Gee.HashMap<string,SqlValue>, Vda.SqlParameters {
    public void set_value (string name, GLib.Value val) {
        var v = SqlValue.new_from_gvalue (val);
        set (name, v);
    }

    public GLib.Value? get_value (string name) {
        if (!has_key (name)) {
            return null;
        }

        return get (name).to_gvalue ();
    }

    public void set_sql_value (string name, SqlValue val) {
        set (name, val);
    }

    public SqlValue get_sql_value (string name) {
        return get (name);
    }

    public bool has_param (string name) {
        return has_key (name);
    }
}
