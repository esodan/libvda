/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * RowModel.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent a row in a table model.
 */
public interface Vda.RowModel : GLib.Object, GLib.ListModel {
  /**
   * Number of columns in the row
   */
  public abstract uint n_columns { get; }
  /**
   * Provides a {@link Vda.ColumnModel} with the given name
   */
  public abstract Vda.ColumnModel? get_column (string name) throws GLib.Error;
  /**
   * Provides a {@link Vda.ColumnModel} with the given order number
   */
  public abstract Vda.ColumnModel? get_column_at (uint col) throws GLib.Error;
  /**
   * Provides a {@link Vda.SqlValue} at given column's name
   */
  public abstract SqlValue? get_value (string name) throws GLib.Error;
  /**
   * Provides a {@link Vda.SqlValue} with at given column's order number
   */
  public abstract SqlValue? get_value_at (uint col) throws GLib.Error;
  /**
   * Provides the string representing the value at given column's name
   */
  public abstract string? get_string (string name) throws GLib.Error;
  /**
   * Provides the string representing the value at given column's order number
   */
  public abstract string? get_string_at (uint col) throws GLib.Error;
}

/**
 * Row Model error codes
 */
public errordomain Vda.RowModelError {
  INVALID_COLUMN_NAME_ERROR,
  INVALID_COLUMN_NUMBER_ERROR
}
