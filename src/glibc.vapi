/**
 * Copyrigth 2019 - Daniel Espinosa <esodan@gmail.com>
 */

namespace GLibc {
  [Compact]
  [CCode (cname="struct lconv", cheader_filename = "locale.h")]
  public class LConv {
    public string decimal_point;
    public string thousands_sep;
    public string grouping;
    public string int_curr_symbol;
    public string currency_symbol;
    public string mon_decimal_point;
    public string mon_thousands_sep;
    public string mon_grouping;
    public string positive_sign;
    public string negative_sign;
    public int    int_frac_digits;
    public int    frac_digits;
    public int    p_cs_precedes;
    public int    n_cs_precedes;
    public int    int_p_cs_precedes;
    public int    int_n_cs_precedes;
    public int    p_sep_by_space;
    public int    n_sep_by_space;
    public int    int_p_sep_by_space;
    public int    int_n_sep_by_space;
    public string p_sign_posn;
    public string n_sign_posn;

    [CCode(cname="localeconv", cheader_filename="locale.h")]
    public static LConv* locale_conventions ();
  }
}
