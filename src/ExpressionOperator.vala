/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * ExpressionOperator.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * An implementation of {@link SqlExpressionOperator}
 */
public class Vda.ExpressionOperator : Expression,
																				SqlExpressionOperator
{
	protected SqlExpressionOperator.Type _operator_type = SqlExpressionOperator.Type.NONE;

	internal SqlExpressionOperator.Type operator_type { get { return _operator_type; } }

	internal SqlExpressionField create_field_expression (string name) {
		var f = new ExpressionField ();
		f.name = name;
		return f;
	}
	internal SqlExpressionValue create_value_expression (GLib.Value? val, Connection cnc) {
		var v = new ExpressionValue ();
		var type = val.type ();
		switch (type) {
			case GLib.Type.INT:
				v.value = new Vda.ValueInteger ();
				break;
			case GLib.Type.UINT:
				v.value = new Vda.ValueUnsignedInteger ();
				break;
			case GLib.Type.INT64:
				v.value = new Vda.ValueInt8 ();
				break;
			case GLib.Type.UINT64:
				v.value = new Vda.ValueUnsignedInt8 ();
				break;
			case GLib.Type.BOOLEAN:
				v.value = new Vda.ValueBool ();
				break;
			case GLib.Type.CHAR:
				v.value = new Vda.ValueByte ();
				break;
			case GLib.Type.UCHAR:
				v.value = new Vda.ValueUnsignedByte ();
				break;
			case GLib.Type.DOUBLE:
				v.value = new Vda.ValueDouble ();
				break;
			case GLib.Type.FLOAT:
				v.value = new Vda.ValueFloat ();
				break;
			case GLib.Type.LONG:
				v.value = new Vda.ValueInteger ();
				break;
			case GLib.Type.STRING:
				v.value = new Vda.ValueString ();
				break;
			case GLib.Type.OBJECT:
				var obj = (val.get_object () as GCalc.MathEquationManager);
				if (obj != null) {
					var ve = new ValueMathExp ();
					((SqlValueMathExp) ve).math = obj;
					v.@value = ve;
				}
				break;
		}

		((Vda.Value) v.@value).force_value (val);
		v.connection = cnc;
		return v;
	}

	internal SqlExpressionValueParameter create_parameter_expression (string name, GLib.Type gtype) {
		var p = new ExpressionValueParameter ();
		p.name = name;
		p.gtype = gtype;
		return p;
	}

  internal SqlExpressionOperator add_and_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorAnd ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }

  internal SqlExpressionOperator add_or_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorOr ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }

  internal SqlExpressionOperator add_eq_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorEq ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_diff_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorDiff ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_like_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorLike ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_gt_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorGt ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_geq_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorGeq ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_leq_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorLeq ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_similar_to_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorSimilarTo ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_null_operator (SqlExpression exp1) {
  	var op = new ExpressionOperatorIsNull ();
  	op.add_expression (exp1);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_not_null_operator (SqlExpression exp1) {
  	var op = new ExpressionOperatorIsNotNull ();
  	op.add_expression (exp1);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_not_operator (SqlExpression exp) {
  	var op = new ExpressionOperatorNot ();
  	op.add_expression (exp);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_true_operator (SqlExpression exp1) {
  	var op = new ExpressionOperatorIsTrue ();
  	op.add_expression (exp1);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_not_true_operator (SqlExpression exp1) {
  	var op = new ExpressionOperatorIsNotTrue ();
  	op.add_expression (exp1);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_false_operator (SqlExpression exp1) {
  	var op = new ExpressionOperatorIsFalse ();
  	op.add_expression (exp1);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_not_false_operator (SqlExpression exp1) {
  	var op = new ExpressionOperatorIsNotFalse ();
  	op.add_expression (exp1);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_unknown_operator (SqlExpression exp1) {
  	var op = new ExpressionOperatorIsUnknown ();
  	op.add_expression (exp1);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_not_unknown_operator (SqlExpression exp1) {
  	var op = new ExpressionOperatorIsNotUnknown ();
  	op.add_expression (exp1);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_in_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorIn ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_not_in_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorNotIn ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_concatenate_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorConcatenate ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_plus_operator (SqlExpression exp1, SqlExpression? exp2) {
  	var op = new ExpressionOperatorPlus ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_minus_operator (SqlExpression exp1, SqlExpression? exp2) {
  	var op = new ExpressionOperatorMinus ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_star_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorStar ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_div_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorDiv ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_regexp_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorRegexp ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_between_operator (SqlExpression exp1,
  																								SqlExpression exp2,
  																								SqlExpression exp3) {
  	var op = new ExpressionOperatorBetween ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	op.add_expression (exp3);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_not_between_operator (SqlExpression exp1,
  																								SqlExpression exp2,
  																								SqlExpression exp3) {
  	var op = new ExpressionOperatorNotBetween ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	op.add_expression (exp3);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_between_symmetric_operator (SqlExpression exp1,
  																								SqlExpression exp2,
  																								SqlExpression exp3) {
  	var op = new ExpressionOperatorBetweenSymmetric ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	op.add_expression (exp3);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_not_between_symmetric_operator (SqlExpression exp1,
  																								SqlExpression exp2,
  																								SqlExpression exp3) {
  	var op = new ExpressionOperatorNotBetweenSymmetric ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	op.add_expression (exp3);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_distinct_from_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorIsDistinctFrom ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
  internal SqlExpressionOperator add_is_not_distinct_from_operator (SqlExpression exp1, SqlExpression exp2) {
  	var op = new ExpressionOperatorIsNotDistinctFrom ();
  	op.add_expression (exp1);
  	op.add_expression (exp2);
  	add_expression (op);
  	return op;
  }
	internal override string to_string () {
		string str = "";
		for (int i = 0; i < get_n_items (); i++) {
			var item = get_item (i) as SqlExpression;
			if (item == null) continue;
			str += item.to_string ();
		}
		if (get_n_items () > 1) {
			str = "("+ str + ")";
		}
		return str;
  }
}


/**
 * Implemenation of {@link SqlExpressionOperatorGroup}
 */
public class Vda.ExpressionOperatorGroup : ExpressionOperator, SqlExpressionOperatorGroup
{
	internal override string to_string () {
		string str = "(";
		for (int i = 0; i < get_n_items (); i++) {
			var item = get_item (i) as SqlExpression;
			str += item.to_string ();
		}
		return str + ")";
  }
}

/**
 * Implemenation of {@link SqlExpressionOperatorMultiterm}
 */
public class Vda.ExpressionOperatorMultiterm : ExpressionOperator, SqlExpressionOperatorMultiterm
{
	protected string _operator_name = "";
	internal override string to_string () {
		string str = "";
		bool start = true;
		for (int i = 0; i < get_n_items (); i++) {
			if (!start) {
				str += " "+_operator_name+" ";
			} else {
				start = false;
			}
			var item = get_item (i) as SqlExpression;
			str += item.to_string ();
		}
		return str;
  }
}


/**
 * Implemenation of {@link SqlExpressionOperatorAnd}
 */
public class Vda.ExpressionOperatorAnd : ExpressionOperatorMultiterm, SqlExpressionOperatorAnd
{
	construct {
		_operator_name = "AND";
		_operator_type = SqlExpressionOperator.Type.AND;
	}
}

/**
 * Implemenation of {@link SqlExpressionOperatorOr}
 */
public class Vda.ExpressionOperatorOr : ExpressionOperatorMultiterm, SqlExpressionOperatorOr
{
	construct {
		_operator_name = "OR";
		_operator_type = SqlExpressionOperator.Type.OR;
	}
}


/**
 * Implemenation of {@link SqlExpressionOperatorBinaryterm}
 */
public class Vda.ExpressionOperatorBinaryterm : ExpressionOperator, SqlExpressionOperatorBinaryterm
{
	protected string _operator_name = "";
	internal override string to_string () {
		string str = "";
		if (get_n_items () > 0) {
			var item1 = get_item (0) as SqlExpression;
			if (item1 != null) {
				 str += item1.to_string ();
			}
		} else {
			str += "<?>";
		}
		str += " "+_operator_name+" ";
		if (get_n_items () > 1) {
			var item2 = get_item (1) as SqlExpression;
			if (item2 != null) {
				str += item2.to_string ();
			}
		} else {
			str += "<?>";
		}
		return str;
  }
}


/**
 * Implemenation of {@link SqlExpressionOperatorEq}
 */
public class Vda.ExpressionOperatorEq : ExpressionOperatorBinaryterm, SqlExpressionOperatorEq
{
	construct {
		_operator_name = "=";
		_operator_type = SqlExpressionOperator.Type.EQ;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorNotEq}
 */
public class Vda.ExpressionOperatorNotEq : ExpressionOperatorBinaryterm, SqlExpressionOperatorNotEq
{
	construct {
		_operator_name = "!=";
		_operator_type = SqlExpressionOperator.Type.EQ;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorDiff}
 */
public class Vda.ExpressionOperatorDiff : ExpressionOperatorBinaryterm, SqlExpressionOperatorDiff
{
	construct {
		_operator_name = "<>";
		_operator_type = SqlExpressionOperator.Type.DIFF;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorGt}
 */
public class Vda.ExpressionOperatorGt : ExpressionOperatorBinaryterm, SqlExpressionOperatorGt
{
	construct {
		_operator_name = ">";
		_operator_type = SqlExpressionOperator.Type.GT;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorLt}
 */
public class Vda.ExpressionOperatorLt : ExpressionOperatorBinaryterm, SqlExpressionOperatorLt
{
	construct {
		_operator_name = "<";
		_operator_type = SqlExpressionOperator.Type.LT;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorGeq}
 */
public class Vda.ExpressionOperatorGeq : ExpressionOperatorBinaryterm, SqlExpressionOperatorGeq
{
	construct {
		_operator_name = ">=";
		_operator_type = SqlExpressionOperator.Type.GEQ;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorLeq}
 */
public class Vda.ExpressionOperatorLeq : ExpressionOperatorBinaryterm, SqlExpressionOperatorLeq
{
	construct {
		_operator_name = "<=";
		_operator_type = SqlExpressionOperator.Type.LEQ;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorRegexp}
 */
public class Vda.ExpressionOperatorRegexp : ExpressionOperatorBinaryterm, SqlExpressionOperatorRegexp
{
	construct {
		_operator_name = "~";
		_operator_type = SqlExpressionOperator.Type.REGEXP;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorStar}
 */
public class Vda.ExpressionOperatorStar : ExpressionOperatorBinaryterm, SqlExpressionOperatorStar
{
	construct {
		_operator_name = "*";
		_operator_type = SqlExpressionOperator.Type.STAR;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorDiv}
 */
public class Vda.ExpressionOperatorDiv : ExpressionOperatorBinaryterm, SqlExpressionOperatorDiv
{
	construct {
		_operator_name = "/";
		_operator_type = SqlExpressionOperator.Type.DIV;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIn}
 */
public class Vda.ExpressionOperatorIn : ExpressionOperatorBinaryterm, SqlExpressionOperatorIn
{
	construct {
		_operator_name = "IN";
		_operator_type = SqlExpressionOperator.Type.IN;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorNotIn}
 */
public class Vda.ExpressionOperatorNotIn : ExpressionOperatorBinaryterm, SqlExpressionOperatorNotIn
{
	construct {
		_operator_name = "NOT IN";
		_operator_type = SqlExpressionOperator.Type.NOT_IN;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorConcatenate}
 */
public class Vda.ExpressionOperatorConcatenate : ExpressionOperatorBinaryterm, SqlExpressionOperatorConcatenate
{
	construct {
		_operator_name = "||";
		_operator_type = SqlExpressionOperator.Type.CONCATENATE;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorSimilarTo}
 */
public class Vda.ExpressionOperatorSimilarTo : ExpressionOperatorBinaryterm, SqlExpressionOperatorSimilarTo
{
	construct {
		_operator_name = "SIMILAR TO";
		_operator_type = SqlExpressionOperator.Type.SIMILAR_TO;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorLike}
 */
public class Vda.ExpressionOperatorLike : ExpressionOperatorBinaryterm, SqlExpressionOperatorLike
{
	construct {
		_operator_name = "LIKE";
		_operator_type = SqlExpressionOperator.Type.LIKE;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorNotLike}
 */
public class Vda.ExpressionOperatorNotLike : ExpressionOperatorBinaryterm, SqlExpressionOperatorNotLike
{
	construct {
		_operator_name = "NOT LIKE";
		_operator_type = SqlExpressionOperator.Type.LIKE;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIlike}
 */
public class Vda.ExpressionOperatorIlike : ExpressionOperatorBinaryterm, SqlExpressionOperatorIlike
{
	construct {
		_operator_name = "ILIKE";
		_operator_type = SqlExpressionOperator.Type.ILIKE;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorNotIlike}
 */
public class Vda.ExpressionOperatorNotIlike : ExpressionOperatorBinaryterm, SqlExpressionOperatorNotIlike
{
	construct {
		_operator_name = "NOT ILIKE";
		_operator_type = SqlExpressionOperator.Type.ILIKE;
	}
}

/**
 * Implemenation of {@link SqlExpressionOperatorBinaryUnaryterm}
 */
public class Vda.ExpressionOperatorBinaryUnaryterm : ExpressionOperator, SqlExpressionOperatorBinaryUnaryterm
{
	protected string _operator_name = "";
	internal override string to_string () {
		string str = _operator_name;
		bool two = get_n_items () > 2 ? true : false;
		var item1 = get_item (0) as SqlExpression;
		string item1_str = "";
		if (item1.get_n_items () > 1) {
			item1_str += "(";
		}
		item1_str += item1.to_string ();
		if (item1.get_n_items () > 1) {
			item1_str += ")";
		}

		if (!two) {
			str = _operator_name + item1_str;
		} else {
			var item2 = get_item (1) as SqlExpression;
			string item2_str = "";
			if (item2.get_n_items () > 1) {
				item2_str += "(";
			}
			item2_str += item2.to_string ();
			if (item2.get_n_items () > 1) {
				item2_str += ")";
			}
			str = item1_str+" "+_operator_name+" "+item2_str;
		}
		return str;
  }
}


/**
 * Implemenation of {@link SqlExpressionOperatorMinus}
 */
public class Vda.ExpressionOperatorMinus : ExpressionOperatorBinaryUnaryterm, SqlExpressionOperatorMinus
{
	construct {
		_operator_name = "-";
		_operator_type = SqlExpressionOperator.Type.MINUS;
	}
}

/**
 * Implemenation of {@link SqlExpressionOperatorPlus}
 */
public class Vda.ExpressionOperatorPlus : ExpressionOperatorBinaryUnaryterm, SqlExpressionOperatorPlus
{
	construct {
		_operator_name = "+";
		_operator_type = SqlExpressionOperator.Type.PLUS;
	}
}


/**
 * Implemenation of {@link SqlExpressionOperatorInitialUnaryterm}
 */
public class Vda.ExpressionOperatorInitialUnaryterm : ExpressionOperator, SqlExpressionOperatorInitialUnaryterm
{
	protected string _operator_name = "";
	internal override string to_string () {
		string str = _operator_name;
		var item1 = get_item (0) as SqlExpression;
		string item1_str = "";
		if (item1.get_n_items () > 1) {
			item1_str += "(";
		}
		item1_str += item1.to_string ();
		if (item1.get_n_items () > 1) {
			item1_str += ")";
		}
		return str+" "+item1_str;
  }
}


/**
 * Implemenation of {@link SqlExpressionOperatorNot}
 */
public class Vda.ExpressionOperatorNot : ExpressionOperatorInitialUnaryterm, SqlExpressionOperatorNot
{
	construct {
		_operator_name = "NOT";
		_operator_type = SqlExpressionOperator.Type.NOT;
	}
}


/**
 * Implemenation of {@link SqlExpressionOperatorFinalUnaryterm}
 */
public class Vda.ExpressionOperatorFinalUnaryterm : ExpressionOperator, SqlExpressionOperatorFinalUnaryterm
{
	protected string _operator_name = "";
	internal override string to_string () {
		var item1 = get_item (0) as SqlExpression;
		string item1_str = "";
		if (item1.get_n_items () > 1) {
			item1_str += "(";
		}
		item1_str += item1.to_string ();
		if (item1.get_n_items () > 1) {
			item1_str += ")";
		}
		return item1_str+" "+_operator_name;
  }
}
/**
 * Implemenation of {@link SqlExpressionOperatorIs}
 */
public class Vda.ExpressionOperatorIs : ExpressionOperatorFinalUnaryterm, SqlExpressionOperatorIs {}
/**
 * Implemenation of {@link SqlExpressionOperatorIsNot}
 */
public class Vda.ExpressionOperatorIsNot : ExpressionOperatorIs, SqlExpressionOperatorIsNot {}
/**
 * Implemenation of {@link SqlExpressionOperatorIsNull}
 */
public class Vda.ExpressionOperatorIsNull : ExpressionOperatorIs, SqlExpressionOperatorIsNull
{
	construct {
		_operator_name = "IS NULL";
		_operator_type = SqlExpressionOperator.Type.IS_NULL;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIsNotNull}
 */
public class Vda.ExpressionOperatorIsNotNull : ExpressionOperatorIsNot, SqlExpressionOperatorIsNotNull
{
	construct {
		_operator_name = "IS NOT NULL";
		_operator_type = SqlExpressionOperator.Type.IS_NOT_NULL;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIsTrue}
 */
public class Vda.ExpressionOperatorIsTrue : ExpressionOperatorIs, SqlExpressionOperatorIsTrue
{
	construct {
		_operator_name = "IS TRUE";
		_operator_type = SqlExpressionOperator.Type.IS_TRUE;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIsNotTrue}
 */
public class Vda.ExpressionOperatorIsNotTrue : ExpressionOperatorIsNot, SqlExpressionOperatorIsNotTrue
{
	construct {
		_operator_name = "IS NOT TRUE";
		_operator_type = SqlExpressionOperator.Type.IS_NOT_TRUE;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIsFalse}
 */
public class Vda.ExpressionOperatorIsFalse : ExpressionOperatorIs, SqlExpressionOperatorIsFalse
{
	construct {
		_operator_name = "IS FALSE";
		_operator_type = SqlExpressionOperator.Type.IS_FALSE;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIsNotFalse}
 */
public class Vda.ExpressionOperatorIsNotFalse : ExpressionOperatorIsNot, SqlExpressionOperatorIsNotFalse
{
	construct {
		_operator_name = "IS NOT FALSE";
		_operator_type = SqlExpressionOperator.Type.IS_NOT_FALSE;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIsNotUnknown}
 */
public class Vda.ExpressionOperatorIsNotUnknown : ExpressionOperatorIsNot, SqlExpressionOperatorIsNotUnknown
{
	construct {
		_operator_name = "IS NOT UNKNOWN";
		_operator_type = SqlExpressionOperator.Type.IS_NOT_UNKNOWN;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIsUnknown}
 */
public class Vda.ExpressionOperatorIsUnknown : ExpressionOperatorIs, SqlExpressionOperatorIsUnknown
{
	construct {
		_operator_name = "IS UNKNOWN";
		_operator_type = SqlExpressionOperator.Type.IS_UNKNOWN;
	}
}

/**
 * Implemenation of {@link SqlExpressionOperatorIsDistinct}
 */
public class Vda.ExpressionOperatorIsDistinct : ExpressionOperatorBinaryterm, SqlExpressionOperatorIsDistinct {}
/**
 * Implemenation of {@link SqlExpressionOperatorIsNotDistinct}
 */
public class Vda.ExpressionOperatorIsNotDistinct : ExpressionOperatorIsDistinct, SqlExpressionOperatorIsNotDistinct {}
/**
 * Implemenation of {@link SqlExpressionOperatorIsDistinctFrom}
 */
public class Vda.ExpressionOperatorIsDistinctFrom : ExpressionOperatorIsDistinct, SqlExpressionOperatorIsDistinctFrom
{
	construct {
		_operator_name = "IS DISTINCT FROM";
		_operator_type = SqlExpressionOperator.Type.IS_DISTINCT_FROM;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorIsNotDistinctFrom}
 */
public class Vda.ExpressionOperatorIsNotDistinctFrom : ExpressionOperatorIsNotDistinct, SqlExpressionOperatorIsNotDistinctFrom
{
	construct {
		_operator_name = "IS NOT DISTINCT FROM";
		_operator_type = SqlExpressionOperator.Type.IS_NOT_DISTINCT_FROM;
	}
}

/**
 * Implemenation of {@link SqlExpressionOperatorThreeterm}
 */
public class Vda.ExpressionOperatorThreeterm : ExpressionOperator, SqlExpressionOperatorThreeterm
{
	protected string _operator_name = "";
	protected string _operator2_name = "";
	internal override string to_string () {
		if (get_n_items () < 3) {
			return "";
		}
		var item1 = get_item (0) as SqlExpression;
		string item1_str = "";
		if (item1.get_n_items () > 1) {
			item1_str += "(";
		}
		item1_str += item1.to_string ();
		if (item1.get_n_items () > 1) {
			item1_str += ")";
		}
		var item2 = get_item (1) as SqlExpression;
		string item2_str = "";
		if (item2.get_n_items () > 1) {
			item2_str += "(";
		}
		item2_str += item2.to_string ();
		if (item2.get_n_items () > 1) {
			item2_str += ")";
		}
		var item3 = get_item (2) as SqlExpression;
		string item3_str = "";
		if (item3.get_n_items () > 1) {
			item3_str += "(";
		}
		item3_str += item3.to_string ();
		if (item3.get_n_items () > 1) {
			item3_str += ")";
		}
		return item1_str+" "+_operator_name+" "+item2_str+" "+_operator2_name+" "+item3_str;
  }
}


/**
 * Implemenation of {@link SqlExpressionOperatorBetween}
 */
public class Vda.ExpressionOperatorBetween : ExpressionOperatorThreeterm, SqlExpressionOperatorBetween {}
/**
 * Implemenation of {@link SqlExpressionOperatorBetweenAnd}
 */
public class Vda.ExpressionOperatorBetweenAnd : ExpressionOperatorBetween, SqlExpressionOperatorBetweenAnd
{
	construct {
		_operator_name = "BETWEEN";
		_operator2_name = "AND";
		_operator_type = SqlExpressionOperator.Type.NOT_BETWEEN;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorNotBetween}
 */
public class Vda.ExpressionOperatorNotBetween : ExpressionOperatorBetween, SqlExpressionOperatorNotBetween {}
/**
 * Implemenation of {@link SqlExpressionOperatorNotBetweenAnd}
 */
public class Vda.ExpressionOperatorNotBetweenAnd : ExpressionOperatorNotBetween, SqlExpressionOperatorNotBetweenAnd
{
	construct {
		_operator_name = "NOT BETWEEN";
		_operator2_name = "AND";
		_operator_type = SqlExpressionOperator.Type.BETWEEN;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorBetweenSymmetric}
 */
public class Vda.ExpressionOperatorBetweenSymmetric : ExpressionOperatorBetween, SqlExpressionOperatorBetweenSymmetric {}
/**
 * Implemenation of {@link SqlExpressionOperatorBetweenSymmetricAnd}
 */
public class Vda.ExpressionOperatorBetweenSymmetricAnd : ExpressionOperatorBetweenSymmetric, SqlExpressionOperatorBetweenSymmetricAnd
{
	construct {
		_operator_name = "BETWEEN SYMMETRIC";
		_operator2_name = "AND";
		_operator_type = SqlExpressionOperator.Type.NOT_BETWEEN_SYMMETRIC;
	}
}
/**
 * Implemenation of {@link SqlExpressionOperatorNotBetweenSymmetric}
 */
public class Vda.ExpressionOperatorNotBetweenSymmetric : ExpressionOperatorBetweenSymmetric, SqlExpressionOperatorNotBetweenSymmetric {}
/**
 * Implemenation of {@link SqlExpressionOperatorNotBetweenSymmetricAnd}
 */
public class Vda.ExpressionOperatorNotBetweenSymmetricAnd : ExpressionOperatorNotBetweenSymmetric, SqlExpressionOperatorNotBetweenSymmetricAnd
{
	construct {
		_operator_name = "NOT BETWEEN SYMMETRIC";
		_operator2_name = "AND";
		_operator_type = SqlExpressionOperator.Type.NOT_BETWEEN_SYMMETRIC;
	}
}
