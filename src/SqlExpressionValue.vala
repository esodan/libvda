/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlValueCommand.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an SQL Expression value
 */
public interface Vda.SqlExpressionValue  : Object, SqlExpression
{
	/**
	 * A {@link Vda.Connection} to be used by the value.
	 */
	public abstract Connection? connection { get; set; }
	/**
	 * Actual value as a {@link SqlValue}
	 */
	public abstract SqlValue @value { get; set; }
	/**
	 * Parses an string Math expression and assign its result as a value.
	 *
	 * Parsed expression is stored as a {@link SqlValueMathExp}
	 */
	public virtual void set_math_expression_value (string str, SqlParameters? @params = null) throws GLib.Error {
		var v = new Vda.ValueMathExp ();
		v.parameters = @params;
    v.parse (str);
    @value = v;
	}
}
