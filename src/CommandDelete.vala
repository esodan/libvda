/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * DeleteCommand.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Implementation of {@link SqlCommandDelete} SQL command
 */
public class Vda.CommandDelete  : Object,
															SqlCommand,
															SqlCommandTableRelated,
															SqlCommandConditional,
															Stringifiable,
															SqlCommandParametrized,
															SqlCommandDelete
{
	Connection _connection;
	Parameters _parameters;

	public CommandDelete (Connection cnc) {
		_connection = cnc;
	}
	// SqlTableRelatedCommand
	internal string table { get; set; }
  internal string allias { get; set; }
	// Stringifiable
	internal string to_string () {
		string str = "";
		try {
			str = stringify ();
		} catch (GLib.Error e) {
			warning ("Fail to convert to string: %s", e.message);
		}
		return str;
	}
	// SqlCommand
	internal Connection connection { get { return _connection; } }
	// SqlConditionalCommand
	SqlExpression _condition = new ExpressionOperatorAnd ();
  internal SqlExpression condition { get { return _condition; } }
	// SqlParametrizedCommand
	internal SqlParameters parameters {
		get {
			if (_parameters == null) {
				_parameters = new Parameters ();
			}
			return _parameters;
		}
	}
}

