/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * Query.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * An interface to be represent any query to be executed by
 * providers
 */
public interface Vda.Query : GLib.Object {
  /**
   * The string representation of SQL command this query use.
   */
  public abstract string sql { owned get; }
  /**
   * A {@link Vda.Connection} used to execute query on.
   */
  public abstract Vda.Connection connection { get; }
  /**
   * Executes the query over the {@link connection}
   */
  public abstract async Vda.Result? execute (GLib.Cancellable? cancellable) throws GLib.Error;
  /**
   * Cancel the query execution.
   */
  public abstract async void cancel ();
  /**
   * The string representation of SQL command with all its values substited if apply.
   *
   * For a simple {@link Query}, returns the value at {@link sql}, but for other types
   * like {@link PreparedQuery} will substitud all values prepared as parameters,
   * see {@link PreparedQuery} for more details.
   */
  public abstract string render_sql () throws GLib.Error;
}

/**
 * Query error codes
 */
public errordomain Vda.QueryError {
  INVALID_QUERY_ERROR,
  INVALID_CONNECTION_ERROR,
  GENERAL_ERROR
}

