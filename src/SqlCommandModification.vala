/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlCommandModification.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an SQL command to modify data in the database
 */
public interface Vda.SqlCommandModification  : Object
{
	/**
	 * Fields in the command to use.
	 *
	 * The use of given field depends on the command's type.
	 * For {@link SqlCommandInsert} this will be the field's value to
	 * insert in the database, for example.
	 */
	public abstract HashModel fields { get; }
	/**
	 * Fields' values in the command to use.
	 *
	 * The use of given value depends on the command's type.
	 * For {@link SqlCommandInsert} this will be the value to
	 * insert in the database, for example.
	 */
	public abstract HashModel values { get; }
	/**
	 * Add a new {@link SqlExpressionField} and a {@link SqlExpressionField}
	 * to {@link fields} and {@link values}, to be used to modify field's
	 * value to new value or append to a table.
	 *
	 * @param name The field name to change/set
	 * @param val The value to set/change the one at field
	 */
	public abstract void add_field_value (string name, GLib.Value? val);
	/**
	 * Add a new {@link SqlExpressionField}.
	 *
	 * New field is not paired with a correspondant {@link ExpressionValue}
	 * so use {@link add_parameter} to add a paired value with
	 * this field, other wise this command will be invalid.
	 *
	 * @param name The field name to change/set
	 */
	public abstract void add_field (string name) throws GLib.Error;
	/**
	 * Add a new {@link ExpressionValue}, checking if it pairs with a {@link SqlExpressionField} in the structure.
	 *
	 * Value will be associated with the field in the order they where added
	 * using {@link add_field}, take care with {@link add_field_parameter_value}
	 * because you can fall in a desorder set of field/value pairs.
	 *
	 * @param val The value to change/set
	 */
	public abstract void add_value (GLib.Value? val) throws GLib.Error;
	/**
	 * Add a new {@link SqlExpressionField} and a {@link SqlExpressionValueParameter}
	 * to {@link fields} and {@link values}, to be used to modify field's
	 * value to new value or append to a table. The parameter can be used
	 * to execute the query with specific values.
	 *
	 * @param field The field name to change/set
	 * @param par The parameter's name
	 * @param gtype The parameter's type
	 */
	public abstract void add_field_parameter_value (string field, string par, Type gtype);
	/**
	 * Add a new {@link SqlExpressionValueParameter} and checks if there is a field to be used with.
	 *
	 * Value will be associated with the field in the order they where added
	 * using {@link add_field}, take care with {@link add_field_parameter_value}
	 * because you can fall in a desorder set of field/value pairs.
	 *
	 * @param par The parameter's name
	 * @param gtype The parameter's type
	 */
	public abstract void add_parameter (string par, Type gtype) throws GLib.Error;
}
