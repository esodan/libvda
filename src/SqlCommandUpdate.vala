/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlCommandUpdate.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an UPDATE SQL command
 */
public interface Vda.SqlCommandUpdate  : Object,
																				SqlCommandTableRelated,
																				SqlCommandModification,
																				SqlCommandConditional,
																				SqlCommand,
																				SqlCommandParametrized
{
	/**
	 * Creates a string representation
	 */
	public virtual string stringify () throws GLib.Error {
		if (table == null) {
			throw new SqlCommandError.INVALID_STRUCTURE_ERROR (_("No table is given"));
		}
		if (fields.get_n_items () != values.get_n_items ()) {
		  throw new SqlCommandError.INVALID_STRUCTURE_ERROR (_("Invalid number of fields or values"));
		}
		bool par = false;
		string str = "UPDATE "+table+" SET ";
    for (int i = 0; i < fields.get_n_items (); i++) {
      var f = fields.get_item (i) as SqlExpressionField;
      if (f == null) continue;
      var v = values.get_item (i) as SqlExpressionValue;
      if (v == null) continue;
      str += f.name + " = ";
      if (v is ExpressionValueParameter) {
        par = true;
        string pval = ((ExpressionValueParameter) v).to_string ();
        if (parameters != null) {
        	var p = parameters.get_sql_value (((ExpressionValueParameter) v).name);
        	if (p != null) {
        		pval = connection.value_to_quoted_string (p);
        	}
        }
        str += pval;
      } else {
        str += connection.value_to_quoted_string (v.@value);
      }
      if (i + 1 < fields.get_n_items ()) {
        str += ", ";
      }
    }
    if (condition != null && condition.get_n_items () != 0) {
    	str += " WHERE " + condition.to_string ();
    }
    return str;
	}
	/**
	 * Create a {@link Query} for execution, using internal structure.
	 *
	 * If {@link ExpressionValueParameter} object is used for values,
	 * then a {@link PreparedQuery} is returned with the given name.
	 */
	public virtual Query to_query (string? name = null) throws GLib.Error {
    string str = stringify ();
  	return connection.parse_string_prepared (name, str);
  }
  /**
   * Parse SQL string commands and construct its internal tree
   */
  public virtual void parse (string sql)  throws GLib.Error {
		Gee.ArrayList<GLib.TokenType> expected = new Gee.ArrayList<GLib.TokenType> ();
    var scanner = new GLib.Scanner (null);
    scanner.input_name = "SQL";
    scanner.input_text (sql, sql.length);
    scanner.config.cpair_comment_single = "//\n";
    scanner.config.skip_comment_multi = false;
    scanner.config.skip_comment_single = false;
    scanner.config.char_2_token = false;
    scanner.config.scan_binary = false;
    scanner.config.scan_octal = false;
    scanner.config.scan_float = false;
    scanner.config.scan_hex = false;
    scanner.config.scan_hex_dollar = false;
    scanner.config.numbers_2_int = false;
    GLib.TokenType token = GLib.TokenType.NONE;
    bool complete_init = false;
    bool table_setted = false;
    bool expected_table_name = false;
    bool set_allias = false;
    bool enable_field = false;
    bool enable_value = false;
    bool param_type = false;
    bool param_identifier = false;
    bool start_math_expression = false;
    bool values = false;
    bool starting = true;
    string param_name = "";
    string param_type_name = "";
    string math_expression = "";
    StringBuilder bstr = new StringBuilder ("");
    while (token != GLib.TokenType.EOF) {
      token = scanner.get_next_token ();
      if (token == GLib.TokenType.EOF) {
      	if (values && enable_value && start_math_expression) {
          start_math_expression = false;
          var expm = new Vda.ExpressionValueMath ();
          expm.connection = connection;
          expm.parse (math_expression);
          // A Math Expression as Value
          add_value (expm);
      	}
        break;
      }
      if (expected.size != 0 && !expected.contains (token)) {
      	string errchar = "";
      	switch (token) {
      		case GLib.TokenType.IDENTIFIER:
      			errchar = scanner.cur_value ().@identifier;
		      	break;
		      case GLib.TokenType.INT:
		      	errchar = scanner.cur_value ().@int.to_string ();
		      	break;
	      	case GLib.TokenType.FLOAT:
      			errchar = "%g".printf (scanner.cur_value ().@float);
		      	break;
      		case GLib.TokenType.STRING:
		        errchar = scanner.cur_value ().@string;
		      	break;
		      case GLib.TokenType.CHAR:
		      	bstr.assign ("");
		      	bstr.append_c ((char) scanner.cur_value ().@char);
          	errchar = bstr.str;
		      	break;
		      case GLib.TokenType.NONE:
		      case GLib.TokenType.LEFT_PAREN:
		      case GLib.TokenType.SYMBOL:
		      case GLib.TokenType.COMMENT_MULTI:
		      case GLib.TokenType.RIGHT_CURLY:
		      case GLib.TokenType.BINARY:
		      case GLib.TokenType.OCTAL:
		      case GLib.TokenType.EQUAL_SIGN:
		      case GLib.TokenType.RIGHT_PAREN:
		      case GLib.TokenType.ERROR:
		      case GLib.TokenType.LAST:
		      case GLib.TokenType.LEFT_BRACE:
		      case GLib.TokenType.EOF:
		      case GLib.TokenType.COMMA:
		      case GLib.TokenType.COMMENT_SINGLE:
		      case GLib.TokenType.LEFT_CURLY:
		      case GLib.TokenType.IDENTIFIER_NULL:
		      case GLib.TokenType.RIGHT_BRACE:
		      case GLib.TokenType.HEX:
		      	break;
      	}
        throw new ParserError.INVALID_TOKEN_ERROR (_("Found an unexpected expression '%s' at Line:Column : %d:%d"),
                                                          errchar, scanner.cur_line (), scanner.cur_position ());
      }
      switch (token) {
        case GLib.TokenType.IDENTIFIER:
          string identifier = scanner.cur_value ().@identifier;
          if (start_math_expression
          			&& identifier.down () != "update"
            		&& identifier.down () != "where"
            		&& identifier.down () != "set") {
            math_expression += identifier;
            break;
					} else if (complete_init && values && enable_value && param_identifier
											&& param_name == "" && !param_type) {
		        param_name = identifier;
		        param_type = true;
		        param_type_name = "";
		        expected.clear ();
		        expected.add (GLib.TokenType.CHAR);
		        break;
         	} else if (complete_init && values && enable_value && param_identifier && param_type && param_type_name == "") {
            param_type_name = scanner.cur_value ().@string.to_string ();
            param_type = false;
            var ptype = SqlExpressionValueParameter.gtype_from_string (param_type_name);
            add_parameter (param_name, ptype);
            enable_value = false;
            enable_field = false;
            param_identifier = false;
            param_type = false;
            param_name = "";
            param_type_name = "";
            expected.clear ();
            expected.add (GLib.TokenType.CHAR);
            expected.add (GLib.TokenType.IDENTIFIER);
            break;
          } else if (starting) {
            if (identifier.down () != "update") {
              throw new ParserError.INVALID_TOKEN_ERROR (_("SQL update command should start with UPDATE"));
            }
            starting = false;
            complete_init = false;
            expected_table_name = true;
            values = false;
            enable_value = false;
            enable_field = false;
            param_identifier = false;
            param_name = "";
            param_type = false;
            table_setted = false;
            break;
          } else if (complete_init && !enable_field
          					&& identifier.down () == "where") {
          	if (start_math_expression) {
          		start_math_expression = false;
				      var expm = new Vda.ExpressionValueMath ();
				      expm.connection = connection;
				      expm.parse (math_expression);
				      // A Math Expression as Value
				      add_value (expm);
          	}
            int start = scanner.cur_position ();
			      string rest = sql.substring (start, sql.length - start);
			      SqlExpression cond = SqlExpression.parse (rest, connection, parameters);
			      condition.add_expression (cond);
			      return;
          } else if (expected_table_name && !table_setted) {
              table = scanner.cur_value ().@string;
              table_setted = true;
              enable_field = false;
              enable_value = false;
              expected_table_name = false;
              values = false;
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
              expected.add (GLib.TokenType.CHAR);
              break;
          } else if (!expected_table_name && identifier.down () == "as" && !set_allias) {
              set_allias = true;
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
              break;
          } else if (!expected_table_name && set_allias) {
              set_allias = false;
              allias = scanner.cur_value ().@string;
              expected.clear ();
              expected.add (GLib.TokenType.CHAR);
              break;
          } else if (!complete_init && !expected_table_name && !set_allias
          						&& table_setted && !values && !enable_field && !enable_value) {
            if (identifier.down () == "set") {
              enable_value = false;
              enable_field = true;
              values = true;
              complete_init = true;
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
              break;
            } else {
              throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid UPDATE declaration: expected SET keyword at Line:Column : %d:%d"),
                                                      scanner.cur_line (), scanner.cur_position ());
            }
          } else if (complete_init && enable_field && values && !param_identifier) {
            add_field (identifier);
            enable_field = false;
            enable_value = false;
            expected.clear ();
            expected.add (GLib.TokenType.CHAR);
            break;
          }
          throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid UPDATE declaration: unexpected identifer '%s' at Line:Column : %d:%d"),
                                                      identifier, scanner.cur_line (), scanner.cur_position ());
        case GLib.TokenType.INT:
        	if (enable_value && values) {
		        if (!start_math_expression) {
		        	start_math_expression = true;
		          math_expression = "";
		        }
            math_expression += scanner.cur_value ().@int.to_string ();
            expected.clear ();
          }
          break;
        case GLib.TokenType.FLOAT:
        	if (enable_value && values && enable_value) {
		        if (!start_math_expression) {
            	start_math_expression = true;
		          math_expression = "";
		        }
            math_expression += "%g".printf (scanner.cur_value ().@float);
            expected.clear ();
          }
          break;
        case GLib.TokenType.STRING:
          // Identifiers
          if (enable_value && values) {
            add_value (scanner.cur_value ().@string);
            enable_value = false;
            enable_field = false;
            expected.clear ();
            expected.add (GLib.TokenType.CHAR);
          }
          break;
        case GLib.TokenType.CHAR:
          var v = scanner.cur_value ().@char;
          if (v == '=') {
          	if (values && !enable_field && !enable_value) {
          		enable_field = false;
          		enable_value = true;
              expected.clear ();
          	}
          } else if (v == ',') {
            // SQL expression separator
            if (start_math_expression && enable_value) {
              start_math_expression = false;
              var expm = new Vda.ExpressionValueMath ();
              expm.connection = connection;
              expm.parse (math_expression);
              // A Math Expression as Value
              add_value (expm);
              enable_value = false;
              enable_field = true;
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
            } else if (values && !enable_value && !enable_field) {
            	enable_field = true;
            	enable_value = false;
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
            }
          } else if (v == ':') {
          	if (values && !enable_field && enable_value && param_identifier
          			&& param_type && param_type_name == "") {
		          token = scanner.get_next_token ();
		          if (token == GLib.TokenType.CHAR) {
		            var nv = scanner.cur_value ().@char;
		            if (nv == ':') {
	                expected.clear ();
	                expected.add (GLib.TokenType.IDENTIFIER);
	                break;
		            }
		          } else {
		            throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid parameter declaration: expected ':' at Line:Column : %d:%d"),
		                                                        scanner.cur_line (), scanner.cur_position ());
		          }
	          } else {
	            throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid value declaration: unexpected ':' at Line:Column : %d:%d"),
	                                                        scanner.cur_line (), scanner.cur_position ());
	          }
          } else if (v == '#') {
            if (values && enable_value && !param_identifier && param_name == "") {
              token = scanner.get_next_token ();
              if (token != GLib.TokenType.CHAR) {
                throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid Parameter declaration: expected '#' at Line:Column : %d:%d"),
                                                      scanner.cur_line (), scanner.cur_position ());
              }
              var vp = scanner.cur_value ().@char;
              if (vp != '#') {
                throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid Parameter declaration: expected '#' got '%s' at Line:Column : %d:%d"),
                                                      bstr.str, scanner.cur_line (), scanner.cur_position ());
              }
              param_identifier = true;
              param_type = false;
              param_name = "";
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
            }
          } else if (v == '-') {
          	if (enable_value && values) {
          		if (!start_math_expression) {
          			start_math_expression = true;
          			math_expression = "";
          		}
          		bstr.assign ("");
              bstr.append_c ((char) v);
              math_expression += bstr.str;
              expected.clear ();
            }
          } else if (v == '+') {
          	if (enable_value && values) {
          		if (!start_math_expression) {
          			start_math_expression = true;
          			math_expression = "";
          		}
          		bstr.assign ("");
              bstr.append_c ((char) v);
              math_expression += bstr.str;
              expected.clear ();
          	}
          } else {
            if (start_math_expression) {
              bstr.assign ("");
              bstr.append_c ((char) v);
              math_expression += bstr.str;
              break;
            }
          }
          break;
	      case GLib.TokenType.NONE:
	      case GLib.TokenType.LEFT_PAREN:
	      case GLib.TokenType.SYMBOL:
	      case GLib.TokenType.COMMENT_MULTI:
	      case GLib.TokenType.RIGHT_CURLY:
	      case GLib.TokenType.BINARY:
	      case GLib.TokenType.OCTAL:
	      case GLib.TokenType.EQUAL_SIGN:
	      case GLib.TokenType.RIGHT_PAREN:
	      case GLib.TokenType.ERROR:
	      case GLib.TokenType.LAST:
	      case GLib.TokenType.LEFT_BRACE:
	      case GLib.TokenType.EOF:
	      case GLib.TokenType.COMMA:
	      case GLib.TokenType.COMMENT_SINGLE:
	      case GLib.TokenType.LEFT_CURLY:
	      case GLib.TokenType.IDENTIFIER_NULL:
	      case GLib.TokenType.RIGHT_BRACE:
	      case GLib.TokenType.HEX:
	      	break;
      }
    }
  }

}
