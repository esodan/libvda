/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * ConnectionParameters.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Connection Parameter
 */
public class Vda.ConnectionParameter : GLib.Object {
  public string name { get; construct set; }
  public string @value { get; set; }
}

/**
 * Connection Parameter for Data Base's name.
 */
public class Vda.ConnectionParameterDbName : Vda.ConnectionParameter {
  construct {
    name = "DB_NAME";
  }
}

/**
 * Connection Parameter for Host for the server to connect to
 */
public class Vda.ConnectionParameterHost : Vda.ConnectionParameter {
  construct {
    name = "HOST";
  }
}

/**
 * Connection Parameter for port for the server to connect to
 */
public class Vda.ConnectionParameterPort : Vda.ConnectionParameter {
  construct {
    name = "PORT";
  }
}

/**
 * Connection Parameter for user's name for the server to connect to
 */
public class Vda.ConnectionParameterUserName : Vda.ConnectionParameter {
  construct {
    name = "USERNAME";
  }
}


/**
 * Connection Parameter for user's password for the server to connect to
 */
public class Vda.ConnectionParameterPassword : Vda.ConnectionParameter {
  construct {
    name = "PASSWORD";
  }
}
/**
 * A class to hold and parse connection string
 */
public class Vda.ConnectionParameters : Gee.HashMap<string,ConnectionParameter> {

  public ConnectionParameters (string cnc_str) {
  	parse (cnc_str);
  }

  public virtual void parse (string cnstring) {
    clear ();
    var p = cnstring.split (";");
    for (int i = 0; i < p.length; i++) {
      if (p[i] == "") {
        continue;
      }
      if (p[i].contains ("=")) {
        var pp = p[i].split ("=");
        if (pp.length == 2) {
          ConnectionParameter par = null;
          switch (pp[0]) {
            case "DB_NAME":
              par = GLib.Object.new (typeof (ConnectionParameterDbName), "value", pp[1]) as ConnectionParameter;
              break;
            case "HOST":
              par = GLib.Object.new (typeof (ConnectionParameterHost), "value", pp[1]) as ConnectionParameter;
              break;
            case "PORT":
              par = GLib.Object.new (typeof (ConnectionParameterPort), "value", pp[1]) as ConnectionParameter;
              break;
            case "USERNAME":
              par = GLib.Object.new (typeof (ConnectionParameterUserName), "value", pp[1]) as ConnectionParameter;
              break;
            case "PASSWORD":
              par = GLib.Object.new (typeof (ConnectionParameterPassword), "value", pp[1]) as ConnectionParameter;
              break;
            default:
              par = GLib.Object.new (typeof (ConnectionParameter), "name", pp[0], "value", pp[1]) as ConnectionParameter;
              break;
          }
          if (par != null)
            @set (par.name, par);
        }
      } else {
        var pd = GLib.Object.new (typeof (ConnectionParameter), "name", p[i], "value", "1") as ConnectionParameter;
        @set ("1", pd);
      }
    }
  }
  public string to_string () {
    var str = "";
    foreach (string k in this.keys) {
      var v = this.get (k);
      str += k+"="+v.@value + ";";
    }
    return str;
  }

  public bool has_param (string name) {
      return has_key (name);
  }
}
