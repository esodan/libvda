/*
 * VodbcConnection.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2023 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Odbc;

public errordomain Vodbc.ConnectionError {
    INVALID_PARAMETER_ERROR
}

/**
 * Implementation of {@link Vda.Connection} for a direct connection to a SQLite file.
 *
 * Connection happends by providing following connection parameters
 *
 * 1.  SERVER_NAME as server name
 * 2.  USER_NAME as database's user's name
 * 3.  AUTHENTICATION as credentials required by the connection
 *
 * {{{
 * var c = new Vodbc.Connection ();
 * c.open_from_string ("SERVER_NAME=localhost;USER_NAME=user;AUTHENTICATION=password");
 * }}}
 */
[Version (since="1.2")]
public class Vodbc.Connection : GLib.Object, Vda.Connection {
    private Odbc.Connection connection = null;
    private Odbc.Environment environment = null;
    private Vda.Connection.Status _status = Vda.Connection.Status.DISCONNECTED;
    private Gee.HashMap<string,PreparedQuery> _queries = new Gee.HashMap<string,PreparedQuery> ();
    private string _cnc_string = null;

    internal unowned Odbc.Connection get_connection () { return connection; }

    // internal unowned Odbc.Environment get_environment () { return environment; }

    // Vda.Connection
    internal Vda.Connection.Status status { get { return _status; } }
    internal ConnectionParameters parameters { get; set; }
    internal bool is_opened {
        get  {
            return connection != null && _status == Vda.Connection.Status.CONNECTED;
        }
    }
    internal string connection_string {
        get {
            _cnc_string = parameters.to_string ();
            return _cnc_string;
        }
    }

    internal async void
    close () throws GLib.Error {
        if (connection == null) return;
        connection = null;
    }
    internal async Vda.Connection.Status
    open () throws GLib.Error
    {
        if (parameters == null) {
            _status = Vda.Connection.Status.DISCONNECTED;
            return Vda.Connection.Status.CANCELED;
        }

        if (!parameters.has_param ("SERVER_NAME")) {
            throw new Vodbc.ConnectionError.INVALID_PARAMETER_ERROR (_("No SERVER_NAME parameter is given"));
        }

        if (!parameters.has_param ("USER_NAME")) {
            throw new Vodbc.ConnectionError.INVALID_PARAMETER_ERROR (_("No USER_NAME parameter is given"));
        }

        if (!parameters.has_param ("AUTHENTICATION")) {
            throw new Vodbc.ConnectionError.INVALID_PARAMETER_ERROR (_("No AUTHENTICATION parameter is given"));
        }

        string server = parameters.get ("SERVER_NAME").@value;
        string user = parameters.get ("USER_NAME").@value;
        string auth = parameters.get ("AUTHENTICATION").@value;

        environment = Odbc.Environment.@new();

        connection = Odbc.Connection.new (environment);
        connection.connect (server.data, user.data, auth.data);

        _status = Vda.Connection.Status.CONNECTED;
        opened ();

        return _status;
    }

    internal async Vda.Connection.Status open_from_string (string cnc_string) throws GLib.Error {
        parameters = new ConnectionParameters (cnc_string);
        return yield open ();
    }

    internal Vda.Query
    parse_string (string sql) throws GLib.Error {
        var q = new Vodbc.Query.from_sql (this, sql);
        return q;
    }

    internal Vda.PreparedQuery?
    parse_string_prepared (string? name, string sql) throws GLib.Error {
        var q = new Vodbc.ParsedQuery (this, name != null ? name : _queries.size.to_string ());
        q.parse (sql);
        _queries.set (name, q);
        return q;
    }

    internal Vda.PreparedQuery? get_prepared_query (string name) {
        return _queries.get (name);
    }

    internal Vda.PreparedQuery?
    query_from_command (SqlCommand cmd, string? name) throws GLib.Error {
        if (this != cmd.connection) {
            throw new
                    Vda
                    .ConnectionError
                    .QUERY_CREATION_ERROR (_("Command is not using same connection"));
        }
        return new Vodbc.ParsedQuery.from_command (cmd, name);
    }

    /**
     * Convert a given ODBC's type's code to a {@link GLib.Type}
     *
     * Returns: a fundamental {@link GLib.Type} or a {@link Vda.SqlValue} derived object type
     */
    public static GLib.Type type_to_gtype (int t) {
        switch (t) {
          case Odbc.SqlDataType.CHAR:
            return typeof (Vda.ValueByte);
          case Odbc.SqlDataType.VARCHAR:
            return GLib.Type.STRING;
          case Odbc.SqlDataType.LONGVARCHAR:
            return GLib.Type.STRING;
          case Odbc.SqlDataType.WCHAR:
            return GLib.Type.STRING;
          case Odbc.SqlDataType.WVARCHAR:
            return GLib.Type.STRING;
          case Odbc.SqlDataType.WLONGVARCHAR:
            return GLib.Type.STRING;
          case Odbc.SqlDataType.DECIMAL:
          case Odbc.SqlDataType.NUMERIC:
          case Odbc.SqlDataType.SMALLINT:
            return typeof (Vda.ValueInt2);
          case Odbc.SqlDataType.INTEGER:
            return GLib.Type.INT;
          case Odbc.SqlDataType.REAL:
          case Odbc.SqlDataType.FLOAT:
            return GLib.Type.FLOAT;
          case Odbc.SqlDataType.DOUBLE:
            return GLib.Type.DOUBLE;
          case Odbc.SqlDataType.BIT:
            return GLib.Type.UCHAR;
          case Odbc.SqlDataType.TINYINT:
            return typeof (Vda.ValueByte);
          case Odbc.SqlDataType.BIGINT:
            return GLib.Type.INT64;
          case Odbc.SqlDataType.BINARY:
          case Odbc.SqlDataType.VARBINARY:
          case Odbc.SqlDataType.LONGVARBINARY:
            return typeof (Vda.ValueBinary);
          case Odbc.SqlDataType.DATE:
            return typeof (Vda.ValueDate);
          case Odbc.SqlDataType.TIME:
            return typeof (Vda.ValueTime);
          // case Odbc.SqlDataType.DATETIME:
          case Odbc.SqlDataType.TIMESTAMP:
            return typeof (Vda.ValueTimestamp);
          case Odbc.SqlDataType.TYPE_DATE:
            return typeof (Vodbc.ValueDataTypeDate);
          case Odbc.SqlDataType.TYPE_TIME:
            return typeof (Vodbc.ValueDataTypeTime);
          case Odbc.SqlDataType.TYPE_TIMESTAMP:
            return typeof (Vodbc.ValueDataTypeTimestamp);
          case Odbc.SqlDataType.GUID:
            return GLib.Type.INT64;
        }

        return GLib.Type.INVALID;
    }

    // ConnectionTransactional
    // internal bool add_savepoint (string? name) throws GLib.Error { return false; }
    // internal bool delete_savepoint (string? name) throws GLib.Error { return false; }
    // internal bool rollback_savepoint (string? name) throws GLib.Error { return false; }
    // internal bool begin_transaction (string? name) throws GLib.Error { return false; }
    // internal bool commit_transaction (string? name) throws GLib.Error { return false; }
    // internal bool rollback_transaction (string? name) throws GLib.Error { return false; }

    // ConnectionRolebased
    // internal Vda.Role? current_user () { return null; }

}

