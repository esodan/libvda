/*
 * VodbcConnection.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2023 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Odbc;
/**
 * An ODBC data type holder
 *
 * Since: 1.2
 */
[Version (since="1.2")]
public class Vodbc.ValueDataType : Vda.ValueDataType
{
  protected int _data_type = 0;

  construct {
    _name = "OdbcDataType";
  }
  /*
   * Provides the code of the ODBC data type
   *
   * Since: 1.2
   */
  public virtual int odbc_data_type {
    get { return (int) _data_type; }
    set { _data_type = (Odbc.SqlDataType) value; }
  }
}

/**
 * A read only ODBC's TYPE_DATE data type holder
 *
 * Since: 1.2
 */
[Version (since="1.2")]
public class Vodbc.ValueDataTypeDate : Vodbc.ValueDataType
{
  construct {
    _data_type = Odbc.SqlDataType.TYPE_DATE;
    _name = "OdbcDataTypeDate";
  }
  internal override int odbc_data_type {
    get { return (int) _data_type; }
    set {}
  }
}

/**
 * A read only ODBC's TYPE_TIME data type holder
 *
 * Since: 1.2
 */
[Version (since="1.2")]
public class Vodbc.ValueDataTypeTime : Vodbc.ValueDataType
{
  construct {
    _data_type = Odbc.SqlDataType.TYPE_TIME;
    _name = "OdbcDataTypeTime";
  }
  internal override int odbc_data_type {
    get { return (int) _data_type; }
    set {}
  }
}

/**
 * A read only ODBC's TYPE_TIMESTAMP data type holder
 *
 * Since: 1.2
 */
[Version (since="1.2")]
public class Vodbc.ValueDataTypeTimestamp : Vodbc.ValueDataType
{
  construct {
    _data_type = Odbc.SqlDataType.TYPE_TIMESTAMP;
    _name = "OdbcDataTypeTimestamp";
  }
  internal override int odbc_data_type {
    get { return (int) _data_type; }
    set {}
  }
}
