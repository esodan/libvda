/*
 * VodbcQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2023 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Odbc;

internal errordomain Vodbc.QueryError {
    FATAL_ERROR,
    STATEMENT_ERROR
}

internal class Vodbc.Query : Object, Vda.Query {
    protected string _sql = null;
    protected Vda.Connection cnc = null;
    private Odbc.Statement stm = null;

    public Query (Vda.Connection con)
      requires (con is Vodbc.Connection)
    {
        cnc = con;
        unowned Odbc.Connection c = ((Vodbc.Connection) con).get_connection ();
        stm = Odbc.Statement.new (c);
    }

    public Query.from_sql (Vda.Connection con, string sql)
      requires (con is Vodbc.Connection)
    {
        cnc = con;
        _sql = sql;
        unowned Odbc.Connection c = ((Vodbc.Connection) con).get_connection ();
        stm = Odbc.Statement.@new (c);
    }

    internal string sql {
        owned get {
            if (_sql != null) {
                return _sql;
            } else {
                return render_sql ();
            }
        }
    }

    internal Vda.Connection connection { get { return cnc; } }

    internal virtual async Vda.Result?
    execute (GLib.Cancellable? cancellable) throws GLib.Error {
        Odbc.Return code = Odbc.Return.ERROR;
        if (cnc == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Database is invalid"));
        }

        unowned Odbc.Connection conn = ((Vodbc.Connection) cnc).get_connection ();

        if (conn == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("ODBC Connection is invalid"));
        }

        if (stm == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Invalid SQL: %s"), _sql);
        }

        code = stm.execute_direct (_sql.data);
        if (code == Odbc.Return.ERROR) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Unable to execute query \"%s\". Parse error.").printf (_sql));
        }

        ssize_t cols = -1;
        stm.get_column_count (out cols);

        if (cols > 0) {
          return new Vodbc.TableModelSequential (this);
        }

        ssize_t rows = -1;
        stm.get_row_count (out rows);

        return new Vda.AffectedRows ((uint) rows);
    }

    internal async void cancel () {
        stm.cancel();
    }

    internal virtual string render_sql () {
        return _sql;
    }

    internal unowned Odbc.Statement get_stm () {
        return stm;
    }
}

class Vodbc.Result : GLib.Object, Vda.Result {
    protected Vodbc.Query _query;

    public Vda.Connection connection { get { return _query.connection; } }

    public Result (Vodbc.Query query) {
        _query = query;
    }
}

class Vodbc.TableModelSequential : Vodbc.Result, Vda.TableModelSequential
{
    bool started = false;
    bool next_success = false;

    // GLib.ListModel interface
    internal Vda.RowModel? current () {
        if (!started || !next_success) {
            return null;
        }

        return new Vodbc.RowModel (_query);
    }
    internal Vda.RowModel? copy_current () {
        return null;
    }

    internal bool next () throws GLib.Error {
        if (!started) {
            next_success = true;
            started = true;
            return true;
        }
        Odbc.Return r = _query.get_stm ().fetch_scroll(FetchDirection.NEXT, 1);
        if (r == Return.ERROR) {
            next_success = false;
            return false;
        }

        next_success = true;
        return true;
    }

    public TableModelSequential (Vodbc.Query query) {
        base (query);
    }
}

internal class Vodbc.RowModel : Object, GLib.ListModel, Vda.RowModel
{
    protected Vodbc.Query _query;
    public uint n_columns {
        get {
            int64 n = -1;

            return (uint) n;
        }
    }

    public Vda.ColumnModel? get_column (string name) throws GLib.Error {
        unowned Odbc.Statement stm = _query.get_stm ();
        int64 n = -1;
        int64 n_length = -1;
        stm.get_column_count (out n);
        for (int16 i = 0; i < n; i++) {
            uint8[] aname = new uint8[1000];
            stm.describe_column (i, out aname, out n_length, null, null, null, null);
            if (((string) aname) == name) {
                return new Vodbc.ColumnModel (_query, (int) i);
            }
        }

        return null;
    }

    public Vda.ColumnModel? get_column_at (uint col) throws GLib.Error  {
        unowned Odbc.Statement stm = _query.get_stm ();
        int64 n = -1;
        stm.get_column_count (out n);
        if (col > n) {
          throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Column is bigger than the columns in the result"));
        }

        return new Vodbc.ColumnModel (_query, col);
    }

    public SqlValue? get_value (string name) throws GLib.Error {
        unowned Odbc.Statement stm = _query.get_stm ();
        int64 n = -1;
        stm.get_column_count (out n);
        for (int16 i = 0; i < n; i++) {
            uint8[] aname = new uint8[1000];
            int16 n_length = -1;
            stm.describe_column (i, out aname, out n_length, null, null, null, null);
            if (((string) aname) == name) {
                return get_value_at ((uint) i);
            }
        }
        return null;
    }

    public SqlValue? get_value_at (uint col) throws GLib.Error {
        unowned Odbc.Statement stm = _query.get_stm ();
        uint8[] aname = new uint8[1000];
        int16 n_length = -1;
        SqlDataType data_type = SqlDataType.CHAR;
        stm.describe_column ((uint16) col, out aname, out n_length, null, out data_type, null, null);
        Vda.SqlValue ret = Vda.SqlValue.new_from_gtype (Vodbc.Connection.type_to_gtype ((int) data_type));

        return ret;
    }

    public string? get_string (string name) throws GLib.Error {
        if (name == null) {
            return null;
        }

        var v = get_value (name);
        if (v == null) {
            return null;
        }

        return v.to_string ();
    }

    public string? get_string_at (uint col) throws GLib.Error {
        var v = get_value_at (col);
        if (v == null) {
            return null;
        }

        return v.to_string ();
    }

    // GLib.ListModel interface
    public uint get_n_items () { return n_columns; }

    public GLib.Type get_item_type () { return typeof (ColumnModel); }

    public GLib.Object? get_item (uint position) {
        if (position > n_columns)
            return null;
        Vda.ColumnModel col = null;
        try {
            col = get_column_at (position);
        } catch (GLib.Error e) {
            warning (_("Error: %s"), e.message);
        }

        return col;
    }

    // Constructor
    public RowModel (Vodbc.Query query) {
        _query = query;
    }
}

class Vodbc.ColumnModel : Object, Vda.ColumnModel {
    private uint col = -1;
    private Vodbc.Query _query;
    private SqlDataType _type;
    private string _name = "";

    public string name {
        get {
            return _name;
        }
    }
    public GLib.Type data_type {
        get {
            var t = Vodbc.Connection.type_to_gtype (_type);
            return t;
        }
    }
    public ColumnModel (Vodbc.Query query, uint col) {
        this.col = col;
        _query = query;
        _type = SqlDataType.CHAR;
        uint8[] aname = new uint8[1000];
        int64 n_length = -1;
        unowned Odbc.Statement stm = _query.get_stm ();
        stm.describe_column ((uint16) col, out aname, out n_length, out _type, null, null, null);
        GLib.StringBuilder sb = new GLib.StringBuilder ();
        for (int n = 0; n < n_length; n++) {
          sb.append_c ((char) aname[n]);
        }

        _name = sb.str;
    }
}
