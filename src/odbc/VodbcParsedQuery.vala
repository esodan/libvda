/*
 * VodbcParsedQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2023 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represent a prepared query implementing {@link Vda.PreparedQuery}.
 *
 * Values required by query can be set by using parameters property.
 */
 internal class Vodbc.ParsedQuery : Vodbc.Query, Vda.PreparedQuery, Vda.ParsedQuery {
    protected string _name;
    protected Vda.SqlCommand _command;
    protected Vda.SqlParameters _params = new Vda.Parameters ();
    private Odbc.Statement stm = null;

    public ParsedQuery (Vda.Connection con, string name) {
        base (con);
        _name = name;
    }
    public ParsedQuery.from_command (Vda.SqlCommand cmd, string? name) {
        base (cmd.connection);
        _name = name;
        _command = cmd;
        debug ("Query created using a Vda.SqlCommand");
    }
    /**
     * Parse an SQL using {@link Vda.Parser}
     */
    internal void parse (string sql) throws GLib.Error {
        if (_command != null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Can't change the internal query definition"));
        }

        _sql = sql;

        debug ("String to parse: %s", _sql);
        if (Regex.match_simple("##\\w*::\\w*", _sql)) {
            debug ("Using VDA's internal SQL parser");
            _command = new Vda.Parser ().parse (_sql, cnc);
        } else {
            debug ("Using native parsing and binding");
            unowned Odbc.Connection cnn = ((Vodbc.Connection) cnc).get_connection ();
            if (cnn == null) {
                throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Invalid connection. Can't parse requested SQL: %s"), _sql);
            }

            stm = Odbc.Statement.@new (cnn);
            Odbc.Return code = stm.prepare (_sql.data);
            if (code == Odbc.Return.ERROR) {
                throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Unable to execute query \"%s\". Parse error.").printf (_sql));
            }

            debug ("SQL was parsed successfully");
        }
    }

    // Query
    internal override string render_sql () {
        if (_command != null && _command is Vda.Stringifiable) {
            return ((Vda.Stringifiable) _command).to_string ();
        }

        warning (_("ODBC doesn't support render SQL using parameters"));
        return _sql;
    }
    // PreparedQuery
    internal string name { get { return _name; } }
    internal Vda.SqlParameters parameters {
        get {
            if (_command != null && _command is Vda.SqlCommandParametrized) {
                return ((Vda.SqlCommandParametrized) _command).parameters;
            }

            return _params;
        }
    }

    internal override async Vda.Result?
    execute (GLib.Cancellable? cancellable) throws GLib.Error {
        Odbc.Return code = Odbc.Return.ERROR;
        if (cnc == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Database is invalid"));
        }

        unowned Odbc.Connection conn = ((Vodbc.Connection) cnc).get_connection ();

        if (conn == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("ODBC Connection is invalid"));
        }

        if (stm == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Invalid SQL: %s"), _sql);
        }

        code = Odbc.Return.ERROR;
        if (command == null) {
            debug ("Using native bind of parameters");
            debug ("Setting parameters' values");
            debug ("Parameters to set: %d", ((Gee.HashMap<string,Vda.SqlValue>) parameters).size);
            foreach (string k in ((Gee.HashMap<string,Vda.SqlValue>) parameters).keys) {
                var v = ((Gee.HashMap<string,Vda.SqlValue>) parameters).get (k);
                int n = int.parse (k);
                if (n == 0) {
                  throw new Vda.ParsedQueryError.PARAMETER_ERROR (_("Parameters should be named as numbers and should be greater than cero"));
                }
                debug ("Applying binding for parameter %s with value: %s", k, v.to_string ());
                if (v is Vda.SqlValueInteger) {
                    int vint = (int) v.to_gvalue ();
                    stm.bind_int_input_parameter (n, &vint, null);
                } else if (v is Vda.SqlValueString) {
                    uint8[] sint = ((string) v.to_gvalue ()).data;
                    stm.bind_string_input_parameter (n, sint, null);
                } else if (v is Vda.SqlValueDouble) {
                    double vd = (double) v.to_gvalue ();
                    stm.bind_double_input_parameter (n, &vd, null);
                } // FIXME: Add more conversions
            }
            code = stm.execute ();
        } else if (command is Vda.Stringifiable) {
          string sqlx = ((Vda.Stringifiable) command).to_string ();
          debug ("SQL rendered to execute: %s", sqlx);
          code = stm.execute_direct (sqlx.data);
        }

        if (code == Odbc.Return.ERROR) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Unable to execute query \"%s\". Parse error.").printf (_sql));
        }

        int16 cols = -1;
        stm.get_column_count (out cols);

        if (cols > 0) {
          return new Vodbc.TableModelSequential (this);
        }

        ssize_t rows = -1;
        stm.get_row_count (out rows);

        return new Vda.AffectedRows ((int) rows);
    }

    // ParsedQuery
    internal Vda.SqlCommand command {
        get {
            return _command;
        }
    }
}
