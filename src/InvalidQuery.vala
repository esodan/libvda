/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * InvalidQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Invalid {@link Query}
 */
public class Vda.InvalidQuery : GLib.Object, Vda.Query {
  private Connection _cnc = null;
  /**
   * A message about parsing the query
   *
   * May contain a descriptive error message
   */
  public string message { get; construct set; }
  public InvalidQuery (string msg) {
    message = msg;
  }
  internal string sql { owned get; }
  internal Vda.Connection connection {
    get {
      return _cnc;
    }
  }
  internal async Vda.Result? execute (GLib.Cancellable? cancellable) throws GLib.Error {
    throw new QueryError.INVALID_QUERY_ERROR (_("Query is invalid, you can't execute it"));
  }
  internal async void cancel () {
    warning (_("Query is invalid, you can't cancel it"));
  }
  internal string render_sql () throws GLib.Error { return "INVALID"; }
}
