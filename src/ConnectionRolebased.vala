/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * ConnectionRolebased.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2020 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represents a database engine connection, with transactions capabilities
 */
public interface Vda.ConnectionRolebased : GLib.Object {
  /**
   * Get a {@link Role} with data about current user or NULL if no
   * role information exists for the current connection.
   */
  public abstract Role? current_role ();
  /**
   * Creates a new {@link Role} using given name and parameters
   */
  public abstract Role? create_role (string name, Vda.Parameters @params) throws GLib.Error;
}

