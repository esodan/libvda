/*
 * TableModelSequential.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2020 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Move direction
 */
public enum Vda.MoveDirection {
    FORWARD,
    BACKWARD,
    RELATIVE
}
/**
 * A {@link Vda.Result} from a select query representing a table with
 * sequential access
 */
public interface Vda.TableModelSequential : GLib.Object, Vda.Result {
    /**
     * Gets current row in the sequence. Returned row
     * alsways points to current row after call {@link next}
     *
     * Returns: null if no current row exists
     */
    public abstract RowModel? current ();
    /**
     * Navigate to the next row data
     */
    public abstract bool next () throws GLib.Error;
    /**
     * Navigate to the previous row data of the current one
     */
    public virtual bool back () throws GLib.Error { return false; }
    /**
     * Navigate to the row data relative to the current row,
     * in forward or backward direction
     */
    public virtual bool
    move (Vda.MoveDirection direction, uint relative) throws GLib.Error {
        uint i = 0;
        while (i != relative) {
            switch (direction) {
                case Vda.MoveDirection.FORWARD:
                    if (!next ()) {
                        return false;
                    }
                    break;
                case Vda.MoveDirection.BACKWARD:
                    if (!back ()) {
                        return false;
                    }
                    break;
                default:
                    break;
            }
            i++;
        }
        return false;
    }
    /**
     * Creates a copy of the current row in the sequence. This object
     * doesn't updates when current row changes.
     */
    public virtual RowModel? copy_current () {
        return null;
    }
}
