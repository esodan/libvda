/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GSqlValueMathExp.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an SQL value as an mathematic expression
 */
public class Vda.ValueMathExp : Object,
                                    Stringifiable,
                                    SqlValue,
                                    SqlValueMathExp
{
	GCalc.MathEquationManager _expm;

	internal SqlParameters parameters { get; set; }

	internal GCalc.MathEquationManager math {
		get {
			return _expm;
		}
		set {
			_expm = value;
		}
	}

	internal string name { get { return "math"; } }

	construct {
		_expm = new GCalc.EquationManager ();
	}

	internal bool parse (string str) {
    var mparser = new GCalc.Parser ();
    try {
    	mparser.parse (str, _expm);
    	return true;
    } catch (GLib.Error e) {
    	warning (_("Can't parse math expression string: '%s'"), str);
    }
    return false;
	}
	internal bool from_value (GLib.Value val) {
		if (val.holds (typeof (string))) {
			return parse ((string) val);
		}
		return false;
	}
	internal SqlValue? cast (Type type) {
		SqlValue v = null;
		if (type != typeof (SqlValue)) {
			return null;
		}
		if (type.is_a (typeof (SqlValueString))) {
			string str = "";
			if (_expm.equations.get_item (0) != null) {
				str = ((GCalc.Expression) _expm.equations.get_item (0)).to_string ();
			}
			v = new ValueString ();
			v.from_value (str);
		}
		return v;
	}
	internal GLib.Value to_gvalue () {
		// just box itself and return
		GLib.Value v = GLib.Value (typeof (Object));
		v.set_object (this);
		return v;
	}
	internal string to_string () {
		message ("STRING FROM MATH VALUE");
		string str = "";
		var eq = _expm.equations.get_item (0) as GCalc.MathEquation;
		if (eq != null) {
			if (parameters != null) {
				try {
					foreach (GCalc.MathExpression e in eq.variables.values) {
						var v = e as GCalc.Variable;
						if (v == null) {
							continue;
						}
						if (v is GCalc.MathParameter) {
							var val = parameters.get_value (v.name);
							((GCalc.MathParameter) v).set_value (val);
						}
					}
				} catch (GLib.Error e) {
					warning ("Error updating parameters for math expression: %s", e.message);
				}
			}
			str = eq.to_string ();
		}
		return str;
	}
	internal string to_string_quoted () {
		string str = to_string ();
		return "'"+str+"'";
	}
	internal string to_sql_expression () {
		string str = to_string ();
		message ("STRIN: %s", str);
		return str;
	}
	internal bool is_compatible (Type type) {
		return false;
	}
}

