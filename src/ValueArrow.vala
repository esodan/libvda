/*
 * ValueArrow.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2024 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * An implementation of {@link Vda.SqlValue}
 */
[Version (since="1.2")]
public abstract class Vda.ValueArrow  : Object, Stringifiable, SqlValue
{
    /**
     * The value's type's name
     */
    protected string _name;
    /**
     * Internal {@link GArrow.Array} with the current value
     */
    protected GArrow.Array _val = new GArrow.NullArray (1);
    /**
     * Used to hold the source of data, when it comes from a {@link Vda.Query}
     * executed over a ADBC connection
     */
    protected Vda.Query _query;

    // Vda.Value interface
    internal string name { get { return _name; } }
    /**
     * Set internal {@link GLib.Value} {@link _val} to the given
     * value, reseting the current value's type to. Use with caution,
     * continue reading.
     *
     * This should be used only on new value implementations. Forcing
     * to a type incompatible with the current one, can produce
     * crashes or un-predictable behavior. Is better if you use
     * {@link SqlValue.cast} to convert the current to value to another.
     */
    public virtual bool force_value (GLib.Value val) {
      try {
        if (_query != null) {
          return false;
        }
        _val = Vda.ValueArrow.array_from_gvalue (val);
      } catch (GLib.Error e) {
        warning (_("Error while creating a string for Arrow Value: %s"), e.message);
        return false;
      }
      return true;
    }
    internal virtual bool from_value (GLib.Value val) {
        try {
          _val = Vda.ValueArrow.array_from_gvalue (val);
        } catch (GLib.Error e) {
          warning (_("Error while creating a string for Arrow Value: %s"), e.message);
          return false;
        }
        return true;
    }
    internal virtual SqlValue? cast (Type type) {
        // FIXME: Switch to create GArrow.Array from a GLib type
        return new Vda.ValueArrowNull ();
    }
    internal bool type_compatible (Type type) {
        // FIXME: Try to create a GArrow.Array from given GLib type
        return false;
    }

    internal virtual bool is_compatible (Type type) {
        return type_compatible (type);
    }
    internal virtual bool parse (string str) { return false; }
    internal virtual GLib.Value to_gvalue () {
        var nv = GLib.Value (GLib.Type.OBJECT);
        nv.set_object (this);
        return nv;
    }
    internal virtual string to_string () {
        try {
          return _val.to_string ();
        } catch (GLib.Error e) {
          warning (_("Error while creating a string for Arrow Value: %s"), e.message);
        }
        return "";
    }
    internal virtual string to_string_quoted () {
        string str = to_string ();
        return "'"+str+"'";
    }
    internal virtual string to_sql_expression () {
        return to_string_quoted ();
    }
    // ValueArrow specific API
    /**
     * Provides the internal {@link GArrow.Array}, but is only readonlly
     */
    public unowned GArrow.Array array { get { return _val; } }
    /**
     * Create a new {@link GArrow.ArrayBuilder} from the given {@link GLib.Type}
     * without a value.
     *
     * If the type is a {@link GLib.Type.OBJECT} this method return a {@link GArrow.ArrayBuilder}
     * and is the user responsibility to convert the object to a suitable {@link GArrow.Array} before
     * append it the the builder.
     */
    public static GArrow.ArrayBuilder array_from_gtype (GLib.Type type) throws GLib.Error {
      GArrow.ArrayBuilder ret = null;
      switch (type) {
        case GLib.Type.INVALID:
        case GLib.Type.NONE:
        case GLib.Type.INTERFACE:
          break;
        case GLib.Type.CHAR:
          ret = new GArrow.Int8ArrayBuilder ();
          break;
        case GLib.Type.UCHAR:
          ret = new GArrow.UInt8ArrayBuilder ();
          break;
        case GLib.Type.BOOLEAN:
          ret = new GArrow.BooleanArrayBuilder ();
          break;
        case GLib.Type.INT:
          ret = new GArrow.IntArrayBuilder ();
          break;
        case GLib.Type.UINT:
          ret = new GArrow.UIntArrayBuilder ();
          break;
        case GLib.Type.LONG:
          ret = new GArrow.Int32ArrayBuilder ();
          break;
        case GLib.Type.ULONG:
          ret = new GArrow.UInt32ArrayBuilder ();
          break;
        case GLib.Type.INT64:
          ret = new GArrow.Int64ArrayBuilder ();
          break;
        case GLib.Type.UINT64:
          ret = new GArrow.UInt64ArrayBuilder ();
          break;
        case GLib.Type.ENUM:
          ret = new GArrow.IntArrayBuilder ();
          break;
        case GLib.Type.FLAGS:
          break;
        case GLib.Type.FLOAT:
          ret = new GArrow.FloatArrayBuilder ();
          break;
        case GLib.Type.DOUBLE:
          ret = new GArrow.DoubleArrayBuilder ();
          break;
        case GLib.Type.STRING:
          ret = new GArrow.StringArrayBuilder ();
          break;
        case GLib.Type.POINTER:
        case GLib.Type.BOXED:
        case GLib.Type.PARAM:
          break;
        case GLib.Type.OBJECT:
          if (type.is_a (typeof (Vda.Stringifiable))) {
            return new GArrow.StringArrayBuilder ();
          } else if (type.is_a (typeof (GArrow.NullArrayBuilder))
                  || type.is_a (typeof (GArrow.NullArray))) {
            return new GArrow.NullArrayBuilder ();
          } if (type.is_a (typeof (GArrow.BooleanArrayBuilder))
                  || type.is_a (typeof (GArrow.BooleanArray))) {
            return new GArrow.BooleanArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Int8ArrayBuilder))
                  || type.is_a (typeof (GArrow.Int8Array))) {
            return new GArrow.Int8ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.UInt8ArrayBuilder))
                  || type.is_a (typeof (GArrow.UInt8Array))) {
            return new GArrow.UInt8ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Int16ArrayBuilder))
                  || type.is_a (typeof (GArrow.Int16Array))) {
            return new GArrow.Int16ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.UInt16ArrayBuilder))
                  || type.is_a (typeof (GArrow.UInt16Array))) {
            return new GArrow.UInt16ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Int32ArrayBuilder))
                  || type.is_a (typeof (GArrow.Int32Array))) {
            return new GArrow.Int32ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.UInt32ArrayBuilder))
                  || type.is_a (typeof (GArrow.UInt32Array))) {
            return new GArrow.UInt32ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Int64ArrayBuilder))
                  || type.is_a (typeof (GArrow.Int64Array))) {
            return new GArrow.Int64ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.UInt64ArrayBuilder))
                  || type.is_a (typeof (GArrow.UInt64Array))) {
            return new GArrow.UInt64ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.HalfFloatArrayBuilder))
                  || type.is_a (typeof (GArrow.HalfFloatArray))) {
            return new GArrow.HalfFloatArrayBuilder ();
          } if (type.is_a (typeof (GArrow.FloatArrayBuilder))
                  || type.is_a (typeof (GArrow.FloatArray))) {
            return new GArrow.FloatArrayBuilder ();
          } if (type.is_a (typeof (GArrow.DoubleArrayBuilder))
                  || type.is_a (typeof (GArrow.DoubleArray))) {
            return new GArrow.DoubleArrayBuilder ();
          } if (type.is_a (typeof (GArrow.BinaryArrayBuilder))
                  || type.is_a (typeof (GArrow.BinaryArray))) {
            return new GArrow.BinaryArrayBuilder ();
          } if (type.is_a (typeof (GArrow.LargeBinaryArrayBuilder))
                  || type.is_a (typeof (GArrow.LargeBinaryArray))) {
            return new GArrow.LargeBinaryArrayBuilder ();
          } if (type.is_a (typeof (GArrow.StringArrayBuilder))
                  || type.is_a (typeof (GArrow.StringArray))) {
            return new GArrow.StringArrayBuilder ();
          } if (type.is_a (typeof (GArrow.LargeStringArrayBuilder))
                  || type.is_a (typeof (GArrow.LargeStringArray))) {
            return new GArrow.LargeStringArrayBuilder ();
          } if (type.is_a (typeof (GArrow.FixedSizeBinaryArrayBuilder))
                  || type.is_a (typeof (GArrow.FixedSizeBinaryArray))) {
            // FIXME: return new GArrow.FixedSizeBinaryArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Date32ArrayBuilder))
                  || type.is_a (typeof (GArrow.Date32Array))) {
            return new GArrow.Date32ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Date64ArrayBuilder))
                  || type.is_a (typeof (GArrow.Date64Array))) {
            return new GArrow.Date64ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.TimestampArrayBuilder))
                  || type.is_a (typeof (GArrow.TimestampArray))) {
            // FIXME: return new GArrow.TimestampArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Time32ArrayBuilder))
                  || type.is_a (typeof (GArrow.Time32Array))) {
            // FIXME: return new GArrow.Time32ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Time64ArrayBuilder))
                  || type.is_a (typeof (GArrow.Time64Array))) {
            // FIXME: return new GArrow.Time64ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.MonthIntervalArrayBuilder))
                  || type.is_a (typeof (GArrow.MonthIntervalArray))) {
            return new GArrow.MonthIntervalArrayBuilder ();
          } if (type.is_a (typeof (GArrow.DayTimeIntervalArrayBuilder))
                  || type.is_a (typeof (GArrow.DayTimeIntervalArray))) {
            return new GArrow.DayTimeIntervalArrayBuilder ();
          } if (type.is_a (typeof (GArrow.MonthDayNanoIntervalArrayBuilder))
                  || type.is_a (typeof (GArrow.MonthDayNanoIntervalArray))) {
            return new GArrow.MonthDayNanoIntervalArrayBuilder ();
          } if (type.is_a (typeof (GArrow.BinaryDictionaryArrayBuilder))) {
            return new GArrow.BinaryDictionaryArrayBuilder ();
          } if (type.is_a (typeof (GArrow.StringDictionaryArrayBuilder))) {
            return new GArrow.StringDictionaryArrayBuilder ();
          } if (type.is_a (typeof (GArrow.ListArrayBuilder))
                  || type.is_a (typeof (GArrow.ListArray))) {
            // FIXME: return new GArrow.ListArrayBuilder ();
          } if (type.is_a (typeof (GArrow.StructArrayBuilder))
                  || type.is_a (typeof (GArrow.StructArray))) {
            // FIXME: return new GArrow.StructArrayBuilder ();
          } if (type.is_a (typeof (GArrow.MapArrayBuilder))
                  || type.is_a (typeof (GArrow.MapArray))) {
            // FIXME: return new GArrow.MapArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Decimal128ArrayBuilder))
                  || type.is_a (typeof (GArrow.Decimal128Array))) {
            // FIXME: return new GArrow.Decimal128ArrayBuilder ();
          } if (type.is_a (typeof (GArrow.Decimal256ArrayBuilder))
                  || type.is_a (typeof (GArrow.Decimal256Array))) {
            // FIXME: return new GArrow.Decimal256ArrayBuilder (GArrow.Decimal256DataType.);
          } if (type.is_a (typeof (GArrow.DenseUnionArrayBuilder))
                  || type.is_a (typeof (GArrow.DenseUnionArray))) {
            // FIXME: return new GArrow.DenseUnionArrayBuilder (null);
          } if (type.is_a (typeof (GArrow.SparseUnionArrayBuilder))
                  || type.is_a (typeof (GArrow.SparseUnionArray))) {
            // FIXME: return new GArrow.SparseUnionArrayBuilder (null);
          }
          break;
        case GLib.Type.VARIANT:
          // FIXME: A GLib.Variant can be converted to an GArrow.Array in a complex structure
          break;
      }

      return ret;
    }


    /**
     * Create a new {@link GArrow.Array} from the given {@link GLib.Value}
     * without a value
     */
    public static GArrow.Array? array_from_gvalue (GLib.Value val) throws GLib.Error {
      GLib.Type type = val.type ();
      GArrow.ArrayBuilder builder = array_from_gtype (type);
      if (builder == null) {
        return null;
      }
      switch (type) {
        case GLib.Type.INVALID:
        case GLib.Type.NONE:
        case GLib.Type.INTERFACE:
          break;
        case GLib.Type.CHAR:
          ((GArrow.Int8ArrayBuilder) builder).append_value ((int8) ((char) val));
          break;
        case GLib.Type.UCHAR:
          ((GArrow.UInt8ArrayBuilder) builder).append_value ((uchar) val);
          break;
        case GLib.Type.BOOLEAN:
          ((GArrow.BooleanArrayBuilder) builder).append_value ((bool) val);
          break;
        case GLib.Type.INT:
          ((GArrow.IntArrayBuilder) builder).append_value ((int) val);
          break;
        case GLib.Type.UINT:
          ((GArrow.UInt8ArrayBuilder) builder).append_value ((uint8) ((uint) val));
          break;
        case GLib.Type.LONG:
          ((GArrow.Int32ArrayBuilder) builder).append_value ((int32) ((long) val));
          break;
        case GLib.Type.ULONG:
          ((GArrow.UInt32ArrayBuilder) builder).append_value ((uint32) ((ulong) val));
          break;
        case GLib.Type.INT64:
          ((GArrow.Int64ArrayBuilder) builder).append_value ((int64) val);
          break;
        case GLib.Type.UINT64:
          ((GArrow.UInt64ArrayBuilder) builder).append_value ((uint64) val);
          break;
        case GLib.Type.ENUM:
          ((GArrow.IntArrayBuilder) builder).append_value ((int) val.get_enum ());
          break;
        case GLib.Type.FLAGS:
          break;
        case GLib.Type.FLOAT:
          ((GArrow.FloatArrayBuilder) builder).append_value ((float) val);
          break;
        case GLib.Type.DOUBLE:
          ((GArrow.DoubleArrayBuilder) builder).append_value ((double) val);
          break;
        case GLib.Type.STRING:
          ((GArrow.StringArrayBuilder) builder).append_value (((string) val).data);
          break;
        case GLib.Type.POINTER:
        case GLib.Type.BOXED:
        case GLib.Type.PARAM:
          break;
        case GLib.Type.OBJECT:
          GLib.Object o = val.get_object ();
          return array_from_object (o);
        case GLib.Type.VARIANT:
          // FIXME: A GLib.Variant can be converted to an GArrow.Array in a complex structure
          break;
      }

      return null;
    }

    /**
     * Create a new {@link GArrow.Array} from the given {@link GLib.Object}
     * if the object is a {@link GArrow.Array} this method append it to the
     * array; but if is a {@link Vda.ValueArrow}
     *
     * Returns: a new {@link GArrow.Array} if the object could be used to
     * create one; a {@link GArrow.NullArray} if not.
     */
    public static GArrow.Array array_from_object (GLib.Object val) throws GLib.Error {
      if (val is GArrow.Array) {
        return (GArrow.Array) val;
      }
      if (val is Vda.ValueArrow) {
        // FIXME:consider to use GArrow.Array.export()
      }
      if (val is Vda.Value) {
        var v = ((Vda.Value) val).to_gvalue ();
        return array_from_gvalue (v);
      }
      if (val is Vda.Stringifiable) {
        var b = new GArrow.StringArrayBuilder ();
        b.append_value (((Vda.Stringifiable) val).to_string ().data);
        return b.finish ();
      }

      var bn = new GArrow.NullArrayBuilder ();
      return bn.finish ();
    }


    /**
     * Calculates a {@link GLib.Type} from a {@link GArrow.DataType}
     * this is just a reference, because GLib types are fundamental
     * while Arrow uses array of objects of the same type
     */
    public static GLib.Type data_type_to_gtype (GArrow.DataType type) {
      if (type is GArrow.NullDataType) {
        return GLib.Type.NONE;
      } else if (type is GArrow.BooleanDataType) {
        return GLib.Type.BOOLEAN;
      } else if (type is GArrow.Int8DataType) {
        return GLib.Type.INT;
      } else if (type is GArrow.Int16DataType) {
        return GLib.Type.INT;
      } else if (type is GArrow.UInt8DataType) {
        return GLib.Type.UINT;
      } else if (type is GArrow.UInt16DataType) {
        return GLib.Type.UINT;
      } else if (type is GArrow.Int32DataType) {
        return GLib.Type.LONG;
      } else if (type is GArrow.UInt32DataType) {
        return GLib.Type.ULONG;
      } else if (type is GArrow.Int64DataType) {
        return GLib.Type.INT64;
      } else if (type is GArrow.UInt64DataType) {
        return GLib.Type.UINT64;
      } else if (type is GArrow.HalfFloatDataType) {
        return GLib.Type.FLOAT;
      } else if (type is GArrow.FloatDataType) {
        return GLib.Type.FLOAT;
      } else if (type is GArrow.DoubleDataType) {
        return GLib.Type.DOUBLE;
      } else if (type is GArrow.BinaryDataType) {
      } else if (type is GArrow.LargeBinaryDataType) {
      } else if (type is GArrow.FixedSizeBinaryDataType) {
      } else if (type is GArrow.StringDataType) {
        return GLib.Type.STRING;
      } else if (type is GArrow.LargeStringDataType) {
        return GLib.Type.STRING;
      } else if (type is GArrow.Date32DataType) {
      } else if (type is GArrow.Date64DataType) {
      } else if (type is GArrow.TimestampDataType) {
      } else if (type is GArrow.Time32DataType) {
      } else if (type is GArrow.Time64DataType) {
      } else if (type is GArrow.MonthIntervalDataType) {
      } else if (type is GArrow.DayTimeIntervalDataType) {
      } else if (type is GArrow.MonthDayNanoIntervalDataType) {
      } else if (type is GArrow.Decimal128DataType) {
      } else if (type is GArrow.Decimal256DataType) {
      } else if (type is GArrow.ExtensionDataType) {
      }

      return GLib.Type.NONE;
    }

    /**
     * Calculates a {@link GLib.Type} from a {@link GArrow.Type}
     * this is just a reference, because GLib types are fundamental
     * while Arrow uses array of objects of the same type
     */
    public static GLib.Type type_to_gtype (GArrow.Type type) {
      switch (type) {
        case GArrow.Type.NA:
          return GLib.Type.INVALID;
        case GArrow.Type.BOOLEAN:
          return GLib.Type.BOOLEAN;
        case GArrow.Type.INT8:
        case GArrow.Type.INT16:
          return GLib.Type.INT;
        case GArrow.Type.UINT8:
        case GArrow.Type.UINT16:
          return GLib.Type.UINT;
        case GArrow.Type.INT32:
          return GLib.Type.LONG;
        case GArrow.Type.UINT32:
          return GLib.Type.ULONG;
        case GArrow.Type.UINT64:
          return GLib.Type.UINT64;
        case GArrow.Type.INT64:
          return GLib.Type.INT64;
        case GArrow.Type.HALF_FLOAT:
          return GLib.Type.FLOAT;
        case GArrow.Type.FLOAT:
          return GLib.Type.FLOAT;
        case GArrow.Type.DOUBLE:
          return GLib.Type.DOUBLE;
        case GArrow.Type.STRING:
          return GLib.Type.STRING;
        case GArrow.Type.BINARY:
        case GArrow.Type.FIXED_SIZE_BINARY:
          break;
        case GArrow.Type.DATE32:
        case GArrow.Type.DATE64:
        case GArrow.Type.TIMESTAMP:
        case GArrow.Type.TIME32:
        case GArrow.Type.TIME64:
        case GArrow.Type.MONTH_INTERVAL:
        case GArrow.Type.DAY_TIME_INTERVAL:
        case GArrow.Type.DECIMAL128:
        case GArrow.Type.DECIMAL256:
        case GArrow.Type.LIST:
        case GArrow.Type.STRUCT:
        case GArrow.Type.SPARSE_UNION:
        case GArrow.Type.DENSE_UNION:
        case GArrow.Type.DICTIONARY:
        case GArrow.Type.MAP:
        case GArrow.Type.EXTENSION:
        case GArrow.Type.FIXED_SIZE_LIST:
        case GArrow.Type.DURATION:
        case GArrow.Type.LARGE_STRING:
        case GArrow.Type.LARGE_BINARY:
        case GArrow.Type.LARGE_LIST:
        case GArrow.Type.MONTH_DAY_NANO_INTERVAL:
        case GArrow.Type.RUN_END_ENCODED:
          break;
      }

      return GLib.Type.INVALID;
    }
}

/**
 * An implementation of {@link SqlValueNull}
 */
[Version (since="1.2")]
public class Vda.ValueArrowNull : ValueArrow, SqlValueNull {
    construct {
        _name = "NULL";
        _val = new GArrow.NullArray (1);
    }

    /**
     * Construct a new {@link Vda.ValueArrowNull} from a given {@link GArrow.NullArray}
     * checkout {@link ValueArrowNull.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowNull.from_array (GArrow.NullArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowNull} from a given {@link GArrow.NullArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.NullArray}. If you want to use
     * a value without a query source use {@link ValueArrowNull.from_array} instead.
     */
    public ValueArrowNull.from_query (Vda.Query query, GArrow.NullArray val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        if (str.down () == "null") {
            return true;
        }
        return false;
    }
    internal override string to_string () {
        return "NULL";
    }
    internal override string to_sql_expression () {
        return "NULL";
    }
    internal override bool is_compatible (Type type) {
        return false;
    }
}

/**
 * An implementation of {@link Vda.SqlValueBlob}.
 *
 * Data is not taked.
 */
[Version (since="1.2")]
public class Vda.ValueArrowBinary : Vda.ValueArrow, Vda.SqlValueBinary {
    /**
     * Construct a new {@link Vda.ValueArrowBinary} from a given {@link GArrow.BinaryArray}
     * checkout {@link ValueArrowBinary.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowBinary.from_array (GArrow.BinaryArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowBinary} from a given {@link GArrow.BinaryArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.BinaryArray}. If you want to use
     * a value without a query source use {@link ValueArrowBinary.from_array} instead.
     */
    public ValueArrowBinary.from_query (Vda.Query query, GArrow.BinaryArray val) {
      _val = val;
      _query = query;
    }
    internal uint size {
        get {
            if (_val != null) {
                return (uint) _val.get_length ();
            }

            return 0;
        }
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.BinaryArrayBuilder ();
          b.append_value (str.data);
          _val = b.finish ();
        } catch (GLib.Error e) {
          GLib.warning (_("Error parsing Binary array: %s"), e.message);
          return false;
        }
        return true;
    }
    internal GLib.Bytes get_bytes () {
      return ((GArrow.BinaryArray) _val).get_value (0);
    }
}


/**
 * An implementation of {@link SqlValueBit}
 */
[Version (since="1.2")]
public class Vda.ValueArrowBit : ValueArrowBinary, SqlValueBit {
    construct {
        _name = "bit";
    }

    /**
     * Construct a new {@link Vda.ValueArrowBit} from a given {@link GArrow.Array}
     * checkout {@link ValueArrowBit.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowBit.from_array (GArrow.BinaryArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowBit} from a given {@link GArrow.BinaryArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.BinaryArray}. If you want to use
     * a value without a query source use {@link ValueArrowBit.from_array} instead.
     */
    public ValueArrowBit.from_query (Vda.Query query, GArrow.BinaryArray val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        string nstr = str.down ();
        try {
          var b = new GArrow.BinaryArrayBuilder ();
          if (nstr == "1") {
              b.append_value ({1});
              _val = b.finish ();
          } else {
              b.append_value ({0});
              _val = b.finish ();
          }
        } catch (GLib.Error e) {
            warning (_("Error while parsing Bit array: %s"), e.message);
        }
        return true;
    }
}

/**
 * An implementation of {@link SqlValueString}
 */
[Version (since="1.2")]
public class Vda.ValueArrowString : ValueArrowBinary, SqlValueString {
    construct {
        _name = "string";
    }

    /**
     * Construct a new {@link Vda.ValueArrowString} from a given {@link GArrow.StringArray}
     * checkout {@link ValueArrowString.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowString.from_array (GArrow.StringArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowString} from a given {@link GArrow.StringArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.StringArray}. If you want to use
     * a value without a query source use {@link ValueArrowString.from_array} instead.
     */
    public ValueArrowString.from_query (Vda.Query query, GArrow.StringArray val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
      try {
        var b = new GArrow.StringArrayBuilder ();
        b.append_value (str.data);
        _val = b.finish ();
      } catch (GLib.Error e) {
        warning (_("Error while parsing string for string array: %s"), e.message);
        return false;
      }
      return true;
    }
    internal override string to_string () {
        return ((GArrow.StringArray) _val).get_string (0);
    }
    internal override string to_string_quoted () {
        if (_val is GArrow.NullArray) {
            return "\'NULL\'";
        }
        string str = ((GArrow.StringArray) _val).get_string (0);
        str.escape (null);
        str = str.replace ("'", "''");
        return "'"+str+"'";
    }
    internal override bool is_compatible (Type type) {
        if (type_compatible (type)) {
            return true;
        }
        if (type.is_a (typeof (SqlValue))) {
            return true;
        }
        return false;
    }
}

/**
 * An implementation of {@link SqlValueXml}
 */
[Version (since="1.2")]
public class Vda.ValueArrowXml : ValueArrowString, SqlValueXml {
    GXml.DomDocument _document = null;

    /**
     * Construct a new {@link Vda.ValueArrowXml} from a given {@link GArrow.StringArray}
     * checkout {@link ValueArrowXml.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowXml.from_array (GArrow.StringArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowXml} from a given {@link GArrow.StringArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.StringArray}. If you want to use
     * a value without a query source use {@link ValueArrowXml.from_array} instead.
     */
    public ValueArrowXml.from_query (Vda.Query query, GArrow.StringArray val) {
      _val = val;
      _query = query;
    }
    internal GXml.DomDocument document {
        get {
            if (_document == null) {
                _document = new GXml.Document () as GXml.DomDocument;
                try {
                    _document.read_from_string (((GArrow.StringArray) _val).get_string (0));
                } catch (GLib.Error e) {
                    warning (_("Error parsing XML string value: %s"), e.message);
                }
            }
            return _document;
        }
    }

    construct {
        _name = "xml";
    }

    internal override bool parse (string str) {
        try {
          var b = new GArrow.StringArrayBuilder ();
          b.append_value (str.data);
          _val = b.finish ();
          if (_document != null) {
            _document.read_from_string (str);
          }
        } catch (GLib.Error e) {
            warning (_("Error parsing XML string: %s"), e.message);
        }
        return true;
    }
    internal override string to_string () {
        if (_document != null) {
            try {
                return _document.write_string ();
            } catch (GLib.Error e) {
                warning (_("Error exporting XML string: %s"), e.message);
            }
        }
        return  ((GArrow.StringArray) _val).get_string (0);
    }
}

/**
 * An implementation of {@link SqlValueJson}
 */
[Version (since="1.2")]
public class Vda.ValueArrowJson : ValueArrowString, SqlValueJson {
    Json.Node _document = null;

    /**
     * Construct a new {@link Vda.ValueArrowJson} from a given {@link GArrow.StringArray}
     * checkout {@link ValueArrowJson.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowJson.from_array (GArrow.StringArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowJson} from a given {@link GArrow.StringArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.StringArray}. If you want to use
     * a value without a query source use {@link ValueArrowJson.from_array} instead.
     */
    public ValueArrowJson.from_query (Vda.Query query, GArrow.StringArray val) {
      _val = val;
      _query = query;
    }
    internal Json.Node document {
        get {
            if (_document == null) {
                try {
                    string s = ((GArrow.StringArray) _val).get_string (0);
                    _document = Json.from_string (s);
                } catch (GLib.Error e) {
                    warning (_("Error parsing JSON string value: %s"), e.message);
                }
            }
            return _document;
        }
    }

    construct {
        _name = "json";
    }

    internal override bool parse (string str) {
        try {
          var b = new GArrow.StringArrayBuilder ();
          b.append_value (str.data);
          _val = b.finish ();
          if (_document != null) {
                  _document = Json.from_string (str);
          }
        } catch (GLib.Error e) {
            warning (_("Error parsing JSON Array value string: %s"), e.message);
            return false;
        }
        return true;
    }
    internal override string to_string () {
        try {
          if (_document != null) {
            return Json.to_string (_document, false);
          }
          return _val.to_string ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing string for "), e.message);
        }
        return "";
    }
}

/**
 * An implementation of {@link SqlValueText}
 */
[Version (since="1.2")]
public class Vda.ValueArrowText : ValueArrowString, SqlValueText {
    construct {
        _name = "text";
    }

    /**
     * Construct a new {@link Vda.ValueArrowText} from a given {@link GArrow.StringArray}
     * checkout {@link ValueArrowText.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowText.from_array (GArrow.StringArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowText} from a given {@link GArrow.StringArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.StringArray}. If you want to use
     * a value without a query source use {@link ValueArrowText.from_array} instead.
     */
    public ValueArrowText.from_query (Vda.Query query, GArrow.StringArray val) {
      _val = val;
      _query = query;
    }
}

/**
 * An implementation of {@link SqlValueName}
 */
[Version (since="1.2")]
public class Vda.ValueArrowName : ValueArrowString, SqlValueName {
    construct {
        _name = "name";
    }
}

/**
 * An implementation of {@link SqlValueBool}
 */
[Version (since="1.2")]
public class Vda.ValueArrowBoolean : ValueArrow, SqlValueBool {
    construct {
        _name = "bool";
    }

    /**
     * Construct a new {@link Vda.ValueArrowBoolean} from a given {@link GArrow.BooleanArray}
     * checkout {@link ValueArrowBoolean.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowBoolean.from_array (GArrow.BooleanArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowBoolean} from a given {@link GArrow.BooleanArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.BooleanArray}. If you want to use
     * a value without a query source use {@link ValueArrowBoolean.from_array} instead.
     */
    public ValueArrowBoolean.from_query (Vda.Query query, GArrow.BooleanArray val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        string nstr = str.down ();
        try {
          var b = new GArrow.BooleanArrayBuilder ();
          if (nstr == "true") {
              b.append_value (true);
              _val = b.finish ();
          } else {
              b.append_value (false);
              _val = b.finish ();
          }
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow boolean Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
    internal override string to_string () {
        try {
          return ((GArrow.BooleanArray) _val).to_string ();
        } catch (GLib.Error e) {
          GLib.warning (_("Error converting boolean to string: %s"), e.message);
        }
        return "FALSE";
    }
    internal override string to_sql_expression () {
        return to_string ();
    }
}

/**
 * An implementation of {@link SqlValueNumeric}
 *
 * If you set a value using this object's methods
 * will override the current data type. For example,
 * using {@link Vda.ValueArrowNumeric.set_double}
 * will set internal value to a {@link GArrow.DoubleArray}
 */
[Version (since="1.2")]
public abstract class Vda.ValueArrowNumeric : Vda.ValueArrow, SqlValueNumeric {
    protected int precision = 6;
    construct {
        _name = "numeric";
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.DoubleArrayBuilder ();
          int i = (int) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow double Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
    internal void set_precision (int p) { precision = p; }
    internal int get_precision () { return precision; }
    internal virtual string format (string str) {
        try {
          return str.printf (_val.to_string ());
        } catch (GLib.Error e) {
          GLib.warning (_("Error formatting Numeric array string: %s"), e.message);
        }
        return "";
    }
    internal double get_double () {
        if (_val is GArrow.DoubleArray) {
          return ((GArrow.DoubleArray) _val).get_value (0);
        }
        if (_val is GArrow.FloatArray) {
          return (double) ((GArrow.FloatArray) _val).get_value (0);
        }
        if (_val is GArrow.HalfFloatArray) {
          return (double) ((GArrow.HalfFloatArray) _val).get_value (0);
        }
        if (_val is GArrow.Date32Array) {
          return (double) ((GArrow.Date32Array) _val).get_value (0);
        }
        if (_val is GArrow.Date64Array) {
          return (double) ((GArrow.Date64Array) _val).get_value (0);
        }
        if (_val is GArrow.Time32Array) {
          return (double) ((GArrow.Time32Array) _val).get_value (0);
        }
        if (_val is GArrow.Time64Array) {
          return (double) ((GArrow.Time64Array) _val).get_value (0);
        }
        if (_val is GArrow.TimestampArray) {
          return (double) ((GArrow.TimestampArray) _val).get_value (0);
        }
        if (_val is GArrow.MonthIntervalArray) {
          return (double) ((GArrow.Time64Array) _val).get_value (0);
        }
        if (_val is GArrow.Int8Array) {
          return (double) ((GArrow.Int8Array) _val).get_value (0);
        }
        if (_val is GArrow.Int16Array) {
          return (double) ((GArrow.Int16Array) _val).get_value (0);
        }
        if (_val is GArrow.Int32Array) {
          return (double) ((GArrow.Int32Array) _val).get_value (0);
        }
        if (_val is GArrow.Int64Array) {
          return (double) ((GArrow.Int64Array) _val).get_value (0);
        }
        if (_val is GArrow.UInt8Array) {
          return (double) ((GArrow.UInt8Array) _val).get_value (0);
        }
        if (_val is GArrow.UInt16Array) {
          return (double) ((GArrow.UInt16Array) _val).get_value (0);
        }
        if (_val is GArrow.UInt32Array) {
          return (double) ((GArrow.UInt32Array) _val).get_value (0);
        }
        if (_val is GArrow.UInt64Array) {
          return (double) ((GArrow.UInt64Array) _val).get_value (0);
        }
        return 0.0;
    }
    internal double get_real () {
        return get_double ();
    }
    internal virtual double get_imaginary () {
        return 0.0;
    }
    internal void set_double (double v) {
        try {
          var b = new GArrow.DoubleArrayBuilder ();
          b.append_value (v);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow Numeric Array Value: %s"), e.message);
        }
    }
    internal void set_real (double r) {
        set_double (r);
    }
    internal virtual void set_imaginary (double img) {}
}

/**
 * An implementation of {@link SqlValueFloat}
 */
[Version (since="1.2")]
public class Vda.ValueArrowFloat : ValueArrowNumeric, SqlValueFloat {
    construct {
        _name = "float";
    }

    /**
     * Construct a new {@link Vda.ValueArrowFloat} from a given {@link GArrow.FloatArray}
     * checkout {@link ValueArrowFloat.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowFloat.from_array (GArrow.FloatArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowFloat} from a given {@link GArrow.FloatArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.FloatArray}. If you want to use
     * a value without a query source use {@link ValueArrowFloat.from_array} instead.
     */
    public ValueArrowFloat.from_query (Vda.Query query, GArrow.FloatArray val) {
      _val = val;
      _query = query;
    }
    internal float get_float () {
        return (float) get_double ();
    }
}

/**
 * An implementation of {@link SqlValueFloat}
 */
[Version (since="1.2")]
public class Vda.ValueArrowHalfFloat : ValueArrowNumeric, SqlValueFloat {
    construct {
        _name = "float";
    }

    /**
     * Construct a new {@link Vda.ValueArrowHalfFloat} from a given {@link GArrow.HalfFloatArray}
     * checkout {@link ValueArrowHalfFloat.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowHalfFloat.from_array (GArrow.HalfFloatArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowHalfFloat} from a given {@link GArrow.HalfFloatArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.HalfFloatArray}. If you want to use
     * a value without a query source use {@link ValueArrowHalfFloat.from_array} instead.
     */
    public ValueArrowHalfFloat.from_query (Vda.Query query, GArrow.HalfFloatArray val) {
      _val = val;
      _query = query;
    }
    internal float get_float () {
        return (float) get_double ();
    }
}

/**
 * An implementation of {@link SqlValueDouble}
 */
[Version (since="1.2")]
public class Vda.ValueArrowDouble : ValueArrowNumeric, SqlValueDouble {
    construct {
        _name = "double";
    }

    /**
     * Construct a new {@link Vda.ValueArrowDouble} from a given {@link GArrow.DoubleArray}
     * checkout {@link ValueArrowDouble.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowDouble.from_array (GArrow.NullArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowDouble} from a given {@link GArrow.DoubleArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.DoubleArray}. If you want to use
     * a value without a query source use {@link ValueArrowDouble.from_array} instead.
     */
    public ValueArrowDouble.from_query (Vda.Query query, GArrow.DoubleArray val) {
      _val = val;
      _query = query;
    }
}

/**
 * Complex implementation of {@link SqlValueNumeric}
 */
public class Vda.ValueArrowComplex : Vda.ValueArrowNumeric
{
    protected GArrow.Array _imaginary;

    construct {
        _name = "complex";
    }

    /**
     * Construct a new {@link Vda.ValueArrowComplex} from a given {@link GArrow.DoubleArray}
     * values; checkout {@link ValueArrowComplex.from_query} if you want to use a data source
     * from ADBC
     *
     * @param val value of the real part of the complex number
     * @param img value of the imaginary part of the complex number
     */
    public ValueArrowComplex.from_array (GArrow.DoubleArray val, GArrow.DoubleArray img) {
      _val = val;
      _imaginary = img;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowComplex} from a given {@link GArrow.DoubleArray}
     * values and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from {@link GArrow.DoubleArray} values. If you want to use
     * a value without a query source use {@link ValueArrowComplex.from_array} instead.
     *
     * @param val value of the real part of the complex
     * @param img value of the imaginary part of the complex
     */
    public ValueArrowComplex.from_query (Vda.Query query, GArrow.DoubleArray val, GArrow.DoubleArray img) {
      _val = val;
      _query = query;
    }

    internal override double get_imaginary () {
        if (_imaginary is GArrow.DoubleArray) {
          return ((GArrow.DoubleArray) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.FloatArray) {
          return (double) ((GArrow.FloatArray) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.HalfFloatArray) {
          return (double) ((GArrow.HalfFloatArray) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.Date32Array) {
          return (double) ((GArrow.Date32Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.Date64Array) {
          return (double) ((GArrow.Date64Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.Time32Array) {
          return (double) ((GArrow.Time32Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.Time64Array) {
          return (double) ((GArrow.Time64Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.TimestampArray) {
          return (double) ((GArrow.TimestampArray) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.MonthIntervalArray) {
          return (double) ((GArrow.Time64Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.Int8Array) {
          return (double) ((GArrow.Int8Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.Int16Array) {
          return (double) ((GArrow.Int16Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.Int32Array) {
          return (double) ((GArrow.Int32Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.Int64Array) {
          return (double) ((GArrow.Int64Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.UInt8Array) {
          return (double) ((GArrow.UInt8Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.UInt16Array) {
          return (double) ((GArrow.UInt16Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.UInt32Array) {
          return (double) ((GArrow.UInt32Array) _imaginary).get_value (0);
        }
        if (_imaginary is GArrow.UInt64Array) {
          return (double) ((GArrow.UInt64Array) _imaginary).get_value (0);
        }
        return 0.0;
    }
    internal override void set_imaginary (double img) {
        try {
          var b = new GArrow.DoubleArrayBuilder ();
          b.append_value (img);
          _imaginary = b.finish ();
        } catch (GLib.Error e) {
          warning (("Error while setting imaginary value to Complex array: %s"), e.message);
        }
    }
}


/**
 * An implementation of {@link SqlValueInteger}
 */
[Version (since="1.2")]
public class Vda.ValueArrowInt : ValueArrowNumeric, SqlValue, SqlValueInteger {
    construct {
        _name = "integer";
    }

    /**
     * Construct a new {@link Vda.ValueArrowInt} from a given {@link GArrow.Int16Array}
     * checkout {@link ValueArrowInt.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowInt.from_array (GArrow.Int16Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowInt} from a given {@link GArrow.Int16Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Int16Array}. If you want to use
     * a value without a query source use {@link ValueArrowInt.from_array} instead.
     */
    public ValueArrowInt.from_query (Vda.Query query, GArrow.Int16Array val) {
      _val = val;
      _query = query;
    }
}


/**
 * An implementation of {@link SqlValueByte}
 */
[Version (since="1.2")]
public class Vda.ValueArrowInt8 : ValueArrowInt, SqlValueByte  {
    construct {
        _name = "int8";
    }
    /**
     * Construct a new {@link Vda.ValueArrowInt8} from a given {@link GArrow.Int8Array}
     * checkout {@link ValueArrowInt8.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowInt8.from_array (GArrow.Int8Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowInt8} from a given {@link GArrow.Int8Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Int8Array}. If you want to use
     * a value without a query source use {@link ValueArrowInt8.from_array} instead.
     */
    public ValueArrowInt8.from_query (Vda.Query query, GArrow.Int8Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.Int8ArrayBuilder ();
          int8 i = (int8) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow Int8 Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
}

/**
 * An implementation of {@link SqlValueInt2}
 */
[Version (since="1.2")]
public class Vda.ValueArrowInt16 : ValueArrowInt, SqlValueInt2  {
    construct {
        _name = "int16";
    }
    /**
     * Construct a new {@link Vda.ValueArrowInt16} from a given {@link GArrow.Int16Array}
     * checkout {@link ValueArrowInt16.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowInt16.from_array (GArrow.Int16Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowInt16} from a given {@link GArrow.Int16Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Int16Array}. If you want to use
     * a value without a query source use {@link ValueArrowInt16.from_array} instead.
     */
    public ValueArrowInt16.from_query (Vda.Query query, GArrow.Int16Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.Int16ArrayBuilder ();
          int16 i = (int16) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow int16 Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
}

/**
 * An implementation of {@link SqlValueInt4}
 */
[Version (since="1.2")]
public class Vda.ValueArrowInt32 : ValueArrowInt, SqlValueInt4 {
    construct {
        _name = "int32";
    }
    /**
     * Construct a new {@link Vda.ValueArrowInt32} from a given {@link GArrow.Int32Array}
     * checkout {@link ValueArrowInt32.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowInt32.from_array (GArrow.Int8Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowInt32} from a given {@link GArrow.Int32Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Int32Array}. If you want to use
     * a value without a query source use {@link ValueArrowInt32.from_array} instead.
     */
    public ValueArrowInt32.from_query (Vda.Query query, GArrow.Int32Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.Int32ArrayBuilder ();
          int32 i = (int32) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow int32 Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
}

/**
 * An implementation of {@link SqlValueInt8}
 */
[Version (since="1.2")]
public class Vda.ValueArrowInt64 : ValueArrowInt, SqlValueInt8 {
    construct {
        _name = "int8";
    }
    /**
     * Construct a new {@link Vda.ValueArrowInt64} from a given {@link GArrow.Int64Array}
     * checkout {@link ValueArrowInt64.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowInt64.from_array (GArrow.Int64Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowInt64} from a given {@link GArrow.Int64Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Int64Array}. If you want to use
     * a value without a query source use {@link ValueArrowInt64.from_array} instead.
     */
    public ValueArrowInt64.from_query (Vda.Query query, GArrow.Int64Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.Int64ArrayBuilder ();
          int64 i = (int64) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow int64 Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
}


/**
 * An implementation of {@link SqlValueUnsignedInteger}
 */
[Version (since="1.2")]
public class Vda.ValueArrowUInt : ValueArrow, SqlValue, SqlValueUnsignedInteger {
    construct {
        _name = "uint";
    }
    /**
     * Construct a new {@link Vda.ValueArrowUInt} from a given {@link GArrow.UInt16Array}
     * checkout {@link ValueArrowUInt.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowUInt.from_array (GArrow.UInt16Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowUInt} from a given {@link GArrow.UInt16Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.UInt16Array}. If you want to use
     * a value without a query source use {@link ValueArrowUInt.from_array} instead.
     */
    public ValueArrowUInt.from_query (Vda.Query query, GArrow.UInt16Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.UIntArrayBuilder ();
          uint i = (uint) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow uint Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
    internal override string to_sql_expression () {
        return to_string ();
    }
}


/**
 * An implementation of {@link SqlValueUnsignedByte}
 */
[Version (since="1.2")]
public class Vda.ValueArrowUInt8 : ValueArrowUInt, SqlValueUnsignedByte  {
    construct {
        _name = "ubyte";
    }
    /**
     * Construct a new {@link Vda.ValueArrowUInt8} from a given {@link GArrow.UInt8Array}
     * checkout {@link ValueArrowUInt8.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowUInt8.from_array (GArrow.UInt8Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowUInt8} from a given {@link GArrow.UInt8Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.UInt8Array}. If you want to use
     * a value without a query source use {@link ValueArrowUInt8.from_array} instead.
     */
    public ValueArrowUInt8.from_query (Vda.Query query, GArrow.UInt8Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.UInt8ArrayBuilder ();
          uint8 i = (int8) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow uint8 Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
}

/**
 * An implementation of {@link SqlValueInt2}
 */
[Version (since="1.2")]
public class Vda.ValueArrowUInt16 : ValueArrowUInt, SqlValueUnsignedInt2  {
    construct {
        _name = "uint2";
    }
    /**
     * Construct a new {@link Vda.ValueArrowUInt16} from a given {@link GArrow.UInt16Array}
     * checkout {@link ValueArrowUInt16.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowUInt16.from_array (GArrow.UInt16Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowUInt16} from a given {@link GArrow.UInt16Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.UInt16Array}. If you want to use
     * a value without a query source use {@link ValueArrowUInt16.from_array} instead.
     */
    public ValueArrowUInt16.from_query (Vda.Query query, GArrow.UInt16Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.UInt16ArrayBuilder ();
          uint16 i = (uint16) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow uint16 Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
}

/**
 * An implementation of {@link SqlValueInt4}
 */
[Version (since="1.2")]
public class Vda.ValueArrowUInt32 : ValueArrowUInt, SqlValueUnsignedInt4 {
    construct {
        _name = "uint4";
    }
    /**
     * Construct a new {@link Vda.ValueArrowUInt32} from a given {@link GArrow.UInt32Array}
     * checkout {@link ValueArrowUInt32.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowUInt32.from_array (GArrow.UInt32Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowUInt32} from a given {@link GArrow.UInt32Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.UInt32Array}. If you want to use
     * a value without a query source use {@link ValueArrowUInt32.from_array} instead.
     */
    public ValueArrowUInt32.from_query (Vda.Query query, GArrow.UInt32Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.UInt32ArrayBuilder ();
          uint32 i = (uint32) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow uint32 Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
    internal override string to_string () {
        return "%I64u".printf ((uint64) _val);
    }
}

/**
 * An implementation of {@link SqlValueInt8}
 */
[Version (since="1.2")]
public class Vda.ValueArrowUInt64 : ValueArrowUInt, SqlValueUnsignedInt8 {
    construct {
        _name = "uint8";
    }
    /**
     * Construct a new {@link Vda.ValueArrowUInt64} from a given {@link GArrow.UInt64Array}
     * checkout {@link ValueArrowUInt64.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowUInt64.from_array (GArrow.UInt64Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowUInt64} from a given {@link GArrow.UInt64Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.UInt64Array}. If you want to use
     * a value without a query source use {@link ValueArrowUInt64.from_array} instead.
     */
    public ValueArrowUInt64.from_query (Vda.Query query, GArrow.UInt64Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.UInt64ArrayBuilder ();
          uint64 i = (uint64) double.parse (str);
          b.append_value (i);
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (_("Error while parsing Arrow uint64 Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
}

/**
 * An implementation of {@link SqlValueOid}
 */
[Version (since="1.2")]
public class Vda.ValueArrowOid : ValueArrowInt64, SqlValueOid  {

    /**
     * Construct a new {@link Vda.ValueArrowOid} from a given {@link GArrow.UInt64Array}
     * checkout {@link ValueArrowOid.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowOid.from_array (GArrow.UInt64Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowOid} from a given {@link GArrow.UInt64Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.UInt64Array}. If you want to use
     * a value without a query source use {@link ValueArrowOid.from_array} instead.
     */
    public ValueArrowOid.from_query (Vda.Query query, GArrow.UInt64Array val) {
      _val = val;
      _query = query;
    }
}

/**
 * An implementation of {@link SqlValueMoney}
 */
[Version (since="1.2")]
public class Vda.ValueArrowMoney : ValueArrowNumeric, SqlValueMoney
{
  private int _int_precision;
  /**
   * Construct a new {@link Vda.ValueArrowMoney} from a given {@link GArrow.DoubleArray}
   * checkout {@link ValueArrowMoney.from_query} if you want to use a data source
   * from ADBC
   */
  public ValueArrowMoney.from_array (GArrow.DoubleArray val) {
    _val = val;
    _query = null;
  }

  /**
   * Construct a new {@link Vda.ValueArrowMoney} from a given {@link GArrow.DoubleArray}
   * and {@link Vda.Query}; make sure the query is from ADBC connections,
   * when executed the query create an stream that should be kept alive the
   * time you access the data from an {@link GArrow.DoubleArray}. If you want to use
   * a value without a query source use {@link ValueArrowMoney.from_array} instead.
   */
  public ValueArrowMoney.from_query (Vda.Query query, GArrow.DoubleArray val) {
    _val = val;
    _query = query;
  }
  construct {
    var lc = GLibc.LConv.locale_conventions ();
    set_precision (lc->frac_digits);
    _int_precision = lc->int_frac_digits;
  }
  private string format_locale (bool international) {
    double v = 0.0;
    if (_val is GArrow.DoubleArray) {
      v = ((GArrow.DoubleArray) _val).get_value (0);
    }
    var i = (int) v;
    var f = v - i;
    int p = get_precision ();
    if (international) {
      p = get_int_precision ();
    }
    var rf = f * GLib.Math.pow (10, p);
    rf = GLib.Math.ceil (rf);
    var ip = "%d".printf (i);
    var fp = "%g".printf (rf);
    var lc = GLibc.LConv.locale_conventions ();
    string mstr = "";
    if (i >= 0 && lc->int_p_cs_precedes == 1 && international) {
      mstr += lc->int_curr_symbol;
    }
    if (i < 0 && lc->int_n_cs_precedes == 1 && international) {
      mstr += lc->int_curr_symbol;
    }
    if (i >= 0 && lc->p_cs_precedes == 1) {
      mstr += lc->currency_symbol;
    }
    if (i < 0 && lc->n_cs_precedes == 1) {
      mstr += lc->currency_symbol;
    }
    unichar c = '\0';
    int index = ip.length;
    int cp = 0;
    StringBuilder sip = new StringBuilder ("");
    while (ip.get_prev_char (ref index, out c)) {
      cp++;
      if (cp == 4) {
        sip.prepend (lc->mon_thousands_sep);
        cp = 0;
      }
      sip.prepend_unichar (c);
    }
    mstr += sip.str + lc->mon_decimal_point + fp;
    return mstr;
  }
  internal string locale () {
      return format_locale (false);
  }
  internal string int_locale () {
      return format_locale (true);
  }
  internal int get_int_precision () {
    return _int_precision;
    }
    internal void set_int_precision (int p) {
      if (p < 0) {
      return;
      }
    _int_precision = p;
    }
  internal override bool parse (string str) {
    unichar c = '\0';
    string nstr = "";
    int i = str.length;
    int m = 0;
    bool p = false;
    while (str.get_prev_char (ref i, out c)) {
      if (!p && (c == ',' || c == '.')) {
        p = true;
      }
      if (!p) {
        m++;
      }
    }
    i = 0;
    while (str.get_next_char (ref i, out c)) {
      if (c.isdigit () || c == '+' || c == '-') {
        nstr += c.to_string ();
      }
    }
    double k = 1;
    if (p) {
        k = GLib.Math.pow (10.0, m);
    }
    double v = double.parse (nstr) / k;
    var b = new GArrow.DoubleArrayBuilder ();
    try {
      b.append_value (v);
      _val = b.finish ();
    } catch (GLib.Error e) {
      warning (_("Error while parsing Arrow OID Array Value: %s"), e.message);
      return false;
    }
    return true;
  }
  internal override string to_string () {
    return locale ();
  }
}

/**
 * An implementation of {@link SqlValueTimestamp}. UTC is the default
 * time zone if not defined.
 */
[Version (since="1.2")]
public class Vda.ValueArrowTimestamp : ValueArrow, SqlValueTimestamp {
    protected DateTime dt;
    construct {
        dt = new DateTime.now ();
        _name = "timestamp with time zone";
        try {
          GArrow.TimestampDataType tdt = new GArrow.TimestampDataType (GArrow.TimeUnit.NANO);
          var b = new GArrow.TimestampArrayBuilder (tdt);
          b.append_value (dt.to_unix ());
          _val = b.finish ();
        } catch (GLib.Error  e) {
          warning (_("Error while initializing Arrow Timestamap array: %s"), e.message);
        }
    }
    /**
     * Construct a new {@link Vda.ValueArrowTimestamp} from a given {@link GArrow.TimestampArray}
     * checkout {@link ValueArrowTimestamp.from_query} if you want to use a data source
     * from ADBC
     *
     * Units and time zone, are defined by given {@link GArrow.TimestampArray} and its
     * {@link GArrow.TimestampDataType}
     */
    public ValueArrowTimestamp.from_array (GArrow.TimestampArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowTimestamp} from a given {@link GArrow.TimestampArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.TimestampArray}. If you want to use
     * a value without a query source use {@link ValueArrowTimestamp.from_array} instead.
     *
     * Units and time zone, are defined by given {@link GArrow.TimestampArray} and its
     * {@link GArrow.TimestampDataType}
     *
     * Parsing timestamps and time, in derived classes, use internally {@link GLib.DateTime.to_unix};
     * so times will be rounded up to the nearest second.
     */
    public ValueArrowTimestamp.from_query (Vda.Query query, GArrow.TimestampArray val) {
      _val = val;
      _query = query;
    }
    // Value
    internal override bool parse (string str) {
        try {
          string nstr = str.replace (" ","T");
          var tz = new TimeZone.utc ();
          var ndt = new DateTime.from_iso8601 (nstr, tz);
          if (ndt != null) {
              dt = ndt.add_days (0);
              GArrow.TimestampDataType tdt = new GArrow.TimestampDataType (GArrow.TimeUnit.MILLI);
              var b = new GArrow.TimestampArrayBuilder (tdt);
              b.append_value (dt.to_unix ());
              _val = b.finish ();
              return true;
          }
        } catch (GLib.Error e) {
          warning (("Error parsing timestamp: %s"), e.message);
        }
        return false;
    }
    internal virtual string format_local () {
        int64 t = ((GArrow.TimestampArray) _val).get_value (0);
        // FIXME: If the above value in the array has a time zone
        // this will not work properly
        var dt = new GLib.DateTime.from_unix_utc (t);
        var ndt = dt.to_local ();
        return ndt.format ("%FT%T%:::z");
    }
    internal virtual string format_utc () {
        int64 t = ((GArrow.TimestampArray) _val).get_value (0);
        var dt = new GLib.DateTime.from_unix_utc (t);
        return dt.format ("%FT%T%:::z");
    }
    internal virtual string format_locale () {
        int64 t = ((GArrow.TimestampArray) _val).get_value (0);
        var dt = new GLib.DateTime.from_unix_utc (t);
        var ndt = dt.to_local ();
        return ndt.format ("%x %X:::z");
    }
    internal virtual DateTime get_timestamp () {
      int64 t = ((GArrow.TimestampArray) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      return dt;
    }
    internal virtual void set_timestamp (DateTime ts) {
        try {
          GArrow.TimestampDataType tdt = new GArrow.TimestampDataType (GArrow.TimeUnit.NANO);
          var b = new GArrow.TimestampArrayBuilder (tdt);
          var nts = ts.to_utc ();
          b.append_value (nts.to_unix ());
          _val = b.finish ();
        } catch (GLib.Error e) {
          GLib.warning (_("Error parsing timestamp: %s"), e.message);
        }
    }
    internal string format_date () {
      int64 t = ((GArrow.TimestampArray) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      return dt.format ("%F");
    }
    internal string format_date_locale () {
      int64 t = ((GArrow.TimestampArray) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      return dt.format ("%x");
    }
    internal string format_time () {
      int64 t = ((GArrow.TimestampArray) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      return dt.format ("%T:::z");
    }
    internal string format_time_local () {
      int64 t = ((GArrow.TimestampArray) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      var ndt = dt.to_local ();
      return ndt.format ("%T%:::z");
    }
    internal string format_time_local_ntz () {
      int64 t = ((GArrow.TimestampArray) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      var ndt = dt.to_local ();
      return ndt.format ("%T%");
    }
}

/**
 * An implementation of {@link SqlValueTimestampNtz} without time zone
 */
[Version (since="1.2")]
public class Vda.ValueArrowTimestampNtz : ValueArrowTimestamp, SqlValueTimestampNtz {
    construct {
        _name = "timestamp without time zone";
    }
    /**
     * Construct a new {@link Vda.ValueArrowTimestampNtz} from a given {@link GArrow.TimestampArray}
     * checkout {@link ValueArrowTimestampNtz.from_query} if you want to use a data source
     * from ADBC
     *
     * Units and time zone, are defined by given {@link GArrow.TimestampArray} and its
     * {@link GArrow.TimestampDataType}; the implementation will use UTC as time zone
     * if not possible to avoid to define one.
     */
    public ValueArrowTimestampNtz.from_array (GArrow.TimestampArray val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowTimestampNtz} from a given {@link GArrow.TimestampArray}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.TimestampArray}. If you want to use
     * a value without a query source use {@link ValueArrowTimestampNtz.from_array} instead.
     *
     * Units and time zone, are defined by given {@link GArrow.TimestampArray} and its
     * {@link GArrow.TimestampDataType}; the implementation will use UTC as time zone
     * if not possible to avoid to define one.
     */
    public ValueArrowTimestampNtz.from_query (Vda.Query query, GArrow.TimestampArray val) {
      _val = val;
      _query = query;
    }
}

/**
 * Implementation of {@link SqlValueTime} a time with time zone
 */
[Version (since="1.2")]
public class Vda.ValueArrowTime32 : ValueArrowTimestamp, SqlValueTime {
    construct {
        _name = "time with time zone";
    }
    /**
     * Construct a new {@link Vda.ValueArrowTime32} from a given {@link GArrow.Time32Array}
     * checkout {@link ValueArrowTime32.from_query} if you want to use a data source
     * from ADBC
     *
     * Units and time zone, are defined by given {@link GArrow.Time32Array} and its
     * {@link GArrow.Time32DataType}; the implementation will use UTC as time zone
     * if not possible to avoid to define one.
     */
    public ValueArrowTime32.from_array (GArrow.Time32Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowTime32} from a given {@link GArrow.Time32Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Time32Array}. If you want to use
     * a value without a query source use {@link ValueArrowTime32.from_array} instead.
     *
     * Units and time zone, are defined by given {@link GArrow.Time32Array} and its
     * {@link GArrow.Time32DataType}; the implementation will use UTC as time zone
     * if not possible to avoid to define one.
     */
    public ValueArrowTime32.from_query (Vda.Query query, GArrow.Time32Array val) {
      _val = val;
      _query = query;
    }
    internal override DateTime get_timestamp () {
      int32 t = ((GArrow.Time32Array) _val).get_value (0);
      var tz = new TimeZone.utc ();
      var dtn = new GLib.DateTime.now ();
      int32 h = (int32) (t/60/60/1000);
      int32 m = (int32) ((t/60/1000) - h * 60);
      double s = (double) ((t/1000) - h * 60 * 60 - m * 60);
      var dt = new GLib.DateTime (tz, dtn.get_year (),
                              dtn.get_month (),
                              dtn.get_day_of_week (),
                              h, m, s);
      return dt;
    }
    internal override bool parse (string str) {
        try {
          string s = str;
          if (str.contains ("T")) {
            string[] p1 = str.split("T");
            if (p1.length == 2) {
              s = p1[1];
            }
          }
          string[] p2 = s.split (":");
          if (p2.length == 3) {
            int h = int.parse (p2[0]);
            int m = int.parse (p2[1]);
            double seg = 0.0;
            double.try_parse (p2[2], out seg, null);
            int32 t = (((int32) h * 60 * 60) + ((int32) m * 60))*1000 + (int32) (seg*1000);
            GArrow.Time32DataType tdt = new GArrow.Time32DataType (GArrow.TimeUnit.MILLI);
            var b = new GArrow.Time32ArrayBuilder (tdt);
            b.append_value (t);
            _val = b.finish ();
            return true;
          } else {
            return false;
          }
        } catch (GLib.Error e) {
          GLib.warning (_("Error parsing timestamp: %s"), e.message);
        }
        return false;
    }
    internal override string to_string () {
      int32 t = ((GArrow.Time32Array) _val).get_value (0);
      int h = (int) (t/60/60/1000);
      int m = (int) ((t/60/1000) - h * 60);
      double s = (double) ((t/1000) - h * 60 * 60 - m * 60);
      return "%02d:%02d:%2.3f".printf(h, m, s);
    }
    internal override string format_local () {
      int64 t = ((GArrow.Time32Array) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      var ndt = dt.to_local ();
      return ndt.format ("%T:::z");
    }
    internal override string format_utc () {
      int64 t = ((GArrow.Time32Array) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      return dt.format ("%T%:::z");
    }
    internal override string format_locale () {
      int64 t = ((GArrow.TimestampArray) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      var ndt = dt.to_local ();
      return ndt.format ("%T%:::z");
    }
}

/**
 * Implementation of {@link SqlValueTime} a time with time zone
 */
[Version (since="1.2")]
public class Vda.ValueArrowTime64 : ValueArrowTimestamp, SqlValueTime {
    construct {
        _name = "time with time zone";
    }
    /**
     * Construct a new {@link Vda.ValueArrowTime64} from a given {@link GArrow.Time64Array}
     * checkout {@link ValueArrowTime64.from_query} if you want to use a data source
     * from ADBC
     *
     * Units and time zone, are defined by given {@link GArrow.Time64Array} and its
     * {@link GArrow.Time64DataType}; the implementation will use UTC as time zone
     * if not possible to avoid to define one.
     */
    public ValueArrowTime64.from_array (GArrow.Time64Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowTime64} from a given {@link GArrow.Time64Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Time64Array}. If you want to use
     * a value without a query source use {@link ValueArrowTime64.from_array} instead.
     *
     * Units and time zone, are defined by given {@link GArrow.Time64Array} and its
     * {@link GArrow.Time64DataType}; the implementation will use UTC as time zone
     * if not possible to avoid to define one.
     */
    public ValueArrowTime64.from_query (Vda.Query query, GArrow.Time64Array val) {
      _val = val;
      _query = query;
    }
    internal override DateTime get_timestamp () {
      int64 t = ((GArrow.Time64Array) _val).get_value (0);
      var tz = new TimeZone.utc ();
      var dtn = new GLib.DateTime.now ();
      int h = (int) (t/60/60/1000000);
      int m = (int) ((t/60/1000000) - h * 60);
      double s = (double) ((t/1000000) - h * 60 * 60 - m * 60);
      var dt = new GLib.DateTime (tz, dtn.get_year (),
                              dtn.get_month (),
                              dtn.get_day_of_week (),
                              h, m, s);
      return dt;
    }
    internal override bool parse (string str) {
        try {
          string s = str;
          if (str.contains ("T")) {
            string[] p1 = str.split("T");
            if (p1.length == 2) {
              s = p1[1];
            }
          }
          string[] p2 = s.split (":");
          if (p2.length == 3) {
            int h = int.parse (p2[0]);
            int m = int.parse (p2[1]);
            double seg = 0.0;
            double.try_parse (p2[2], out seg, null);
            int64 t = (((int64) h * 60 * 60) + ((int64) m * 60))*1000000 + (int64) (seg*1000000);
            GArrow.Time64DataType tdt = new GArrow.Time64DataType (GArrow.TimeUnit.MICRO);
            var b = new GArrow.Time64ArrayBuilder (tdt);
            b.append_value (t);
            _val = b.finish ();
            return true;
          } else {
            return false;
          }
        } catch (GLib.Error e) {
          GLib.warning (_("Error parsing timestamp: %s"), e.message);
        }
        return false;
    }
    internal override string to_string () {
      int64 t = ((GArrow.Time64Array) _val).get_value (0);
      int h = (int) (t/60/60/1000000);
      int m = (int) ((t/60/1000000) - h * 60);
      double s = (double) ((t/1000000) - h * 60 * 60 - m * 60);
      return "%02d:%02d:%2.6f".printf(h, m, s);
    }
    internal override string format_local () {
      int64 t = ((GArrow.Time64Array) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      var ndt = dt.to_local ();
      return ndt.format ("%T:::z");
    }
    internal override string format_utc () {
      int64 t = ((GArrow.Time64Array) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      var ndt = dt.to_utc ();
      return ndt.format ("%T%:::z");
    }
    internal override string format_locale () {
      int64 t = ((GArrow.Time64Array) _val).get_value (0);
      var dt = new GLib.DateTime.from_unix_utc (t);
      var ndt = dt.to_local ();
      return ndt.format ("%T%:::z");
    }
}

/**
 * An implementation of {@link SqlValueTimeNtz} as time without time zone
 */
[Version (since="1.2")]
public class Vda.ValueArrowTimeNtz : Vda.ValueArrowTimestampNtz, SqlValueTimeNtz
{
    construct {
        _name = "time without time zone";
    }
    /**
     * Construct a new {@link Vda.ValueArrowTimeNtz} from a given {@link GArrow.Time32Array}
     * checkout {@link ValueArrowTimeNtz.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowTimeNtz.from_array (GArrow.Time32Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowTimeNtz} from a given {@link GArrow.Time32Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Time32Array}. If you want to use
     * a value without a query source use {@link ValueArrowTimeNtz.from_array} instead.
     */
    public ValueArrowTimeNtz.from_query (Vda.Query query, GArrow.Time32Array val) {
      _val = val;
      _query = query;
    }
    internal override string format_local () {
        var ndt = dt.to_local ();
        return ndt.format ("%T");
    }
    internal override string format_utc () {
        var ndt = dt.to_utc ();
        return ndt.format ("%T");
    }
    internal override string format_locale () {
        var ndt = dt.to_local ();
        return ndt.format ("%T");
    }
}

/**
 * An implementation of {@link SqlValueDate}
 */
[Version (since="1.2")]
public class Vda.ValueArrowDate32 : Vda.ValueArrow, Vda.SqlValueDate {
    construct {
        _name = "date";
        try {
          var b = new GArrow.Date32ArrayBuilder ();
          var d = new DateTime.now ();
          if (d != null) {
            b.append_value ((int32) d.to_unix ());
            _val = b.finish ();
          }
        } catch (GLib.Error e) {
          warning (("Error while initializing Date Array Value: %s"), e.message);
        }
    }
    /**
     * Construct a new {@link Vda.ValueArrowDate32} from a given {@link GArrow.Date32Array}
     * checkout {@link ValueArrowDate32.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowDate32.from_array (GArrow.Date32Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowDate32} from a given {@link GArrow.Date32Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Date32Array}. If you want to use
     * a value without a query source use {@link ValueArrowDate32.from_array} instead.
     */
    public ValueArrowDate32.from_query (Vda.Query query, GArrow.Date32Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.Date32ArrayBuilder ();
          var tz = new TimeZone.local ();
          var d = new DateTime.from_iso8601 (str, tz);
          if (d != null) {
            b.append_value ((int32) d.to_unix ());
            _val = b.finish ();
          }
        } catch (GLib.Error e) {
          warning (("Error while initializing Date Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
    /**
     * Returns a representation of the value as a {@link GLib.Date}
     */
    internal Date get_date () {
        Date nd = Date ();
        int64 t = ((GArrow.Date32Array) _val).get_value (0);
        var dt = new DateTime.from_unix_utc (t);
        nd.set_dmy ((GLib.DateDay) dt.get_day_of_week (),
                             (GLib.DateMonth) dt.get_month (),
                             (GLib.DateYear) dt.get_year ());
        return nd;
    }
    /**
     * Initialize the internal {@link GLib.Date} to the year, month and day
     * of given {@link GLib.Date}
     */
    internal void set_date (Date nd) {
        try {
          var b = new GArrow.Date32ArrayBuilder ();
          var d = new DateTime.local ((int) nd.get_day (),
                            (int) nd.get_month (),
                            (int) nd.get_year (),
                            0, 0, 0.0);
          b.append_value ((int32) d.to_unix ());
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (("Error while initializing Date Array Value: %s"), e.message);
        }
    }
}

/**
 * An implementation of {@link SqlValueDate}
 */
[Version (since="1.2")]
public class Vda.ValueArrowDate64 : Vda.ValueArrow, Vda.SqlValueDate {
    construct {
        _name = "date";
        try {
          var b = new GArrow.Date64ArrayBuilder ();
          var d = new DateTime.now ();
          if (d != null) {
            b.append_value (d.to_unix ());
            _val = b.finish ();
          }
        } catch (GLib.Error e) {
          warning (("Error while initializing Date Array Value: %s"), e.message);
        }
    }
    /**
     * Construct a new {@link Vda.ValueArrowDate64} from a given {@link GArrow.Date64Array}
     * checkout {@link ValueArrowDate64.from_query} if you want to use a data source
     * from ADBC
     */
    public ValueArrowDate64.from_array (GArrow.Date64Array val) {
      _val = val;
      _query = null;
    }

    /**
     * Construct a new {@link Vda.ValueArrowDate64} from a given {@link GArrow.Date64Array}
     * and {@link Vda.Query}; make sure the query is from ADBC connections,
     * when executed the query create an stream that should be kept alive the
     * time you access the data from an {@link GArrow.Date64Array}. If you want to use
     * a value without a query source use {@link ValueArrowDate64.from_array} instead.
     */
    public ValueArrowDate64.from_query (Vda.Query query, GArrow.Date64Array val) {
      _val = val;
      _query = query;
    }
    internal override bool parse (string str) {
        try {
          var b = new GArrow.Date64ArrayBuilder ();
          var tz = new TimeZone.local ();
          var d = new DateTime.from_iso8601 (str, tz);
          if (d != null) {
            b.append_value (d.to_unix ());
            _val = b.finish ();
          }
        } catch (GLib.Error e) {
          warning (("Error while initializing Date Array Value: %s"), e.message);
          return false;
        }
        return true;
    }
    /**
     * Returns a representation of the value as a {@link GLib.Date}
     */
    internal Date get_date () {
        Date nd = Date ();
        int64 t = ((GArrow.Date64Array) _val).get_value (0);
        var dt = new DateTime.from_unix_utc (t);
        nd.set_dmy ((GLib.DateDay) dt.get_day_of_week (),
                             (GLib.DateMonth) dt.get_month (),
                             (GLib.DateYear) dt.get_year ());
        return nd;
    }
    /**
     * Initialize the internal {@link GLib.Date} to the year, month and day
     * of given {@link GLib.Date}
     */
    internal void set_date (Date nd) {
        try {
          var b = new GArrow.Date64ArrayBuilder ();
          var d = new DateTime.local ((int) nd.get_day (),
                            (int) nd.get_month (),
                            (int) nd.get_year (),
                            0, 0, 0.0);
          b.append_value ((int32) d.to_unix ());
          _val = b.finish ();
        } catch (GLib.Error e) {
          warning (("Error while initializing Date Array Value: %s"), e.message);
        }
    }
}
