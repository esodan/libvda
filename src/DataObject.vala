/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * DataObject.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent managable row data in a Table in the database.
 *
 * Properties should have its nick in the form '@field_name::id' where
 * 'field_name' is the name of your field in the database's table, supporting
 * any case, including spaces. '@' is just a mark to detect the property
 * as one to be mapped to a database's table's field. '::id' should be
 * added only on properties containing an ID to get data from database.
 *
 * In order to be able to execute operations over the database, you should
 * set {@link database_connection} with the connection to the databae
 * and {@link database_table_name} to the table's name in the database to
 * execute operations over.
 *
 * If more than one property with a nickname using a '::id' suffix is located,
 * the object takes the actual value of each property to create a filter
 * condition with an AND operator as the form
 * 'field_name = property_value AND field_name2 = property_value2'
 *
 * In the following example, the properties name, phone and email, are used
 * to store data from the database when {@link update_data_from_db} is called.
 *
 * If you try to retrive data from the database, first you should set
 * the properties phone and email, in the example bellow, then call
 * {@link update_data_from_db}, this will ejecute a SELECT query using
 * a WHERE condition in the form "phone = 'phonevalue' AND email = 'emailvalue' "
 * as a filter that should return just one row.
 *
 * Once you have the data from any source, like calling {@link update_data_from_db},
 * is possible to update any property then call {@link update_data_into_db} to
 * UPDATE the data in the table's database.
 *
 * When a new row is required, insert a row, set all the properties you want to
 * insert with the row and then call {@link insert_data_into_db}. Mark a property
 * with a nickname using a '::auto' suffix to ignore it in the hope it is
 * automatically set by the database engine. In the example bellow, the country
 * property is automatically set by the database engine, as a default value, for
 * example.
 *
 * {{{
 *   public class Client : Object, Vda.DataObject {
 *     public string database_table_name { get; construct set; }
 *     public Vda.Connection database_connection { get; set; }
 *     public Cancellable cancellable { get; set; }
 *
 *     [Description (nick="@id::pkey::auto")]
 *     public int id { get; set; }
 *
 *     [Description (nick="@name")]
 *     public string name { get; set; }
 *
 *     [Description (nick="@phone::id")]
 *     public string phone { get; set; }
 *
 *     [Description (nick="@email::id")]
 *     public string email { get; set; }
 *
 *     [Description (nick="@country::auto")]
 *     public string country { get; set; }
 *   }
 * }}}
 */
public interface Vda.DataObject  : Object {
	/**
	 * Get Database connection
	 */
	public abstract Vda.Connection database_connection { get; set; }
	/**
	 * Database connection
	 */
	public abstract string database_table_name { get; construct set; }
	/**
	 * Cancellable object
	 */
	public abstract Cancellable cancellable { get; set; }
	/**
	 * Update object's properties' values with the ones in the database.
	 *
	 * This method uses all properties marked as '::id' in its nick.
	 */
	public virtual async void update_data_from_db () throws GLib.Error {
		yield update_data_from_db_full (false);
	}
	/**
	 * Update object's properties' values with the ones in the database.
	 *
	 * This method uses all properties marked as '::pkey' in its nick.
	 */
	public virtual async void update_data_from_db_pkey () throws GLib.Error {
		yield update_data_from_db_full (true);
	}
	/**
	 * Update object's properties' values with the ones in the database.
	 *
	 * Take '::pkey' or '::id' marked property, by its nick, to generate the query.
	 *
	 * Use @param use_pkey to ignore the '::id' marked properties and use the '::pkey'
	 * ones: useful when you already knows the primary
	 * key of the row and you want to update the fields marked as '::id'.
	 *
	 * @param use_pkey enable using primary keys to generate queries, if false use '::id' ones
	 */
	public virtual async void update_data_from_db_full (bool use_pkey = false) throws GLib.Error {
		var q = create_select_query (this, use_pkey).to_query ();
		unowned ObjectClass objclass = this.get_class ();
		var lp = objclass.list_properties ();
		foreach (ParamSpec spec in lp) {
			string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				string fn = nick.replace ("@", "");
				if ((!nickl.has_suffix ("::auto") && fn.contains ("::id") && !use_pkey)
					 || (use_pkey && fn.contains ("::pkey"))) {
					fn = fn.replace ("::id", "");
					fn = fn.replace ("::pkey", "");
					fn = fn.replace ("::auto", "");
					GLib.Value val = GLib.Value (spec.value_type);
					this.get_property (spec.get_name (), ref val);
					q.parameters.set_value (fn, val);
				}
			}
		}
		var res = yield q.execute (cancellable);

		if (res is InvalidResult) {
			throw new DataObjectError.SELECT_ERROR (_("Invalid result from query to get data from database: %s"), ((InvalidResult) res).message);
		}

		if (!(res is TableModel || res is TableModelSequential)) {
			debug ("No data was updated from database because no one exists");
			return;
		}

		RowModel r = null;
		if (res is TableModel) { 
			var t = (TableModel) res;
			if (t.get_n_items () != 1) {
				throw new DataObjectError.SELECT_ERROR (_("Invalid number of rows in the result, to get data from database. Got: %u"), t.get_n_items ());
			}
			r = (RowModel) t.get_item (0);
		}

		if (res is TableModelSequential) {
			var t = (TableModelSequential) res;
			if (!t.next ()) {
				throw new DataObjectError.SELECT_ERROR (_("Can't access to first row from result"));
			}

			r = t.current ();
		}

		if (r == null){
			throw new DataObjectError.SELECT_ERROR (_("Invalid row in the table model, while getting data from database"));
		}

		foreach (ParamSpec spec in lp) {
			string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				string fn = nick.replace ("@", "");
				fn = fn.replace ("::id", "");
				fn = fn.replace ("::auto", "");
				fn = fn.replace ("::pkey", "");
				var dbv = r.get_value (fn);
				if (spec.value_type.is_a (typeof (SqlValue))) {
					set_property (fn, dbv);
				} else {
					SqlValue vval = dbv.cast (spec.value_type);
					if (!(vval is SqlValueNull)) {
						this.set_property (spec.get_name (), vval.to_gvalue ());
					} else {
						this.set (fn, null, null);
					}
				}
			}
		}
	}

	/**
	 * Insert a new row with the object's properties with the same name as for
	 * data's query's columns' name.
	 *
	 * This is a one time operation, if you want to know how your data has been saved
	 * like the asigned ID or automatic values, if any, you should query it from the data base,
	 * creating a new instance and using {@link update_data_from_db} if you know
	 * all ID's you have used before.
	 */
	public virtual async void insert_data_into_db () throws GLib.Error {
		var q = create_insert_query (this).to_query () as PreparedQuery;
		update_parameters (this, q);
		yield q.execute (cancellable);
	}
	/**
	 * Delete object in the database using marked id properties
	 *
	 * This is a convenient method around {@link delete_data_from_db_full}
	 */
	public virtual async void delete_data_from_db () throws GLib.Error {
		yield delete_data_from_db_full (false);
	}
	/**
	 * Delete object in the database using marked primary key properties
	 *
	 * This is a convenient method around {@link delete_data_from_db_full}
	 */
	public virtual async void delete_data_from_db_pkey () throws GLib.Error {
		yield delete_data_from_db_full (true);
	}
	/**
	 * Delete object in the database using marked properties
	 *
	 * Take '::pkey' or '::id' marked property, by its nick, to generate the query.
	 *
	 * Use @param use_pkey to ignore the '::id' marked properties and use the '::pkey'
	 * ones: useful when you already knows the primary
	 * key of the row and you want to update the fields marked as '::id'.
	 *
	 * @param use_pkey enable using primary keys to generate queries, if false use '::id' ones
	 */
	public virtual async void delete_data_from_db_full (bool use_pkey = false) throws GLib.Error {
		var q = create_delete_query (this, use_pkey).to_query () as PreparedQuery;
		unowned ObjectClass objclass = this.get_class ();
		var lp = objclass.list_properties ();
		foreach (ParamSpec spec in lp) {string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				if ((!use_pkey && nickl.contains ("::id") && !nickl.has_suffix ("::auto"))
						|| (use_pkey && nickl.contains ("::pkey"))) {
					string fn = nick.replace ("@", "");
					fn = fn.replace ("::id", "");
					fn = fn.replace ("::pkey", "");
					fn = fn.replace ("::auto", "");
					GLib.Value val = GLib.Value (spec.value_type);
					this.get_property (spec.get_name (), ref val);
					q.parameters.set_value (fn, val);
				}
			}
		}
		yield q.execute (cancellable);
	}

	/**
	 * Update object's data in the database using marked id properties
	 *
	 * This method uses all properties marked as '::id' in its nick.
	 */
	public virtual async void update_data_into_db () throws GLib.Error {
		yield update_data_into_db_full (false);
	}

	/**
	 * Update object's data in the database using marked primary key properties
	 *
	 * This method uses all properties marked as '::id' in its nick.
	 */
	public virtual async void update_data_into_db_pkey () throws GLib.Error {
		yield update_data_into_db_full (true);
	}

	/**
	 * Update object's data in the database
	 *
	 * Take '::pkey' or '::id' marked property, by its nick, to generate the query.
	 *
	 * Use @param use_pkey to ignore the '::id' marked properties and use the '::pkey'
	 * ones: useful when you already knows the primary
	 * key of the row and you want to update the fields marked as '::id'.
	 *
	 * @param use_pkey enable using primary keys to generate queries, if false use '::id' ones
	 */
	public virtual async void update_data_into_db_full (bool use_pkey = false) throws GLib.Error {
		var q = create_update_query (this, use_pkey).to_query () as PreparedQuery;
		update_parameters (this, q, use_pkey);
		yield q.execute (cancellable);
	}

	/**
	 * Update values from a {@link TableModel} at given row number
	 */
	public virtual void update_from_row (TableModel table, uint nrow) throws GLib.Error {
		var row = table.get_item (nrow) as RowModel;
		if (row == null) {
			throw new DataObjectError.SELECT_ERROR (_("Invalid row number %ld"), nrow);
		}
		unowned ObjectClass objclass = get_class ();
		var lp = objclass.list_properties ();
		foreach (ParamSpec spec in lp) {string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				string fn = nick.replace ("@", "");
				fn = fn.replace ("::id", "");
				fn = fn.replace ("::pkey", "");
				fn = fn.replace ("::auto", "");
				var col = row.get_column (fn);
				if (col != null) {
					GLib.Value nval = GLib.Value (spec.value_type);
					if (spec.value_type.is_a (typeof (SqlValue))) {
						set_property (spec.get_name (), row.get_value (fn));
					} else {
						bool valid = false;
						GLib.Value val = row.get_value (fn).to_gvalue ();
						if (GLib.Value.type_compatible (val.type (), spec.value_type)) {
							val.copy (ref nval);
							valid = true;
						} else {
							if (GLib.Value.type_transformable (val.type (), spec.value_type)) {
								val.transform (ref nval);
								valid = true;
							}
						}
						if (valid) {
							set_property (spec.get_name (), row.get_value (fn).to_gvalue ());
						}
					}
				}
			}
		}
	}
	private static void update_parameters (Object obj, PreparedQuery q, bool use_pkey = false) throws GLib.Error {
		if (q.parameters == null) {
			throw new DataObjectError.PARAMETERS_ERROR (_("No parameter is present in Prepared Query"));
		}
		unowned ObjectClass objclass = obj.get_class ();
		var lp = objclass.list_properties ();
		foreach (ParamSpec spec in lp) {string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				string fn = nick.replace ("@", "");
				if (fn.contains ("::pkey") && !use_pkey) {
					continue;
				}
				fn = fn.replace ("::id", "");
				fn = fn.replace ("::pkey", "");
				fn = fn.replace ("::auto", "");
				GLib.Value val = GLib.Value (spec.value_type);
				obj.get_property (spec.get_name (), ref val);
				if (spec.value_type.is_a (typeof (SqlValue))) {
					q.parameters.set_sql_value (fn, val.get_object () as SqlValue);
				} else {
					q.parameters.set_value (fn, val);
				}
			}
		}
	}
	/**
	 * Creates a {@link SqlCommandSelect} to SELECT a {@link DataObject} using
	 * the object's properties with the mark, in the nicks, ::id and ::pkey, filtering
	 * all other objects.
	 *
	 * @param obj the object to create the command from
	 * @param use_pkey change the use of ::id properties with the ones marked as ::pkey
	 *
	 * @return a new {@link SqlCommandSelect}
	 */
	public static Vda.SqlCommandSelect create_select_query (DataObject obj, bool use_pkey = false) throws GLib.Error {
		if (obj.database_connection == null) {
			throw new DataObjectError.NO_CONNECTION_ERROR (_("No connection was set to object"));
		}
		var q = new CommandSelect (obj.database_connection);
		q.add_table (obj.database_table_name, "t");
		var cond = q.condition as SqlExpressionOperator;
		unowned ObjectClass objclass = obj.get_class ();
		var lp = objclass.list_properties ();
		foreach (ParamSpec spec in lp) {
			string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				string fn = nickl.replace ("@", "");
				if ((!fn.has_suffix ("::auto") && !use_pkey && fn.has_suffix ("::id"))
						|| (use_pkey && fn.contains ("::pkey"))) {
					fn = fn.replace ("::id", "");
					fn = fn.replace ("::pkey", "");
					fn = fn.replace ("::auto", "");
					var fe = cond.create_field_expression (fn);
					var fv = cond.create_parameter_expression (fn, spec.value_type);
					cond.add_eq_operator (fe, fv);
				}
				fn = fn.replace ("::id", "");
				fn = fn.replace ("::auto", "");
				fn = fn.replace ("::pkey", "");
				q.add_field (fn, "t", null);
			}
		}
		return q;
	}
	/**
	 * Creates an {@link SqlCommandSelect} query, for all objects of the same type
	 * in the database
	 *
	 * @param obj a {@link DataObject} to create a query from
	 */
  public static SqlCommandSelect create_select_all (DataObject obj) {
    var q = new CommandSelect (obj.database_connection);
		q.add_table (obj.database_table_name, "t");
		unowned ObjectClass objclass = obj.get_class ();
		var lp = objclass.list_properties ();
		foreach (ParamSpec spec in lp) {
			string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				string fn = nick.replace ("@", "");
				fn = fn.replace ("::id", "");
				fn = fn.replace ("::auto", "");
				fn = fn.replace ("::pkey", "");
				q.add_field (fn, "t", null);
			}
		}
		return q;
  }
	/**
	 * Creates an {@link SqlCommandInsert} to INSERT a {@link DataObject}
	 *
	 * Only properties with a nickname as the form '@param_name' prefix are considered
	 * as parameters,where 'param_name' is the parameter name used and added.
	 *
	 * Use the returned object and modify at your needs, then create a {@link PreparedQuery}
	 * using {@link SqlCommandInsert.to_query} so you can use {@link Query.execute}
	 * to run your command.
	 */
	public static Vda.SqlCommandInsert create_insert_query (DataObject obj) throws GLib.Error {
		var q = new CommandInsert (obj.database_connection);
		if (q.parameters == null) {
			throw new DataObjectError.INSERT_ERROR (_("Insert command hasn't a parameters object"));
		}
		q.table = obj.database_table_name;
		unowned ObjectClass objclass = obj.get_class ();
		var lp = objclass.list_properties ();
		foreach (ParamSpec spec in lp) {
			string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				string fn = nick.replace ("@", "");
				if (!fn.has_suffix ("::auto")) {
					fn = fn.replace ("::id", "");
					fn = fn.replace ("::pkey", "");
					q.add_field_parameter_value (fn, fn, spec.value_type);
				}
			}
		}
		return q;
	}
	/**
	 * Creates a {@link SqlCommandDelete} to DELETE a {@link DataObject}.
	 *
	 * Use the returned object and modify at your needs, then create a {@link PreparedQuery}
	 * using {@link SqlCommandDelete.to_query} so you can use {@link Query.execute}
	 * to run your command.
	 */
	public static Vda.SqlCommandDelete create_delete_query (DataObject obj, bool use_pkey = false) throws GLib.Error {
		var q = new CommandDelete (obj.database_connection);
		q.table = obj.database_table_name;
		var cond = q.condition as SqlExpressionOperator;
		unowned ObjectClass objclass = obj.get_class ();
		var lp = objclass.list_properties ();
		bool has_condition = false;
		foreach (ParamSpec spec in lp) {
			string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				string fn = nick.replace ("@", "");
				if ((!use_pkey && fn.contains ("::id") && !fn.has_suffix ("::auto"))
						|| (use_pkey && fn.contains ("::pkey"))) {
					fn = fn.replace ("::id", "");
					fn = fn.replace ("::pkey", "");
					fn = fn.replace ("::auto", "");
					var fe = cond.create_field_expression (fn);
					var fv = cond.create_parameter_expression (fn, spec.value_type);
					cond.add_eq_operator (fe, fv);
					has_condition = true;
				}
			}
		}
		if (!has_condition) {
			throw new DataObjectError.DELETE_ERROR ("No condition was added for DELETE command. check your object definition for ::id or ::pkey nicks in properties");
		}
		return q;
	}
	/**
	 *
	 * Creates a {@link SqlCommandUpdate} to UPDATE a {@link DataObject}.
	 *
	 * Use the returned object and modify at your needs, then create a {@link PreparedQuery}
	 * using {@link SqlCommandUpdate.to_query} so you can use {@link Query.execute}
	 * to run your command.
	 *
	 * @param obj a {@link DataObject} used to create query from
	 * @param use_pkey if true property marked with @???:pkey is used instead of '::id'
	 */
	public static Vda.SqlCommandUpdate create_update_query (DataObject obj, bool use_pkey = false) throws GLib.Error {
		var q = new CommandUpdate (obj.database_connection);
		q.table = obj.database_table_name;
		var cond = q.condition as SqlExpressionOperator;
		unowned ObjectClass objclass = obj.get_class ();
		var lp = objclass.list_properties ();
		bool has_condition = false;
		foreach (ParamSpec spec in lp) {
			string nick = spec.get_nick ();
			string nickl = nick.down ();
			if (nickl.has_prefix ("@")) {
				string fn = nick.replace ("@", "");
				if ((!nickl.has_suffix ("::auto") && !use_pkey && fn.contains ("::id"))
							|| (use_pkey && fn.contains ("::pkey"))) {
					fn = fn.replace ("::id", "");
					fn = fn.replace ("::pkey", "");
					fn = fn.replace ("::auto", "");
					var fe = cond.create_field_expression (fn);
					var fv = cond.create_parameter_expression (fn, spec.value_type);
					cond.add_eq_operator (fe, fv);
					has_condition = true;
				}
				if (!nickl.has_suffix ("::auto")) {
					fn = fn.replace ("::id", "");
					fn = fn.replace ("::pkey", "");
					q.add_field_parameter_value (fn, fn, spec.value_type);
				}
			}
		}
		if (!has_condition) {
			throw new DataObjectError.UPDATE_ERROR (_("No condition was added in UPDATE command. Check your object definition for ::id or ::pkey nicks in properties"));
		}
		return q;
	}
}

/**
 * Data Object error codes
 */
public errordomain Vda.DataObjectError {
	SELECT_ERROR,
	INSERT_ERROR,
	DELETE_ERROR,
	UPDATE_ERROR,
	PARAMETERS_ERROR,
	NO_CONNECTION_ERROR
}
