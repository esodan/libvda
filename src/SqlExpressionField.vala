/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlExpressionField.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent field in a column as an expression
 */
public interface Vda.SqlExpressionField  : Object, SqlExpression
{
	/**
	 * A table's name referenced by the field
	 */
	public abstract string table_ref { get; set; }
	/**
	 * A table's colunm's name referenced by the field
	 */
	public abstract string name { get; set; }
	/**
	 * A table's column's name alias referenced by the field
	 */
	public abstract string? allias { get; set; }
}
