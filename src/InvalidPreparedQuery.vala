/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * InvalidPreparedQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Invalid {@link PreparedQuery}
 */
public class Vda.InvalidPreparedQuery : Vda.InvalidQuery, Vda.PreparedQuery {
  private Vda.SqlParameters _parameters = null;
  public InvalidPreparedQuery (string msg) {
    base (msg);
  }

  internal string name { get { return ""; } }
  internal Vda.SqlParameters parameters {
    get {
      return _parameters;
    }
  }
}

