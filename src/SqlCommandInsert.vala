/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlCommandInsert.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an INSERT SQL command
 */
public interface Vda.SqlCommandInsert  : Object,
																				SqlCommandTableRelated,
																				SqlCommandModification,
																				SqlCommand,
																				SqlCommandParametrized
{
	/**
	 * Creates a string representation
	 */
	public virtual string stringify () throws GLib.Error {
		if (table == null) {
			throw new SqlCommandError.INVALID_STRUCTURE_ERROR (_("No table is given"));
		}
		if (fields.get_n_items () != values.get_n_items ()) {
		  throw new SqlCommandError.INVALID_STRUCTURE_ERROR (_("Invalid number of fields or values"));
		}
		bool par = false;
		string str = "INSERT INTO "+table+" (";
    for (int i = 0; i < fields.get_n_items (); i++) {
      var f = fields.get_item (i) as SqlExpressionField;
      if (f == null) continue;
      str += f.name;
      if (i + 1 < fields.get_n_items ()) {
        str += ", ";
      }
    }

    str += ") VALUES ";
    string vals = "(";
    for (int i = 0; i < values.get_n_items (); i++) {
      var v = values.get_item (i) as SqlExpressionValue;
      if (v == null) continue;
      if (v is SqlExpressionValueParameter) {
        par = true;
        string pval = ((ExpressionValueParameter) v).to_string ();
        if (parameters != null) {
        	var p = parameters.get_sql_value (((ExpressionValueParameter) v).name);
        	if (p != null) {
        		pval = connection.value_to_quoted_string (p);
        	}
        }
      	vals += pval;
      } else {
      	vals += connection.value_to_quoted_string (v.@value);
      }

      if (i + 1 < values.get_n_items ()) {
        vals += ", ";
      }
    }
    str += vals+")";
    return str;
	}
	/**
	 * Create a {@link Query} for execution, using internal structure.
	 *
	 * If {@link ExpressionValueParameter} object is used for values,
	 * then a {@link PreparedQuery} is returned with the given name.
	 */
	public virtual Query to_query (string? name = null) throws GLib.Error {
    return connection.query_from_command (this, name);
  }
  /**
   * Parse SQL string commands and construct its internal tree
   */
  public virtual void parse (string sql)  throws GLib.Error {
		Gee.ArrayList<GLib.TokenType> expected = new Gee.ArrayList<GLib.TokenType> ();
    var scanner = new GLib.Scanner (null);
    scanner.input_name = "SQL";
    scanner.input_text (sql, sql.length);
    scanner.config.cpair_comment_single = "//\n";
    scanner.config.skip_comment_multi = false;
    scanner.config.skip_comment_single = false;
    scanner.config.char_2_token = false;
    scanner.config.scan_binary = false;
    scanner.config.scan_octal = false;
    scanner.config.scan_float = false;
    scanner.config.scan_hex = false;
    scanner.config.scan_hex_dollar = false;
    scanner.config.numbers_2_int = false;
    GLib.TokenType token = GLib.TokenType.NONE;
    bool complete_init = false;
    bool table_setted = false;
    bool expected_table_name = false;
    bool set_allias = false;
    bool parentesis_open = false;
    bool enable_field = false;
    bool enable_value = false;
    bool param_type = false;
    bool param_identifier = false;
    bool start_math_expression = false;
    bool open_math_parens = false;
    bool values = false;
    bool starting = true;
    string param_name = "";
    string param_type_name = "";
    string math_expression = "";
    StringBuilder bstr = new StringBuilder ("");
    while (token != GLib.TokenType.EOF) {
      token = scanner.get_next_token ();
      if (token == GLib.TokenType.EOF) {
        break;
      }
      if (expected.size != 0 && !expected.contains (token)) {
        throw new ParserError.INVALID_TOKEN_ERROR (_("Found an unexpected expression at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
      }
      switch (token) {
        case GLib.TokenType.IDENTIFIER:
          string identifier = scanner.cur_value ().@identifier;
          if (start_math_expression) {
            math_expression += identifier;
          }
          // SQL commands and identifiers
          if (starting) {
            if (identifier.down () != "insert") {
              throw new ParserError.INVALID_TOKEN_ERROR (_("SQL insert command should start with INSERT"));
            }
            starting = false;
          } else if (expected_table_name) {
            if (!table_setted) {
              table =scanner.cur_value ().@string;
              table_setted = true;
              enable_field = false;
              enable_value = false;
              expected_table_name = false;
              values = false;
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
              expected.add (GLib.TokenType.CHAR);
            } else {
              if (identifier.down () == "as" && !set_allias) {
                set_allias = true;
                expected.clear ();
                expected.add (GLib.TokenType.IDENTIFIER);
              } else if (set_allias) {
                set_allias = false;
                allias =scanner.cur_value ().@string;
                expected.clear ();
                expected.add (GLib.TokenType.CHAR);
              } else {
                expected.clear ();
                expected.add (GLib.TokenType.CHAR);
              }
            }
          } else if (!complete_init && !expected_table_name) {
            if (identifier.down () == "into") {
              complete_init = true;
              expected_table_name = true;
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
            } else {
              throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid INSERT declaration: expected INTO at Line:Column : %d:%d"),
                                                      scanner.cur_line (), scanner.cur_position ());
            }
          } else if (complete_init && table_setted && !values && !enable_field && !parentesis_open) {
            if (identifier.down () == "values") {
              enable_value = false;
              enable_field = false;
              values = true;
              expected.clear ();
              expected.add (GLib.TokenType.CHAR);
            } else {
              throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid INSERT declaration: expected VALUES keyword at Line:Column : %d:%d"),
                                                      scanner.cur_line (), scanner.cur_position ());
            }
          } else if (enable_field && parentesis_open && !values) {
            add_field (identifier);
            expected.clear ();
            expected.add (GLib.TokenType.CHAR);
          } else if (enable_value && param_identifier && param_name == "" && !param_type) {
            param_name = identifier;
            param_type = false;
            param_type_name = "";
            expected.clear ();
            expected.add (GLib.TokenType.CHAR);
          } else if (enable_value && param_identifier && param_type && param_type_name == "") {
            param_type_name =scanner.cur_value ().@string.to_string ();
            param_type = false;
            // FIXME: Find SqlValue type to use here
            var ptype = GLib.Type.from_name (param_type_name);
            add_parameter (param_name, ptype);
            enable_value = true;
            param_identifier = false;
            param_type = false;
            enable_field = false;
            param_name = "";
            param_type_name = "";
            expected.clear ();
            expected.add (GLib.TokenType.CHAR);
          } else {
            throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid INSERT declaration: unexpected declaration at Line:Column : %d:%d"),
                                                      scanner.cur_line (), scanner.cur_position ());
          }
          break;
        case GLib.TokenType.INT:
          if (start_math_expression) {
            math_expression += scanner.cur_value ().@int.to_string ();
          } else if (enable_value) {
            start_math_expression = true;
            math_expression = "";
            math_expression += scanner.cur_value ().@int.to_string ();
            expected.clear ();
          }
          break;
        case GLib.TokenType.FLOAT:
          if (start_math_expression) {
            math_expression += "%g".printf (scanner.cur_value ().@float);
          } else if (enable_value) {
            start_math_expression = true;
            math_expression = "";
            math_expression += scanner.cur_value ().@int.to_string ();
            expected.clear ();
          }
          break;
        case GLib.TokenType.STRING:
          // Identifiers
          if (enable_value) {
            add_value (scanner.cur_value ().@string);
          }
          break;
        case GLib.TokenType.CHAR:
          var v = scanner.cur_value ().@char;
          if (v == '(') {
            if (start_math_expression) {
              math_expression += "(";
              open_math_parens = true;
            } else if (!enable_field && !parentesis_open && !values && table_setted) {
            	enable_field = true;
            	enable_value = false;
              parentesis_open = true;
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
            } else if (!enable_value && !parentesis_open && values) {
              parentesis_open = true;
              enable_value = true;
              enable_field = false;
              expected.clear ();
              expected.add (GLib.TokenType.STRING);
              expected.add (GLib.TokenType.CHAR);
              expected.add (GLib.TokenType.INT);
              expected.add (GLib.TokenType.FLOAT);
            }
          } else if (v == ')') {
            if (start_math_expression) {
              if (open_math_parens) {
                math_expression +=")";
                open_math_parens = false;
              } else {
                start_math_expression = false;
                var mathv = new Vda.ExpressionValueMath ();
                mathv.connection = connection;
                mathv.parse (math_expression);
                math_expression = "";
                // A Math Expression as Value
                if (enable_value) {
                  add_value (mathv);
                }
              }
            } else if (!parentesis_open) {
              throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid INSERT declaration: unexpected ')' at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
            } else if (parentesis_open && enable_field) {
	            parentesis_open = false;
	            enable_field = false;
	            enable_value = false;
	            expected.clear ();
	            expected.add (GLib.TokenType.IDENTIFIER);
	          } else if (parentesis_open && enable_value) {
	            parentesis_open = false;
	            enable_field = false;
	            enable_value = false;
	            expected.clear ();
	          }
          } if (v == ',') {
            // SQL expression separator
            if (start_math_expression && enable_value) {
              start_math_expression = false;
              var mathv = new Vda.ExpressionValueMath ();
              mathv.connection = connection;
              mathv.parse (math_expression);
              // A Math Expression as Value
              if (enable_value) {
                add_value (mathv);
              }
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
              expected.add (GLib.TokenType.CHAR);
              expected.add (GLib.TokenType.STRING);
              expected.add (GLib.TokenType.INT);
              expected.add (GLib.TokenType.FLOAT);
            } else if (parentesis_open && enable_field) {
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
            } else if (parentesis_open && enable_value) {
              enable_value = true;
              param_identifier = false;
              param_name = "";

              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
              expected.add (GLib.TokenType.CHAR);
              expected.add (GLib.TokenType.STRING);
              expected.add (GLib.TokenType.INT);
              expected.add (GLib.TokenType.FLOAT);
            }
          } else if (v == ':') {
          	if (parentesis_open && param_identifier) {
		          token = scanner.get_next_token ();
		          if (token == GLib.TokenType.CHAR) {
		            var nv = scanner.cur_value ().@char;
		            if (nv == ':') {
		              // SQL double colon for cast operations
		              if (!param_type && param_type_name == "") {
		                param_type = true;
		                expected.clear ();
		                expected.add (GLib.TokenType.IDENTIFIER);
		              }
		            }
		          } else {
		            throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid parameter declaration: expected ':' at Line:Column : %d:%d"),
		                                                        scanner.cur_line (), scanner.cur_position ());
		          }
	          } else {
	            throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid value declaration: unexpected ':' at Line:Column : %d:%d"),
	                                                        scanner.cur_line (), scanner.cur_position ());
	          }
          } else if (v == '#') {
            if (parentesis_open && enable_value && !param_identifier && param_name == "") {
              token = scanner.get_next_token ();
              if (token != GLib.TokenType.CHAR) {
                throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid Parameter declaration: expected '#' at Line:Column : %d:%d"),
                                                      scanner.cur_line (), scanner.cur_position ());
              }
              var vp = scanner.cur_value ().@char;
              if (vp != '#') {
                throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid Parameter declaration: expected '#' got '%s' at Line:Column : %d:%d"),
                                                      bstr.str, scanner.cur_line (), scanner.cur_position ());
              }
              param_identifier = true;
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
            }
          } else if (v == '-') {
          	if (!start_math_expression && parentesis_open && enable_value && values) {
          		start_math_expression = true;
          		bstr.assign ("");
              bstr.append_c ((char) v);
              math_expression += bstr.str;
          	}
          } else if (v == '+') {
          	if (!start_math_expression && parentesis_open && enable_value && values) {
          		start_math_expression = true;
          		bstr.assign ("");
              bstr.append_c ((char) v);
              math_expression += bstr.str;
          	}
          } else {
            if (start_math_expression) {
              bstr.assign ("");
              bstr.append_c ((char) v);
              math_expression += bstr.str;
              break;
            }
          }
          break;
        case GLib.TokenType.NONE:
        case GLib.TokenType.LEFT_PAREN:
        case GLib.TokenType.SYMBOL:
        case GLib.TokenType.COMMENT_MULTI:
        case GLib.TokenType.RIGHT_CURLY:
        case GLib.TokenType.BINARY:
        case GLib.TokenType.OCTAL:
        case GLib.TokenType.EQUAL_SIGN:
        case GLib.TokenType.RIGHT_PAREN:
        case GLib.TokenType.ERROR:
        case GLib.TokenType.LAST:
        case GLib.TokenType.LEFT_BRACE:
        case GLib.TokenType.EOF:
        case GLib.TokenType.COMMA:
        case GLib.TokenType.COMMENT_SINGLE:
        case GLib.TokenType.LEFT_CURLY:
        case GLib.TokenType.IDENTIFIER_NULL:
        case GLib.TokenType.RIGHT_BRACE:
        case GLib.TokenType.HEX:
        	break;
      }
    }
  }
}
