/*
 * AdbcQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2024 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

internal errordomain VdaGadbc.QueryError {
    FATAL_ERROR,
    STATEMENT_ERROR
}

internal class VdaGadbc.Query : Object, Vda.Query {
    protected string _sql = null;
    protected Vda.Connection cnc = null;
    internal GADBC.Statement stm = null;
    internal void *c_abi_array_stream = null;
    int64 n_rows_affected;
    GArrow.RecordBatchReader reader = null;

    public Query (Vda.Connection con) {
        cnc = con;
    }

    public Query.from_sql (Vda.Connection con, string sql) {
        cnc = con;
        _sql = sql;
    }

    ~Query () {
      if (c_abi_array_stream != null) {
        GLib.free (c_abi_array_stream);
        c_abi_array_stream = null;
      }
    }

    public string sql {
        owned get {
            if (_sql != null) {
                return _sql;
            } else {
                return render_sql ();
            }
        }
    }

    public Vda.Connection connection { get { return cnc; } }

    public async Vda.Result?
    execute (GLib.Cancellable? cancellable) throws GLib.Error {
        if (cnc == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Database is invalid"));
        }

        GADBC.Database db = ((VdaGadbc.Connection) cnc).database;

        if (db == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("SQLite Database is invalid"));
        }

        string xsql = _sql;
        bool is_result = false;

        if (this is VdaGadbc.ParsedQuery) {
            var cmd = ((VdaGadbc.ParsedQuery) this).command;
            if (cmd != null) {
                debug ("Binding using native VDA command");
                // Using native bind of parameters
                ((VdaGadbc.ParsedQuery) this).bind_parameters ();
                xsql = ((VdaGadbc.ParsedQuery) this).render_sql ();
                if (cmd is SqlCommandSelect) {
                  is_result = true;
                }
            }
        } else {
          // Try to autodetect the type of query
          if (_sql.has_prefix ("SELECT")) {
            is_result = true;
          }
        }

        stm = new GADBC.Statement (((VdaGadbc.Connection) cnc).connection);
        stm.set_sql_query (xsql);

        if (is_result) {
          if (!stm.execute (true, out c_abi_array_stream, out n_rows_affected)) {
            return new InvalidResult (_("Unable to execute query: \"%s\"").printf (xsql));
          }
          reader = GArrow.RecordBatchReader.import (c_abi_array_stream);
          return new VdaGadbc.TableModelSequential (this);
        } else {
          if (!stm.execute (false, out c_abi_array_stream, out n_rows_affected)) {
            return new InvalidResult (_("Unable to execute query \"%s\"").printf (xsql));
          }
          return new AffectedRows ((int) n_rows_affected);
        }

        return new InvalidResult(_("Invalid query"));
    }


    internal async void cancel () {
        warning (_("Not implemented"));
    }

    internal virtual string render_sql () {
        return _sql;
    }

    internal GArrow.RecordBatchReader get_reader () {
      return reader;
    }
}

class VdaGadbc.Result : GLib.Object, Vda.Result {
    protected VdaGadbc.Query _query;

    public Vda.Connection connection { get { return _query.connection; } }

    public Result (VdaGadbc.Query query) {
        _query = query;
    }
}

class VdaGadbc.TableModelSequential : VdaGadbc.Result, Vda.TableModelSequential
{
    internal GArrow.RecordBatchReader _reader;
    internal GArrow.RecordBatch _current;
    // GLib.ListModel interface
    internal Vda.RowModel? current () {
        if (_current == null) {
          return null;
        }
        return new VdaGadbc.RowModel (_query, _current);
    }
    // FIXME: We need a struct to copy values to and keep them
    // wihtout depend on the query
    internal Vda.RowModel? copy_current () {
        return null;
    }

    internal bool next () throws GLib.Error {
        _current = _reader.read_next ();
        return _current != null;
    }

    public TableModelSequential (VdaGadbc.Query q) {
      base (q);
      _reader = q.get_reader ();
    }
}

class VdaGadbc.RowModel : Object, GLib.ListModel, Vda.RowModel
{
    protected GArrow.RecordBatchReader _reader;
    protected VdaGadbc.Query _query;
    protected GArrow.RecordBatch _current;

    // Constructor
    public RowModel (VdaGadbc.Query query, GArrow.RecordBatch cur) {
        _query = query;
        _current = cur;
        _reader = query.get_reader ();
    }

    public uint n_columns {
        get {
            return _current.get_n_columns ();
        }
    }

    public Vda.ColumnModel? get_column (string name) throws GLib.Error {
        for (int i = 0; i < n_columns; i++) {
          if (_current.get_column_name (i) == name) {
            return new VdaGadbc.ColumnModel (_query, _current, i);
          }
        }

        return null;
    }

    public Vda.ColumnModel? get_column_at (uint col) throws GLib.Error  {
        if (col > _current.get_n_columns ()) {
          throw new Vda.RowModelError.
                  INVALID_COLUMN_NUMBER_ERROR (_("Invalid column number: %u"), col);
        }

        return new VdaGadbc.ColumnModel (_query, _current, col);
    }

    public SqlValue? get_value (string name) throws GLib.Error {
        for (int i = 0; i < n_columns; i++) {
          if (_current.get_column_name (i) == name) {
            return get_value_at (i);
          }
        }

        return null;
    }

    public SqlValue? get_value_at (uint col) throws GLib.Error {
        if (col > _current.get_n_columns ()) {
          throw new Vda.RowModelError.INVALID_COLUMN_NUMBER_ERROR (_("Invalid column number: %u"), col);
        }

        GArrow.Array data = _current.get_column_data ((int) col);
        var type = data.get_value_type ();
        switch (type) {
            case NA:
              return new Vda.ValueArrowNull.from_query (_query, (GArrow.NullArray) data);
            case BOOLEAN:
              return new Vda.ValueArrowBoolean.from_query (_query, (GArrow.BooleanArray) data);
            case UINT8:
              return new Vda.ValueArrowUInt8.from_query (_query, (GArrow.UInt8Array) data);
            case INT8:
              return new Vda.ValueArrowInt8.from_query (_query, (GArrow.Int8Array) data);
            case UINT16:
              return new Vda.ValueArrowUInt16.from_query (_query, (GArrow.UInt16Array) data);
            case INT16:
              return new Vda.ValueArrowInt16.from_query (_query, (GArrow.Int16Array) data);
            case UINT32:
              return new Vda.ValueArrowInt32.from_query (_query, (GArrow.Int32Array) data);
            case INT32:
              return new Vda.ValueArrowUInt32.from_query (_query, (GArrow.UInt32Array) data);
            case UINT64:
              return new Vda.ValueArrowUInt64.from_query (_query, (GArrow.UInt64Array) data);
            case INT64:
              return new Vda.ValueArrowInt64.from_query (_query, (GArrow.Int64Array) data);
            case HALF_FLOAT:
              return new Vda.ValueArrowHalfFloat.from_query (_query, (GArrow.HalfFloatArray) data);
            case FLOAT:
              return new Vda.ValueArrowFloat.from_query (_query, (GArrow.FloatArray) data);
            case DOUBLE:
              return new Vda.ValueArrowDouble.from_query (_query, (GArrow.DoubleArray) data);
            case STRING:
              return new Vda.ValueArrowString.from_query (_query, (GArrow.StringArray) data);
            case BINARY:
              return new Vda.ValueArrowBinary.from_query (_query, (GArrow.BinaryArray) data);
            case FIXED_SIZE_BINARY:
              //return new Vda.ValueArrowFixedSizeBinary (data);
            case DATE32:
              return new Vda.ValueArrowDate32.from_query (_query, (GArrow.Date32Array) data);
            case DATE64:
              return new Vda.ValueArrowDate64.from_query (_query, (GArrow.Date64Array) data);
            case TIMESTAMP:
              return new Vda.ValueArrowTimestamp.from_query (_query, (GArrow.TimestampArray) data);
            case TIME32:
              return new Vda.ValueArrowTime32.from_query (_query, (GArrow.Time32Array) data);
            case TIME64:
              return new Vda.ValueArrowTime64.from_query (_query, (GArrow.Time64Array) data);
            case MONTH_INTERVAL:
              //return new Vda.ValueArrowMonthInterval (data);
            case DAY_TIME_INTERVAL:
              //return new Vda.ValueArrowDayTimeInterval (data);
            case DECIMAL128:
              //return new Vda.ValueArrowDecimal128 (data);
            case DECIMAL256:
              //return new Vda.ValueArrowDecimal128 (data);
            case LIST:
              //return new Vda.ValueArrowList (data);
            case STRUCT:
              //return new Vda.ValueArrowStruct (data);
            case SPARSE_UNION:
              //return new Vda.ValueArrowSparseUnion (data);
            case DENSE_UNION:
              //return new Vda.ValueArrowDenseUnion (data);
            case DICTIONARY:
              //return new Vda.ValueArrowDictionary (data);
            case MAP:
              //return new Vda.ValueArrowMap (data);
            case EXTENSION:
              //return new Vda.ValueArrowExtension (data);
            case FIXED_SIZE_LIST:
              //return new Vda.ValueArrowFixedSizeList (data);
            case DURATION:
              //return new Vda.ValueArrowDuration (data);
            case LARGE_STRING:
              //return new Vda.ValueArrowLargeString (data);
            case LARGE_BINARY:
              //return new Vda.ValueArrowLargeBinary (data);
            case LARGE_LIST:
              //return new Vda.ValueArrowLargeList (data);
            case MONTH_DAY_NANO_INTERVAL:
              //return new Vda.ValueArrowMonthDayNanoInterval (data);
            case RUN_END_ENCODED:
              return new Vda.ValueNull ();
        }

        return null;
    }

    public string? get_string (string name) throws GLib.Error {
        if (name == null) {
            return null;
        }

        var v = get_value (name);
        if (v == null) {
            return null;
        }

        return v.to_string ();
    }

    public string? get_string_at (uint col) throws GLib.Error {
        var v = get_value_at (col);
        if (v == null) {
            return null;
        }

        return v.to_string ();
    }

    // GLib.ListModel interface
    public uint get_n_items () { return n_columns; }

    public GLib.Type get_item_type () { return typeof (ColumnModel); }

    public GLib.Object? get_item (uint position) {
        if (position > n_columns)
            return null;
        Vda.ColumnModel col = null;
        try {
            col = get_column_at (position);
        } catch (GLib.Error e) {
            warning (_("Error: %s"), e.message);
        }

        return col;
    }
}

class VdaGadbc.ColumnModel : Object, Vda.ColumnModel {
    uint col = -1;
    VdaGadbc.Query _query;
    protected GArrow.RecordBatchReader _reader;
    GArrow.RecordBatch _current;

    public ColumnModel (VdaGadbc.Query query, GArrow.RecordBatch record, uint col) {
        this.col = col;
        _query = query;
        _current = record;
        _reader = query.get_reader ();
    }

    public string name {
        get {
            return _current.get_column_name ((int) col);
        }
    }
    public GLib.Type data_type {
        get {
            var schema = _reader.get_schema ();
            var field = schema.get_field (col);
            var t = Vda.ValueArrow.data_type_to_gtype (field.get_data_type ());
            return t;
        }
    }
}
