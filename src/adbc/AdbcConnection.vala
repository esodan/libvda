/*
 * AdbcConnection.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2024 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;

public errordomain VdaGadbc.ConnectionError {
    CONNECTION_ERROR,
    INVALID_DRIVER_ERROR,
    PARAMETER_MISSING_ERROR,
    QUERY_CREATION_ERROR
}

/**
 * Implementation of {@link Vda.Connection} for a direct connection to Apache Arrow ADBC.
 *
 * Connection happens by providing following connection parameters:
 *
 * 1. "DRIVER"
 *
 * When used on Arrow ADBC SQLite connection, set 'driver' parameter to 'adbc_driver_sqlite'
 * and add following parameter:
 * 1. "URI" (use an URI to the database file or ':memory:' for in memory database
 *
 *  {{{
 * var c = new VdaGadbc.Connection ();
 * c.open_from_string ("DRIVER=adbc_driver_sqlite; URI=:memory:");
 * }}}
 */
public class VdaGadbc.Connection : GLib.Object, Vda.Connection {
    private GADBC.Connection _connection;
    private GADBC.Database _database;
    private Vda.Connection.Status _status = Vda.Connection.Status.DISCONNECTED;
    private Gee.HashMap<string,PreparedQuery> _queries = new Gee.HashMap<string,PreparedQuery> ();
    private string _cnc_string = "";

    public GADBC.Connection connection { get { return _connection; } }
    public GADBC.Database database { get { return _database; } }

    // Vda.Connection
    internal Vda.Connection.Status status { get { return _status; } }
    internal ConnectionParameters parameters { get; set; }
    internal bool is_opened {
        get  {
            return database != null && _status == Vda.Connection.Status.CONNECTED;
        }
    }
    internal string connection_string {
        get {
            _cnc_string = parameters.to_string ();
            return _cnc_string;
        }
    }

    internal async void
    close () throws GLib.Error {
        if (_database == null) return;
        _database.release ();
        _database = null;
    }
    internal async Vda.Connection.Status
    open () throws GLib.Error
    {
        debug ("Checking connection parameters...");
        if (parameters == null) {
            _status = Vda.Connection.Status.DISCONNECTED;
            return Vda.Connection.Status.CANCELED;
        }

        if (!parameters.has_param ("DRIVER")) {
            throw new VdaGadbc.ConnectionError.INVALID_DRIVER_ERROR (_("A driver is required to open a connection to a ADBC source"));
        }

        string drv = parameters.get ("DRIVER").@value;
        debug ("Checking required DRIVER: %s", drv);
        switch (drv) {
          case "adbc_driver_sqlite":
            message("Open database using SQLite Driver");
            open_sqlite_driver ();
            break;
          default:
            throw new VdaGadbc.ConnectionError.INVALID_DRIVER_ERROR (_("Invalid driver requested: %s"), drv);
        }

        if (_status == Vda.Connection.Status.CONNECTED)
          opened ();

        return _status;
    }

    internal async Vda.Connection.Status open_from_string (string cnc_string) throws GLib.Error {
        parameters = new ConnectionParameters (cnc_string);
        return yield open ();
    }

    internal Vda.Query
    parse_string (string sql) throws GLib.Error {
        var q = new VdaGadbc.Query.from_sql (this, sql);
        return q;
    }

    internal Vda.PreparedQuery?
    parse_string_prepared (string? name, string sql) throws GLib.Error {
        var q = new VdaGadbc.ParsedQuery (this, name != null ? name : _queries.size.to_string ());
        q.parse (sql);
        _queries.set (name, q);
        return q;
    }

    internal Vda.PreparedQuery? get_prepared_query (string name) {
        return _queries.get (name);
    }

    internal Vda.PreparedQuery?
    query_from_command (SqlCommand cmd, string? name) throws GLib.Error {
        if (this != cmd.connection) {
            throw new
                    VdaGadbc
                    .ConnectionError
                    .QUERY_CREATION_ERROR (_("Command is not using same connection"));
        }
        return new VdaGadbc.ParsedQuery.from_command (cmd, name);
    }

    internal void open_sqlite_driver () throws GLib.Error {
        _status = Vda.Connection.Status.DISCONNECTED;
        if (!parameters.has_param ("URI")) {
            throw new VdaGadbc.ConnectionError.PARAMETER_MISSING_ERROR (_("ADBC SQLite driver, requires the database's URI"));
        }
        string uri = parameters.get ("URI").@value;
        if (uri == null) {
          throw new
                VdaGadbc
                .ConnectionError
                .CONNECTION_ERROR (_("GADBC SQLite driver requires 'uri' parameter"));
        }
        _database = new GADBC.Database ();
        _database.set_option ("driver", "adbc_driver_sqlite");
        _database.set_option ("uri", uri);
        _database.init ();
        _connection = new GADBC.Connection ();
        _connection.init (database);
        _status = Vda.Connection.Status.CONNECTED;
    }

}

