/*
 * AdbcParsedQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2024 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represent a prepared query implementing {@link Vda.PreparedQuery}.
 *
 * Values required by query can be set by using paramenters property.
 */
 internal class VdaGadbc.ParsedQuery : VdaGadbc.Query, Vda.PreparedQuery, Vda.ParsedQuery {
    protected string _name;
    protected Vda.SqlCommand _command;
    protected Vda.SqlParameters _params = new Vda.Parameters ();

    public ParsedQuery (Vda.Connection con, string name) {
        base (con);
        _name = name;
        cnc = con;
    }
    public ParsedQuery.from_command (Vda.SqlCommand cmd, string? name) {
        base (cmd.connection);
        _name = name;
        _command = cmd;
        debug ("Query created using a Vda.SqlCommand");
    }
    /**
     * Parse an SQL using {@link Vda.Parser}
     */
    public void parse (string sql) throws GLib.Error {
        if (_command != null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Can't change the internal query definition"));
        }

        _sql = sql;

        debug ("String to parse: %s", _sql);
        if (Regex.match_simple("##\\w*::\\w*", _sql)) {
            debug ("Using VDA's internal SQL parser");
            _command = new Vda.Parser ().parse (_sql, cnc);
        } else {
          // FIXME:
        }
    }

    // Query
    public override string render_sql () {
        if (_command != null && _command is Vda.Stringifiable) {
            return ((Vda.Stringifiable) _command).to_string ();
        } else if (_command == null) {
            if (stm == null) {
                warning (_("Unable to render SQL: no statement were prepared"));
                return "";
            }

            bind_parameters ();
            return "";
        }

        return "";
    }
    // PreparedQuery
    public string name { get { return _name; } }
    public Vda.SqlParameters parameters {
        get {
            if (_command != null && _command is Vda.SqlCommandParametrized) {
                return ((Vda.SqlCommandParametrized) _command).parameters;
            }

            return _params;
        }
    }

    internal void bind_parameters () {
        debug ("Setting parameters' values");
        debug ("Parameters to set: %d", ((Gee.HashMap<string,Vda.SqlValue>) parameters).size);
        foreach (string k in ((Gee.HashMap<string,Vda.SqlValue>) parameters).keys) {
            var v = ((Gee.HashMap<string,Vda.SqlValue>) parameters).get (k);
            debug ("Applying binding for value: %s", v.to_string ());
            if (v is Vda.SqlValueInteger) {
            } else if (v is Vda.SqlValueString) {
            } else if (v is Vda.SqlValueDouble) {
            } // FIXME: Add more conversions
        }
    }

    // ParsedQuery
    public Vda.SqlCommand command {
        get {
            return _command;
        }
    }
}
