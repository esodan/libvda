/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * ConnectionTransactional.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represents a database engine connection, with transactions capabilities
 */
public interface Vda.ConnectionTransactional : GLib.Object {
  /**
   * Stablish a point where is possible to rollback
   */
  public abstract bool add_savepoint (string? name) throws GLib.Error;
  /**
   * Deletes a point where was possible to rollback
   */
  public abstract bool delete_savepoint (string? name) throws GLib.Error;
  /**
   * Rollback to a predefined savepoint
   */
  public abstract bool rollback_savepoint (string? name) throws GLib.Error;
  /**
   * Starts a transaction with the option to rollback to
   */
  public abstract bool begin_transaction (string? name) throws GLib.Error;
  /**
   * Commits all operations in transaction and ends.
   */
  public abstract bool commit_transaction (string? name) throws GLib.Error;
  /**
   * Rollback transaction to the last point stablished by {@link begin_transaction}
   */
  public abstract bool rollback_transaction (string? name) throws GLib.Error;
}

