/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlCommandDelete.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an SQL DELETE command
 */
public interface Vda.SqlCommandDelete : Object,
																				SqlCommandTableRelated,
																				SqlCommandConditional,
																				SqlCommand,
																				SqlCommandParametrized
{
	/**
	 * Creates a string representation of a {@link SqlCommandDelete}
	 */
	public virtual string stringify () throws GLib.Error {
		if (table == null) {
			throw new SqlCommandError.INVALID_STRUCTURE_ERROR (_("No table is given"));
		}
		string str = "DELETE FROM "+table;
    if (condition != null && condition.get_n_items () != 0) {
    	str += " WHERE " + condition.to_string ();
    }
    return str;
	}
	/**
	 * Create a {@link Query} for execution, using internal structure.
	 *
	 * If {@link SqlExpressionValueParameter} object is used for values in condition,
	 * then a {@link PreparedQuery} is returned with the given name.
	 */
	public virtual Query to_query (string? name = null) throws GLib.Error {
    string str = stringify ();
    return connection.parse_string_prepared (name, str);
  }
  /**
   * Parse SQL string commands and construct its internal tree
   */
  public virtual void parse (string sql)  throws GLib.Error {
		Gee.ArrayList<GLib.TokenType> expected = new Gee.ArrayList<GLib.TokenType> ();
    var scanner = new GLib.Scanner (null);
    scanner.input_name = "SQL";
    scanner.input_text (sql, sql.length);
    scanner.config.cpair_comment_single = "//\n";
    scanner.config.skip_comment_multi = false;
    scanner.config.skip_comment_single = false;
    scanner.config.char_2_token = false;
    scanner.config.scan_binary = false;
    scanner.config.scan_octal = false;
    scanner.config.scan_float = false;
    scanner.config.scan_hex = false;
    scanner.config.scan_hex_dollar = false;
    scanner.config.numbers_2_int = false;
    GLib.TokenType token = GLib.TokenType.NONE;
    bool complete_init = false;
    bool table_setted = false;
    bool expected_table_name = false;
    bool set_allias = false;
    bool from = false;
    bool starting = true;
    while (token != GLib.TokenType.EOF) {
      token = scanner.get_next_token ();
      if (token == GLib.TokenType.EOF) {
        break;
      }
      if (expected.size != 0 && !expected.contains (token)) {
        throw new ParserError.INVALID_TOKEN_ERROR (_("Found an unexpected expression at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
      }
      switch (token) {
        case GLib.TokenType.IDENTIFIER:
          string identifier = scanner.cur_value ().@identifier;
          // SQL commands and identifiers
          if (starting) {
            if (identifier.down () != "delete") {
              throw new ParserError.INVALID_TOKEN_ERROR (_("SQL delete command should start with DELETE"));
            }
            starting = false;
          } else if (expected_table_name) {
            if (!table_setted) {
              table = scanner.cur_value ().@string;
              table_setted = true;
              set_allias = false;
              expected_table_name = false;
            } else {
              if (identifier.down () == "as" && !set_allias) {
                set_allias = true;
              } else if (set_allias && identifier.down () == "where") {
              	throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid DELETE declaration: expected table allias at Line:Column : %d:%d"),
                                                      scanner.cur_line (), scanner.cur_position ());
              } else {
              	allias = identifier;
              	set_allias = false;
              }
            }
          } else if (!complete_init && !table_setted && !from) {
            if (identifier.down () == "from") {
		          complete_init = true;
		          expected_table_name = true;
              from = true;
            } else {
              throw new ParserError.INVALID_TOKEN_ERROR (_("Invalid DELETE declaration: expected FROM keyword at Line:Column : %d:%d"),
                                                      scanner.cur_line (), scanner.cur_position ());
            }
          } else if (!expected_table_name && identifier.down () == "where") {
            int start = scanner.cur_position ();
		        string rest = sql.substring (start, sql.length - start);
		        SqlExpression cond = SqlExpression.parse (rest, connection, parameters);
		        condition.add_expression (cond);
		        return;
          }
          break;
        case GLib.TokenType.NONE:
        case GLib.TokenType.LEFT_PAREN:
        case GLib.TokenType.SYMBOL:
        case GLib.TokenType.COMMENT_MULTI:
        case GLib.TokenType.RIGHT_CURLY:
        case GLib.TokenType.BINARY:
        case GLib.TokenType.OCTAL:
        case GLib.TokenType.EQUAL_SIGN:
        case GLib.TokenType.FLOAT:
        case GLib.TokenType.RIGHT_PAREN:
        case GLib.TokenType.ERROR:
        case GLib.TokenType.LAST:
        case GLib.TokenType.LEFT_BRACE:
        case GLib.TokenType.INT:
        case GLib.TokenType.EOF:
        case GLib.TokenType.COMMA:
        case GLib.TokenType.STRING:
        case GLib.TokenType.COMMENT_SINGLE:
        case GLib.TokenType.LEFT_CURLY:
        case GLib.TokenType.CHAR:
        case GLib.TokenType.IDENTIFIER_NULL:
        case GLib.TokenType.RIGHT_BRACE:
        case GLib.TokenType.HEX:
        	break;
      }
    }
  }
}

