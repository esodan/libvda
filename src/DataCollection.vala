/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * DataCollection.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implementators will be able to execute a SELECT to get all objects
 * in the database related to a {@link parent} object.
 *
 * {{{
 *
 * public class Address : Object, Vda.DataObject {
 *    // DataObject
 *    public string database_table_name { get; construct set; }
 *    public Vda.Connection database_connection { get; set; }
 *    public Cancellable cancellable { get; set; }
 *
 *    // Database mapping
 *    [Description (nick="@id::pkey::auto")]
 *    public int id { get; set; }
 *    [Description (nick="@street")]
 *    public string street { get; set; }
 *    [Description (nick="@client::id")]
 *    public int client { get; set; }
 *    construct {
 *      database_table_name = "address";
 *    }
 *  }
 *  public class Addresses : Object, Vda.DataCollection {
 *    private string _parent_property;
 *    private DataObject _parent;
 *    private GLib.Type _object_type;
 *    private string _ref_field;
 *
 *    public Vda.Connection database_connection { get; set; }
 *    public string parent_property { get { return _parent_property; } }
 *    public DataObject parent { get { return _parent; } }
 *    public GLib.Type object_type { get { return _object_type; } }
 *    public string ref_field{ get { return _ref_field; } }
 *    public Cancellable cancellable { get; set; }
 *
 *    construct  {
 *      _parent_property = "id";
 *      _ref_field = "client";
 *      _object_type = typeof (Address);
 *    }
 *
 *    public Addresses (Client client) {
 *      _parent = client;
 *    }
 *  }
 *
 *  public class Client : Object, Vda.DataObject {
 *    Vda.Connection _cnc = null;
 *    // DataObject
 *    public string database_table_name { get; construct set; }
 *    public Vda.Connection database_connection {
 *      get {
 *        return _cnc;
 *      }
 *      set {
 *        _cnc = value;
 *        addresses.database_connection = _cnc;
 *      }
 *    }
 *    public Cancellable cancellable { get; set; }
 *
 *    // Database mapping
 *   [Description (nick="@id::pkey::auto")]
 *    public int id { get; set; }
 *    [Description (nick="@name")]
 *    public string name { get; set; }
 *    [Description (nick="@description")]
 *    public string description { get; set; }
 *    [Description (nick="@phone::id")]
 *    public string phone { get; set; }
 *    public Addresses addresses { get; set; }
 *
 *    construct {
 *     database_table_name = "clients";
 *      addresses = new Addresses (this);
 *    }
 *  }
 * }}}
 *
 * Using the above code a query, by accessing the Addresses
 * object, you can call {@link get_objects} and a the following
 * query will be executed:
 *
 * {{{
 * SELECT * FROM address WHERE address.client = 100;
 * }}}
 *
 * Above number 100, is calculated from the current instance
 * of Client object's id property.
 */
public interface Vda.DataCollection : Object {
	/**
	 * Get Database connection
	 */
	public abstract Vda.Connection database_connection { get; set; }
  /**
   * Conical name of the parent's property to be used in the filter.
   */
  public abstract string parent_property { get; }
  /**
   * Parent of the collection, where there is a one to many relation
   * between the {@link DataObject} representing a single row
   * in a table and a set of rows in another table.
   */
  public abstract DataObject parent { get; }
  /**
   * The target Object's {@link GLib.Type} of a {@link DataObject}
   * to get information from like: the database's table's name.
   * Internally a temporally instance is created to access the required
   * information.
   */
  public abstract GLib.Type object_type { get; }
  /**
   * Is a canonical property's name in the target {@link DataObject},
   * to be used in the filter to find it the row in the table has
   * relation with the {@link parent}
   *
   * A condition is added to a SELECT command in the form
   * parent_table.parent_property = target_object_table.ref_field
   *
   * See at {@link parent} and {@link parent_property} for more information.
   */
  public abstract string ref_field { get; }
  /**
   * Tries to cancell the execution of a SELECT command.
   */
  public abstract Cancellable cancellable { get; set; }
  /**
   * Builds and execute a SELECT command with the given parameters,
   * and returns a  {@link TableModel} with all related rows.
   */
  public virtual async TableModel get_objects () throws GLib.Error {
    unowned ObjectClass pc = parent.get_class ();
    weak ParamSpec? pp = pc.find_property (parent_property);
    if (pp == null) {
      throw new DataCollectionError.INVALID_PROPERTY_ERROR (_("Invalid parent's property: '%s'"), parent_property);
    }
    GLib.Value pval = GLib.Value (pp.value_type);
    parent.get_property (pp.name, ref pval);
    var to = Object.new (object_type) as DataObject;
    to.database_connection = database_connection;
    unowned ObjectClass oc = to.get_class ();
    weak ParamSpec? p = oc.find_property (ref_field);
    if (p == null) {
      throw new DataCollectionError.INVALID_PROPERTY_ERROR (_("Referenced object's property is invalid: '%s'"), ref_field);
    }
    string fn = p.get_nick ();
    if (!("@" in fn)) {
      throw new DataCollectionError.INVALID_PROPERTY_ERROR (_("Object's property reference is not marked as a database's field using '@' nickname prefix for: '%s'"), ref_field);
    }
    var q = DataObject.create_select_all (to);
    var cond = q.condition as SqlExpressionOperator;
    fn = fn.replace ("@", "");
    fn = fn.replace ("::id", "");
    fn = fn.replace ("::auto", "");
    fn = fn.replace ("::pkey", "");
	  var fe = cond.create_field_expression (fn);
	  var fv = cond.create_value_expression (pval, database_connection);
	  cond.add_eq_operator (fe, fv);
	  var res = yield q.to_query ().execute (cancellable);
	  return res as TableModel;
  }
}

/**
 * Data Collection error codes
 */
public errordomain Vda.DataCollectionError {
  INVALID_PROPERTY_ERROR
}
