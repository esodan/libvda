/*
 * SqlExpressionValueMath.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2023 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an SQL Math Expression value
 */
[Version (since="1.2")]
public interface Vda.SqlExpressionValueMath  : Object, SqlExpression, SqlExpressionValue
{
    /*
     * Parse a string to generate a {@link Vda.ValueMathExp}
     */
    public abstract void parse (string str);
}
