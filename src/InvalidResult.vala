/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * InvalidResult.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an invalid {@link Result} after execute a query
 */
public class Vda.InvalidResult : GLib.Object, Result {
  /**
   * Message about the an executed query.
   *
   * May contain an error message
   */
  public string message { get; construct set; }

  /**
   * Construct a invalid result with a message
   */
  public InvalidResult (string msg) {
    message = msg;
  }
}

