/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlExpressionValueParameter.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent a value associated to a parameter in an SQL expression
 *
 * The default string representation if no parameter with the same name
 * is found is: ##parameter_name::parameter_type, where 'parameter_name'
 * is the actual name and 'parameter_type' is the {@link GLib.Type}'s name,
 * see {@link gtype_from_string} for some custom type's names supported when
 * {@link SqlExpression.parse} is used to parse from a string.
 *
 * In the following example, a SELECT query is created with a paramenter
 * with the name 'id' and type 'integer' of type {@link GLib.Type.INT}
 * {{{
 * var sql = "SELECT * WHERE id = ##id::integer";
 * var q = connection.parse_string_prepared (sql);
 * q.parameters.set_Value ("id", 10);
 * var res = q.execute (null);
 * }}}
 */
public interface Vda.SqlExpressionValueParameter  : Object, SqlExpression, SqlExpressionValue
{
	/**
	 * A set of {@link Vda.SqlParameters} this paramenter belongs to
	 */
	public abstract SqlParameters parameters { get; set; }
	/**
	 * The paramenter's name
	 */
	public abstract string name   { get; set; }
	/**
	 * The paramenter's type as {@link GLib.Type}
	 */
	public abstract Type   gtype  { get; set; }
	/**
	 * Parse a string representing a parameter declaration.
	 *
	 * Parameters should be declared as: ##param_name::param_type
	 * where param_name is the name of the parameter and param_type
	 * is the parameter's type's name.
	 */
	public virtual void parse (string str) throws GLib.Error {
		if (!str.has_prefix ("##")) {
			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Invalid parameter declaration: expected ## as prefix"));
		}
		if ("::" in str) {
			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Invalid parameter declaration: expected '::' as name and type separator"));
		}
		var nstr = str.replace ("##", "");
		string[] parts = nstr.split ("::");
		if (parts.length != 2) {
			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Invalid parameter declaration: more than one '::' is used"));
		}
		Type ptype = Type.from_name (parts[1]);
		if (ptype == Type.INVALID) {
			ptype = gtype_from_string (parts[1]);
			if (ptype == Type.INVALID) {
				throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Invalid parameter declaration: invalid type's nmae"));
			}
		}
		name = parts[0];
		gtype = ptype;
	}
	/**
	 * Returns a {@link GLib.Type} based on given string.
	 *
	 * Standard GLib names are supported and some equivalent extenssions:
	 *
	 * 1.  integer for gint
	 * 1.  uinteger for guint
	 * 1.  float for gfloat
	 * 1.  double for gdouble
	 * 1.  string for gchararray
	 */
	public static Type gtype_from_string (string str) {
		Type t = Type.from_name (str);
		if (t == Type.INVALID) {
			switch (str.down ()) {
				case "integer":
					t = typeof (int);
					break;
				case "uinteger":
					t = typeof (uint);
					break;
				case "float":
					t = typeof (float);
					break;
				case "double":
					t = typeof (double);
					break;
				case "string":
					t = typeof (string);
					break;
				default:
					t = Type.INVALID;
					break;
			}
		}
		return t;
	}
}
