/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * CommandModification.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Implementation of {@link SqlCommandModification}
 */
public class Vda.CommandModification  : Object,
															SqlCommand,
															SqlCommandTableRelated,
															SqlCommandModification,
															SqlCommandParametrized
{
	HashModel _fields = new HashList ();
	HashModel _values = new HashList ();
	Connection _connection;
	Parameters _parameters;

	construct {
		_parameters = new Parameters ();
	}

	public CommandModification (Connection cnc) {
		_connection = cnc;
	}
	// SqlCommand
	internal Connection connection { get { return _connection; } }
	// SqlTableRelatedCommand
	internal string table { get; set; }
  internal string allias { get; set; }
  // SqlCommandModification
  internal HashModel fields { get { return _fields; } }
  internal HashModel values { get { return _values; } }
	internal void add_field_value (string name, GLib.Value? val) {
		var f = new ExpressionField ();
		f.name = name;
		_fields.add (f);
		var ve = new ExpressionOperator ();
		var v = ve.create_value_expression (val, connection);
		_values.add (v);
	}
	internal void add_field (string name) throws GLib.Error {
		var f = new ExpressionField ();
		f.name = name;
		_fields.add (f);
	}
	internal void add_value (GLib.Value? val)  throws GLib.Error {
		if (((Gee.Collection) _values).size + 1 > ((Gee.Collection) _fields).size) {
			throw new SqlCommandError.INVALID_STRUCTURE_ERROR (_("New value will not pair with a field"));
		}
		var ve = new ExpressionOperator ();
		var v = ve.create_value_expression (val, connection);
		
		_values.add (v);
	}

	internal void add_field_parameter_value (string field, string par, Type gtype) {
		var f = new ExpressionField ();
		f.name = field;
		_fields.add (f);
		var v = new ExpressionValueParameter ();
		v.name = par;
		v.gtype = gtype;
		_values.add (v);
	}
	internal void add_parameter (string par, Type gtype)  throws GLib.Error {
		if (((Gee.Collection) _values).size + 1 > ((Gee.Collection) _fields).size) {
			throw new SqlCommandError.INVALID_STRUCTURE_ERROR (_("New parameter will not pair with a field"));
		}
		var v = new ExpressionValueParameter ();
		v.name = par;
		v.gtype = gtype;
		_values.add (v);
	}
	// SqlCommandParametrized
	internal SqlParameters parameters {
		get {
			return _parameters;
		}
	}
}

