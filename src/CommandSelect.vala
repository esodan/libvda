/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * CommandSelect.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Implementation of {@link SqlCommandSelect}
 */
public class Vda.CommandSelect  : Object,
															SqlCommand,
															SqlCommandConditional,
															Stringifiable,
															SqlCommandParametrized,
															SqlCommandSelect
{
	HashModel _fields = new HashList ();
	HashModel _tables = new HashList ();
	Connection _connection;
	Parameters _parameters;

	public CommandSelect (Connection cnc) {
		_connection = cnc;
	}
	// Stringifiable
	internal string to_string () {
		string str = "";
		try {
			str = stringify ();
		} catch (GLib.Error e) {
			warning (_("Fail to convert to string: %s"), e.message);
		}
		return str;
	}
	// SqlCommand
	internal Connection connection { get { return _connection; } }
	// SqlConditionalCommand
	private SqlExpression _condition = new ExpressionOperatorAnd ();
  internal SqlExpression condition { get { return _condition; } }
  // SqlSelectCommand
	internal HashModel fields { get { return _fields; } }
	internal HashModel tables { get { return _tables; } }
	internal void add_field (string field, string? table_ref, string? allias = null) {
		for (int i = 0; i < fields.get_n_items (); i++) {
			var f = fields.get_item (i) as SqlExpressionField;
			if (f == null) continue;
			if (field.down () == f.name.down ()) {
				if (allias == null && f.allias == null && table_ref == null && f.table_ref == null) {
					return;
				} else if (allias == null && f.allias == null && table_ref != null && f.table_ref != null) {
					if (table_ref.down () == f.table_ref.down ()) {
						return;
					}
				} else if (allias != null && f.allias != null&& table_ref == null && f.table_ref == null) {
					if (allias.down () == f.allias.down ()) {
						return;
					}
				} else if (allias != null && f.allias != null&& table_ref != null && f.table_ref != null) {
					if (allias.down () == f.allias.down () && table_ref.down () == f.table_ref.down ()) {
						return;
					}
				}
			}
		}
		if (table_ref != null && table_ref != "") {
			bool found = false;
			for (int i = 0; i < tables.get_n_items (); i++) {
				var t = tables.get_item (i) as SqlTableReference;
				if (t == null) continue;
				if (t.name.down () == table_ref.down ()) {
					found = true;
					break;
				}
				if (t.allias != null) {
					if (t.allias.down () == table_ref.down ()) {
						found = true;
						break;
					}
				}
			}
			if (!found) {
				var tr = new TableReference ();
				tr.name = table_ref;
				tables.add (tr);
			}
		}
		var f = new ExpressionField ();
		f.table_ref = (table_ref != null && table_ref != "" ? table_ref : null);
		f.name = field;
		if (allias != null) {
			f.allias = allias;
		}
		fields.add (f);
	}
	internal void add_table (string name, string? allias = null) {
		bool found = false;
		for (int i = 0; i < tables.get_n_items (); i++) {
			var t = tables.get_item (i) as SqlTableReference;
			if (t == null) continue;
			if (t.name.down () == name.down ()
				|| (t.allias != null && t.allias.down () == allias.down ())
				) {
				found = true;
				break;
			}
			if (allias != null) {
				if (t.name.down () == allias.down ()) {
					tables.remove (t);
					found = false;
					break;
				}
			}
		}
		if (!found) {
			var tr = new TableReference ();
			tr.name = name;
			tr.allias = allias;
			tables.add (tr);
		}
	}
	internal void add_value_field (GLib.Value val, string? allias) {
		string colname = allias;
		if (colname == null) {
			colname = "?Column%u?".printf (fields.get_n_items ());
		}
		var v = new ExpressionValue ();
		SqlValue sqlv = new Vda.Value ();
		if (val.holds (typeof (GCalc.Expression))) {
			sqlv = new ValueMathExp ();
			sqlv.from_value (val);
		} else {
			((Vda.Value) sqlv).force_value (val);
		}
		v.@value = sqlv;
		fields.add (v);
	}
	internal void add_math_exp_field (string exp, string? allias) throws GLib.Error {
		var expm = new GCalc.EquationManager ();
    var mparser = new GCalc.Parser ();
    mparser.parse (exp, expm);
    add_value_field (expm, allias);
	}
	// SqlParametrizedCommand
	internal SqlParameters parameters {
		get {
			if (_parameters == null) {
				_parameters = new Parameters ();
			}
			return _parameters;
		}
	}
}

