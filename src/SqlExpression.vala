/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlExpression.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an expression in an SQL command.
 */
public interface Vda.SqlExpression  : Object, GLib.ListModel
{
	/**
	 * Add an expression as a child expression
	 */
	public abstract void add_expression (SqlExpression exp);
	/**
	 * Removes an expression as a child expression
	 */
	public abstract void remove_expression (SqlExpression exp);
	/**
	 * Creates a string representation of this expression.
	 *
	 * If the expressions takes its value from a parameter's value,
	 * like {@link SqlExpressionValueParameter} and is set, the returned
	 * string is the parameter's string representation instead.
	 */
	public abstract string to_string ();
	/**
	 * Add an string math expression as a child expression.
	 *
	 * A {@link SqlExpressionValue} is created and added as a child expression and
	 * its {@link SqlExpressionValue.connection} is set to given {@link Connection}
	 */
	public virtual void add_math_expression (string str, Connection cnc, SqlParameters? @params = null) throws GLib.Error {
    var ev = new ExpressionValue ();
    ev.connection = cnc;
    ev.set_math_expression_value (str, @params);
    add_expression (ev);
	}
	/**
	 * Parse a math string expression and add it as a child
	 */
	public static SqlExpression parse (string str, Connection cnc, SqlParameters? @params = null) throws GLib.Error {
		Gee.ArrayList<GLib.TokenType> expected = new Gee.ArrayList<GLib.TokenType> ();
    var scanner = new GLib.Scanner (null);
    scanner.input_name = "SQL";
    scanner.input_text (str, str.length);
    scanner.config.cpair_comment_single = "\n";
    scanner.config.skip_comment_multi = false;
    scanner.config.skip_comment_single = false;
    scanner.config.char_2_token = false;
    scanner.config.scan_binary = false;
    scanner.config.scan_octal = false;
    scanner.config.scan_float = false;
    scanner.config.scan_hex = false;
    scanner.config.scan_hex_dollar = false;
    scanner.config.numbers_2_int = false;
    scanner.config.scan_identifier_1char = true;

    GLib.TokenType token = GLib.TokenType.NONE;

    SqlExpression current = null;
    SqlExpression current_comp = null;
    SqlExpression current_oper = null;
    SqlExpression current_last = null;
    SqlExpressionField current_field = null;
    SqlExpressionValue current_val = null;
    SqlExpressionValueParameter current_param = null;

    bool enable_math_expression = false;
    StringBuilder math_expression = new StringBuilder ("");

    while (token != GLib.TokenType.EOF) {
      token = scanner.get_next_token ();
      if (token == GLib.TokenType.EOF) {
				if (current_comp != null) {
					if (current_comp is SqlExpressionOperatorBinaryterm) {
						current_comp.add_expression (current);
						current = current_comp;
						current_comp = null;
					}
				}
      	if (enable_math_expression) {
      		if (current is SqlExpressionValue) {
						((SqlExpressionValue) current).set_math_expression_value (math_expression.str, @params);
					} else {
						current.add_math_expression (math_expression.str, cnc, @params);
					}
      	}
        break;
      }
      if (expected.size != 0 && !expected.contains (token)) {
        throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Found an unexpected expression at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
      }
      switch (token) {
        case GLib.TokenType.IDENTIFIER:
        	string identifier = scanner.cur_value ().@string;
        	if (identifier.down () == "and") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid AND declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current is SqlExpressionOperatorBetween || current is SqlExpressionOperatorBetweenSymmetric) {
				  		if (current.get_n_items () == 0) {
				  			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid BETWEEN declaration, no initial expressions provided, at Line:Column : %d:%d"),
                                                        scanner.cur_line (), scanner.cur_position ());
				  		}
				  		var be = current.get_item (0) as SqlExpression;
	      			if (current_val != null && enable_math_expression) {
			  				current_val.set_math_expression_value (math_expression.str, @params);
			  				enable_math_expression = false;
			  				current.add_expression (current_val);
				  		}
				  		if (current.get_n_items () != 2) {
				  			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid BETWEEN declaration, no initial range expression provided, at Line:Column : %d:%d"),
                                                        scanner.cur_line (), scanner.cur_position ());
				  		}
				  		var ie = current.get_item (1) as SqlExpression;
				  		if (current is SqlExpressionOperatorNotBetweenSymmetric) {
				  			current = new ExpressionOperatorNotBetweenSymmetricAnd ();
				  		} else if (current is SqlExpressionOperatorNotBetween) {
				  			current = new ExpressionOperatorNotBetweenAnd ();
				  		} else if (current is SqlExpressionOperatorBetweenSymmetric) {
				  			current = new ExpressionOperatorBetweenSymmetricAnd ();
				  		} else {
				  			current = new ExpressionOperatorBetweenAnd ();
				  		}
	      			current.add_expression (be);
	      			current.add_expression (ie);
	      			current_comp = null;
        		} else {
        			if (current_val != null && enable_math_expression) {
		    				current_val.set_math_expression_value (math_expression.str, @params);
		    				enable_math_expression = false;
		    				current.add_expression (current_val);
			    		}
			    		if (!(current is SqlExpressionOperatorAnd)) {
		      			var andop = new ExpressionOperatorAnd ();
				    		andop.add_expression (current);
				    		current = andop;
		      		}
		      	}
        	} else if (identifier.down () == "or") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid OR declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
		    		if (current_val != null && enable_math_expression) {
	    				current_val.set_math_expression_value (math_expression.str, @params);
	    				enable_math_expression = false;
	    				current.add_expression (current_val);
		    		}
	      		if (!(current is SqlExpressionOperatorOr)) {
		      		var orop = new ExpressionOperatorOr ();
		      		orop.add_expression (current);
		      		current = orop;
	      		}
        	} else if (identifier.down () == "like") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid LIKE declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp != null && current_comp is SqlExpressionOperatorNot) {
        			var notlikeop = new ExpressionOperatorNotLike ();
        			notlikeop.add_expression (current);
        			current = notlikeop;
        			current_comp = null;
        		} else {
		      		var likeop = new ExpressionOperatorLike ();
		      		likeop.add_expression (current);
		      		current = likeop;
		      	}
						if (current_val != null && enable_math_expression) {
							current_val.set_math_expression_value (math_expression.str, @params);
							enable_math_expression = false;
							current.add_expression (current_val);
						}
        	} else if (identifier.down () == "ilike") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid ILIKE declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp != null && current_comp is SqlExpressionOperatorNot) {
        			var notilikeop = new ExpressionOperatorNotIlike ();
        			notilikeop.add_expression (current);
        			current = notilikeop;
        			current_comp = null;
        		} else {
		      		var ilikeop = new ExpressionOperatorIlike ();
		      		ilikeop.add_expression (current);
		      		current = ilikeop;
		      	}
						if (current_val != null && enable_math_expression) {
							current_val.set_math_expression_value (math_expression.str, @params);
							enable_math_expression = false;
							current.add_expression (current_val);
						}
        	} else if (identifier.down () == "in") {
	      		if (current == null) {
	      			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid IN declaration at Line:Column : %d:%d"),
	                                                        scanner.cur_line (), scanner.cur_position ());
	      		}
        		if (current_comp != null && current_comp is SqlExpressionOperatorNot) {
        			var notinop = new ExpressionOperatorNotIn ();
        			notinop.add_expression (current);
        			current = notinop;
        			current_comp = null;
        		} else {
			    		if (current is SqlExpressionValue && enable_math_expression) {
			    			((SqlExpressionValue) current).set_math_expression_value (math_expression.str, @params);
		    				enable_math_expression = false;
			    		}
		      		var inop = new ExpressionOperatorIn ();
		      		inop.add_expression (current);
		      		current = inop;
        		}
        	} else if (identifier.down () == "null") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid NOT declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected declaration: invalid NULL declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp is SqlExpressionOperatorIsNot) {
        			var isnotnullop = new ExpressionOperatorIsNotNull ();
        			isnotnullop.add_expression (current);
        			current = isnotnullop;
        			current_comp = null;
        		} else if (current_comp is SqlExpressionOperatorIs) {
        			var isnullop = new ExpressionOperatorIsNull ();
        			isnullop.add_expression (current);
        			current = isnullop;
        			current_comp = null;
        		}
		    		if (current_val != null && enable_math_expression) {
	    				current_val.set_math_expression_value (math_expression.str, @params);
	    				enable_math_expression = false;
	    				current.add_expression (current_val);
		    		}
        	} else if (identifier.down () == "true") {
    				var fval = new ValueBool ();
    				fval.from_value (true);
    				var fvalc = new ExpressionValue ();
    				fvalc.connection = cnc;
    				fvalc.@value = fval;
        		if (current == null) {
        			current = fvalc;
        		} else if (current_comp != null) {
        			message (current_comp.get_type ().name ());
        			if (current_comp is SqlExpressionOperatorIsNot) {
		      			var isnottrueop = new ExpressionOperatorIsNotTrue ();
		      			isnottrueop.add_expression (current);
		      			current = isnottrueop;
		      			current_comp = null;
		      		} else if (current_comp is SqlExpressionOperatorIs) {
		      			var istrueop = new ExpressionOperatorIsTrue ();
		      			istrueop.add_expression (current);
		      			current = istrueop;
		      			current_comp = null;
		      		} else if (current_comp is SqlExpressionOperatorGt) {
		      			current_comp.add_expression (current);
		      			current = current_comp;
		      			current_comp = null;
		      		}
		      	} else {
		      		current.add_expression (fvalc);
		      	}
        	} else if (identifier.down () == "false") {
    				var fval = new ValueBool ();
    				fval.from_value (false);
    				var fvalc = new ExpressionValue ();
    				fvalc.connection = cnc;
    				fvalc.@value = fval;
        		if (current == null) {
        			current = fvalc;
        		} else if (current_comp != null) {
        			if (current_comp is SqlExpressionOperatorIsNot) {
		      			var isnottrueop = new ExpressionOperatorIsNotFalse ();
		      			isnottrueop.add_expression (current);
		      			current = isnottrueop;
		      			current_comp = null;
		      		} else if (current_comp is SqlExpressionOperatorIs) {
		      			var istrueop = new ExpressionOperatorIsFalse ();
		      			istrueop.add_expression (current);
		      			current = istrueop;
		      			current_comp = null;
		      		} else if (current_comp is SqlExpressionOperatorGt) {
		      			current_comp.add_expression (current);
		      			current = current_comp;
		      			current_comp = null;
		      		}
		      	} else {
		      		current.add_expression (fvalc);
		      	}
        	} else if (identifier.down () == "unknown") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid UNKNOWN declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp is SqlExpressionOperatorIsNot) {
        			var isnunknop = new ExpressionOperatorIsNotUnknown ();
        			isnunknop.add_expression (current);
        			current = isnunknop;
        			current_comp = null;
        		} else if (current_comp == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid UNKNOWN declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		} else if (current_comp is SqlExpressionOperatorIs) {
        			var isunknop = new ExpressionOperatorIsUnknown ();
        			isunknop.add_expression (current);
        			current = isunknop;
        			current_comp = null;
        		} else {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected IS or IS NOT expression: invalid UNKNOWN declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
            }
        	} else if (identifier.down () == "from") {

        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid FROM declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid FROM declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		} else if (current_comp is SqlExpressionOperatorIsNotDistinct) {
        			var isndistop = new ExpressionOperatorIsNotDistinctFrom ();
        			isndistop.add_expression (current);
        			current = isndistop;
        			current_comp = null;
        		} else if (current_comp is SqlExpressionOperatorIsDistinct) {
        			var isdistop = new ExpressionOperatorIsDistinctFrom ();
        			isdistop.add_expression (current);
        			current = isdistop;
        			current_comp = null;
        		} else {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected IS DITINCT or IS NOT DISTINCT expression: invalid UNKNOWN declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
            }
        	} else if (identifier.down () == "distinct") {

        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid DISTINCT declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid DISTINCT declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		} if (current_comp is SqlExpressionOperatorIsNot) {
        			current_comp = new ExpressionOperatorIsNotDistinct ();
        		} else if (current_comp is SqlExpressionOperatorIs) {
        			current_comp = new ExpressionOperatorIsDistinct ();
        		} else {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected IS or IS NOT expression: invalid DISTINCT declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
            }
        	} else if (identifier.down () == "between") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid BETWEEN declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp != null && current_comp is SqlExpressionOperatorNot) {
        			current_comp = new ExpressionOperatorNotBetween ();
        			current_comp.add_expression (current);
        			current = current_comp;
        			current_comp = null;
        		} else {
      				current_comp = new ExpressionOperatorBetween ();
        			current_comp.add_expression (current);
        			current = current_comp;
        			current_comp = null;
      			}
        	} else if (identifier.down () == "symmetric") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid BETWEEN SYMMETRIC declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current is SqlExpressionOperatorBetween) {
        			if (current.get_n_items () == 0) {
		      			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid BETWEEN SYMMETRIC declaration, missing initial expression, at Line:Column : %d:%d"),
		                                                        scanner.cur_line (), scanner.cur_position ());
		      		}
        			var be = current.get_item (0) as SqlExpression;
        			if (current is SqlExpressionOperatorNotBetween) {
        				current = new ExpressionOperatorNotBetweenSymmetric ();
        			} else {
        				current = new ExpressionOperatorBetweenSymmetric ();
        			}
      				current.add_expression (be);
        		} else {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Unexpected expression: invalid SYMMETRIC keword at Line:Column : %d:%d"),
		                                                        scanner.cur_line (), scanner.cur_position ());
        		}
        	} else if (identifier.down () == "not") {
        		if (current_comp != null && current_comp is SqlExpressionOperatorIs) {
        			current_comp = new ExpressionOperatorIsNot ();
        		} else {
        			current_comp = new ExpressionOperatorNot ();
        		}
        	} else if (identifier.down () == "is") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid IS declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		current_comp = new ExpressionOperatorIs ();
        	} else if (identifier.down () == "similar") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid SIMILAR TO declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		current_comp = new ExpressionOperatorSimilarTo ();
        	} else if (identifier.down () == "to") {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid SIMILAR TO declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp != null && current_comp is SqlExpressionOperatorSimilarTo) {
			    		if (current_val != null && enable_math_expression) {
		    				current_val.set_math_expression_value (math_expression.str, @params);
		    				enable_math_expression = false;
		    				current.add_expression (current_val);
			    		}
        			current_comp.add_expression (current);
        			current = current_comp;
        			current_comp = null;
        		}
        	} else {
        		// is an identifier
			    	var f = new ExpressionField ();
			    	f.name = identifier;
		      	if (current == null) {
				    	current = f;
				    	current_field = f;
							if (current_comp != null) {
								current_comp.add_expression (current);
								current = current_comp;
								current_comp = null;
							}
		      	} else {
		      		if (enable_math_expression) {
		      			math_expression.append (identifier);
		      		} else {
		      			if (current_param != null) {
		      				if (current_param.name == null) {
		      					current_param.name = identifier;
		      				} else {
		      					var gtype = SqlExpressionValueParameter.gtype_from_string (identifier);
		      					if (gtype != Type.INVALID) {
		      						current_param.gtype = gtype;
			    						if (current_oper != null) {
			    							current_oper.add_expression (current_param);
			    							current_last = current_param;
			    							current_oper = null;
	      								current_param = null;
			    						} else {
			    							current.add_expression (current_param);
			    							current_last = current_param;
	      								current_val = current_param;
	      								current_param = null;
			    						}
	      						} else {
        							throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Invalid parameter expression: invalid type name for parameter at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
	      						}
	      					}
		      			} else {
									current_field = f;
									current_last = f;
									current.add_expression (f);
									if (current_comp != null) {
										current_comp.add_expression (current);
										current = current_comp;
										current_comp = null;
									}
      					}
	      			}
		      	}
		      }
        	break;
        case GLib.TokenType.CHAR:
        	char v = (char) scanner.cur_value ().@char;
        	if (v == '!') {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid '!' declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		current_comp = new ExpressionOperatorNot ();
        	} else if (v == '=') {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid '=' declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_oper != null && current_oper is SqlExpressionOperatorEq) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid '=' character at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
		    		if (current_val != null && enable_math_expression) {
	    				current_val.set_math_expression_value (math_expression.str, @params);
	    				enable_math_expression = false;
	    				current.add_expression (current_val);
		    		}
		    		current_oper = null;
        		if (current_comp is SqlExpressionOperatorNot) {
        			var neq = new ExpressionOperatorNotEq ();
        			current_oper = neq;
        			current_comp = null;
        		} else if (current_comp is SqlExpressionOperatorGt) {
        			var gteqop = new ExpressionOperatorGeq ();
        			current_oper = gteqop;
        			current_comp = null;
        		} else if (current_comp is SqlExpressionOperatorLt) {
        			var leqop = new ExpressionOperatorLeq ();
        			current_oper = leqop;
        			current_comp = null;
        		} else if (current_comp == null) {
				  		var eqop = new ExpressionOperatorEq ();
	      			current_oper = eqop;
        		}
			  		if (current_val != null) {
	      			current_oper.add_expression (current_val);
	      			current.remove_expression (current_val);
	      			current_val = null;
	      			if (current is SqlExpressionValue) {
	      				current = current_oper;
	      			}
	      		}
			  		if (current_field != null) {
	      			current_oper.add_expression (current_field);
	      			current.remove_expression (current_field);
	      			current_field = null;
	      			if (current is SqlExpressionField) {
	      				current = current_oper;
	      			}
	      		}
	      		if (current_param != null) {
	      			current_oper.add_expression (current_param);
      				current.remove_expression (current_param);
	      			current_param = null;
	      			current_last = null;
	      			if (current is SqlExpressionValueParameter) {
	      				current = current_oper;
	      			}
	      		}
	      		if (current is SqlExpressionOperatorGroup) {
	      			current_oper.add_expression (current);
	      			current = current_oper;
	      		} else {
	      			if (current != current_oper) {
      					current.add_expression (current_oper);
	      			}
      			}
        	} else if (v == '<') {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid '<' declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp != null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Incomplete operator declaration: invalid '<' declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		current_comp = new ExpressionOperatorLt ();
        	} else if (v == '>') {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Expected expression: invalid '<' declaration at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		if (current_comp != null && current_comp is SqlExpressionOperatorLt) {
			    		if (current_val != null && enable_math_expression) {
		    				current_val.set_math_expression_value (math_expression.str, params);
		    				enable_math_expression = false;
		    				current.add_expression (current_val);
			    		}
        			var diffop = new ExpressionOperatorDiff ();
        			diffop.add_expression (current);
        			current = diffop;
        			current_comp = null;
        		} else {
		      		current_comp = new ExpressionOperatorGt ();
		    		}
        	} else if (v == '+') {
	      		take_field_as_math_expression (ref enable_math_expression,
	      																	ref current, ref current_last,
	      																	ref current_val,
	      																	scanner, math_expression, cnc, "+");
        		if (!enable_math_expression) {
        			enable_math_expression = true;
        			math_expression.assign ("");
        			var plv = new ExpressionValue ();
        			current_val = plv;
        			plv.connection = cnc;
        			if (current == null) {
        				current = plv;
        			}
        		}
        		math_expression.append_c (v);
        	} else if (v == '-') {
	      		take_field_as_math_expression (ref enable_math_expression,
	      																	ref current, ref current_last,
	      																	ref current_val,
	      																	scanner, math_expression, cnc, "-");
        		if (!enable_math_expression) {
        			enable_math_expression = true;
        			math_expression.assign ("");
        			var plv = new ExpressionValue ();
        			current_val = plv;
        			plv.connection = cnc;
        			if (current == null) {
        				current = plv;
        			}
        		}
        		math_expression.append_c (v);
        	} else if (v == '(') {
        		if (!enable_math_expression) {
        			enable_math_expression = true;
        			math_expression.assign ("");
        			var plv = new ExpressionValue ();
        			plv.connection = cnc;
        			if (current == null) {
        				current = plv;
        				current_val = plv;
        			}
        		}
        		math_expression.append_c (v);
        	} else if (v == ')') {
        		if (!enable_math_expression) {
        			enable_math_expression = true;
        			math_expression.assign ("");
        			var plv = new ExpressionValue ();
        			plv.connection = cnc;
        			if (current == null) {
        				current = plv;
        			}
        		}
        		math_expression.append_c (v);
        	} else if (v == '^') {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Unexpected start expression: invalid '^' operator at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		take_field_as_math_expression (ref enable_math_expression,
        																	ref current, ref current_last,
        																	ref current_val,
        																	scanner, math_expression, cnc, "^");
        		math_expression.append_c (v);
        	} else if (v == '*') {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Unexpected start expression: invalid '*' operator at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		take_field_as_math_expression (ref enable_math_expression,
        																	ref current, ref current_last,
        																	ref current_val,
        																	scanner, math_expression, cnc, "*");
        		math_expression.append_c (v);
        	} else if (v == '/') {
        		if (current == null) {
        			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Unexpected start expression: invalid '/' operator at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        		}
        		take_field_as_math_expression (ref enable_math_expression,
        																	ref current, ref current_last,
        																	ref current_val,
        																	scanner, math_expression, cnc, "/");
        		math_expression.append_c (v);
        		message (math_expression.str);
        	} else if (v == '#') {
        		if (current == null) {
		      		current_param = new ExpressionValueParameter ();
		      		current = current_param;
        		} else if (current_param == null) {
        			current_param = new ExpressionValueParameter ();
        		} else if (current_param != null) {
        			if (current_param.connection == null) {
        				current_param.connection = cnc;
        				current_param.parameters = @params;
        			} else {
        				throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Unexpected expression: invalid '#' character at Line:Column : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        			}
        		}
        	} else if (v == '.') {
        		if (current_field != null) {
        			current_field.table_ref = current_field.name;
        			token = scanner.get_next_token ();
        			if (token != GLib.TokenType.IDENTIFIER) {
        				throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Unexpected expression: expenting an field identifier : %d:%d"),
                                                          scanner.cur_line (), scanner.cur_position ());
        			}
        			current_field.name = scanner.cur_value ().@string;
        		}
        	} else {
        		if (enable_math_expression) {
        			math_expression.append_c (v);
        		}
        	}
        	break;
        case GLib.TokenType.INT:
        		if (!enable_math_expression) {
        			enable_math_expression = true;
        			math_expression.assign ("");
        			var plv = new ExpressionValue ();
        			plv.connection = cnc;
        			current_val = plv;
        			if (current == null) {
        				current = plv;
        			}
        		}
        		math_expression.append (scanner.cur_value ().@int.to_string ());
        		break;
        case GLib.TokenType.FLOAT:
        		if (!enable_math_expression) {
        			enable_math_expression = true;
        			math_expression.assign ("");
        			var plv = new ExpressionValue ();
        			plv.connection = cnc;
        			current_val = plv;
        			if (current == null) {
        				current = plv;
        			}
        		}
        		math_expression.append ("%g".printf (scanner.cur_value ().@float));
        		break;

        case GLib.TokenType.STRING:
        	var strval = new ExpressionValue ();
    			strval.connection = cnc;
    			strval.value = new ValueString ();
    			strval.value.parse (scanner.cur_value ().@string);
    			current_val = strval;
    			if (current == null) {
    				current = strval;
    			} else {
    				if (current_oper != null) {
    					current_oper.add_expression (strval);
    				} else {
    					current.add_expression (strval);
    				}
    			}
        	break;
	      case GLib.TokenType.NONE:
	      case GLib.TokenType.LEFT_PAREN:
	      case GLib.TokenType.SYMBOL:
	      case GLib.TokenType.COMMENT_MULTI:
	      case GLib.TokenType.RIGHT_CURLY:
	      case GLib.TokenType.BINARY:
	      case GLib.TokenType.OCTAL:
	      case GLib.TokenType.EQUAL_SIGN:
	      case GLib.TokenType.RIGHT_PAREN:
	      case GLib.TokenType.ERROR:
	      case GLib.TokenType.LAST:
	      case GLib.TokenType.LEFT_BRACE:
	      case GLib.TokenType.EOF:
	      case GLib.TokenType.COMMA:
	      case GLib.TokenType.COMMENT_SINGLE:
	      case GLib.TokenType.LEFT_CURLY:
	      case GLib.TokenType.IDENTIFIER_NULL:
	      case GLib.TokenType.RIGHT_BRACE:
	      case GLib.TokenType.HEX:
	      	break;
      }
    }
    if (current == null) {
			throw new SqlExpressionError.INVALID_EXPRESSION_ERROR (_("Invalid expression: parsed object expression is invalid"));
		}
		return current;
	}
	internal static void take_field_as_math_expression (ref bool enable_math_expression,
																							ref SqlExpression? current,
																							ref SqlExpression? current_last,
																							ref SqlExpressionValue? current_val,
																							GLib.Scanner scanner,
																							StringBuilder math_expression,
																							Connection cnc,
																							string context) throws GLib.Error
	{
		if (enable_math_expression && current_last == null) {
			return;
		}
		if ((!(current is SqlExpressionField) || !(current is SqlExpressionValueParameter))
				 && current_last != null) {
			current.remove_expression (current_last);
		}
		enable_math_expression = true;
		math_expression.assign ("");
		var fnv = new ExpressionValue ();
		fnv.connection = cnc;
		if (current is SqlExpressionField) {
			math_expression.append (((SqlExpressionField) current).name);
			current = fnv;
			current_val = fnv;
		} else if (current_last != null && current_last is SqlExpressionField) {
			math_expression.append (((SqlExpressionField) current_last).name);
		} else if (current is SqlExpressionValueParameter) {
			math_expression.append ("$");
			math_expression.append (((SqlExpressionValueParameter) current).name);
			current = fnv;
			current_val = fnv;
		} else if (current_last != null && current_last is SqlExpressionValueParameter) {
			math_expression.append ("$");
			math_expression.append (((SqlExpressionValueParameter) current_last).name);
		}
		current_last = null;
	}
}

/**
 * SQL Expression error codes
 */
public errordomain Vda.SqlExpressionError {
	INVALID_EXPRESSION_ERROR
}
