/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * ExpressionValueParameter.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implemenation of {@link SqlExpressionValueParameter}
 */
public class Vda.ExpressionValueParameter  : ExpressionValue, SqlExpressionValueParameter
{
	internal SqlParameters parameters { get; set; }
	internal string name { get; set; }
	internal Type   gtype  { get; set; }
	internal override string to_string () {
		string str = "NULL";
		SqlValue p = null;
		if (parameters != null) {
			p = parameters.get_sql_value (name);
		}
  	if (p != null) {
  		str = connection.value_to_quoted_string (p);
  	} else {
			string tn = gtype.name ();
			if (gtype.is_a (typeof (SqlValue))) {
				tn = @value.name;
			}
			str = "##"+name+"::"+tn;
		}
		return str;
	}
}

