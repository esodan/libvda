/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlValue.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an SQL value.
 *
 * A value can be serialized to/from string using
 * {@link Stringifiable.to_string} and {@link parse}
 *
 * Use {@link cast} to convert this value to any
 * supported or compatible {@link GLib.Type}, so is possible
 * to convert to any {@link SqlValue} implementation type.
 */
public interface Vda.SqlValue  : Object, Stringifiable
{
	/**
	 * Value's name
	 */
	public abstract string name { get; }
	/**
	 * Parse a string representation of a value
	 */
	public abstract bool parse (string str);
	/**
	 * Set this object's value from a {@link GLib.Value}
	 */
	public abstract bool from_value (GLib.Value val);
	/**
	 * Tries to cast this object to a compatible {@link GLib.Type}
	 *
	 * Returns: a new {@link SqlValue} or NULL if fails
	 */
	public abstract SqlValue? cast (Type type);
	/**
	 * Checks if this object is compatible with a given {@link GLib.Type}.
	 *
	 * Compatible means, this value can use {@link cast} to create a new
	 * {@link SqlValue} holding a transformed version of the value, depending
	 * on its type.
	 */
	public abstract bool is_compatible (Type type);
	/**
	 * Convert to a {@link GLib.Value}
	 */
	public abstract GLib.Value to_gvalue ();
	/**
	 * Returns a single quoted string representation of the value.
	 */
	public abstract string to_string_quoted ();
	/**
	 * Returns an SQL expression of the value.
	 *
	 * The value will be converted to string and quoted if required.
	 */
	public abstract string to_sql_expression ();
	/**
	 * Creates a new {@link SqlValue} for the given {@link GLib.Value}
	 */
	public static SqlValue new_from_gvalue (GLib.Value? val) {
		SqlValue v = new ValueNull ();
		if (val.holds (typeof (string))) {
			v = new ValueString ();
		} else if (val.holds (typeof (int))) {
			v = new ValueInteger ();
		} else if (val.holds (typeof (float))) {
			v = new ValueFloat ();
		} else if (val.holds (typeof (double))) {
			v = new ValueDouble ();
		} else if (val.holds (typeof (bool))) {
			v = new ValueBool ();
		} else if (val.holds (typeof (DateTime))) {
			v = new ValueTimestamp ();
		} else if (val.holds (typeof (Date))) {
			v = new ValueDate ();
		}
		v.from_value (val);
		return v;
	}
	/**
	 * Create a new {@link SqlValue} from a {@link GLib.Type}.
	 *
	 * Default handler is returned.
	 */
	public static SqlValue? new_from_gtype (GLib.Type type) {
		if (type.is_a (typeof (SqlValue))) {
			return Object.new (type) as SqlValue;
		} else if (type == typeof (string)) {
			return new ValueString ();
		} else if (type == typeof (bool)) {
			return new ValueBool ();
		} else if (type == typeof (int)) {
			return new ValueInteger ();
		} else if (type == typeof (int64)) {
			return new ValueInt8 ();
		} else if (type == typeof (float)) {
			return new ValueFloat ();
		} else if (type == typeof (double)) {
			return new ValueDouble ();
		} else if (type == typeof (DateTime)) {
			return new ValueTimestamp ();
		} else if (type == typeof (Date)) {
			return new ValueDate ();
		}
		return null;
	}
}

/**
 * Null representation of a value
 */
public interface Vda.SqlValueNull : Object, SqlValue {}
/**
 * A boolean value
 */
public interface Vda.SqlValueBool : Object, SqlValue {}
/**
 * A one bit value
 */
public interface Vda.SqlValueBit : Object, SqlValue {}

/**
 * A string value, by default using UTF-8.
 */
public interface Vda.SqlValueString : Object, SqlValue {}

/**
 * A string value holding XML data.
 */
public interface Vda.SqlValueXml : Object, SqlValue, SqlValueString {
	public abstract GXml.DomDocument document { get; }
}

/**
 * A string value holding JSON data.
 */
public interface Vda.SqlValueJson : Object, SqlValue, SqlValueString {
	public abstract Json.Node document { get; }
}
/**
 * A text string value, as a long or undefined length string
 */
public interface Vda.SqlValueText : Object, SqlValue, SqlValueString {}
/**
 * A text string value, representing a user's name
 */
public interface Vda.SqlValueName : Object, SqlValue, SqlValueString {}
/**
 * A value representing an Integer
 */
public interface Vda.SqlValueInteger : Object, SqlValue {}
/**
 * A value representing an Integer of one (8 bits) byte long
 */
public interface Vda.SqlValueByte : Object, SqlValue, SqlValueInteger {}
/**
 * A value representing an Integer of two bytes (16 bits) long
 */
public interface Vda.SqlValueInt2 : Object, SqlValue, SqlValueInteger {}
/**
 * A value representing an Integer of four bytes (32 bits) long
 */
public interface Vda.SqlValueInt4 : Object, SqlValue, SqlValueInteger {}
/**
 * A value representing an Integer of eight (64 bits) bytes long
 */
public interface Vda.SqlValueInt8 : Object, SqlValue, SqlValueInteger {}
/**
 * A value representing an unsiged integer
 */
public interface Vda.SqlValueUnsignedInteger : Object, SqlValue {}
/**
 * A value representing an Integer of one byte long
 */
public interface Vda.SqlValueUnsignedByte : Object, SqlValue, SqlValueUnsignedInteger {}
/**
 * A value representing an Integer of two bytes long
 */
public interface Vda.SqlValueUnsignedInt2 : Object, SqlValue, SqlValueUnsignedInteger {}
/**
 * A value representing an Integer of four bytes long
 */
public interface Vda.SqlValueUnsignedInt4 : Object, SqlValue, SqlValueUnsignedInteger {}
/**
 * A value representing an Integer of eight bytes long
 */
public interface Vda.SqlValueUnsignedInt8 : Object, SqlValue, SqlValueUnsignedInteger {}
/**
 * A value representing an Integer for database object identification
 */
public interface Vda.SqlValueOid : Object, SqlValue, SqlValueInteger {}
/**
 * A value representing a number with variable precision
 */
public interface Vda.SqlValueNumeric : Object, SqlValue {
	/**
	 * Sets the precision of the number
	 */
	public abstract void set_precision (int p);
	/**
	 * Gets the precision of the number
	 */
	public abstract int get_precision ();
	/**
	 * Creates a string representing the number using the given format
	 * as for {@link string.printf}
	 */
	public abstract string format (string str);
	/**
	 * Gets the number as a double
	 */
	public abstract double get_double ();
	/**
	 * Sets the number from a double
	 */
	public abstract void set_double (double v);
	/**
	 * Gets the real part of an imaginary number
	 */
	public abstract double get_real ();
	/**
	 * Sets the real part of an imaginary number
	 */
	public abstract void set_real (double r);
	/**
	 * Gets the imaginary part of an imaginary number
	 */
	public abstract double get_imaginary ();
	/**
	 * Sets the imaginary part of an imaginary number
	 */
	public abstract void set_imaginary (double img);
}
/**
 * Value representing a float single precision number
 */
public interface Vda.SqlValueFloat : Object, SqlValue, SqlValueNumeric {
	/**
	 * Converts the internal value to a {@link float} number
	 */
	public abstract float get_float ();
}
/**
 * Value representing a float double precision number
 */
public interface Vda.SqlValueDouble : Object, SqlValue, SqlValueNumeric {}

/**
 * Value representing a float point number for use a monetary quantities
 */
public interface Vda.SqlValueMoney : Object, SqlValue, SqlValueNumeric
{
	/**
	 * Format a string representation of a {@link SqlValueMoney} using current locale
	 */
	public abstract string locale ();
	/**
	 * Format a string representation of a {@link SqlValueMoney} using current locale
	 * adding the international currency symbol
	 */
	public abstract string int_locale ();
	/**
	 * Returns current number of digits for international currency, by the fault
	 * it is set to current locale
	 */
	public abstract int get_int_precision ();
	/**
	 * Change the current number of digits for international currency, by the fault
	 * it is set to current locale
	 */
	public abstract void set_int_precision (int p);
}

/**
 * Value representing a date
 */
public interface Vda.SqlValueDate : Object, SqlValue {
	/**
	 * Gets the {@link GLib.Date} of the value
	 */
	public abstract Date get_date ();
	/**
	 * Sets the value from a {@link GLib.Date}
	 */
	public abstract void set_date (Date ts);
}
/**
 * Value representing a timestamp with time zone
 */
public interface Vda.SqlValueTimestamp : Object, SqlValue {
	/**
	 * Creates a representation of the timestamp translated to local
	 * time
	 */
	public abstract string format_local ();
	/**
	 * Creates a representation of the timestamp translated to UTC
	 */
	public abstract string format_utc ();
	/**
	 * Creates a representation of the timestamp with the date
	 * part in localized format.
	 */
	public abstract string format_locale ();
	/**
	 * Creates a representation of the timestamp with just the date
	 * part in international format.
	 */
	public abstract string format_date ();
	/**
	 * Creates a representation of the timestamp with just the date
	 * part in localized format.
	 */
	public abstract string format_date_locale ();
	/**
	 * Creates a representation of the timestamp with just the time
	 * part with time zone.
	 */
	public abstract string format_time ();
	/**
	 * Creates a representation of the timestamp with just the time
	 * part translated to local time with time zone.
	 */
	public abstract string format_time_local ();
	/**
	 * Creates a representation of the timestamp with just the time
	 * part translated to local time without time zone.
	 */
	public abstract string format_time_local_ntz ();
	/**
	 * Returns a {@link GLib.DateTime} with the actual value
	 */
	public abstract GLib.DateTime get_timestamp ();
	/**
	 * Sets the actual timestamp to the given {@link GLib.DateTime}
	 */
	public abstract void set_timestamp (GLib.DateTime ts);
}
/**
 * Value representing a timestamp without time zone
 */
public interface Vda.SqlValueTimestampNtz : Object, SqlValue, SqlValueTimestamp {}
/**
 * Value representing a time with time zone
 */
public interface Vda.SqlValueTime : Object, SqlValue, SqlValueTimestamp {}
/**
 * Value representing a time without time zone
 */
public interface Vda.SqlValueTimeNtz : Object, SqlValue, SqlValueTimestampNtz {}

/**
 * Value representing byte sequense data
 */
public interface Vda.SqlValueBinary : Object, SqlValue {
	/**
	 * Provides data size
	 */
	public abstract uint size { get; }
	/**
	 * Get a copy of the data as a {@link GLib.Bytes}
	 */
	public abstract GLib.Bytes get_bytes ();

	/**
	 * Provides a stream to write data to value
	 */
	public virtual GLib.OutputStream?  get_out_stream () {
		return null;
	}
	/**
	 * Provides a stream to read data from value
	 */
	public virtual GLib.InputStream? get_input_stream () {
		return null;
	}
}
/**
 * Value representing a large binary object
 */
public interface Vda.SqlValueBlob : Object, SqlValueBinary {
	/**
	 * A {@link Connection} to access large binary data
	 */
	public abstract Connection connection { get; }
	/**
	 * Creates a new large binary data from stream
	 */
	public virtual void create (GLib.InputStream stream) {}
	/**
	 * Creates a new large binary data from stream
	 */
	public virtual void @delete () {}
	/**
	 * Writes the binary large object's contents to a file
	 */
	public virtual void write (GLib.File file) {}
	/**
	 * Read a file to set its content to a binary large object
	 * in the database
	 */
	public virtual void read (GLib.File file) {}
}
/**
 * Value representing a large binary object with an associated object's
 * identifier
 */
public interface Vda.SqlValueBlobOid : Object, SqlValueBlob {
	/**
	 * A identification of the large binary data in {@link SqlValueBlob.connection}
	 */
	public abstract uint identification { get; }
}
/**
 * A value representing a geometric point
 */
public interface Vda.SqlValueGeometricPoint : Object, SqlValue {
	/**
	 * X coordinate of a geometric point
	 */
	public abstract double x { get; set; }
	/**
	 * Y coordinate of a geometric point
	 */
	public abstract double y { get; set; }
}

/**
 * A Math Expression holder value
 */
public interface Vda.SqlValueMathExp : Object, SqlValue
{
	/**
	 * A mathematic expression manager of the value
	 */
	public abstract GCalc.MathEquationManager math { get; set; }

	/**
	 * Parameters in a mathematic expression
	 */
	public abstract SqlParameters parameters { get; set; }
}

/**
 * A DataType holder value
 */
public interface Vda.SqlValueDataType : Object, SqlValue {}
