/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * GSchema.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2011-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gee;
using Gda;
using Vda;

namespace Vgda
{
	/**
	 * Introspectable {@link Vda.SchemaMeta} implementation
	 *
	 * Stability: unstable
	 */
	public class GSchema : Object, ObjectMeta, NamedObjectMeta, SchemaMeta
	{
		private bool _update_meta = false;

		public HashMap<string,TableMeta> _tables = new HashMap<string,TableMeta> ();
		// Object Interface
		public Vda.Connection connection { get; set; }

		public bool update_meta {
			get { return _update_meta; }
			set { _update_meta = value; }
		}

		public void update () throws Error
		{
			(connection as GProvider).cnc.update_meta_store (null); // FIXME: just update schemas
			var store = (connection as GProvider).cnc.get_meta_store ();
			tables.clear ();
			var vals = new HashTable<string,GLib.Value?> (str_hash,str_equal);
			GLib.Value v = name;
			vals.set ("name", v);
			var mt =
				store.extract ("SELECT * FROM _tables WHERE schema_name = ##name::string",
								vals);
			for (int r = 0; r < mt.get_n_rows (); r++) {
				var t = new GTable ();
				t.connection = connection;
				t.name = (string) mt.get_value_at (mt.get_column_index ("table_name"), r);
				t.table_type =
					TableMeta.TableType.from_string(
							(string) mt.get_value_at (mt.get_column_index ("table_type"), r));
				t.schema = this;
				tables.set (t.name, t);
			}
		}

		public void save () throws Error {}
		public void append () throws Error {}
		public void drop (bool cascade) throws Error {}
		// NamedObjectMeta Interface
		public string name { get; set; }
		// SchemaMeta Interface
		public CatalogMeta           catalog { get; set; }
		public Collection<TableMeta> tables { owned get { return _tables.values; } }
	}
}
