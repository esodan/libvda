/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * GRecordCollection.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2011-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gee;
using Gda;
using Vda;

namespace Vgda {
	/**
	 * Introspectable collection of {@link Vda.RecordMeta} asociated to a {@link Vda.TableMeta}
	 *
	 * Stability: unstable
	 */
	public class GRecordCollection : AbstractCollection<RecordMeta>, RecordCollectionMeta
	{
		private DataModel _model;
		private TableMeta   _table;

		public TableMeta    table { get { return table; } }

		public Vda.Connection connection { get; set; }

		public GRecordCollection (DataModel m, TableMeta table)
		{
			_model = m;
			_table = table;
		}
		// AbstractCollection Implementation
		public override bool add (RecordMeta item)
		{
			try {
				int r = _model.append_row ();
				foreach (FieldMeta f in item.fields) {
					_model.set_value_at (_model.get_column_index (f.name), r, f.get_value ());
				}
				return true;
			}
			catch (Error e) { GLib.warning (e.message); }
			return false;
		}
		public override void clear ()
		{
			try {
				var iter = _model.create_iter ();
				while (iter.move_next ()) {
					_model.remove_row (iter.get_row ());
				}
				((DataProxy) _model).apply_all_changes ();
			}
			catch (Error e) { GLib.warning (e.message); }
		}
		public override bool contains (RecordMeta item)
		{
			bool found = true;
			var iter = _model.create_iter ();
			while (iter.move_next ()) {
				foreach (FieldMeta k in item.keys) {
					GLib.Value id = iter.get_value_at (iter.data_model.get_column_index (k.name));
					GLib.Value v = k.get_value ();
					if (Gda.value_compare (id,v) != 0)
						found = false;
				}
				if (found) break;
			}
			return found;
		}
		public override Gee.Iterator<RecordMeta> iterator ()
		{
			Gda.DataModelIter iter;
			iter = _model.create_iter ();
			return new GRecordCollectionIterator (iter, _table);
		}
		public override bool remove (RecordMeta item)
		{
			var iter = _model.create_iter ();
			while (iter.move_next ()) {
				bool found = true;
				foreach (FieldMeta k in item.keys) {
					GLib.Value id = iter.get_value_at (iter.data_model.get_column_index (k.name));
					GLib.Value v = k.get_value ();
					if (Gda.value_compare (id,v) != 0)
						found = false;
				}
				if (found) {
						try {
								_model.remove_row (iter.get_row ());
								((DataProxy)_model).apply_all_changes ();
								return true;
						} catch (GLib.Error e) {
								warning ("Error: %s".printf (e.message));
								return false;
						}
				}
			}
			return false;
		}
		public override bool read_only {
			get {
				var f = _model.get_access_flags ();
				if ( (f & Gda.DataModelAccessFlags.INSERT
					& Gda.DataModelAccessFlags.UPDATE
					& Gda.DataModelAccessFlags.DELETE) != 0 )
					return true;
				else
					return false;
			}
		}
		public override int size {
			get {
				return _model.get_n_rows ();
			}
		}
		//
		public string to_string ()
		{
			return _model.dump_as_string ();
		}
	}
}
