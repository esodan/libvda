/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gda;
using Vda;
/**
 * Implementation of {link Vda.Query} using GDA's statements
 */
internal class Vgda.GQuery : GLib.Object, Vda.Query {
  internal Vda.Connection cnc = null;
  internal Gda.Statement stmt = null;
  internal GParametersSet _parameters = null;
  internal string _sql;

  public GQuery (Vda.Connection connection, string sql) {
    this.cnc = connection;
    _sql = sql;
    try {
      Gda.Set s = null;
      stmt = ((GProvider) connection).cnc.parse_sql_string (_sql, out s);
      if (s != null) {
        _parameters = new GParametersSet (s);
      }
    } catch (GLib.Error e) {
      warning ("SQL string is invalid: %s", e.message);
      stmt = null;
    }
  }

  public GQuery.using_statement (Vda.Connection connection, Gda.Statement stmt) {
    cnc = connection;
    this.stmt = stmt;
  }

  // Query implementation
  public Vda.Connection connection { get { return cnc; } }
  public string sql {
    owned get {
      if (connection == null) return "";
      try {
        Gda.Set p = null;
        if (_parameters != null)
          p = _parameters.@set;
        string str = stmt.to_sql_extended (((GProvider) connection).cnc, p, Gda.StatementSqlFlag.PRETTY, null);
        return str;
      } catch (GLib.Error e) {
        return _sql;
      }
    }
  }
  public virtual async Vda.Result? execute (GLib.Cancellable? cancellable) throws GLib.Error {
    if (connection == null || stmt == null) {
      throw new QueryError.INVALID_CONNECTION_ERROR ("No connection is related");
    }
    if (cancellable != null) {
      if (cancellable.is_cancelled ()) {
        return new InvalidResult ("Query was canceled");
      }
    }
    Gda.Set last_inserted_row = null;
    Gda.Set p = null;
    if (_parameters != null)
      p = _parameters.@set;
    GLib.Object res;
    res = ((GProvider) connection).cnc.statement_execute (stmt,
                                                p,
                                                Gda.StatementModelUsage.RANDOM_ACCESS,
                                                out last_inserted_row);
    if (res is Gda.DataModel) {
      return new GTableModel (res as Gda.DataModel);
    }
    if (res is Gda.Set) {
      var impacted_rows = ((Gda.Set) res).get_holder ("IMPACTED_ROWS");
      int n = 0;
      if (impacted_rows != null)
        n = impacted_rows.get_value ().get_int ();
      if (last_inserted_row == null) {
        return new Vda.AffectedRows (n) as Result;
      } else {
        return new GInserted (n, last_inserted_row) as Result;
      }
    }
    return new InvalidResult ("No useful data in result");
  }
  public virtual async void cancel () {}

  public string render_sql () throws GLib.Error {
    Gda.Set p = null;
    if (_parameters != null)
      p = _parameters.@set;
    return stmt.to_sql_extended (((GProvider) connection).cnc, p, Gda.StatementSqlFlag.PRETTY, null);
  }
}

