/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * GField.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2011-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gee;
using Gda;
using Vda;

namespace Vgda
{
	/**
	 * Introspected database's field in a table
	 *
	 * Stability: unstable
	 */
	public class GField : Object, FieldMeta
	{
		private GLib.Value?       val;
		private string            _name;
		private string            _column_name;
		private FieldMeta.Attribute _attributes;
		// DbField Interface
		public GLib.Value get_value () {
			return val;
		}
		public void set_value (GLib.Value val) {
			this.val = GLib.Value (val.type ());
			val.copy (ref this.val);
		}
		public string name {
			get { return _name; }
			set { _name = value; }
		}
		public string column_name {
			get { return _column_name; }
		}
		public FieldMeta.Attribute attributes {
			get { return _attributes; }
		}
		public GField (string col_name, FieldMeta.Attribute attr)
		{
			_column_name = col_name;
			_attributes = attr;
			_name = col_name;
		}
	}
}
