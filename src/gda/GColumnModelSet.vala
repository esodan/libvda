/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GColumnModelSet.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gda;

/**
 * A set of columns, implementing {@link Vda.ColumnModel}
 */
internal class Vgda.GDbColumnSet : GLib.Object, Vda.ColumnModel {
  private string _name = "";
  private GLib.Type _data_type = GLib.Type.INVALID;

  public new Gda.Set @set { get; internal set; }

  public GDbColumnSet (Gda.Set @set, string name) {
    var h = @set.get_holder (name);
    if (h != null) {
      _name = name;
      _data_type = h.get_g_type ();
    }
  }

  // DbColumn implementation
  public string name { get { return _name; } }
  public GLib.Type data_type { get { return _data_type; } }
}

