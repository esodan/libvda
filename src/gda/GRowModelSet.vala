/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GRowModelSet.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gda;
using Vda;

/**
 * A set of rows, implementing {@link GLib.ListModel} and {@link Vda.DbRow}
 * using a {@link Gda.Set} as source
 */
internal class Vgda.GRowModelSet : GLib.Object, GLib.ListModel, Vda.RowModel {
  public GRowModelSet (Gda.Set @set) {
    this.@set = @set;
  }

  public new Gda.Set @set { get; internal set; }

  // DbRow implementation
  public uint n_columns {
    get {
      var h = _set.get_nth_holder (0);
      if (h == null) return 0;
      int i = 0;
      while (h != null) {
        i++;
        h = _set.get_nth_holder (i);
      }
      return i;
    }
  }
  public Vda.ColumnModel? get_column (string name) throws GLib.Error {
    return new GDbColumnSet (_set, name);
  }
  public Vda.ColumnModel? get_column_at (uint col) throws GLib.Error
  {
    var h = _set.get_nth_holder ((int) col);
    if (h == null) return null;
    return new GDbColumnSet (_set, h.name);
  }
  public SqlValue? get_value (string name) throws GLib.Error {
    var h = _set.get_holder (name);
    if (h == null) return null;
    var v = new Vda.Value ();
    v.force_value (h.get_value ());
    return v;
  }
  public SqlValue? get_value_at (uint col) throws GLib.Error {
    var h = _set.get_nth_holder ((int) col);
    if (h == null) return null;
    var v = new Vda.Value ();
    v.force_value (h.get_value ());
    return v;
  }

  public string? get_string (string name) throws GLib.Error {
    var v = get_value (name);
    if (v != null) {
      return Gda.value_stringify (v);
    }
    return null;
  }
  public string? get_string_at (uint col) throws GLib.Error {
    var v = get_value_at (col);
    if (v != null) {
      return Gda.value_stringify (v);
    }
    return null;
  }

  // GLib.ListModel interface
  public uint get_n_items () { return n_columns; }
  public GLib.Type get_item_type () { return typeof (DbColumn); }
  public GLib.Object? get_item (uint position) {
    var h = _set.get_nth_holder ((int) position);
    if (h != null) {
      return new GDbColumnSet (_set, h.get_id ());
    }
    return null;
  }
}

