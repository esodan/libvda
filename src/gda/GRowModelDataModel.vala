/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GRowModelDataModel.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gda;
using Vda;
/**
 * A set of rows, implementing {@link GLib.ListModel} and {@link Vda.DbRow}
 * using a {@link Gda.DataModel} as source
 */
internal class Vgda.GRowModelDataModel : GLib.Object, GLib.ListModel, Vda.RowModel {
  private Gda.DataModel _model = null;
  private uint _row = -1;

  public GRowModelDataModel (Gda.DataModel model, uint row) {
    _model = model;
    _row = row;
  }
  // DbRow implementation
  public uint n_columns {
    get { return (uint) _model.get_n_columns (); }
  }
  public Vda.ColumnModel? get_column (string name) throws GLib.Error {
    uint index = (uint) _model.get_column_index (name);
    return new GColumnModelDataModel (_model, index);
  }
  public Vda.ColumnModel? get_column_at (uint col) throws GLib.Error
    requires (col < _model.get_n_columns ())
  {
    return new GColumnModelDataModel (_model, col);
  }
  public SqlValue? get_value (string name) throws GLib.Error {
    var v = new Vda.Value ();
    try {
      v.force_value (_model.get_value_at (_model.get_column_index (name), (int) _row));
    } catch (GLib.Error e) {
      warning ("Error getting value for row: %s", e.message);
    }
    return v;
  }
  public SqlValue? get_value_at (uint col) throws GLib.Error {
    var v = new Vda.Value ();
    try {
      var mv = _model.get_value_at ((int) col, (int) _row);
      v.force_value (mv);
    } catch (GLib.Error e) {
      warning ("Invalid column index");
    }
    return v;
  }

  public string? get_string (string name) throws GLib.Error {
    var v = _model.get_value_at (_model.get_column_index (name), (int) _row);
    if (v != null) {
      return Gda.value_stringify (v);
    }
    return null;
  }
  public string? get_string_at (uint col) throws GLib.Error {
    var v = _model.get_value_at ((int) col, (int) _row);
    if (v != null) {
      return Gda.value_stringify (v);
    }
    return null;
  }
  // GLib.ListModel interface
  public uint get_n_items () { return n_columns; }
  public GLib.Type get_item_type () { return typeof (DbColumn); }
  public GLib.Object? get_item (uint position) {
    if (position >= _model.get_n_rows ())
      return null;
    return new GColumnModelDataModel (_model, position);
  }
}

