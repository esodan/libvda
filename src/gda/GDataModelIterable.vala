/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * GDataModelIterable.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2011-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 using Gee;
 using Gda;

 namespace Vgda {
	/**
	 * An iterable collection implementation based on {@link Gda.DataModel}
	 */
 	internal class GDataModelIterable : Gee.AbstractCollection<Value?>, Gda.DataModel
 	{
 		private Gda.DataModel model;

 		internal GDataModelIterable (Gda.DataModel model) {
 			this.model = model;
 		}

 		// Iterable Interface

 		internal override Iterator<Value?> iterator ()
 		{
 			return new GDataModelIterator (this.model);
 		}

		// Interface Collection
		internal override bool add (Value? item) {
			try {
				int i = this.model.append_row ();
				if (i >= 0)
					return true;
			} catch {}

			return false;
		}

		/**
		 * {@inheritDoc}
		 *
		 * {@inheritDoc}<< BR >>
		 * <<BR>>
		 * If the collection is a proxy, you need to apply to make changes permanently.
		 */
		internal override void clear () {
			try {
				for (int i = 0; i < this.model.get_n_rows (); i++ ) {
					this.model.remove_row (i);
				}
			} catch {}
		}

		internal override bool contains (Value? item)
		{
			try {
				for (int r = 0; r < this.model.get_n_rows (); r++) {
					for (int c = 0; c < this.model.get_n_columns (); c++) {
						Value v = this.model.get_value_at (c, r);
						if (Gda.value_compare (v, item) == 0)
							return true;
					}
				}
			} catch {}
			return false;
		}

		/**
		 * {@inheritDoc}
		 *
		 * {@inheritDoc}<< BR >>
		 * << BR >>
		 * Search for the first item in the collection that match item and removes it.<<BR>>
		 * <<BR>>
		 * ''Caution:'' Removing a single value removes all the row.
		 * <<BR>>
		 * If the collection is a database proxy, you need to apply to make changes permanently.
		 */
		internal override bool remove (Value? item) {
			for (int r = 0; r < this.model.get_n_rows (); r++) {
				for (int c = 0; c < this.model.get_n_columns (); c++) {
					try {
						Value v = this.model.get_value_at (c, r);
						if (Gda.value_compare (v, item) == 0) {
							this.model.remove_row (r);
							return true;
						}
					}
					catch {
						continue;
					}
				}
			}

			return false;
		}

		internal override bool read_only {
			get {
				if (this.model is Gda.DataProxy)
					return ((Gda.DataProxy) this.model).is_read_only ();

				return true;
			}
		}
		internal override Gee.Collection<Value?> read_only_view {
			owned get {
				if (this.model is Gda.DataProxy)
					return (Gee.Collection<Value?>)
								new GDataModelIterable (((DataProxy)this.model).get_proxied_model ());
				return (Gee.Collection<Value?>) new GDataModelIterable (this.model);
			}
		}

		internal override int size {
			get {
				return this.model.get_n_columns () * this.model.get_n_rows ();
			}
		}

		// Interface Gda.DataModel
		// internal int append_row () throws GLib.Error {
		// 	return this.model.append_row ();
		// }

		// internal Gda.DataModelIter create_iter () {
		// 	return this.model.create_iter ();
		// }

		// internal unowned Gda.Column? describe_column (int col) {
		// 	return this.model.describe_column (col);
		// }

		// internal Gda.DataModelAccessFlags get_access_flags () {
		// 	return this.model.get_access_flags ();
		// }

		// internal Gda.ValueAttribute get_attributes_at (int col, int row) {
		// 	return this.model.get_attributes_at (col, row);
		// }

		// internal int get_n_columns () {
		// 	return this.model.get_n_columns ();
		// }

		// internal int get_n_rows () {
		// 	return this.model.get_n_rows ();
		// }

		// internal unowned GLib.Value? get_value_at (int col, int row) throws GLib.Error {
		// 	return this.model.get_value_at (col, row);
		// }

		// internal int append_values (GLib.List<GLib.Value?>? values) throws GLib.Error {
		// 	return this.model.append_values (values);
		// }

		// internal int get_row_from_values (GLib.SList<GLib.Value?> values, [CCode (array_length = false)] int[] cols_index) {
		// 	return this.model.get_row_from_values (values, cols_index);
		// }
		// internal bool set_values (int row, GLib.List<GLib.Value?>? values) throws GLib.Error {
		// 	return this.model.set_values (row, values);
		// }

		// THIS FUNCTIONS HAVEN'T DEFAULT IMPLEMENTATION OR internal API AND THEN CAN'T BE IMPLEMENTED HERE
		// internal bool get_notify () {
		// 	return this.model.get_notify ();
		// }

		// internal bool iter_at_row (Gda.DataModelIter iter, int row) {
		// 	return false;
		// }

		// internal bool iter_next (Gda.DataModelIter iter) {
		// 	return false;
		// }

		// internal bool iter_prev (Gda.DataModelIter iter) {
		// 	return false;
		// }

		// internal bool iter_set_value (Gda.DataModelIter iter, int col, GLib.Value value) throws GLib.Error {
		// 	return false;
		// }

		// internal void set_notify (bool do_notify_changes) {}
		// internal bool remove_row (int row) throws GLib.Error {
		// 	return this.model.remove_row (row);
		// }

		// internal void send_hint (Gda.DataModelHint hint, GLib.Value? hint_value) {
		// 	this.model.send_hint (hint, hint_value);
		// }

		// internal bool set_value_at (int col, int row, GLib.Value value) throws GLib.Error {
		// 	return this.model.set_value_at (col, row, value);
		// }

		// internal unowned GLib.Error[] get_exceptions () {
		// 	return this.model.get_exceptions ();
		// }

		// internal void freeze () {
		// 	this.model.freeze ();
		// }

		// internal void thaw () {
		// 	this.model.thaw ();
		// }
 	}
 }
 
