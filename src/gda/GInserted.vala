/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GInserted.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gda;
using Vda;

/**
 * A query result from an insert execution
 */
internal class Vgda.GInserted : GLib.Object, Vda.Result, Vda.Inserted {
  private uint _number = 0;
  private Vda.RowModel _last_inserted = null;

  public GInserted (uint rows, Gda.Set new_row) {
    _number = rows;
    _last_inserted = new GRowModelSet (new_row) as RowModel;
  }
  // Vda.Inserted interface
  public uint number { get { return _number; } }
  public Vda.RowModel last_inserted { get { return _last_inserted as RowModel; } }
}

