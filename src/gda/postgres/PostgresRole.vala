/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * PostgresRole.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
/**
 * An interface for database Access Control
 */
internal class Vgpg.PostgresRole : GLib.Object, Vda.Role {
  Vda.Connection _connection;
  int64 _oid = -1;
  public Vda.Connection connection { get { return _connection; } }
  public async string name () throws GLib.Error {
    string user = "main";
    if (connection.is_opened) {
      string sql = "SELECT current_user;";
      if (_oid != -1) {
        sql = "SELECT rolename FROM pg_authid WHERE oid = "+_oid.to_string ();
      }
      try {
        var q = connection.parse_string (sql);
        var res = yield q.execute (null);
        if (res is TableModel) {
          var r = ((GLib.ListModel) res).get_item (0) as RowModel;
          if (r != null) {
            string u = r.get_string_at (0);
            if (u != null) {
              user = u;
            }
          }
        }
      } catch (GLib.Error e) {
        warning (_("Get current user fail: %s"), e.message);
      }
    }
    return user;
  }
  public async HashModel membership () throws GLib.Error {
    var c = new HashList ();
      if (connection.is_opened) {
      try {
        var q = connection.parse_string ("SELECT oid, member FROM pg_auth_members WHERE member = (SELECT oid FROM pg_authid WHERE rolname = current_user);");
        var res = yield q.execute (null);
        if (res is TableModel) {
          var t = res as GLib.ListModel;
          for (int i = 0; i < t.get_n_items (); i++) {
            var m = t.get_item (i) as RowModel;
            if (m == null) continue;
            c.add (m);
          }
        }
      } catch (GLib.Error e) {
        warning (_("Get current user's membership fail: %s"), e.message);
      }
    }
    return c;
  }
  public async Role.Grant privilages (MetaObject object) throws GLib.Error {
    throw new RoleError.PROVILAGE_ERROR (_("Not implemented"));
  }
  public async void change_privilages (MetaObject object, Role.Grant grant) throws GLib.Error {
    throw new RoleError.PROVILAGE_ERROR (_("Not implemented"));
  }
  public PostgresRole (Vda.Connection cnc) {
    _connection = cnc;
    _oid = -1;
  }
  public PostgresRole.user_oid (Vda.Connection cnc, int64 oid) {
    _connection = cnc;
    _oid = oid;
  }
}

