/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * GCatalog.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2012-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gda;
using Gee;
using Vda;

namespace Vgda {
	/**
	 * Introspected database catalog as a top level object in a database
	 *
	 * Stability: unstable
	 */
	public class GCatalog : Object, ObjectMeta, NamedObjectMeta, CollectionMeta, CatalogMeta
	{
		Collection<SchemaMeta> _schemas = new Gee.HashSet<SchemaMeta> ();
		// DbObject interface
		// DbObject Interface
		public Vda.Connection connection { get; set; }
		public bool update_meta { get; set; }
		public void update () throws Error {}
		public void save () throws Error {}
		public void append () throws Error {}
		public void drop (bool cascade) throws Error {}
		// DbNamedObject Interface
		public string name { get; set; }
		// DbCollection interface
		public Collection<SchemaMeta> schemas { owned get { return _schemas; } }
	}
}
