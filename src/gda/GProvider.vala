/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GProvider.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gda;
using Gee;
using Vda;
/**
 * Implementation of {@link Vda.Connection} using GDA's providers
 */
public class Vgda.GProvider : GLib.Object, Vda.Connection {
  protected string _provider = null;
  protected string _cnc_string = null;
  private Gda.Connection _cnc = null;
  private Gee.HashMap<string, PreparedQuery> queries = new Gee.HashMap<string, PreparedQuery> ();
  private Vda.Connection.Status _status = Vda.Connection.Status.DISCONNECTED;
  private ConnectionParameters _parameters;

  /**
   * Provides access to {@link Gda.Connection} object to access all
   * GDA features
   */
  public Gda.Connection gda_connection { get { return _cnc; } }

  internal Vda.Connection.Status status { get { return _status; } }
  internal Gda.Connection cnc { get { return _cnc; } }

  // Vda.Connection interface
  internal ConnectionParameters parameters {
    get {
      return _parameters;
    }
    set {
      _parameters = value;
      _cnc_string = _parameters.to_string ();
    }
  }
  internal bool is_opened {
    get {
      if (cnc == null) return false;
      return cnc.is_opened ();
    }
  }
  internal string connection_string { get { return _cnc_string; } }

  internal async void close ()  throws GLib.Error {
    if (cnc == null) return;
    if (cnc.close ()) {
      closed ();
    }
  }
  internal async Vda.Connection.Status open () throws GLib.Error {
    _cnc_string = _parameters.to_string ();
    return yield open_from_string (_cnc_string);
  }
  internal async Vda.Connection.Status open_from_string (string cnc_string) throws GLib.Error {
    _cnc_string = cnc_string;
    if (_parameters == null) {
      _parameters = new ConnectionParameters (_cnc_string);
    }
    if (_provider == null) {
      _status = Vda.Connection.Status.CANCELED;
      canceled ("No connection string was given");
      return Vda.Connection.Status.CANCELED;
    }
    try {
      _cnc = Gda.Connection.open_from_string (_provider, _cnc_string, null, Gda.ConnectionOptions.NONE);
      if (cnc.is_opened ()) {
        _status = Vda.Connection.Status.CONNECTED;
        opened ();
        return CONNECTED;
      }
    } catch (GLib.Error e) {
      _status = Vda.Connection.Status.CANCELED;
      canceled (e.message);
      return Vda.Connection.Status.CANCELED;
    }
    _status = Vda.Connection.Status.CANCELED;
    canceled ("No connection was made");
    return Vda.Connection.Status.CANCELED;
  }

  internal Vda.Query parse_string (string sql) throws GLib.Error {
    if (cnc == null) {
      return new InvalidPreparedQuery ("No connection is set") as Vda.PreparedQuery;
    }
    if (!cnc.is_opened ()) {
      return new InvalidPreparedQuery ("Connection is not opened") as Vda.PreparedQuery;
    }
    return new GQuery (this, sql);
  }
  internal Vda.PreparedQuery? parse_string_prepared (string? name, string sql) throws GLib.Error {
    if (cnc == null) {
      return new InvalidPreparedQuery ("No connection is set") as PreparedQuery;
    }
    if (!cnc.is_opened ()) {
      return new InvalidPreparedQuery ("Connection is not opened") as Vda.PreparedQuery;
    }
    var q = new GPreparedQuery (this, sql, name);
    if (name != null) {
      queries.set (name, q as PreparedQuery);
    }
    return q as PreparedQuery;
  }
  internal Vda.PreparedQuery? get_prepared_query (string name) {
    return queries.get (name);
  }

  internal Vda.PreparedQuery? query_from_command (Vda.SqlCommand cmd, string? name) throws GLib.Error {
    if (cmd is Vda.Stringifiable) {
      return new GPreparedQuery (this, ((Vda.Stringifiable) cmd).to_string (), name);
    }
    return null;
  }
  // internal string value_to_string (GLib.Value val) {
  //   return Gda.value_stringify (val);
  // }
  internal string value_to_quoted_string (SqlValue v) {
    var val = v.to_gvalue ();
    return cnc.get_provider ().value_to_sql_string (this.cnc, val);
  }

  // ConnectionRolebased
  // internal virtual Role? current_user () {
  //   return null;
  // }

  // ConnectionTransactional
  // internal bool add_savepoint (string? name) throws GLib.Error {
  //   if (cnc == null) return false;
  //   if (cnc.add_savepoint (name)) {
  //     return true;
  //   }
  //   return false;
  // }
  // internal bool delete_savepoint (string? name) throws GLib.Error {
  //   if (cnc == null) return false;
  //   if (cnc.delete_savepoint (name)) {
  //     return true;
  //   }
  //   return false;
  // }
  // internal bool rollback_savepoint (string? name) throws GLib.Error {
  //   if (cnc == null) return false;
  //   if (cnc.rollback_savepoint (name)) {
  //     return true;
  //   }
  //   return false;
  // }
  // internal bool begin_transaction (string? name) throws GLib.Error {
  //   if (cnc == null) return false;
  //   if (cnc.begin_transaction (name, TransactionIsolation.SERVER_DEFAULT)) {
  //     return true;
  //   }
  //   return false;
  // }
  // internal bool commit_transaction (string? name) throws GLib.Error {
  //   if (cnc == null) return false;
  //   if (cnc.commit_transaction (name)) {
  //     return true;
  //   }
  //   return false;
  // }
  // internal bool rollback_transaction (string? name) throws GLib.Error {
  //   if (cnc == null) return false;
  //   if (cnc.commit_transaction (name)) {
  //     return true;
  //   }
  //   return false;
  // }

}


internal class Vgda.GConnectionParameters : Vda.ConnectionParameters {
  public string provider { get; set; }
  public GConnectionParameters (string cnc_str) {
  	base (cnc_str);
  }
}

