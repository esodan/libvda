/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GTableModel.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gda;
using Vda;

/**
 * A table implementing {@link GLib.ListModel} as a result of a query execution
 */
internal class Vgda.GTableModel : GLib.Object, GLib.ListModel, Vda.Result, Vda.TableModel {
  private Gda.DataModel _model = null;
  public GTableModel (Gda.DataModel model) {
    _model = model;
  }
  // GLib.ListModel interface
  public uint get_n_items () { return (uint) _model.get_n_rows (); }

  public GLib.Type get_item_type () { return typeof (RowModel); }

  public GLib.Object? get_item (uint position) {
    if (position > _model.get_n_rows ())
      return null;
    return new GRowModelDataModel (_model, position) as Object;
  }
}
