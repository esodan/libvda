/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GParametersSet.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gda;

/**
 * Parameters for server database connection
 */
internal class Vgda.GParametersSet : GLib.Object, Vda.SqlParameters {

  public GParametersSet (Gda.Set @set) {
    _set = @set;
  }

  public new Gda.Set @set { get; internal set; }

  // Parameters interface
  public void set_value (string name, GLib.Value val) {
    var h = @set.get_holder (name);
    if (h != null) {
      try {
        h.set_value (val);
      } catch (GLib.Error e) {
        warning (_("Value can't be set for parameter named: %s"), name);
      }
    } else {
      warning (_("No parameter '%s' exists"), name);
    }
  }
  public GLib.Value? get_value (string name) {
    var h = @set.get_holder (name);
    if (h != null) {
      return h.get_value ();
    }
    return null;
  }
  public void set_sql_value (string name, Vda.SqlValue val) {
    var h = @set.get_holder (name);
    if (h != null) {
      try {
        h.set_value (val.to_gvalue ());
      } catch (GLib.Error e) {
        warning (_("Value can't be set for parameter named: %s"), name);
      }
    } else {
      warning ("No parameter '%s' exists", name);
    }
  }
  public Vda.SqlValue get_sql_value (string name) {
    var h = @set.get_holder (name);
    if (h != null) {
      var v = new Vda.Value ();
      v.force_value (h.get_value ());
      return v;
    }
    return new Vda.ValueNull ();
  }
  public bool has_param (string name) {
      return @set.get_holder (name) != null;
  }
}

