/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GColumnModelDataModel.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gda;

/**
 * A set of rows, implementing {@link Vda.DbColumn}
 * and using a {@link Gda.DataModel} as source
 */
internal class Vgda.GColumnModelDataModel : GLib.Object, Vda.ColumnModel {
  private Gda.DataModel _model = null;
  private string _name = "";
  private GLib.Type _data_type = GLib.Type.INVALID;
  private uint _index = 0;

  public GColumnModelDataModel (Gda.DataModel model, uint index) {
    _index = index;
    _model = model;
    var c = _model.describe_column ((int) index);
    _data_type = c.get_g_type ();
    _name = c.get_name ();
  }

  // DbColumn implementation
  public string name { get { return _name; } }
  public GLib.Type data_type { get { return _data_type; } }
}

