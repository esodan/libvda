/* -* Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -* */
/*
 * Value.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * An implementation of {@link Vda.SqlValue}
 */
public class Vda.Value  : Object, Stringifiable, SqlValue
{
	protected string _name;
	protected GLib.Value _val = GLib.Value(typeof (ValueNull));
	internal string name { get { return _name; } }
	/**
	 * Set internal {@link GLib.Value} {@link _val} to the given
	 * value, reseting the current value's type to. Use with caution,
	 * continue reading.
	 *
	 * This should be used only on new value implementations. Forcing
	 * to a type incompatible with the current one, can produce
	 * crashes or un-predictable behavior. Is better if you use
	 * {@link SqlValue.cast} to convert the current to value to another.
	 */
	public virtual bool force_value (GLib.Value val) {
		_val = GLib.Value (val.type ());
		val.copy (ref _val);
		return true;
	}
	internal virtual bool from_value (GLib.Value val) {
		if (GLib.Value.type_compatible (val.type (), _val.type ())) {
			val.copy (ref _val);
			return true;
		}
		if (GLib.Value.type_transformable (val.type (), _val.type ())) {
			val.transform (ref _val);
			return true;
		}
		return false;
	}
	internal virtual SqlValue? cast (Type type) {
		if (!is_compatible (type)) {
			return new ValueNull ();
		}
		SqlValue obj = SqlValue.new_from_gtype (type);
		if (obj.from_value (_val)) {
			return obj;
		}
		return new ValueNull ();
	}
	internal bool type_compatible (Type type) {
		if (GLib.Value.type_transformable (_val.type (), type)) {
			return true;
		}
		return false;
	}

	internal virtual bool is_compatible (Type type) {
		return type_compatible (type);
	}
	internal virtual bool parse (string str) { return false; }
	internal virtual GLib.Value to_gvalue () {
		var nv = GLib.Value (_val.type ());
		_val.copy (ref nv);
		return nv;
	}
	internal virtual string to_string () {
		if (_val.holds (typeof (string))) {
			return (string) _val;
		}
		if (_val.holds (typeof (GLib.Object))) {
			GLib.Object o = (GLib.Object) _val;
			if (o is Vda.Stringifiable) {
				Vda.Stringifiable so = (Vda.Stringifiable) o;
				string stro = so.to_string ();
				return stro;
			}
			if (o is Vda.ExpressionValue) {
				Vda.ExpressionValue so = (Vda.ExpressionValue) o;
				string stro = so.to_string ();
				return stro;
			}
		}
		string str = transform_value_to_string (_val);
		if (str == "") {
			GLib.Value v = GLib.Value (typeof (string));
			if (!GLib.Value.type_transformable (_val.type (), typeof (string))) {
				return "NULL";
			}
			_val.transform (ref v);
			return (string) v;
		}

		return "NULL";
	}
	internal virtual string to_string_quoted () {
		string str = to_string ();
		return "'"+str+"'";
	}
	internal virtual string to_sql_expression () {
		return to_string ();
	}
}

internal string transform_value_to_string (GLib.Value val)
{
  message ("Transf to STRING: %s", val.type().name());
  if (val.holds (typeof (int))) {
    return "%d".printf ((int) val);
  }
  if (val.holds (typeof (int64))) {
    return ("%"+int64.FORMAT).printf ((int64) val);
  }
  if (val.holds (typeof (int16))) {
    return "%d".printf ((int16) val);
  }
  if (val.holds (typeof (int32))) {
    return "%d".printf ((int32) val);
  }
  if (val.holds (typeof (int8))) {
    return "%d".printf ((int8) val);
  }
  if (val.holds (typeof (string))) {
    return (string) val;
  }
  if (val.holds (typeof (float))) {
    return "%f".printf ((float) val);
  }
  if (val.holds (typeof (double))) {
    return "%lf".printf ((double) val);
  }

  return "";
}

/**
 * An implementation of {@link SqlValueNull}
 */
public class Vda.ValueNull : Value, SqlValueNull {
	construct {
		_name = "NULL";
		_val = GLib.Value(typeof (SqlValueNull));
	}
	internal override bool parse (string str) {
		if (str.down () == "null") {
			return true;
		}
		return false;
	}
	internal override string to_string () {
		return "NULL";
	}
	internal override string to_sql_expression () {
		return "NULL";
	}
	internal override bool is_compatible (Type type) {
		return false;
	}
}

/**
 * An implementation of {@link SqlValueString}
 */
public class Vda.ValueString : Value, SqlValueString {
	construct {
		_name = "string";
		_val = GLib.Value(typeof (string));
	}
	internal override bool parse (string str) {
		_val.set_string (str);
		return true;
	}
	internal override string to_string () {
		return (string) _val;
	}
	internal override string to_string_quoted () {
		string str = (string) _val;
		if (str == null) {
			return "\'NULL\'";
		}

		str.escape (null);
		str = str.replace ("'", "''");
		return "'"+str+"'";
	}
	internal override bool is_compatible (Type type) {
		if (type_compatible (type)) {
			return true;
		}
		if (type.is_a (typeof (SqlValue))) {
			return true;
		}
		return false;
	}
}

/**
 * An implementation of {@link SqlValueXml}
 */
public class Vda.ValueXml : ValueString, SqlValueXml {
	GXml.DomDocument _document = null;

	internal GXml.DomDocument document {
	    get {
	        if (_document == null) {
	            _document = new GXml.Document () as GXml.DomDocument;
		        try {
		            _document.read_from_string (_val.get_string ());
		        } catch (GLib.Error e) {
		            warning (_("Error parsing XML string value: %s"), e.message);
		        }
	        }
	        return _document;
	    }
	}

	construct {
		_name = "xml";
	}

	internal override bool parse (string str) {
		_val.set_string (str);
		if (_document != null) {
		    try {
		        _document.read_from_string (str);
		    } catch (GLib.Error e) {
		        warning (_("Error parsing XML string: %s"), e.message);
		    }
		}
		return true;
	}
	internal override string to_string () {
		if (_document != null) {
		    try {
		        return _document.write_string ();
		    } catch (GLib.Error e) {
		        warning (_("Error exporting XML string: %s"), e.message);
		    }
		}
		return _val.get_string ();
	}
}

/**
 * An implementation of {@link SqlValueJson}
 */
public class Vda.ValueJson : ValueString, SqlValueJson {
	Json.Node _document = null;

	internal Json.Node document {
	    get {
	        if (_document == null) {
		        try {
		            _document = Json.from_string (_val.get_string ());
		        } catch (GLib.Error e) {
		            warning (_("Error parsing JSON string value: %s"), e.message);
		        }
	        }
	        return _document;
	    }
	}

	construct {
		_name = "json";
	}

	internal override bool parse (string str) {
		_val.set_string (str);
		if (_document != null) {
		    try {
		        _document = Json.from_string (str);
		    } catch (GLib.Error e) {
		        warning (_("Error parsing JSON string: %s"), e.message);
		    }
		}
		return true;
	}
	internal override string to_string () {
		if (_document != null) {
		    return Json.to_string (_document, false);
		}
		return _val.get_string ();
	}
}
/**
 * An implementation of {@link SqlValueText}
 */
public class Vda.ValueText : ValueString, SqlValueText {
	construct {
		_name = "text";
	}
}
/**
 * An implementation of {@link SqlValueName}
 */
public class Vda.ValueName : ValueString, SqlValueName {
	construct {
		_name = "name";
	}
}
/**
 * An implementation of {@link SqlValueBool}
 */
public class Vda.ValueBool : Value, SqlValueBool {
	construct {
		_name = "bool";
		_val = GLib.Value(typeof (bool));
	}
	internal override bool parse (string str) {
		string nstr = str.down ();
		if (nstr == "true") {
			_val = true;
		} else {
			_val = false;
		}
		return true;
	}
	internal override string to_string () {
		return ((bool) _val) ? "TRUE" : "FALSE";
	}
	internal override string to_sql_expression () {
		return to_string ();
	}
}
/**
 * An implementation of {@link SqlValueBit}
 */
public class Vda.ValueBit : ValueBool, SqlValueBit {
	construct {
		_name = "bit";
	}
	internal override bool parse (string str) {
		string nstr = str.down ();
		if (nstr == "1") {
			_val = true;
		} else {
			_val = false;
		}
		return true;
	}
	internal override string to_string () {
		return ((bool) _val) ? "1" : "0";
	}
}
/**
 * An implementation of {@link SqlValueInteger}
 */
public class Vda.ValueInteger : Value, SqlValue, SqlValueInteger {
	construct {
		_name = "integer";
		_val = GLib.Value(typeof (int));
	}
	internal override bool parse (string str) {
		_val = GLib.Value(typeof (int));
		_val.set_int ((int) double.parse (str));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (int))) {
      return "%d".printf ((int) _val);
    }

    return transform_value_to_string (_val);
	}
	internal override string to_sql_expression () {
		return to_string ();
	}
}
/**
 * An implementation of {@link SqlValueByte}
 */
public class Vda.ValueByte : ValueInteger, SqlValueByte  {
	construct {
		_name = "byte";
		_val = GLib.Value(typeof (int8));
	}
	internal override bool parse (string str) {
		_val.set_int ((int) ((int8) double.parse (str)));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (int8))) {
      return "%d".printf ((int8) _val);
    }

    return transform_value_to_string (_val);
	}
}
/**
 * An implementation of {@link SqlValueInt2}
 */
public class Vda.ValueInt2 : ValueInteger, SqlValueInt2  {
	construct {
		_name = "int16";
		_val = GLib.Value(typeof (int16));
	}
	internal override bool parse (string str) {
		_val.set_int ((int) ((int16) double.parse (str)));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (int16))) {
      return "%d".printf ((int16) _val);
    }

    return transform_value_to_string (_val);
	}
}
/**
 * An implementation of {@link SqlValueInt4}
 */
public class Vda.ValueInt4 : ValueInteger, SqlValueInt4 {
	construct {
		_name = "int32";
		_val = GLib.Value(typeof (int32));
	}
	internal override bool parse (string str) {
		_val.set_int64 ((int64) ((int32) double.parse (str)));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (int32))) {
      return "%d".printf ((int32) _val);
    }

    return transform_value_to_string (_val);
	}
}
/**
 * An implementation of {@link SqlValueInt8}
 */
public class Vda.ValueInt8 : ValueInteger, SqlValueInt8 {
	construct {
		_name = "int64";
		_val = GLib.Value(typeof (int64));
	}
	internal override bool parse (string str) {
		_val.set_int64 ((int64) double.parse (str));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (int64))) {
      return ("%"+int64.FORMAT).printf ((int64) _val);
    }

    return transform_value_to_string (_val);
	}
}

/**
 * An implementation of {@link SqlValueInteger}
 */
public class Vda.ValueUnsignedInteger : Value, SqlValue, SqlValueUnsignedInteger {
	construct {
		_name = "uint";
		_val = GLib.Value(typeof (uint));
	}
	internal override bool parse (string str) {
		_val.set_uint ((uint) double.parse (str));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (uint))) {
      return "%u".printf ((uint) _val);
    }

    return transform_value_to_string (_val);
	}
	internal override string to_sql_expression () {
		return to_string ();
	}
}
/**
 * An implementation of {@link SqlValueByte}
 */
public class Vda.ValueUnsignedByte : ValueUnsignedInteger, SqlValueUnsignedByte  {
	construct {
		_name = "ubyte";
		_val = GLib.Value(typeof (uint8));
	}
	internal override bool parse (string str) {
		_val.set_uint ((uint)((uint8) double.parse (str)));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (uint8))) {
      return "%d".printf ((uint8) _val);
    }

    return transform_value_to_string (_val);
	}
}
/**
 * An implementation of {@link SqlValueInt2}
 */
public class Vda.ValueUnsignedInt2 : ValueUnsignedInteger, SqlValueUnsignedInt2  {
	construct {
		_name = "uint2";
		_val = GLib.Value(typeof (uint16));
	}
	internal override bool parse (string str) {
		_val.set_uint ((uint)((uint16) double.parse (str)));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (uint16))) {
      return "%d".printf ((uint16) _val);
    }

    return transform_value_to_string (_val);
	}
}
/**
 * An implementation of {@link SqlValueInt4}
 */
public class Vda.ValueUnsignedInt4 : ValueUnsignedInteger, SqlValueUnsignedInt4 {
	construct {
		_name = "uint4";
		_val = GLib.Value(typeof (uint32));
	}
	internal override bool parse (string str) {
		_val.set_uint64 ((uint64) ((uint32) double.parse (str)));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (uint32))) {
      return "%I64u".printf ((uint32) _val);
    }

    return transform_value_to_string (_val);
	}
}
/**
 * An implementation of {@link SqlValueInt8}
 */
public class Vda.ValueUnsignedInt8 : ValueUnsignedInteger, SqlValueUnsignedInt8 {
	construct {
		_name = "uint8";
		_val = GLib.Value(typeof (uint64));
	}
	internal override bool parse (string str) {
		_val.set_uint64 ((uint64) ((uint64) double.parse (str)));
		return true;
	}
	internal override string to_string () {
    if (_val.holds (typeof (uint64))) {
      return ("%"+uint64.FORMAT).printf ((uint64) _val);
    }

    return transform_value_to_string (_val);
	}
}

/**
 * An implementation of {@link SqlValueOid}
 */
public class Vda.ValueOid : ValueInteger, SqlValueOid  {}

/**
 * An implementation of {@link SqlValueNumeric}
 */
public class Vda.ValueNumeric : Value, SqlValueNumeric {
	int precision = 6;
	construct {
		_name = "numeric";
		_val = GLib.Value(typeof (double));
	}
	internal void set_precision (int p) { precision = p; }
	internal int get_precision () { return precision; }
	internal virtual string format (string str) {
		return str.printf ((double) _val);
	}
	internal override bool parse (string str) {
		_val.set_double (double.parse (str));
		return true;
	}
	internal override string to_string () {
		return "%g".printf (_val.get_double ());
	}
	internal override string to_sql_expression () {
		return to_string ();
	}
	internal override bool from_value (GLib.Value val) {
		if (val.type ().is_a (typeof (ValueNumeric))) {
			var v = val.get_object () as ValueNumeric;
			if (v == null) {
				return false;
			}
			_val = ((SqlValueNumeric) val.get_object ()).get_double ();
		}
		if (val.holds (typeof (float))) {
			_val = (double) ((float) val);
			return true;
		}
		if (val.holds (typeof (double))) {
			_val = (double) val;
			return true;
		}
		if (val.holds (typeof (bool))) {
			_val = (double) ((bool) val);
			return true;
		}
		if (val.holds (typeof (string))) {
			return parse ((string) val);
		}
		if (val.holds (typeof (int))) {
			_val = (double) ((int) val);
			return true;
		}
		if (val.holds (typeof (int64))) {
			_val = (double) ((int64) val);
			return true;
		}
		if (val.holds (typeof (int16))) {
			_val = (double) ((int16) val);
			return true;
		}
		if (val.holds (typeof (int32))) {
			_val = (double) ((int32) val);
			return true;
		}
		if (val.holds (typeof (int8))) {
			_val = (double) ((int8) val);
			return true;
		}
		if (val.holds (typeof (long))) {
			_val = (double) ((long) val);
			return true;
		}
		if (val.holds (typeof (uint))) {
			_val = (double) ((uint) val);
			return true;
		}
		if (val.holds (typeof (uint16))) {
			_val = (double) ((float) val);
			return true;
		}
		if (val.holds (typeof (uint32))) {
			_val = (double) ((uint32) val);
			return true;
		}
		if (val.holds (typeof (uint64))) {
			_val = (double) ((uint64) val);
			return true;
		}
		if (val.holds (typeof (ulong))) {
			_val = (double) ((ulong) val);
			return true;
		}
		if (val.holds (typeof (short))) {
			_val = (double) ((short) val);
			return true;
		}
		if (val.holds (typeof (ushort))) {
			_val = (double) ((ushort) val);
			return true;
		}
		if (val.holds (typeof (char))) {
			_val = (double) ((char) val);
			return true;
		}
		if (val.holds (typeof (uchar))) {
			_val = (double) ((uchar) val);
			return true;
		}
		if (val.holds (typeof (size_t))) {
			_val = (double) ((size_t) val);
			return true;
		}
		return false;
	}
	internal override bool is_compatible (Type type) {
		if (type_compatible (type)) {
			return true;
		}
		if (type.is_a (typeof (SqlValueNumeric))) {
			return true;
		}
		if (type.is_a (typeof (SqlValueMoney))) {
			return true;
		}
		if (type == typeof (float)) {
			return true;
		}
		if (type == typeof (double)) {
			return true;
		}
		if (type == typeof (int)) {
			return true;
		}
		if (type == typeof (int16)) {
			return true;
		}
		if (type == typeof (int32)) {
			return true;
		}
		if (type == typeof (int64)) {
			return true;
		}
		if (type == typeof (uint)) {
			return true;
		}
		if (type == typeof (uint16)) {
			return true;
		}
		if (type == typeof (uint32)) {
			return true;
		}
		if (type == typeof (uint64)) {
			return true;
		}
		if (type == typeof (string)) {
			return true;
		}
		if (type == typeof (char)) {
			return true;
		}
		if (type == typeof (uchar)) {
			return true;
		}
		if (type == typeof (short)) {
			return true;
		}
		if (type == typeof (ushort)) {
			return true;
		}
		return false;
	}
	internal double get_double () {
		return (double) _val;
	}
	internal double get_real () {
		return (double) _val;
	}
	internal double get_imaginary () {
		return 0.0;
	}
	internal void set_double (double v) {
		_val = v;
	}
	internal void set_real (double r) {
		_val = r;
	}
	internal void set_imaginary (double img) {
		// FIXME:
	}
}
/**
 * An implementation of {@link SqlValueFloat}
 */
public class Vda.ValueFloat : ValueNumeric, SqlValueFloat {
	construct {
		_name = "float";
		_val = GLib.Value(typeof (float));
	}
	internal override bool parse (string str) {
		_val.set_float ((float) ((float) double.parse (str)));
		return true;
	}
	internal float get_float () {
		return (float) ((float) _val);
	}
	internal override string to_string () {
    if (_val.holds (typeof (float))) {
      return "%g".printf ((float) _val);
    }

    return transform_value_to_string (_val);
	}
}
/**
 * An implementation of {@link SqlValueDouble}
 */
public class Vda.ValueDouble : ValueNumeric, SqlValueDouble {
	construct {
		_name = "double";
		_val = GLib.Value(typeof (double));
	}
}
/**
 * An implementation of {@link SqlValueMoney}
 */
public class Vda.ValueMoney : ValueNumeric, SqlValueMoney
{
  private int _int_precision;
  construct {
    var lc = GLibc.LConv.locale_conventions ();
    set_precision (lc->frac_digits);
    _int_precision = lc->int_frac_digits;
  }
  private string format_locale (bool international) {
    var v = (double) _val;
    var i = (int) v;
    var f = v - i;
    int p = get_precision ();
    if (international) {
      p = get_int_precision ();
    }
    var rf = f * GLib.Math.pow (10, p);
    rf = GLib.Math.ceil (rf);
    var ip = "%d".printf (i);
    var fp = "%g".printf (rf);
    var lc = GLibc.LConv.locale_conventions ();
    string mstr = "";
    if (i >= 0 && lc->int_p_cs_precedes == 1 && international) {
      mstr += lc->int_curr_symbol;
    }
    if (i < 0 && lc->int_n_cs_precedes == 1 && international) {
      mstr += lc->int_curr_symbol;
    }
    if (i >= 0 && lc->p_cs_precedes == 1) {
      mstr += lc->currency_symbol;
    }
    if (i < 0 && lc->n_cs_precedes == 1) {
      mstr += lc->currency_symbol;
    }
    unichar c = '\0';
    int index = ip.length;
    int cp = 0;
    StringBuilder sip = new StringBuilder ("");
    while (ip.get_prev_char (ref index, out c)) {
      cp++;
      if (cp == 4) {
        sip.prepend (lc->mon_thousands_sep);
        cp = 0;
      }
      sip.prepend_unichar (c);
    }
    mstr += sip.str + lc->mon_decimal_point + fp;
    return mstr;
  }
  internal string locale () {
	  return format_locale (false);
  }
  internal string int_locale () {
	  return format_locale (true);
  }
	internal int get_int_precision () {
    return _int_precision;
	}
	internal void set_int_precision (int p) {
	  if (p < 0) {
      return;
	  }
    _int_precision = p;
	}
  internal override bool parse (string str) {
    unichar c = '\0';
    string nstr = "";
    int i = str.length;
    int m = 0;
    bool p = false;
    while (str.get_prev_char (ref i, out c)) {
      if (!p && (c == ',' || c == '.')) {
        p = true;
      }
      if (!p) {
        m++;
      }
    }
    i = 0;
    while (str.get_next_char (ref i, out c)) {
      if (c.isdigit () || c == '+' || c == '-') {
        nstr += c.to_string ();
      }
    }
    double k = 1;
    if (p) {
        k = GLib.Math.pow (10.0, m);
    }
    double v = double.parse (nstr) / k;
	  _val.set_double (v);
	  return true;
  }
  internal override string to_string () {
    return locale ();
  }
}

/**
 * An implementation of {@link SqlValueTimestamp} with time zone
 */
public class Vda.ValueTimestamp : Value, SqlValueTimestamp {
	protected DateTime dt;
	construct {
		dt = new DateTime.now ();
		_name = "timestamp with time zone";
		_val = GLib.Value (typeof (DateTime));
		_val = dt;
	}
	// Value
	internal override bool from_value (GLib.Value val) {
		if (val.holds (typeof (ValueTimestamp))) {
			var v = val.get_object () as ValueTimestamp;
			if (v == null) {
				return false;
			}
			dt = v.get_timestamp ().add_days (0);
			_val = dt;
		}
		if (val.holds (typeof (string))) {
			return parse ((string) val);
		}
		if (val.holds (typeof (DateTime))) {
			DateTime nd = ((DateTime) val).add_days (0);
			set_timestamp (nd);
			return true;
		}
		return false;
	}
	internal override bool parse (string str) {
        string nstr = str.replace (" ","T");
        var tz = new TimeZone.local ();
		var ndt = new DateTime.from_iso8601 (nstr, tz);
		if (ndt != null) {
			dt = ndt.add_days (0);
			return true;
		}
		return false;
	}
	internal override GLib.Value to_gvalue () {
		return _val;
	}
	internal override string to_string () {
		return dt.format ("%FT%T%:::z");
	}
	internal virtual string format_local () {
		var ndt = dt.to_local ();
		return ndt.format ("%FT%T%:::z");
	}
	internal virtual string format_utc () {
		var ndt = dt.to_utc ();
		return ndt.format ("%FT%T%:::z");
	}
	internal virtual string format_locale () {
		var ndt = dt.to_local ();
		return ndt.format ("%x %X:::z");
	}
	internal DateTime get_timestamp () { return dt; }
	internal void set_timestamp (DateTime ts) {
		dt = ts.add_days (0);
		_val = dt;
	}
	internal override bool is_compatible (Type type) {
		if (type_compatible (type)) {
			return true;
		}
		if (type.is_a (typeof (SqlValueTimestamp))) {
			return true;
		}
		if (type == typeof (DateTime)) {
			return true;
		}
		return false;
	}
	internal override SqlValue? cast (Type type) {
		if (!is_compatible (type)) {
			return new ValueNull ();
		}
		if (type.is_a (typeof (SqlValueTimestamp))) {
			var v = new ValueTimestamp ();
			v.parse (to_string ());
			return v;
		}
		if (type == typeof (DateTime)) {
			var v = new ValueTimestamp ();
			v.set_timestamp (dt);
			return v;
		}
		return new ValueNull ();
	}

	internal string format_date () {
	  return dt.format ("%F");
	}
	internal string format_date_locale () {
	  return dt.format ("%x");
	}
	internal string format_time () {
	  return dt.format ("%T:::z");
	}
	internal string format_time_local () {
	  var ndt = dt.to_local ();
		return ndt.format ("%T%:::z");
	}
	internal string format_time_local_ntz () {
	  var ndt = dt.to_local ();
		return ndt.format ("%T%");
	}
}

/**
 * An implementation of {@link SqlValueTimestampNtz} without time zone
 */
public class Vda.ValueTimestampNtz : ValueTimestamp, SqlValueTimestampNtz {
	construct {
		_name = "timestamp without time zone";
	}
	internal override string to_string () {
		return dt.format ("%FT%T");
	}
}
/**
 * Implementation of {@link SqlValueTime} a time with time zone
 */
public class Vda.ValueTime : ValueTimestamp, SqlValueTime {
	construct {
		_name = "time with time zone";
	}
	internal override bool from_value (GLib.Value val) {
		if (val.holds (typeof (ValueTime))) {
			var v = val.get_object () as ValueTime;
			if (v == null) {
				return false;
			}
			dt = v.get_timestamp ().add_days (0);
		}
		if (val.holds (typeof (string))) {
			return parse ((string) val);
		}
		return false;
	}
	internal override bool parse (string str) {
		string d = (new DateTime.now_local ()).format ("%F");
		d += "T"+str;
		var tz = new TimeZone.local ();
		var ndt = new DateTime.from_iso8601 (d, tz);
		if (ndt != null) {
			dt = ndt.add_days (0);
			return true;
		}
		return false;
	}
	internal override string to_string () {
		return dt.format ("%T%:::z");
	}
	internal override string format_local () {
		var ndt = dt.to_local ();
		return ndt.format ("%T:::z");
	}
	internal override string format_utc () {
		var ndt = dt.to_utc ();
		return ndt.format ("%T%:::z");
	}
	internal override string format_locale () {
		var ndt = dt.to_local ();
		return ndt.format ("%T%:::z");
	}
}
/**
 * An implementation of {@link SqlValueTimeNtz} as time without time zone
 */
public class Vda.ValueTimeNtz : Vda.ValueTimestampNtz, SqlValueTimeNtz
{
	construct {
		_name = "time without time zone";
	}
	internal override bool from_value (GLib.Value val) {
		if (val.holds (typeof (ValueTimeNtz))) {
			var v = val.get_object () as ValueTimeNtz;
			if (v == null) {
				return false;
			}

			dt = v.get_timestamp ().add_days (0);
		}
		if (val.holds (typeof (string))) {
			return parse ((string) val);
		}
		return false;
	}
	internal override bool parse (string str) {
		string d = (new DateTime.now_local ()).format ("%F");
		d += "T"+str;
		var tz = new TimeZone.local ();
		var ndt = new DateTime.from_iso8601 (d, tz);
		if (ndt != null) {
			dt = ndt.add_days (0);
			return true;
		}
		return false;
	}
	internal override string to_string () {
		return dt.format ("%T");
	}
	internal override string format_local () {
		var ndt = dt.to_local ();
		return ndt.format ("%T");
	}
	internal override string format_utc () {
		var ndt = dt.to_utc ();
		return ndt.format ("%T");
	}
	internal override string format_locale () {
		var ndt = dt.to_local ();
		return ndt.format ("%T");
	}
}

/**
 * An implementation of {@link SqlValueDate}
 */
public class Vda.ValueDate : Value, SqlValueDate {
	protected Date d;
	construct {
		_name = "date";
		_val = GLib.Value(typeof (Date));
		d = Date ();
		var ndt = new DateTime.now_local ();
		d.set_dmy ((GLib.DateDay) ndt.get_day_of_month (),
							(GLib.DateMonth) ndt.get_month (),
							(GLib.DateYear) ndt.get_year ());
	}
	internal override bool parse (string str) {
		var nd = new ValueTimestamp ();
		if (nd.parse (str)) {
			var ndt = nd.get_timestamp ();
			d.set_dmy ((GLib.DateDay) ndt.get_day_of_month (),
								(GLib.DateMonth) ndt.get_month (),
								(GLib.DateYear) ndt.get_year ());
			return true;
		}
		return false;
	}
	internal override GLib.Value to_gvalue () {
		GLib.Value v = GLib.Value (typeof (GLib.Date));
		v.set_boxed ((void*) &d);
		return v;
	}
	/**
	 * Returns a copy of the internal {@link GLib.Date}
	 */
	internal Date get_date () {
		Date nd = Date ();
		nd.set_dmy (d.get_day (), d.get_month (), d.get_year ());
		return nd;
	}
	/**
	 * Initialize the internal {@link GLib.Date} to the year, month and day
	 * of given {@link GLib.Date}
	 */
	internal void set_date (Date nd) {
		d.set_dmy (nd.get_day (), nd.get_month (), nd.get_year ());
	}
	internal override string to_string () {
		return "%d-%d-%d".printf (d.get_year (), d.get_month (), d.get_day ());
	}
}

/**
 * An implementation of {@link Vda.SqlValueBlob}.
 *
 * Data is not taked.
 */
public class Vda.ValueBinary : Vda.Value, Vda.SqlValueBinary {
	GLib.Bytes _bytes = null;
	internal uint size {
	    get {
	        if (_bytes != null) {
	            return _bytes.length;
	        }

	        return 0;
	    }
	}

	/**
	 * Data is copied
	 */
	public ValueBinary.with_data (uint8[] d) {
	    _bytes = new GLib.Bytes (d);
	}
	/**
	 * Data is taken, no copy is performed
	 */
	public ValueBinary.take (uint8[] d) {
	    _bytes = new GLib.Bytes.take (d);
	}

    // SqlValueBinary
	internal GLib.Bytes get_bytes () {
	    return _bytes;
	}
	// SqlValue
	/**
	 * Data is copied.
	 */
	internal override bool parse (string str) {
	    _bytes = new GLib.Bytes (str.data);
	    return true;
	}
}

/**
 * A DataType holder value
 *
 * Since: 1.2
 */
public class Vda.ValueDataType : Vda.Value, SqlValueDataType
{
  construct {
    _name = "DataType";
    _val = GLib.Value(typeof (GLib.Type));
  }
}

// public interface Vda.SqlValueBlob : Object, SqlValueBinary {}

// public interface Vda.SqlValueGeometricPoint : Object, SqlValue {
// 	public abstract double x { get; set; }
// 	public abstract double y { get; set; }
// }
