/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * Expression.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implemenation of {@link SqlExpression}
 */
public class Vda.Expression : Gee.ArrayList<SqlExpression>,
																		GLib.ListModel,
																		SqlExpression
{
	internal void add_expression (SqlExpression exp) {
		if (contains (exp)) {
			warning ("Expression already present: aborting");
		}
		add (exp);
	}
	internal void remove_expression (SqlExpression exp) {
		remove (exp);
	}
	internal virtual string to_string () {
		return "<?>";
	}
  // GLib.ListModel
  internal Object? get_item (uint position) {
    return @get ((int) position) as Object;
  }
  internal Type get_item_type () {
    return typeof (SqlExpression);
  }
  internal uint get_n_items () {
    return size;
  }
}

