/* HashList.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gee;

/**
 * Implemenation of {@link HashModel}
 */
public class Vda.HashList : Gee.ArrayList<Object>,
                              GLib.ListModel,
                              Vda.HashModel
{
  internal new void add (Object object) {
    base.add (object);
  }
  internal new void remove (Object object) {
    base.remove (object);
  }

  internal Object? find (Object key) {
    return @get (index_of (key));
  }

  internal Object? get_item (uint position) {
    return @get ((int) position);
  }
  internal Type get_item_type () { return typeof (Object); }
  internal uint get_n_items () { return (uint) size; }
}
