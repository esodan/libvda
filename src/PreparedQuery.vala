/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * PreparedQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represented a prepared query. Values required by
 * query can be set by using {@link parameters} property.
 *
 * This kind of query holds parameters in an SQL command
 * so you can change its values in order to execute it. Use {@link parameters}
 * to set values using {@link Vda.SqlParameters} interface.
 *
 * {@link Query.sql} will holds a string representing the query
 * with all defined parameters you can set. Parameters have a name and a {@link GLib.Type}
 * and is represented in the string as: ##param_name::param_type, where ## is used
 * to identify where a parameter declaration starts and :: is used to put apart the
 * parameter's name and its type's name.
 */
public interface Vda.PreparedQuery : GLib.Object, Vda.Query {
  public abstract string name { get; }
  public abstract Vda.SqlParameters parameters { get; }
}
