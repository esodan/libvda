/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * ConnectionBlob.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represents a connection with large objects handling capabilities.
 *
 * This kind of connections can hold large binary objects
 * outside tables and refer it with an indentifier
 */
public interface Vda.ConnectionBlob : GLib.Object {
  /**
   * Creates a new large object in the database engine.
   *
   * The identifier should be set to a column in a table, in order
   * to avoid orphan objects.
   */
  public abstract SqlValueBlob create (GLib.InputStream stream);
  /**
   * Deletes a large object in the database engine.
   *
   * The identifier should be removed from the table it is used,
   * in order to avoid errors on reading data.
   */
  public abstract void @delete (SqlValueBlob val);
}

