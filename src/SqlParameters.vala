/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlParameters.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A set of {@link Vda.SqlExpressionValueParameter} paramenters
 * for a {@link Vda.PreparedQuery}.
 */
public interface Vda.SqlParameters : GLib.Object {
  /**
   * Set a value to a paremeter
   */
  public abstract void set_value (string name, GLib.Value val);
  /**
   * Get a value from a paremeter with the given name as a {@link GLib.Value}
   */
  public abstract GLib.Value? get_value (string name);
  /**
   * Set a value to a paremeter as a {@link Vda.SqlValue}
   */
  public abstract void set_sql_value (string name, SqlValue val);
  /**
   * Get a value from a paremeter with the given name as a {@link Vda.SqlValue}
   */
  public abstract SqlValue get_sql_value (string name);
  /**
   * Verify if the parameter with the given name exists
   */
  public abstract bool has_param (string name);
}
