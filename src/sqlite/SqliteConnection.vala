/*
 * ConnectionPostgresql.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2020 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Sqlite;

public errordomain Vsqlite.ConnectionError {
    INVALID_FILE_ERROR
}

/**
 * Implementation of {@link Vda.Connection} for a direct connection to a SQLite file.
 *
 * Connection happends by providing following connection paramters
 *
 * 1.  DB_NAME as the file's URI
 * 1.  READONLY for a read only connection
 * 1.  READWRITE for a read-write connection (default)
 * 1.  READWRITE_OPEN_CREATE to open a new file
 * 1.  OPEN_MEMORY creates a on memory connection
 * 1.  OPEN_NOMUTEX open the database in a multi-thread mode, so others can connect too using its own connection
 * 1,  OPEN_FULLMUTEX open the database in a serialized multi-thread mode, so others can connect too using same connection
 * 1.  OPEN_SHAREDCACHE The database is opened shared cache enabled
 * 1.  OPEN_PRIVATECACHE The database is opened shared cache disabled
 * 1.  OPEN_NOFOLLOW The database filename is not allowed to be a symbolic link
 *
 * {{{
 * var c = new Vsqlite.Connection ();
 * c.open_from_string ("DB_NAME=/home/user/database.db");
 * }}}
 */
public class Vsqlite.Connection : GLib.Object, Vda.Connection {
    private Sqlite.Database database = null;
    private Vda.Connection.Status _status = Vda.Connection.Status.CONNECTED;
    private Gee.HashMap<string,PreparedQuery> _queries = new Gee.HashMap<string,PreparedQuery> ();
    private string _cnc_string = null;

    internal unowned Sqlite.Database get_database () { return database; }

    // Vda.Connection
    internal Vda.Connection.Status status { get { return _status; } }
    internal ConnectionParameters parameters { get; set; }
    internal bool is_opened {
        get  {
            return database != null && _status == Vda.Connection.Status.CONNECTED;
        }
    }
    internal string connection_string {
        get {
            _cnc_string = parameters.to_string ();
            return _cnc_string;
        }
    }

    internal async void
    close () throws GLib.Error {
        if (database == null) return;
        database = null;
    }
    internal async Vda.Connection.Status
    open () throws GLib.Error
    {
        if (parameters == null) {
            _status = Vda.Connection.Status.DISCONNECTED;
            return Vda.Connection.Status.CANCELED;
        }

        if (!parameters.has_param ("DB_NAME")) {
            throw new Vsqlite.ConnectionError.INVALID_FILE_ERROR (_("No file given as DB_NAME parameter"));
        }

        string str = parameters.get ("DB_NAME").@value;

        int flags = 0;
        if (parameters.has_param ("OPEN_READONLY")) {
            flags |= Sqlite.OPEN_READONLY;
        }

        if (parameters.has_param ("OPEN_READWRITE_CREATE")) {
            flags |= Sqlite.OPEN_READWRITE | Sqlite.OPEN_CREATE;
        }

        if (parameters.has_param ("OPEN_READWRITE")) {
            flags |= Sqlite.OPEN_READWRITE;
        }

        if (parameters.has_param ("OPEN_MEMORY")) {
            flags |= Sqlite.OPEN_MEMORY;
        }

        if (parameters.has_param ("OPEN_NOMUTEX")) {
            flags |= Sqlite.OPEN_NOMUTEX;
        }

        if (parameters.has_param ("OPEN_FULLMUTEX")) {
            flags |= Sqlite.OPEN_FULLMUTEX;
        }

        if (parameters.has_param ("OPEN_SHAREDCACHE")) {
            flags |= Sqlite.OPEN_SHAREDCACHE;
        }

        if (parameters.has_param ("OPEN_PRIVATECACHE")) {
            flags |= Sqlite.OPEN_PRIVATECACHE;
        }

        // if (parameters.has_param ("OPEN_NOFOLLOW")) {
        //     flags |= Sqlite.OPEN_NOFOLLOW;
        // }

        if (flags == 0) {
            flags |= Sqlite.OPEN_READWRITE | Sqlite.OPEN_CREATE;
        }

        var dbfile = GLib.File.new_for_path (str);
        if (str.has_prefix("file://")) {
            dbfile = GLib.File.new_for_uri (str);
        }

        int res = Sqlite.Database.open_v2 (
            dbfile.get_path (),
            out database,
            flags);

        if (res != Sqlite.OK) {
            warning ("Can't create the db");
            canceled (_("Cant' create the database"));
            return _status;
        }

        _status = Vda.Connection.Status.CONNECTED;
        opened ();

        return _status;
    }

    internal async Vda.Connection.Status open_from_string (string cnc_string) throws GLib.Error {
        parameters = new ConnectionParameters (cnc_string);
        return yield open ();
    }

    internal Vda.Query
    parse_string (string sql) throws GLib.Error {
        var q = new Vsqlite.Query.from_sql (this, sql);
        return q;
    }

    internal Vda.PreparedQuery?
    parse_string_prepared (string? name, string sql) throws GLib.Error {
        var q = new Vsqlite.ParsedQuery (this, name != null ? name : _queries.size.to_string ());
        q.parse (sql);
        _queries.set (name, q);
        return q;
    }

    internal Vda.PreparedQuery? get_prepared_query (string name) {
        return _queries.get (name);
    }

    internal Vda.PreparedQuery?
    query_from_command (SqlCommand cmd, string? name) throws GLib.Error {
        if (this != cmd.connection) {
            throw new
                    Vda
                    .ConnectionError
                    .QUERY_CREATION_ERROR (_("Command is not using same connection"));
        }
        return new Vsqlite.ParsedQuery.from_command (cmd, name);
    }

    /**
     * Convert a given SQLite's type's code to a {@link GLib.Type}
     */
    public GLib.Type type_to_gtype (int t) {
        switch (t) {
            case Sqlite.INTEGER:
                return GLib.Type.INT64;
            case Sqlite.FLOAT:
                return GLib.Type.DOUBLE;
            case Sqlite.BLOB:
                return typeof (Vda.SqlValueBinary);
            case Sqlite.TEXT:
                return GLib.Type.STRING;
            case Sqlite.NULL:
                return typeof (Vda.ValueNull);
        }

        return GLib.Type.INVALID;
    }
    
    /**
     * Convert a given SQLite return code from queries execution
     * to a string describing the error based on SQLite
     * documentation
     */
    public string code_to_string (int code) {
    	switch (code) {
    	    case Sqlite.ERROR:
    	        return "ERROR: generic error code that is used when no other more specific error code is available";
    	    case Sqlite.BUSY:
    	        return "BUSY: database file could not be written (or in some cases read) because of concurrent activity by some other database connection";
    	    case Sqlite.NOMEM:
    	        return "NOMEM: unable to allocate all the memory it needed to complete the operation";
    	    case Sqlite.READONLY:
    	        return "READONLY: attempt is made to alter some data for which the current database connection does not have write permission";
    	    case Sqlite.CORRUPT:
    	        return "CORRUPT: database file has been corrupted";
    	    case Sqlite.FULL:
    	        return "FULL: write could not complete because the disk is full";
    	    case Sqlite.CANTOPEN:
    	        return "CANTOPEN: unable to open a file";
    	    case Sqlite.SCHEMA:
    	        return "SCHEMA: database schema has changed";
    	    case Sqlite.TOOBIG:
    	        return "TOOBIG: string or BLOB was too large";
    	    case Sqlite.CONSTRAINT:
    	        return "CONSTRAINT: SQL constraint violation occurred while trying to process an SQL statement";
    	    case Sqlite.MISMATCH:
    	        return "MISMATCH: datatype mismatch";
    	    case Sqlite.MISUSE:
    	        return "MISUSE: application uses any SQLite interface in a way that is undefined or unsupported";
    	    case Sqlite.AUTH:
    	        return "AUTH: the authorizer callback indicates that an SQL statement being prepared is not authorized";
    	}
    	
    	return "Unknown code: %d".printf (code);
    }

    // ConnectionTransactional
    // internal bool add_savepoint (string? name) throws GLib.Error { return false; }
    // internal bool delete_savepoint (string? name) throws GLib.Error { return false; }
    // internal bool rollback_savepoint (string? name) throws GLib.Error { return false; }
    // internal bool begin_transaction (string? name) throws GLib.Error { return false; }
    // internal bool commit_transaction (string? name) throws GLib.Error { return false; }
    // internal bool rollback_transaction (string? name) throws GLib.Error { return false; }

    // ConnectionRolebased
    // internal Vda.Role? current_user () { return null; }

}
