/*
 * PostgresQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Sqlite;

internal errordomain Vsqlite.QueryError {
    FATAL_ERROR,
    STATEMENT_ERROR
}

internal class Vsqlite.Query : Object, Vda.Query {
    protected string _sql = null;
    protected Vda.Connection cnc = null;
    internal Sqlite.Statement stm = null;
    internal int code;
    //int tryes = 100;

    public Query (Vda.Connection con) {
        cnc = con;
    }

    public Query.from_sql (Vda.Connection con, string sql) {
        cnc = con;
        _sql = sql;
    }

    public string sql {
        owned get {
            if (_sql != null) {
                return _sql;
            } else {
                return render_sql ();
            }
        }
    }

    public Vda.Connection connection { get { return cnc; } }

    public async Vda.Result?
    execute (GLib.Cancellable? cancellable) throws GLib.Error {
        if (cnc == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Database is invalid"));
        }

        unowned Sqlite.Database db = ((Vsqlite.Connection) cnc).get_database ();

        if (db == null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("SQLite Database is invalid"));
        }

        if (this is Vsqlite.ParsedQuery) {
            if (((Vsqlite.ParsedQuery) this).command == null) {
                if (stm == null) {
                    throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Invalid SQL: %s"), _sql);
                }

                debug ("Binding");
                // Using native bind of parameters
                ((Vsqlite.ParsedQuery) this).bind_parameters ();
            } else {
                string sqlx = ((Vsqlite.ParsedQuery) this).render_sql ();
                code = db.prepare_v2 (sqlx, -1, out stm, null);
                if (code != Sqlite.OK) {
                    string msg = ((Vsqlite.Connection) connection).code_to_string (code);
                    throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Unable to prepare query \"%s\". Parse returns code: %s").printf (sqlx, msg));
                }

                debug ("SQL rendered to execute: %s", sqlx);
            }
        } else {
            debug ("SQL to execute: %s", _sql);
            code = db.prepare_v2 (_sql, -1, out stm, null);
            if (code != Sqlite.OK) {
                string msg = ((Vsqlite.Connection) connection).code_to_string (code);
                (_("Unable to parse query \"%s\". Parse returns code: %s").printf (_sql, msg));
            }
        }

        code = stm.step ();
        
        switch (code) {
            case Sqlite.ROW:
                debug ("Sequential TableModel returned");
                return new Vsqlite.TableModelSequential (this);
            case Sqlite.OK:
            case Sqlite.DONE:
                int rows = -1;
                rows = db.changes ();
                debug ("Affected rows: %d", rows);
                return new Vda.AffectedRows (rows);
            default:
                string msg = ((Vsqlite.Connection) connection).code_to_string (code);
                throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Query \"%s\" results in code: %s").printf (sql, msg));
        }
    }
    
//    private bool try_parse () {
//        code = ((Vsqlite.Connection) cnc).get_database ().prepare_v2 (sql, -1, out stm, null);
//        if (code == Sqlite.OK) {
//            return GLib.Source.REMOVE;
//        }
//        
//        tryes--;
//        
//        if (tryes > 0) {
//            return GLib.Source.CONTINUE;
//        }
//        
//        return GLib.Source.REMOVE;
//    }
//    
//    private bool try_execute () {
//        code = stm.step ();
//        if (code == Sqlite.OK || code == Sqlite.ROW || code == Sqlite.DONE) {
//            return GLib.Source.REMOVE;
//        }
//        
//        tryes--;
//        
//        if (tryes > 0) {
//            return GLib.Source.CONTINUE;
//        }
//        
//        return GLib.Source.REMOVE;
//    }

    public async void cancel () {
        warning (_("Not implemented"));
    }

    public virtual string render_sql () {
        return _sql;
    }

    internal unowned Sqlite.Statement get_stm () {
        return stm;
    }
}

class Vsqlite.Result : GLib.Object, Vda.Result {
    protected Vsqlite.Query _query;

    public Vda.Connection connection { get { return _query.connection; } }

    public Result (Vsqlite.Query query) {
        _query = query;
    }
}

class Vsqlite.TableModelSequential : Vsqlite.Result, Vda.TableModelSequential
{
    bool started = false;
    bool next_success = false;

    // GLib.ListModel interface
    internal Vda.RowModel? current () {
        if (!started || !next_success) {
            return null;
        }

        return new Vsqlite.RowModel (_query);
    }
    // FIXME: We need a struct to copy values to and keep them
    // wihtout depend on the query
    internal Vda.RowModel? copy_current () {
        return null;
    }

    internal bool next () throws GLib.Error {
        if (!started) {
            next_success = true;
            started = true;
            return true;
        }
        int r = _query.get_stm ().step ();
        if (r != Sqlite.ROW) {
            next_success = false;
            return false;
        }

        next_success = true;
        return true;
    }

    public TableModelSequential (Vsqlite.Query query) {
        base (query);
    }
}

class Vsqlite.RowModel : Object, GLib.ListModel, Vda.RowModel
{
    protected Vsqlite.Query _query;
    public uint n_columns {
        get {
            return (uint) _query.get_stm ().data_count ();
        }
    }

    public Vda.ColumnModel? get_column (string name) throws GLib.Error {
        unowned Sqlite.Statement stm = _query.get_stm ();
        for (int i = 0; i < stm.data_count (); i++) {
            if (stm.column_name (i) == name) {
                return new Vsqlite.ColumnModel (_query, i);
            }
        }

        return null;
    }

    public Vda.ColumnModel? get_column_at (uint col) throws GLib.Error  {
        return new Vsqlite.ColumnModel (_query, col);
    }

    public SqlValue? get_value (string name) throws GLib.Error {
        unowned Sqlite.Statement stm = _query.get_stm ();
        for (int i = 0; i < stm.data_count (); i++) {
            if (stm.column_name (i) == name) {
                return get_value_at (i);
            }
        }
        return null;
    }

    public SqlValue? get_value_at (uint col) throws GLib.Error {
        unowned Sqlite.Statement stm = _query.get_stm ();
        var type = stm.column_type ((int) col);
        Vda.Value ret = null;
        switch (type) {
            case Sqlite.INTEGER:
                ret = new Vda.ValueInt8 ();
                int64 i = stm.column_int64 ((int) col);
                ret.from_value (i);
                break;
            case Sqlite.FLOAT:
                ret = new Vda.ValueDouble ();
                double d = stm.column_double ((int) col);
                ret.from_value (d);
                break;
            case Sqlite.BLOB:
                ret = new Vda.ValueBinary.with_data ((uint8[]) stm.column_blob ((int) col));
                break;
            case Sqlite.TEXT:
                ret = new Vda.ValueText ();
                string str = stm.column_text ((int) col);
                ret.from_value (str);
                break;
            case Sqlite.NULL:
            default:
                  ret = new Vda.ValueNull ();
                break;
        }

        return ret;
    }

    public string? get_string (string name) throws GLib.Error {
        if (name == null) {
            return null;
        }
        
        var v = get_value (name);
        if (v == null) {
            return null;
        }

        return v.to_string ();
    }

    public string? get_string_at (uint col) throws GLib.Error {
        var v = get_value_at (col);
        if (v == null) {
            return null;
        }

        return v.to_string ();
    }

    // GLib.ListModel interface
    public uint get_n_items () { return n_columns; }

    public GLib.Type get_item_type () { return typeof (ColumnModel); }

    public GLib.Object? get_item (uint position) {
        if (position > n_columns)
            return null;
        Vda.ColumnModel col = null;
        try {
            col = get_column_at (position);
        } catch (GLib.Error e) {
            warning (_("Error: %s"), e.message);
        }

        return col;
    }

    // Constructor
    public RowModel (Vsqlite.Query query) {
        _query = query;
    }
}

class Vsqlite.ColumnModel : Object, Vda.ColumnModel {
    uint col = -1;
    Vsqlite.Query _query;

    public string name {
        get {
            return _query.get_stm ().column_name ((int) col);
        }
    }
    public GLib.Type data_type {
        get {
            var t = ((Vsqlite.Connection) _query.connection)
                        .type_to_gtype (_query.get_stm ().column_type ((int) col));
            return t;
        }
    }
    public ColumnModel (Vsqlite.Query query, uint col) {
        this.col = col;
        _query = query;
    }
}
