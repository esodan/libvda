/*
 * GParsedQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represent a prepared query implementing {@link Vda.PreparedQuery}.
 *
 * Values required by query can be set by using paramenters property.
 */
 internal class Vsqlite.ParsedQuery : Vsqlite.Query, Vda.PreparedQuery, Vda.ParsedQuery {
    protected string _name;
    protected Vda.SqlCommand _command;
    protected Vda.SqlParameters _params = new Vda.Parameters ();

    public ParsedQuery (Vda.Connection con, string name) {
        base (con);
        _name = name;
        cnc = con;
    }
    public ParsedQuery.from_command (Vda.SqlCommand cmd, string? name) {
        base (cmd.connection);
        _name = name;
        _command = cmd;
        debug ("Query created using a Vda.SqlCommand");
    }
    /**
     * Parse an SQL using {@link Vda.Parser}
     */
    public void parse (string sql) throws GLib.Error {
        if (_command != null) {
            throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Can't change the internal query definition"));
        }
        
        _sql = sql;

        debug ("String to parse: %s", _sql);
        if (Regex.match_simple("##\\w*::\\w*", _sql)) {
            debug ("Using VDA's internal SQL parser");
            _command = new Vda.Parser ().parse (_sql, cnc);
        } else {
            debug ("Using native parsing and binding");
            unowned Sqlite.Database db = ((Vsqlite.Connection) cnc).get_database ();
            if (db == null) {
                throw new Vda.QueryError.INVALID_QUERY_ERROR (_("Invalid connection. Can't parse requested SQL: %s"), _sql);
            }
            
            code = db.prepare_v2 (_sql, -1, out stm, null);
            if (code != Sqlite.OK) {
                string msg = ((Vsqlite.Connection) connection).code_to_string (code);
                throw new Vda.QueryError
                        .INVALID_QUERY_ERROR (_("Native parser: Unable to parse query \"%s\". Prepare statement return code: %s").printf (_sql, msg));
            }

            debug ("SQL was parsed successfully");
        }
    }

    // Query
    public override string render_sql () {
        if (_command != null && _command is Vda.Stringifiable) {
            return ((Vda.Stringifiable) _command).to_string ();
        } else if (_command == null) {
            if (stm == null) {
                warning (_("Unable to render SQL: no statement were prepared"));
                return "";
            }
            
            bind_parameters ();
            debug ("Expanding SQLite statement with values");
            return stm.expanded_sql ();
        }

        return "";
    }
    // PreparedQuery
    public string name { get { return _name; } }
    public Vda.SqlParameters parameters {
        get {
            if (_command != null && _command is Vda.SqlCommandParametrized) {
                return ((Vda.SqlCommandParametrized) _command).parameters;
            }

            return _params;
        }
    }
    
    internal void bind_parameters () {
        debug ("Setting parameters' values");
        stm.reset ();
        stm.clear_bindings ();
        debug ("Parameters to set: %d", ((Gee.HashMap<string,Vda.SqlValue>) parameters).size);
        foreach (string k in ((Gee.HashMap<string,Vda.SqlValue>) parameters).keys) {
            var v = ((Gee.HashMap<string,Vda.SqlValue>) parameters).get (k);
            debug ("Applying binding for value: %s", v.to_string ());
            if (v is Vda.SqlValueInteger) {
                stm.bind_int (stm.bind_parameter_index (k), (int) v.to_gvalue ());
            } else if (v is Vda.SqlValueString) {
                stm.bind_text (stm.bind_parameter_index (k), (string) v.to_gvalue ());
            } else if (v is Vda.SqlValueDouble) {
                stm.bind_double (stm.bind_parameter_index (k), (double) v.to_gvalue ());
            } // FIXME: Add more conversions
        }
    }
    
    // ParsedQuery
    public Vda.SqlCommand command {
        get {
            return _command;
        }
    }
}
