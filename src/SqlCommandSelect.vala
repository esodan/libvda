/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlCommandSelect.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an SELECT SQL command
 */
public interface Vda.SqlCommandSelect  : Object,
																				SqlCommandConditional,
																				SqlCommand,
																				SqlCommandParametrized
{
	/**
	 * A list of {@link SqlExpressionField}
	 */
	public abstract HashModel fields { get; }
	/**
	 * A list of {@link SqlTableReference}
	 */
	public abstract HashModel tables { get; }
	/**
	 * Add a field at the given table to the SELECT command.
	 *
	 * If table is given and it is not already in {@link tables},
	 * it is added automatically. If not table is given, the field
	 * is added in the hope it will be referenced as a table, but
	 * checked by the connection at execution time.
	 *
	 * If the field was already added, a new request is ignored.
	 */
	public abstract void add_field (string field, string? table_ref, string? alias = null);
	/**
	 * Add a table with the given name and allias to the SELECT command.
	 *
	 * Any table already added with the same allias, will be removed and
	 * the new table will be used as the table reference for the fields
	 * already added using table's allias.
	 */
	public abstract void add_table (string name, string? allias = null);
	/**
	 * Add a value as a field with the given allias.
	 */
	public abstract void add_value_field (GLib.Value val, string? allias);
	/**
	 * Add a Math expression string as a field with the given allias.
	 */
	public abstract void add_math_exp_field (string exp, string? allias) throws GLib.Error;
	/**
	 * Creates a string representation
	 */
	public virtual string stringify () throws GLib.Error {
		string str = "SELECT ";
		if (fields.get_n_items () == 0) {
			throw new SqlCommandSelectError.INVALID_FIELDS_ERROR (_("Invalid number of fields: you should provide at least one or '*'"));
		}
		var ff = fields.get_item (0) as SqlExpressionField;
		if (ff == null) {
			throw new SqlCommandSelectError.INVALID_FIELDS_ERROR (_("Invalid field: you should provide a valid field object"));
		}
		if (ff.name == "*") {
			str += "*";
		} else {
		  for (int i = 0; i < fields.get_n_items (); i++) {
		    var f = fields.get_item (i) as SqlExpressionField;
		    if (f == null) continue;
		    str += (f.table_ref != null ? f.table_ref + "." : "") + f.name;
		    if (f.allias != null) {
		    	str += " AS "+f.allias;
		    }
		    if (i + 1 < fields.get_n_items ()) {
		      str += ", ";
		    }
		  }
		}
    str += " FROM ";
    for (int i = 0; i < tables.get_n_items (); i++) {
      var t = tables.get_item (i) as SqlTableReference;
      if (t == null) continue;
      str += t.name;
      if (t.allias != null) {
      	str += " AS "+ t.allias;
      }
      if (i + 1 < tables.get_n_items ()) {
        str += ", ";
      }
    }
    if (condition != null && condition.get_n_items () != 0) {
    	str += " WHERE " + condition.to_string ();
    }
    return str;
  }
	/**
	 * Create a {@link Query} for execution, using internal structure.
	 *
	 * If {@link SqlExpressionValueParameter} object is used in {@link SqlCommandConditional.condition},
	 * then a {@link PreparedQuery} will be created when converted to {@link Query}.
	 */
	public virtual PreparedQuery to_query (string? name = null) throws GLib.Error {
		string str = stringify ();
		debug ("Query to create SELECT command: %s", str);
		return connection.parse_string_prepared (name, str);
	}
  /**
   * Parse SQL string commands and construct its internal tree
   */
  public virtual void parse (string sql)  throws GLib.Error {
		Gee.ArrayList<GLib.TokenType> expected = new Gee.ArrayList<GLib.TokenType> ();
    var scanner = new GLib.Scanner (null);
    scanner.input_name = "SQL";
    scanner.input_text (sql, sql.length);
    scanner.config.cpair_comment_single = "//\n";
    scanner.config.skip_comment_multi = false;
    scanner.config.skip_comment_single = false;
    scanner.config.char_2_token = false;
    scanner.config.scan_binary = false;
    scanner.config.scan_octal = false;
    scanner.config.scan_float = false;
    scanner.config.scan_hex = false;
    scanner.config.scan_hex_dollar = false;
    scanner.config.numbers_2_int = false;
    scanner.config.scan_identifier_1char = true;
    GLib.TokenType token = GLib.TokenType.NONE;
    bool complete_init = false;
    bool expected_table_name = false;
    bool enable_field = false;
    bool start_math_expression = false;
    bool starting = true;
    bool enable_field_ref_allias = false;
    bool enable_table_ref_allias = false;
    bool from = false;
    StringBuilder math_expression = new StringBuilder ("");
    string table_ref = "";
    string table_ref_allias = null;
    string field_ref = "";
    string str_val = "";
    while (token != GLib.TokenType.EOF) {
      token = scanner.get_next_token ();
      if (token == GLib.TokenType.EOF) {
      	if (expected_table_name && enable_table_ref_allias && !enable_field) {
          add_table (table_ref, table_ref_allias);
      	}
        break;
      }
      if (expected.size != 0 && !expected.contains (token)) {
        throw new ParserError.INVALID_TOKEN_ERROR (_("Found an unexpected expression at Line:Column : %d:%d : \"%s\""),
                                                          scanner.cur_line (), scanner.cur_position (), sql);
      }
      switch (token) {
        case GLib.TokenType.IDENTIFIER:
          string identifier = scanner.cur_value ().@identifier;
          // SQL commands and identifiers
          if (starting) {
            if (identifier.down () != "select") {
              throw new ParserError.INVALID_TOKEN_ERROR (_("SQL select command should start with SELECT"));
            }
            starting = false;
            complete_init = true;
            enable_field = true;
          } else if (expected_table_name && !enable_table_ref_allias && !enable_field) {
            table_ref = scanner.cur_value ().@string;
            enable_table_ref_allias = true;
            table_ref_allias = null;
            expected.clear ();
            expected.add (GLib.TokenType.IDENTIFIER);
            expected.add (GLib.TokenType.CHAR);
        	} else if (expected_table_name && enable_table_ref_allias && !enable_field) {
            if (identifier.down () == "as") {
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
            } else if (identifier.down () == "where") {
            	add_table (table_ref, table_ref_allias);
          		expected_table_name = false;
              int start = scanner.cur_position ();
				      string rest = sql.substring (start, sql.length - start);
				      SqlExpression cond = SqlExpression.parse (rest, connection, parameters);
				      condition.add_expression (cond);
				      return;
            } else {
            	table_ref_allias = identifier;
              expected.clear ();
              expected.add (GLib.TokenType.CHAR);
              expected.add (GLib.TokenType.IDENTIFIER);
            }
          } else if (enable_field && !expected_table_name) {
          	if (identifier.down () == "from") {
		          complete_init = true;
		          expected_table_name = true;
              enable_field = false;
              from = true;
              if (field_ref != null) {
              	add_field (field_ref, table_ref, null);
              	field_ref = null;
              }
              expected.clear ();
              expected.add (GLib.TokenType.IDENTIFIER);
              break;
            } else {
          		field_ref = identifier;
          	}
          	enable_field_ref_allias = true;
            expected.clear ();
            expected.add (GLib.TokenType.IDENTIFIER);
            expected.add (GLib.TokenType.CHAR);
          } else if (enable_field && enable_field_ref_allias && complete_init) {
          	if (identifier.down () == "as") {
		          expected.clear ();
		          expected.add (GLib.TokenType.IDENTIFIER);
          	} else {
          		add_field (field_ref, table_ref, identifier);
              expected.clear ();
		          expected.add (GLib.TokenType.IDENTIFIER);
          	}
          } else if (enable_field && start_math_expression
          					&& !expected_table_name
          					&& !enable_field_ref_allias) {
          	if (identifier.down () == "as") {
          		enable_field_ref_allias = true;
		          expected.clear ();
		          expected.add (GLib.TokenType.IDENTIFIER);
          	} else if (enable_field && start_math_expression
          						&& !expected_table_name
          						&& enable_field_ref_allias) {
          		field_ref = identifier;
          	} else {
          		math_expression.append (identifier);
              expected.clear ();
		          expected.add (GLib.TokenType.IDENTIFIER);
          	}
          } else if (enable_field && str_val != "" && enable_field_ref_allias) {
          	if (identifier.down () == "as") {
              expected.clear ();
		          expected.add (GLib.TokenType.IDENTIFIER);
          	} else {
          		field_ref = identifier;
          	}
          }
          break;
        case GLib.TokenType.CHAR:
          var v = scanner.cur_value ().@char;
          if (v == ',') {
          	if (enable_field && enable_field_ref_allias && complete_init && !expected_table_name) {
          		enable_field_ref_allias = false;
          		add_field (field_ref, table_ref, null);
          		field_ref = table_ref = null;
              expected.clear ();
		          expected.add (GLib.TokenType.IDENTIFIER);
          	} else if (expected_table_name && enable_table_ref_allias && !enable_field) {
          		enable_table_ref_allias = false;
              add_table (table_ref, table_ref_allias);
              expected.clear ();
		          expected.add (GLib.TokenType.IDENTIFIER);
          	} else if (enable_field && !enable_field_ref_allias && !expected_table_name && start_math_expression) {
          		start_math_expression = false;
          		add_math_exp_field (math_expression.str, field_ref);
              expected.clear ();
		          expected.add (GLib.TokenType.IDENTIFIER);
          	} else if (enable_field && str_val != "" && enable_field_ref_allias) {
          		add_value_field (str_val, field_ref);
          		str_val = "";
          		enable_field_ref_allias = false;
          	}
          } else if (v == '+' || v == '-' || v == '(') {
          	if (enable_field && !enable_field_ref_allias && !expected_table_name) {
		      		if (start_math_expression) {
		      			start_math_expression = true;
		      			field_ref = null;
		            expected.clear ();
		          }
          		math_expression.append_c ((char) v);
          	}
          } else if (v == '*') {
          	if (enable_field && !expected_table_name) {
          		add_field ("*", null, null);
	            expected.clear ();
		          expected.add (GLib.TokenType.IDENTIFIER);
          	}
          } else if (v == '.') {
          	if (start_math_expression) {
          		math_expression.append_c ((char) v);
          	} else if (enable_field && !start_math_expression) {
							table_ref = field_ref;
							field_ref = null;
		          expected.add (GLib.TokenType.IDENTIFIER);
          	}
          } else {
          	if (start_math_expression && enable_field && !enable_field_ref_allias && !expected_table_name) {
          		math_expression.append_c ((char) v);
          	}
          }
          break;
        case GLib.TokenType.INT:
        	if (enable_field && !enable_field_ref_allias && !expected_table_name) {
        		if (start_math_expression) {
        			start_math_expression = true;
	      			field_ref = null;
              expected.clear ();
            }
        		field_ref = null;
        		math_expression.append (scanner.cur_value ().@int.to_string ());
        	}
        	break;
        case GLib.TokenType.FLOAT:
        	if (enable_field && !enable_field_ref_allias && !expected_table_name) {
        		if (start_math_expression) {
        			start_math_expression = true;
	      			field_ref = null;
              expected.clear ();
            }
        		math_expression.append ("%g".printf (scanner.cur_value ().@int));
        	}
        	break;
        case GLib.TokenType.STRING:
        	if (enable_field && !enable_field_ref_allias && !expected_table_name) {
        		str_val = scanner.cur_value ().@string;
        		enable_field_ref_allias = true;
      			field_ref = null;
            expected.clear ();
	          expected.add (GLib.TokenType.IDENTIFIER);
	          expected.add (GLib.TokenType.CHAR);
        	}
        	break;
        case GLib.TokenType.NONE:
        case GLib.TokenType.LEFT_PAREN:
        case GLib.TokenType.SYMBOL:
        case GLib.TokenType.COMMENT_MULTI:
        case GLib.TokenType.RIGHT_CURLY:
        case GLib.TokenType.BINARY:
        case GLib.TokenType.OCTAL:
        case GLib.TokenType.EQUAL_SIGN:
        case GLib.TokenType.RIGHT_PAREN:
        case GLib.TokenType.ERROR:
        case GLib.TokenType.LAST:
        case GLib.TokenType.LEFT_BRACE:
        case GLib.TokenType.EOF:
        case GLib.TokenType.COMMA:
        case GLib.TokenType.COMMENT_SINGLE:
        case GLib.TokenType.LEFT_CURLY:
        case GLib.TokenType.IDENTIFIER_NULL:
        case GLib.TokenType.RIGHT_BRACE:
        case GLib.TokenType.HEX:
        	break;
      }
    }
  }
}

/**
 * SQL SELECT Command error codes
 */
public errordomain Vda.SqlCommandSelectError {
	INVALID_FIELDS_ERROR
}
