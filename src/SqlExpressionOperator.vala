/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * SqlExpressionOperator.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Represent an operator in an {@link SqlExpression}
 */
public interface Vda.SqlExpressionOperator  : Object, SqlExpression
{
	/**
	 * The operator {@link SqlExpressionOperator.Type}
	 */
	public abstract SqlExpressionOperator.Type operator_type { get; }

	/**
	 * Creates an {@link SqlExpressionField} suitable to be added to a expression
	 */
	public abstract SqlExpressionField create_field_expression (string name);

	/**
	 * Creates an {@link SqlExpressionValue} suitable to be added to a expression
	 */
	public abstract SqlExpressionValue create_value_expression (GLib.Value? val, Connection cnc);

	/**
	 * Creates an {@link SqlExpressionValueParameter} suitable to be added to a expression
	 */
	public abstract SqlExpressionValueParameter create_parameter_expression (string name, GLib.Type gtype);
	/**
	 * Creates an {@link SqlExpressionOperatorAnd} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_and_operator     (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorOr} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_or_operator      (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorEq} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_eq_operator      (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorDiff} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_diff_operator 	 (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorLike} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_like_operator    (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorGt} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_gt_operator      (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorGeq} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_geq_operator     (SqlExpression exp1, SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorLeq} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_leq_operator     (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorSimilarTo} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_similar_to_operator (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorIsNull} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_null_operator (SqlExpression exp1);
	/**
	 * Creates an {@link SqlExpressionOperatorIsNotNull} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_not_null_operator (SqlExpression exp1);
	/**
	 * Creates an {@link SqlExpressionOperatorNot} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_not_operator     (SqlExpression exp);
	/**
	 * Creates an {@link SqlExpressionOperatorIsTrue} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_true_operator (SqlExpression exp1);
	/**
	 * Creates an {@link SqlExpressionOperatorIsNotTrue} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_not_true_operator (SqlExpression exp1);
	/**
	 * Creates an {@link SqlExpressionOperatorIsFalse} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_false_operator (SqlExpression exp1);
	/**
	 * Creates an {@link SqlExpressionOperatorIsNotFalse} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_not_false_operator (SqlExpression exp1);
	/**
	 * Creates an {@link SqlExpressionOperatorIsUnknown} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_unknown_operator (SqlExpression exp1);
	/**
	 * Creates an {@link SqlExpressionOperatorIsNotUnknown} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_not_unknown_operator (SqlExpression exp1);
	/**
	 * Creates an {@link SqlExpressionOperatorIn} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_in_operator      (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorNotIn} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_not_in_operator  (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorConcatenate} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_concatenate_operator (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorPlus} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_plus_operator    (SqlExpression exp1,
  																								SqlExpression? exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorMinus} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_minus_operator   (SqlExpression exp1,
  																								SqlExpression? exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorStar} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_star_operator    (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorDiv} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_div_operator     (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorRegexp} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_regexp_operator  (SqlExpression exp1,
																									SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorBetween} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_between_operator (SqlExpression exp1,
  																								SqlExpression exp2,
  																								SqlExpression exp3);
	/**
	 * Creates an {@link SqlExpressionOperatorNotBetween} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_not_between_operator (SqlExpression exp1,
  																								SqlExpression exp2,
  																								SqlExpression exp3);
	/**
	 * Creates an {@link SqlExpressionOperatorBetweenSymmetric} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_between_symmetric_operator (SqlExpression exp1,
  																								SqlExpression exp2,
  																								SqlExpression exp3);
	/**
	 * Creates an {@link SqlExpressionOperatorNotBetweenSymmetric} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_not_between_symmetric_operator (SqlExpression exp1,
  																								SqlExpression exp2,
  																								SqlExpression exp3);
	/**
	 * Creates an {@link SqlExpressionOperatorIsDistinctFrom} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_distinct_from_operator (SqlExpression exp1,
  																								SqlExpression exp2);
	/**
	 * Creates an {@link SqlExpressionOperatorIsNotDistinctFrom} suitable to be added to a expression
	 */
  public abstract SqlExpressionOperator add_is_not_distinct_from_operator (SqlExpression exp1,
  																								SqlExpression exp2);
  public enum Type {
		NONE, AND, OR, EQ, IS, LIKE,
		BETWEEN, NOT_BETWEEN, BETWEEN_SYMMETRIC, NOT_BETWEEN_SYMMETRIC,
		IS_DISTINCT_FROM, IS_NOT_DISTINCT_FROM,
		GT, LT, GEQ, LEQ, DIFF,
		REGEXP,	REGEXP_CI, NOT_REGEXP, NOT_REGEXP_CI, SIMILAR_TO,
		IS_NULL, IS_NOT_NULL, IS_TRUE, IS_NOT_TRUE, IS_FALSE, IS_NOT_FALSE,
		IS_UNKNOWN, IS_NOT_UNKNOWN,
		NOT, IN, NOT_IN, CONCATENATE, PLUS, MINUS, STAR, DIV,
		REM, BITAND, BITOR, BITNOT, ILIKE
	}
}


/**
 * An {@link SqlExpressionOperator} for grouping expressions
 */
public interface Vda.SqlExpressionOperatorGroup : Object, SqlExpressionOperator {}
/**
 * An {@link SqlExpressionOperator} for multiple terms operators
 */
public interface Vda.SqlExpressionOperatorMultiterm : Object, SqlExpressionOperator {}
/**
 * An {@link SqlExpressionOperator} for logic AND operator
 */
public interface Vda.SqlExpressionOperatorAnd : Object, SqlExpressionOperatorMultiterm {}
/**
 * An {@link SqlExpressionOperator} for logic OR operator
 */
public interface Vda.SqlExpressionOperatorOr : Object, SqlExpressionOperatorMultiterm {}
/**
 * An {@link SqlExpressionOperator} for two terms operators
 */
public interface Vda.SqlExpressionOperatorBinaryterm : Object, SqlExpressionOperator {}
/**
 * An {@link SqlExpressionOperator} for logic EQUAL (=) operator
 */
public interface Vda.SqlExpressionOperatorEq : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for logic NOT EQUAL (!=) operator
 */
public interface Vda.SqlExpressionOperatorNotEq : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for logic DIFF operator
 */
public interface Vda.SqlExpressionOperatorDiff : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for logic GREATHER THAN (>) operator
 */
public interface Vda.SqlExpressionOperatorGt : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for logic LESS THAN (<) operator
 */
public interface Vda.SqlExpressionOperatorLt : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for logic GREATHER OR EQUAL THAN (>=) operator
 */
public interface Vda.SqlExpressionOperatorGeq : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for logic LESS OR EQUAL THAN (<=) operator
 */
public interface Vda.SqlExpressionOperatorLeq : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for REGULAR EXPRESSION operator
 */
public interface Vda.SqlExpressionOperatorRegexp : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for STAR (*) operator
 */
public interface Vda.SqlExpressionOperatorStar : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for DIVISION (/) operator
 */
public interface Vda.SqlExpressionOperatorDiv : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for IN operator
 */
public interface Vda.SqlExpressionOperatorIn : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for NOT IN operator
 */
public interface Vda.SqlExpressionOperatorNotIn : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for CONCATENATE operator for strings
 */
public interface Vda.SqlExpressionOperatorConcatenate : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for SIMILAR TO operator for strings
 */
public interface Vda.SqlExpressionOperatorSimilarTo : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for LIKE operator for strings
 */
public interface Vda.SqlExpressionOperatorLike : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for NOT LIKE operator for strings
 */
public interface Vda.SqlExpressionOperatorNotLike : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for ILIKE operator for strings
 */
public interface Vda.SqlExpressionOperatorIlike : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for NOT ILIKE operator for strings
 */
public interface Vda.SqlExpressionOperatorNotIlike : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for two or one term operators
 */
public interface Vda.SqlExpressionOperatorBinaryUnaryterm : Object, SqlExpressionOperator {}
/**
 * An {@link SqlExpressionOperator} for MINUS (-) operator
 */
public interface Vda.SqlExpressionOperatorMinus : Object, SqlExpressionOperatorBinaryUnaryterm {}
/**
 * An {@link SqlExpressionOperator} for MINUS (+) operator
 */
public interface Vda.SqlExpressionOperatorPlus : Object, SqlExpressionOperatorBinaryUnaryterm {}
/**
 * An {@link SqlExpressionOperator} for one term initial located operators
 */
public interface Vda.SqlExpressionOperatorInitialUnaryterm : Object, SqlExpressionOperator {}
/**
 * An {@link SqlExpressionOperator} for NOT operator
 */
public interface Vda.SqlExpressionOperatorNot : Object, SqlExpressionOperatorInitialUnaryterm {}
/**
 * An {@link SqlExpressionOperator} for one term after located operators
 */
public interface Vda.SqlExpressionOperatorFinalUnaryterm : Object, SqlExpressionOperator {}
/**
 * An {@link SqlExpressionOperator} for IS operator
 */
public interface Vda.SqlExpressionOperatorIs : Object, SqlExpressionOperatorFinalUnaryterm {}
/**
 * An {@link SqlExpressionOperator} for IS NOT operator
 */
public interface Vda.SqlExpressionOperatorIsNot : Object, SqlExpressionOperatorIs {}
/**
 * An {@link SqlExpressionOperator} for IS NULL operator
 */
public interface Vda.SqlExpressionOperatorIsNull : Object, SqlExpressionOperatorIs {}
/**
 * An {@link SqlExpressionOperator} for IS NOT NULL operator
 */
public interface Vda.SqlExpressionOperatorIsNotNull : Object, SqlExpressionOperatorIsNot {}
/**
 * An {@link SqlExpressionOperator} for IS TRUE operator
 */
public interface Vda.SqlExpressionOperatorIsTrue : Object, SqlExpressionOperatorIs {}
/**
 * An {@link SqlExpressionOperator} for IS NOT TRUE operator
 */
public interface Vda.SqlExpressionOperatorIsNotTrue : Object, SqlExpressionOperatorIsNot {}
/**
 * An {@link SqlExpressionOperator} for IS FALSE operator
 */
public interface Vda.SqlExpressionOperatorIsFalse : Object, SqlExpressionOperatorIs {}
/**
 * An {@link SqlExpressionOperator} for IS NOT FALSE operator
 */
public interface Vda.SqlExpressionOperatorIsNotFalse : Object, SqlExpressionOperatorIsNot {}
/**
 * An {@link SqlExpressionOperator} for IS UNKNOWN operator
 */
public interface Vda.SqlExpressionOperatorIsUnknown : Object, SqlExpressionOperatorIs {}
/**
 * An {@link SqlExpressionOperator} for IS NOT UNKNOWN operator
 */
public interface Vda.SqlExpressionOperatorIsNotUnknown : Object, SqlExpressionOperatorIs {}
/**
 * An {@link SqlExpressionOperator} for IS DISTINCT operator
 */
public interface Vda.SqlExpressionOperatorIsDistinct : Object, SqlExpressionOperatorBinaryterm {}
/**
 * An {@link SqlExpressionOperator} for IS NOT DISTINCT operator
 */
public interface Vda.SqlExpressionOperatorIsNotDistinct : Object, SqlExpressionOperatorIsDistinct {}
/**
 * An {@link SqlExpressionOperator} for IS DISTINCT FROM operator
 */
public interface Vda.SqlExpressionOperatorIsDistinctFrom : Object, SqlExpressionOperatorIsDistinct {}
/**
 * An {@link SqlExpressionOperator} for IS NOT DISTINCT FROM operator
 */
public interface Vda.SqlExpressionOperatorIsNotDistinctFrom : Object, SqlExpressionOperatorIsNotDistinct {}
/**
 * An {@link SqlExpressionOperator} for three terms operator
 */
public interface Vda.SqlExpressionOperatorThreeterm : Object, SqlExpressionOperator {}
/**
 * An {@link SqlExpressionOperator} for BETWEEN auxiliary void operator
 */
public interface Vda.SqlExpressionOperatorBetween : Object, SqlExpressionOperatorThreeterm {}
/**
 * An {@link SqlExpressionOperator} for BETWEEN / AND operator, in the form x BETWEEN y AND z
 */
public interface Vda.SqlExpressionOperatorBetweenAnd : Object, SqlExpressionOperatorBetween {}
/**
 * An {@link SqlExpressionOperator} for NOT BETWEEN auxiliary void operator
 */
public interface Vda.SqlExpressionOperatorNotBetween : Object, SqlExpressionOperatorBetween {}
/**
 * An {@link SqlExpressionOperator} for NOT BETWEEN / AND operator, in the form x NOT BETWEEN y AND z
 */
public interface Vda.SqlExpressionOperatorNotBetweenAnd : Object, SqlExpressionOperatorNotBetween {}
/**
 * An {@link SqlExpressionOperator} for BETWEEN SYMMETRIC auxiliary void operator
 */
public interface Vda.SqlExpressionOperatorBetweenSymmetric : Object, SqlExpressionOperatorBetween {}
/**
 * An {@link SqlExpressionOperator} for BETWEEN SYMMETRIC / AND operator, in the form x BETWEEN SYMMETRIC y AND z
 */
public interface Vda.SqlExpressionOperatorBetweenSymmetricAnd : Object, SqlExpressionOperatorBetweenSymmetric {}
/**
 * An {@link SqlExpressionOperator} for NOT BETWEEN SYMMETRIC auxiliary void operator
 */
public interface Vda.SqlExpressionOperatorNotBetweenSymmetric : Object, SqlExpressionOperatorBetweenSymmetric {}
/**
 * An {@link SqlExpressionOperator} for NOT BETWEEN SYMMETRIC / AND operator, in the form x NOT BETWEEN SYMMETRIC y AND z
 */
public interface Vda.SqlExpressionOperatorNotBetweenSymmetricAnd : Object, SqlExpressionOperatorNotBetweenSymmetric {}

