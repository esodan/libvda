/* HashModel.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A {@link GLib.ListModel} using a {@link GLib.Object} as a hash key
 * to find objects in the collection.
 */
public interface Vda.HashModel : Object, GLib.ListModel
{
  /**
   * Adds a new object to the collection and use itself to
   * generate the hash required to find it.
   */
  public abstract void add (Object object);
  /**
   * Search the object in the collection using the given key.
   */
  public abstract Object? find (Object key);
  /**
   * Remove the object from the collection
   */
  public abstract void remove (Object object);
}
