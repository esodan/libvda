/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * Role.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * An interface for database Access Control
 */
public interface Vda.Role : GLib.Object {
  /**
   * Current user name connected to the database
   */
  public abstract Connection connection { get; }
  /**
   * Current user name connected to the database
   */
  public abstract async string name () throws GLib.Error;
  /**
   * Current user name membership as {@link Role} objects
   */
  public abstract async HashModel membership () throws GLib.Error;
  /**
   * Current user permissions on given object
   */
  public abstract async Grant privilages (MetaObject object) throws GLib.Error;
  /**
   * Current user permissions on given object
   */
  public abstract async void change_privilages (MetaObject object, Grant grant) throws GLib.Error;
  /**
   * Current user permissions on given object
   */
  [Flags]
  public enum Grant {
    NONE,
    SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER,
    CREATE, USAGE, PRIVILEGES
  }
}

/**
 * Connection error codes
 */
public errordomain Vda.RoleError {
  PROVILAGE_ERROR
}

