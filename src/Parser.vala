/* Parser.vala
 *
 * Copyright (C) 2018  Daniel Espinosa <esodan@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *      Daniel Espinosa <esodan@gmail.com>
 */

/**
 * An implementation of {@link SqlParser}
 */
public class Vda.Parser : Object, SqlParser {
  /**
   * Use given connection and create a {@link SqlCommandParametrized} object
   * by parsing given string.
   */
  public Vda.SqlCommandParametrized parse (string str, Vda.Connection cnc) throws GLib.Error {
    if (str.down ().has_prefix ("select")) {
      var cmd = new CommandSelect (cnc);
      cmd.parse (str);
      return cmd;
    }
    if (str.down ().has_prefix ("insert")) {
      var cmd = new CommandInsert (cnc);
      cmd.parse (str);
      return cmd;
    }
    if (str.down ().has_prefix ("update")) {
      var cmd = new CommandUpdate (cnc);
      cmd.parse (str);
      return cmd;
    }
    if (str.down ().has_prefix ("delete")) {
      var cmd = new CommandDelete (cnc);
      cmd.parse (str);
      return cmd;
    }
    throw new ParserError.INVALID_TOKEN_ERROR (_("Not supported command"));
  }
}

/**
 * Parser error codes
 */
public errordomain Vda.ParserError {
  INVALID_TOKEN_ERROR,
  INVALID_EXPRESSION_ERROR
}

