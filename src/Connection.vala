/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * Connection.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * An interface representing a database engine connection.
 *
 * Core functionality are provided. Features provided by
 * database engines will be exposed by the object implementing
 * this interface or by future interfaces.
 */
public interface Vda.Connection : GLib.Object {
  /**
   * Connection to database engine status as {@link Status}
   */
  public abstract Status status { get; }
  /**
   * Set of connection parameters used with the database engine
   */
  public abstract ConnectionParameters parameters { get; set; }
  /**
   * Check if the database is opened or not.
   */
  public abstract bool is_opened { get; }
  /**
   * A string representation of all connection parameters
   */
  public abstract string connection_string { get; }

  /**
   * Signal emitted when the connection has been closed
   */
  public signal void closed ();
  /**
   * Signal emitted when the connection has been stablished
   */
  public signal void opened ();
  /**
   * Signal emitted when the connection start a process of closing
   */
  public signal void closing ();
  /**
   * Signal emitted when the connection has been canceled
   */
  public signal void canceled (string message);
  /**
   * Signal emitted when the connection has been canceled due to a timeout
   */
  public signal void timeout ();
  /**
   * Thrown if the connection to the server is not available any.
   */
  public signal void disconnected ();
  /**
   * Should throw {@link canceled} event at connection closed.
   */
  public abstract async void close () throws GLib.Error;
  /**
   * Start a connection to a database. Initialy will return {@link Status.IN_PROGRESS}
   * if the connection has been started. Returns {@link Status.CANCELED} if the connection
   * can't be started.
   *
   * If the connection is not done after a timeout, will returns {@link Status.CANCELED}.
   *
   * If the connection is stablished, {@link opened} signal is rised and {@link status}
   * is set to {@link Status.CONNECTED}
   *
   */
  public abstract async Status open () throws GLib.Error;
  /**
   * Open a connection to a database engine using a string with all required parameters.
   */
  public abstract async Status open_from_string (string cnc_string) throws GLib.Error;
  /**
   * Parse a string and return a {@link Query} to execute it as an SQL command
   */
  public abstract Vda.Query parse_string (string sql) throws GLib.Error;
  /**
   * String to parse contains {@link Vda.SqlExpressionValueParameter}
   * to construct the final SQL command
   * to execute. The query is saved using the given name and retrieved by
   * {@link get_prepared_query}
   */
  public abstract Vda.PreparedQuery? parse_string_prepared (string? name, string sql) throws GLib.Error;
  /**
   * Finds a saved {@link PreparedQuery}
   */
  public abstract Vda.PreparedQuery? get_prepared_query (string name);
  /**
   * Creates a {@link Query} from a {@link SqlCommand}
   */
  public abstract Vda.PreparedQuery? query_from_command (SqlCommand cmd, string? name) throws GLib.Error;

  /**
   * Quotes a string representation of a given value, as expected by
   * the connection.
   */
  public virtual string value_to_quoted_string (SqlValue v) {
    if (v is SqlValueNull) {
      return "NULL";
    }
    if (v is SqlValueString) {
      string str = v.to_string ();
      if (str == null) {
        return "NULL";
      }
      if (str.contains("'")) {
        str = str.replace("'","''");
      }
      return "'"+str+"'";
    } else if (v is SqlValueDate
      || v is SqlValueTimestamp) {
      return "'"+v.to_string()+"'";
    } else if (v is SqlValueInteger
            || v is SqlValueNumeric
            || v is SqlValueMathExp) {
      return v.to_string ();
    }
    return v.to_sql_expression ();
  }
  /**
   * Extract information about connection's locale settings.
   *
   * Supported categories are
   *
   * * LC_COLLATE Shows the collation order locale
   * * LC_CTYPE Shows the character classification and case conversion locale
   * * LC_MESSAGES Shows the language in which messages are displayed
   * * LC_MONETARY Shows the locale for formatting monetary amounts
   * * LC_NUMERIC Shows the locale for formatting numbers
   * * LC_TIME Shows the locale for formatting date and time values
   *
   * @param category name of the category to query
   */
  public virtual string locale (string category) { return ""; }
  /**
   * Status of the connection
   */
  public enum Status {
    INVALID,
    /**
     * Connection is in progress.
     */
    IN_PROGRESS,
    /**
     * Connection is canceled, because authentication or parameters
     */
    CANCELED,
    /**
     * Connection is made to the server, but still in progress
     */
    MADE,
    /**
     * Connection is disconnected.
     */
     DISCONNECTED,
    /**
     * Connection is disconnected, because no response from the server.
     */
     TIMEOUT,
    /**
     * Connection is done and ready.
     */
     CONNECTED
  }
}

/**
 * Connection error codes
 */
public errordomain Vda.ConnectionError {
  NO_DATABASE_NAME_ERROR,
  SERVER_ERROR,
  QUERY_CREATION_ERROR
}

