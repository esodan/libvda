/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * PostgresQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Postgres;

internal errordomain Vpg.QueryError {
  SERVER_FATAL_ERROR
}

internal class Vpg.Query : Object, Vda.Query {
  protected string _sql = null;
  protected Vda.Connection cnc = null;
  internal Postgres.Result res = null;

  public Query (Vda.Connection con) {
    cnc = con;
  }
  public Query.from_sql (Vda.Connection con, string sql) {
    cnc = con;
    _sql = sql;
  }

  public string sql {
    owned get {
      if (_sql != null) {
        return _sql;
      } else {
        return render_sql ();
      }
    }
  }
  public Vda.Connection connection { get { return cnc; } }
  public async Vda.Result? execute (GLib.Cancellable? cancellable) throws GLib.Error {
    string nsql = render_sql ();
    res = ((Vpg.Connection) cnc).get_database ().exec (nsql);
    var s = res.get_status ();
    if (s == Postgres.ExecStatus.FATAL_ERROR) {
      string errmsg = res.get_error_message ();
      throw new QueryError.SERVER_FATAL_ERROR (errmsg);
    }
    if (s == Postgres.ExecStatus.BAD_RESPONSE) {
      return new InvalidResult (_("Bad response from the server"));
    }
    if (s == Postgres.ExecStatus.TUPLES_OK) {
      return new Vpg.TableModel (this);
    }
    string strres = res.get_cmd_tuples ();
    if (strres != "") {
      return new Vda.AffectedRows (int.parse (strres));
    }
    return new InvalidResult (_("No valid result was calculated"));
  }
  public async void cancel () {
    warning (_("Not implemented"));
  }
  public virtual string render_sql () {
    return _sql;
  }
  internal unowned Postgres.Result get_result () { return res; }
}

class Vpg.Result : Object, Vda.Result {
  protected Vpg.Query _query;
  protected Vda.Connection cnc;
  public Vda.Connection connection { get { return cnc; } }
  public Result (Vpg.Query query) {
    _query = query;
  }
}

class Vpg.TableModel : Vpg.Result, GLib.ListModel, Vda.TableModel {

  // GLib.ListModel interface
  public uint get_n_items () {
    return (uint) _query.get_result ().get_n_tuples ();
  }

  public GLib.Type get_item_type () {
    return typeof (RowModel);
  }

  public GLib.Object? get_item (uint position) {
    if (position > get_n_items ())
      return null;
    return new Vpg.RowModel (_query, (int) position) as Object;
  }
  public TableModel (Vpg.Query query) {
    base (query);
  }
}

class Vpg.RowModel : Object, GLib.ListModel, Vda.RowModel {
  protected Vpg.Query _query;
  int row = -1;
  public uint n_columns {
    get {
      return (uint) _query.get_result ().get_n_fields ();
    }
  }
  public Vda.ColumnModel? get_column (string name) throws GLib.Error {
    int nc = _query.get_result ().get_field_number (name);
    if (nc == -1) {
      throw new Vda.RowModelError.INVALID_COLUMN_NAME_ERROR (_("Can't find column. Invalid column name: '%s'"), name);
    }
    return new Vpg.ColumnModel (_query, nc);
  }
  public Vda.ColumnModel? get_column_at (uint col) throws GLib.Error  {
    string cn = _query.get_result ().get_field_name ((int) col);
    if (cn == null) {
      throw new Vda.RowModelError.INVALID_COLUMN_NUMBER_ERROR (_("Can't find column. Invalid column number: %u"), col);
    }
    return new Vpg.ColumnModel (_query, col);
  }
  public SqlValue? get_value (string name) throws GLib.Error {
    int nc = _query.get_result ().get_field_number (name);
    if (nc == -1) {
      throw new Vda.RowModelError.INVALID_COLUMN_NAME_ERROR (_("Can't get value. Invalid column name: '%s'"), name);
    }
    if (_query.get_result ().is_null (row, nc)) {
      return new ValueNull ();
    }
    uint oid = (uint) _query.get_result ().get_field_type (nc);
    var v = ((Vpg.Connection) _query.connection).create_value_from_oid (oid);
    string val = _query.get_result ().get_value (row, nc);
    v.parse (val);
    return v;
  }
  public SqlValue? get_value_at (uint col) throws GLib.Error {
    string nc = _query.get_result ().get_field_name ((int) col);
    if (nc == null) {
      throw new Vda.RowModelError.INVALID_COLUMN_NUMBER_ERROR (_("Can't get value. Invalid column number %u"), col);
    }
    if (_query.get_result ().is_null (row, (int) col)) {
      return new ValueNull ();
    }
    uint oid = (uint) _query.get_result ().get_field_type ((int) col);
    var v = ((Vpg.Connection) _query.connection).create_value_from_oid (oid);
    string val = _query.get_result ().get_value (row, (int) col);
    v.parse (val);
    return v;
  }
  public string? get_string (string name) throws GLib.Error {
    int nc = _query.get_result ().get_field_number (name);
    if (nc == -1) {
      throw new Vda.RowModelError.INVALID_COLUMN_NAME_ERROR (_("Invalid column name %s"), name);
    }
    if (_query.get_result ().is_null (row, nc)) {
      return new ValueNull ().to_string ();
    }
    string c = _query.get_result ().get_value (row, nc).dup ();
    return c;
  }
  public string? get_string_at (uint col) throws GLib.Error {
    string cn = _query.get_result ().get_field_name ((int) col);
    if (cn == null) {
      throw new Vda.RowModelError.INVALID_COLUMN_NUMBER_ERROR (_("Can't get value. Invalid column number %u"), col);
    }
    if (_query.get_result ().is_null (row, (int) col)) {
      return new ValueNull ().to_string ();
    }
    string c = _query.get_result ().get_value (row, (int) col).dup ();
    return c;
  }

  // GLib.ListModel interface
  public uint get_n_items () { return n_columns; }

  public GLib.Type get_item_type () { return typeof (ColumnModel); }

  public GLib.Object? get_item (uint position) {
    if (position > n_columns)
      return null;
    return new Vpg.ColumnModel (_query, position) as Object;
  }

  // Constructor
  public RowModel (Vpg.Query query, int row) {
    _query = query;
    this.row = row;
  }
}

class Vpg.ColumnModel : Object, Vda.ColumnModel {
  uint col = -1;
  Vpg.Query _query = null;

  public string name {
    get {
      return _query.get_result ().get_field_name ((int) col);
    }
  }
  public GLib.Type data_type {
    get {
      var oid = (uint) _query.get_result ().get_field_type ((int) col);
      return ((Vpg.Connection) _query.connection).for_data_type_oid (oid);
    }
  }
  public ColumnModel (Vpg.Query query, uint col) {
    this.col = col;
    _query = query;
  }
}
