/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * ConnectionPostgresql.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2018-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vda;
using Postgres;
/**
 * Implementation of {@link Vda.Connection} for a direct connection to a PostgreSQL server
 */
public class Vpg.Connection : GLib.Object, Vda.Connection {
  private Postgres.Database database = null;
  private uint time = 10;
  private Vda.Connection.Status _status = Vda.Connection.Status.DISCONNECTED;
  private string _cnc_string;
  private Gee.HashMap<uint,string> _types_name = new Gee.HashMap<uint,string> ();
  private Gee.HashMap<uint,GLib.Type> _types = new Gee.HashMap<uint,GLib.Type> ();
  private Gee.HashMap<string,PreparedQuery> _queries = new Gee.HashMap<string,PreparedQuery> ();

  internal unowned Postgres.Database get_database () { return database; }

  internal SqlValue create_value_from_oid (uint oid) {
    if (_types.size == 0) {
      _populate_types ();
    }
    var t = _types.get (oid);
    var o = Object.new (t) as SqlValue;
    return o;
  }
  internal GLib.Type for_data_type_oid (uint oid) {
    if (_types.size == 0) {
      _populate_types ();
    }
    var t = _types.get (oid);
    return t;
  }

  private void _populate_types ()  {
    // Populate types
    var res = database.exec ("SELECT oid, typname FROM pg_type");
    for (int i = 0; i < res.get_n_tuples (); i++) {
      uint id = -1;
      string oidv = res.get_value (i,0);
      if (oidv != null) {
        id = (uint) double.parse (oidv);
      }
      string tname = res.get_value (i,1);
      if (id != -1 && tname != null) {
        _types_name.set (id, tname);
        GLib.Type dtype = GLib.Type.INVALID;
        switch (tname) {
          case "any":
            break;
          case "abstime":
            break;
          case "address":
            break;
          case "bool":
            dtype = typeof (ValueBool);
            break;
          case "bit":
            dtype = typeof (ValueBit);
            break;
          case "box":
            break;
          case "bpchar":
            break;
          case "bytea":
            break;
          case "cardinal_number":
            break;
          case "character_data":
            break;
          case "character_sets":
            break;
          case "char":
            break;
          case "cid":
            break;
          case "cidr":
            break;
          case "circle":
            break;
          case "cstring":
            break;
          case "date":
            dtype = typeof (ValueDate);
            break;
          case "daterange":
            break;
          case "float4":
            dtype = typeof (ValueFloat);
            break;
          case "float8":
            dtype = typeof (ValueDouble);
            break;
          case "gtsvector":
            break;
          case "inet":
            break;
          case "int2":
            dtype = typeof (ValueInt2);
            break;
          case "int2vector":
            break;
          case "int4":
            dtype = typeof (ValueInt4);
            break;
          case "int4range":
            break;
          case "int8":
            dtype = typeof (ValueInt8);
            break;
          case "int8range":
            break;
          case "interval":
            break;
          case "json":
            dtype = typeof (ValueJson);
            break;
          case "jsonb":
            break;
          case "line":
            break;
          case "lseg":
            break;
          case "macaddr":
            break;
          case "macaddr8":
            break;
          case "money":
            dtype = typeof (ValueMoney);
            break;
          case "name":
            dtype = typeof (ValueName);
            break;
          case "numeric":
            dtype = typeof (ValueNumeric);
            break;
          case "numrange":
            break;
          case "oid":
            dtype = typeof (ValueOid);
            break;
          case "oidvector":
            break;
          case "path":
            break;
          case "point":
            break;
          case "polygon":
            break;
          case "record":
            break;
          case "reltime":
            break;
          case "smgr":
            break;
          case "text":
            dtype = typeof (ValueText);
            break;
          case "tid":
            break;
          case "time":
            dtype = typeof (ValueTimeNtz);
            break;
          case "time_stamp":
            dtype = typeof (ValueTimestampNtz);
            break;
          case "timestamp":
            dtype = typeof (ValueTimestampNtz);
            break;
          case "timestamptz":
            dtype = typeof (ValueTimestamp);
            break;
          case "timetz":
            dtype = typeof (ValueTime);
            break;
          case "tinterval":
            break;
          case "transforms":
            break;
          case "uuid":
            break;
          case "varbit":
            break;
          case "varchar":
            dtype = typeof (ValueString);
            break;
          case "void":
            break;
          case "xid":
            break;
          case "xml":
            dtype = typeof (ValueXml);
            break;
          case "yes_or_no":
            break;
        }
        if (dtype != GLib.Type.INVALID) {
          _types.set (id, dtype);
        }
      }
    }
  }
  internal string locale (string category) {
    string sql = "SHOW ";
    switch (category) {
      case "LC_COLLATE":
        sql += "lc_collate";
        break;
      case "LC_CTYPE":
        sql += "lc_ctype";
        break;
      case "LC_MESSAGES":
        sql += "lc_messages";
        break;
      case "LC_MONETARY":
        sql += "lc_monetary";
        break;
      case "LC_NUMERIC":
        sql += "lc_numeric";
        break;
      case "LC_TIME":
        sql += "lc_time";
        break;
      default:
        return "";
    }
    var res = database.exec (sql);
    if (res.get_n_tuples () == 0) {
      return "";
    }
    return res.get_value (0,0);
  }

  // Vda.Connection
  internal Vda.Connection.Status status { get { return _status; } }
  internal ConnectionParameters parameters { get; set; }
  internal bool is_opened {
    get  {
      if (database == null)
        return false;
      return database.get_status () == Postgres.ConnectionStatus.OK;
    }
  }
  internal string connection_string { get { return _cnc_string; } }

  internal async void close () throws GLib.Error {
    if (database == null) return;
    database = null;
  }
  internal async Vda.Connection.Status open () throws GLib.Error {
    if (parameters == null) {
      _status = Vda.Connection.Status.DISCONNECTED;
      return Vda.Connection.Status.CANCELED;
    }
    var h = parameters.get ("HOST");
    if (h == null) {
      throw new Vda.ConnectionError.NO_DATABASE_NAME_ERROR (_("No database's host is given"));
    }
    var db = parameters.get ("DB_NAME");
    if (db == null) {
      throw new Vda.ConnectionError.NO_DATABASE_NAME_ERROR (_("No database's name is given"));
    }
    var user = parameters.get ("USERNAME");
    var pass = parameters.get ("PASSWORD");
    _cnc_string = "postgresql://";
    if (user != null)
      _cnc_string += user.@value;
    if (pass != null)
      _cnc_string += ":"+ pass.@value+"@";
    _cnc_string += h.@value+"/"+db.@value;
    bool first = true;
    foreach (Vda.ConnectionParameter par in parameters.values) {
      switch (par.name) {
        case "DB_NAME":
        case "HOST":
        case "PORT":
        case "USERNAME":
        case "PASSWORD":
          continue;
        default:
          if (!first) {
            _cnc_string += "?";
          } else {
            _cnc_string += "&";
            first = false;
          }
          _cnc_string += par.name + "=" + par.@value;
          break;
      }
    }
    time = 1000;
    GLib.Idle.add (()=>{
      database = Postgres.connect_db (_cnc_string);
      if (database == null) {
        _status = Vda.Connection.Status.CANCELED;
        canceled ("Was not possible to create a connection handler");
        return GLib.Source.REMOVE;
      }
      switch (database.get_status()) {
        case Postgres.ConnectionStatus.OK:
        case Postgres.ConnectionStatus.MADE:
          _status = Vda.Connection.Status.CONNECTED;
          opened ();
          break;
        case Postgres.ConnectionStatus.BAD:
          _status = Vda.Connection.Status.CANCELED;
          canceled (database.get_error_message ());
          break;
        default:
          time--;
          if (time <= 0) {
            _status = Vda.Connection.Status.TIMEOUT;
            timeout ();
          }
          break;
      }
      return GLib.Source.REMOVE;
    });
    // Timeout.add (10, ()=>{
    //   bool res = GLib.Source.CONTINUE;
    //     message ("Connection Status: %s", status.to_string ());
    //   switch (database.get_status()) {
    //     case Postgres.ConnectionStatus.OK:
    //     case Postgres.ConnectionStatus.MADE:
    //       _status = Vda.Connection.Status.CONNECTED;
    //       opened ();
    //       res = GLib.Source.REMOVE;
    //       break;
    //     case Postgres.ConnectionStatus.BAD:
    //       _status = Vda.Connection.Status.CANCELED;
    //       canceled (database.get_error_message ());
    //       res = GLib.Source.REMOVE;
    //       break;
    //     default:
    //       time--;
    //       if (time <= 0) {
    //         _status = Vda.Connection.Status.TIMEOUT;
    //         timeout ();
    //         res = GLib.Source.REMOVE;
    //       }
    //       break;
    //   }
    //   return res;
    // });
    _status = Vda.Connection.Status.IN_PROGRESS;
    return _status;
  }
  internal async Vda.Connection.Status open_from_string (string cnc_string) throws GLib.Error {
    parameters = new ConnectionParameters (cnc_string);
    return yield open ();
  }

  internal Vda.Query parse_string (string sql) throws GLib.Error {
    var q = new Vpg.Query.from_sql (this, sql);
    return q;
  }
  internal Vda.PreparedQuery? parse_string_prepared (string? name, string sql) throws GLib.Error {
    var q = new Vpg.ParsedQuery (this, name != null ? name : "");
    q.parse (sql);
    _queries.set (name, q);
    return q;
  }
  internal Vda.PreparedQuery? get_prepared_query (string name) {
    return _queries.get (name);
  }

  internal Vda.PreparedQuery? query_from_command (SqlCommand cmd, string? name) throws GLib.Error {
    if (this != cmd.connection) {
      throw new Vda.ConnectionError.QUERY_CREATION_ERROR (_("Command is not using same connection"));
    }
    return new ParsedQuery.from_command (cmd, name);
  }

  // ConnectionTransactional
  // internal bool add_savepoint (string? name) throws GLib.Error { return false; }
  // internal bool delete_savepoint (string? name) throws GLib.Error { return false; }
  // internal bool rollback_savepoint (string? name) throws GLib.Error { return false; }
  // internal bool begin_transaction (string? name) throws GLib.Error { return false; }
  // internal bool commit_transaction (string? name) throws GLib.Error { return false; }
  // internal bool rollback_transaction (string? name) throws GLib.Error { return false; }
  //
  // ConnectionRolebased
  //
  // internal Role? current_user () {
  //   return new PostgresRole (this);
  // }

}

