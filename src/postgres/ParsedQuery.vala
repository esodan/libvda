/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GParsedQuery.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represent a prepared query implementing {@link Vda.PreparedQuery}.
 *
 * Values required by query can be set by using paramenters property.
 */
internal class Vpg.ParsedQuery : Vpg.Query, Vda.PreparedQuery, Vda.ParsedQuery {
  protected string _name;
  protected Vda.SqlCommand _command;
  protected Vda.SqlParameters _dummy_params = new Vda.Parameters ();
  protected Vda.Connection _connection;
  public ParsedQuery (Vda.Connection con, string name) {
    base (con);
    _name = name;
    _connection = con;
  }
  public ParsedQuery.from_command (Vda.SqlCommand cmd, string? name) {
    base (cmd.connection);
    _name = name;
    _command = cmd as Vda.SqlCommand;
  }
  /**
   * Parse an SQL using {@link Vda.Parser}
   */
  public void parse (string sql) throws GLib.Error {
    _command = new Vda.Parser ().parse (sql, _connection);
  }
  // Query
  public override string render_sql () {
    if (_command is Vda.Stringifiable) {
      return ((Vda.Stringifiable) _command).to_string ();
    }
    return "";
  }
  // PreparedQuery
  public string name { get { return _name; } }
  public Vda.SqlParameters parameters {
    get {
      if (_command != null && _command is Vda.SqlCommandParametrized) {
        return ((Vda.SqlCommandParametrized) _command).parameters;
      }
      warning (_("No internal command was set, so dummy parameters object has been returned"));
      return _dummy_params;
    }
  }
  // ParsedQuery
  public Vda.SqlCommand command { get { return _command; } }
}
