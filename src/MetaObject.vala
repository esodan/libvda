/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * ObjectMeta.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2011-2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gee;

namespace Vda {
	/**
	 * Base interface for introspected database's objects
	 */
	public interface MetaObject : Object
	{
		public abstract Connection   connection { get; set; }
	}

	/**
	 * Base interface for named introspected database's objects
	 */
	public interface MetaNamedObject : Object, MetaObject
	{
		public abstract string       name { get; set; }
	}

	/**
	 * Meta Data Object error codes
	 */
	public errordomain MetaObjectError {
    	APPEND,
    	UPDATE,
    	SAVE,
    	DROP
    }

}
