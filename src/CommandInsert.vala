/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * CommandInsert.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Implementation of {@link SqlCommandInsert} SQL command
 */
public class Vda.CommandInsert  : CommandModification,
															Stringifiable,
															SqlCommandInsert
{
	public CommandInsert (Connection cnc) {
		base (cnc);
	}
	// Stringifiable
	internal string to_string () {
		string str = "";
		try {
			str = stringify ();
		} catch (GLib.Error e) {
			warning (_("Fail to convert to string: %s"), e.message);
		}
		return str;
	}
}

