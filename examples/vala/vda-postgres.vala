/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * vda-gda-sqlite.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Vda;

class App : Object {

  public static int main () {
		GLib.MainLoop loop = new GLib.MainLoop ();
		var cnc = new Vpg.Connection ();
		// Connect to opened signal once your database has been opened
		cnc.opened.connect (()=>{
			try {
				stdout.printf ("Executing query...\n");
				var q = cnc.parse_string ("SELECT * FROM users");
				q.execute.begin (null, (obj, res)=>{
					try {
						var r = q.execute.end (res);
						stdout.printf ("Accessing the returned data...\n");
						if (r is TableModel) {
							var t = r as TableModel;
							for (int i = 0; i < t.get_n_items (); i++) {
								var row = t.get_item (i) as Vda.RowModel;
								if (row == null) {
									warning ("Invalid returned object type: expected RowModel");
									continue;
								}
								stdout.printf ("Row %d ===================\n", i);
								for (int j = 0; j < row.get_n_items (); j++) {
									var c = row.get_column_at (j);
									var v = row.get_value_at (j);
									stdout.printf ("%s = '%s' | ", c.name, v.to_string ());
								}
								stdout.printf ("\n===================\n");
							}
						} else {
							warning ("Returned object is not a TableModel");
						}
						loop.quit ();
					} catch (Error e) {
						warning ("Error executing query: %s", e.message);
					}
				});
			} catch (Error e) {
				warning ("Error parsing query: %s", e.message);
			}
		});
		cnc.open_from_string.begin ("HOST=localhost;DB_NAME=test;USERNAME=test;PASSWORD=test1", (obj, res)=>{
			try {
				cnc.open_from_string.end (res);
				stdout.printf ("Connecting...\n");
			} catch (Error e) {
				warning ("Error opening the database: %s", e.message);
			}
		});
  	loop.run ();
  	return 0;
  }
}

