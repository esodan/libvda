/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * vda-gda-sqlite.vala
 *
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libvda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libvda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Vda;
using Vgda;
using Gda;

class App : Object {
	private Vda.Connection cnc;
	private string cnc_params = "DB_NAME=test;DB_DIR=.";
	GLib.MainLoop loop;

	construct {
		loop = new GLib.MainLoop ();
    cnc = new Vgsl.Connection ();
    cnc.opened.connect (()=>{
    	message ("Connection made successfully");
    	db_init.begin ((obj, res)=>{
		  	try {
		  		db_init.end (res);
		  		run_queries.begin ();
				} catch (GLib.Error e) {
					warning ("Initializing DB Error: %s", e.message);
					loop.quit ();
				}
    	});
    });
	}
	async void db_init () throws GLib.Error {
		message ("Creating tables");
		var q1 = cnc.parse_string ("CREATE TABLE users (id PRIMARY KEY, name text)");
		yield q1.execute (null);
		var q2 = cnc.parse_string ("CREATE TABLE groups (id PRIMARY KEY, name text)");
		yield q2.execute (null);
		var q3 = cnc.parse_string ("CREATE TABLE membership (id PRIMARY KEY, user integer, 'group' integer)");
		yield q3.execute (null);
		message ("Inserting Data into database");
		var q4 = cnc.parse_string ("INSERT INTO users (name) VALUES ('VdaUser1')");
		yield q4.execute (null);
		message ("Inserting user 2");
		var q5 = cnc.parse_string ("INSERT INTO users (name) VALUES ('VdaUser2')");
		yield q5.execute (null);
		var q6 = cnc.parse_string ("INSERT INTO groups (name) VALUES ('VdaGroup1')");
		yield q6.execute (null);
		var q7 = cnc.parse_string ("INSERT INTO groups (name) VALUES ('VdaGroup2')");
		yield q7.execute (null);
		var q8 = cnc.parse_string ("INSERT INTO membership (user, \"group\") VALUES (1, 1)");
		yield q8.execute (null);
		message ("Database initialized");
	}
	public int run () {
    // Open a connection is an async operation so call it and continue on opened event's callback
    cnc.open_from_string.begin (cnc_params, (obj, res)=>{
    	try {
    		cnc.open_from_string.end (res);
    	} catch (GLib.Error e) {
    		warning ("Error while opening connection: %s", e.message);
    		loop.quit ();
    	}
    });

    loop.run ();
    return 0;
	}
	public async void run_queries () throws GLib.Error {
  	// Query data from database
  	var qs = cnc.parse_string ("SELECT * FROM users");
  	var res = yield qs.execute (null);
  	var table = res as TableModel;
  	bool first = true;
  	for (int i = 0; i < table.get_n_items (); i++) {
  		var row = table.get_item (i) as RowModel;
  		for (int j = 0; j < row.get_n_items (); j++) {
				if (first){
					var c = row.get_column ("name");
					stdout.printf (c.name+"\n");
					first = false;
				}
				stdout.printf (row.get_string ("name")+"\n");
  		}
  	}
  	loop.quit ();
	}
  public static int main () {
  	var app = new App ();
  	stdout.printf ("If using GDA as subproject, then try to\n");
  	stdout.printf ("set GDA_TOP_BUILD_DIR environment variable to ${path_to}/subprojects/libgda\n");
  	return app.run ();
  }
}

