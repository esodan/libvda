# Vala Data Access Library

Is a set of interfaces and classes to access Data Bases.

## Backends

Backends are implementations of VDA for specific dabases. They are provided as
independent libraries depending on libvda. All backends are optional and will
be build by default if its dependencies are met; they can be disable.

### Native backends

A native backend is an implementation of VDA interfaces using the C and/or a Vala
bindings of a database engine, no intermidate are required.

#### PostgreSQL backend

VDA has a native implementation of its interfaces for [PostgreSQL](https://www.postgresql.org/).

#### SQLite backend

VDA has a native implementation of its interfaces for [SQLite](https://www.sqlite.org/).

#### GDA Backend

There are a set of classes implementing VDA interfaces using GDA as backend.
Allowing to use its features by using GDA's API.

Currently `PostgreSQL` and `SQLite` are supported from GDA bidings.

## Implementing New Providers

VDA requires to incrementally implement interfaces in order to add features. First implement
`Vda.Connection` interface for basic support.

### Implement Vda.Connection

`Vda.Connection` requires to implement parsing a connection string to fill `Vda.ConnectionParameters`.
Implementators should allow to change connection parameters in order to change the way a connection
is opened.

`Vda.Connection` should be opened using `open()` and return the its status, they could be
inmediatelly opened or should wait until it is stablished. Implement `close()` to close and
release resources. `open_from_string()` should takes a string, parse it to fill connection
parameters and be available from `connection_string` property.

In order to run queries, implementors should allow to parse commands, SQL for example, using
a string as a source using `parse_string()` returning an implementation of `Vda.Query`.

`parse_string_prepared()`should return an implementation of `Vda.PreparedQuery` object.

Optionally implementators can use `query_from_command()` using a `Vda.SqlCommand` to takes an
SQL command structure to create a native query to be used by the connection.

If your database use a different quote for a given `Vda.SqlValue` to be used in an SQL command,
if `''` is used, you don't need to implement your own.

### Implement Vda.Query

`Vda.Query` stores an string of the command to execute over the database. `render_sql()` can
return `sql` property value or create a string representation using database's connection
capabilities.

Use `execute()` to run the query on the dabase connection, using its own API, and return aç
`Vda.Result`, to get access to the data or result status.

### Implement Vda.PreparedQuery

If you run queries with different values, to change the results, you should implement
`Vda.PreparedQuery` givin it a name to fast access using the provider native API as the
best practice if supported.

Use `parameters` property to set values on a `Vda.SqlParameters` holding `Vda.SqlValue` objects
supported by the native prepared query in the provided. Is a implementators responsability,
translate values to native values in the native prepared query, quoting as required.

This is the more secure way to set values to queries, running many times with different
paramenters (values), in order to avoid SQL injection attacks!

### Implement Vda.Result

Once a native query has been executed, the database should return a `Vda.Result`. If the
return is a fail, return a `Vda.InvalidResult` with a optionally description of the error
using `Vda.InvalidResult.message` property.

If the result of the query is an affected number of rows, you can return `Vda.SqlAffectedRows`
object implementation; some examples of this is when running and `DELETE` or `INSERT`
commands when SQL is in use; access to the number of rows using its `number` property
implementation. Use `Vda.Inserted` to implement access to the last row inserted. 

If the query returns data implementators should implement `Vda.TableModel` result, when all
data is available at once. If the content should access sequentially use an implementation
of `Vda.TableModelSequential`.

### Implement Vda.TableModel

If the returned result from `Vda.Query.execute()` contains data, that should be available
using an implementation of `Vda.TableModel`.

`Vda.TableModel` represent a table, with rows representation using `Vda.RowModel` implementations
and columns represented by `Vda.ColumnModel` implementation.

Access to rows should be using `GLib.ListModel` API, using numbered rows.

### Implement Vda.TableModelSequential

As for `Vda.TableModel` data should be accessed using `Vda.RowModel`, but current row should
be accessed using `current()` and navigate using `back()`, `next()` and `move()`, depending on
the capabilities of the database native API.

### Implement Vda.RowModel

A row has a set of columns you can get access to using `get_column()` and `get_column_at()` API,
implementator shoud provide an implementation of `Vda.ColumnModel` for data information.

To access data in a row, implement `get_value()` and `get_value_at()`. Is usefull to provide
an string representation of the data implementing `get_string()`and `get_string_at()`. Values
are represented using `Vda.SqlValue` implementations by the provided, depeding on the data
contained in the row at given position.

### Implement Vda.SqlValue

There are native implementations of `Vda.SqlValue` sub-interfaces, for basic data like string
and numbers, or more complex ones like XML or JSON, from `Vda.Value` derived classes.
Is an implementators' responsibility detect returned data and translate the data to fill
VDA's native values. Is possible to create your own datatypes by implementing `Vda.SqlValue`
interface, if you do so, add an issue to create more native VDA data classes supporting
your data for portability to other providers.

Implementators, should use its `Vda.Connection` implementation in order to provide data
conversion to `Vda.SqlValue` values. 

#### Arraw porting

[Apache Arrow](https://arrow.apache.org/) from its site: "Apache Arrow defines a language-independent
columnar memory format for flat and hierarchical data, organized for efficient analytic operations
on modern hardware like CPUs and GPUs. The Arrow memory format also supports zero-copy reads for
lightning-fast data access without serialization overhead".

## Dependencies

### Required

`libgee-0.8`

### Optional

For PostgreSQL:

1.  `libpq`

For SQLite:

1.  `sqlite3`

For GDA backend and its dependencies for the target database engine:

1.  `libgda-6.0`

## Build, Test and install
If all the dependencies and test databases are in place, just go: 

```
$ meson _build
$ cd _build
$ ninja
$ ninja install
$ meson test
```

Make sure to set `--prefix` to the base directory for installation, like `/usr`
if you need system wide installation.

### Test Databases for PostgreSQL

VDA unit tests for PostgreSQL requires, in order to succeed, you have the following databases and users:

1.  A Database called `test`
1.  A Database called `vda_test`
1.  A user called `test` with password `test1`

The `test` user should be able to connect to `test` and `vda_test` databases using a localhost connection.

# Documentation

There are API bindings for Vala for master version located at its 
[repository pages](https://esodan.pages.gitlab.gnome.org/libvda/).

There is also a [Wiki in its repository too](https://gitlab.gnome.org/esodan/libvda/wikis/home).

# Bindings

VDA is written in Vala and produce GObject Introspection descriptions, so it has native bindings
built from sources:

1.  Vala Bindings
2.  C API
3.  Python by GObject Introspection
4.  JavaScript by GObject Introspection
5.  Any supported language using GObject Introspection

# Data Handling

## DataObject

VDA provides an interface to map almost automatically any `GLib.Object` class
to a Database's row in a table, allowing:

* Map properties to Row's fields
* Automatic Data type Conversions
* Create new rows (`INSERT`)
* Changes values (`UPDATE`)
* Remove data (`DELETE`)

An object just need to implement `Vda.DataObject`, set the database's connection
and the table's name the data comes from/to.

Data is mapped to object's properties, which has the same nick name as the
table's correspoding column's name. Is important to declare primary keys, so
select will bind data for only one table's row; while update and delete queries,
are executed only for one object, for details check `Vda.DataObject` documentation. 

# Incremental Features based on Interfaces

Connection features interfaces are desiged to be implemented incrementatly,
to provide connection characteristics depending on the database provider.

The more interfaces are implemented, the more features are available to the programmer.

Future characteristics will be added in next versions, so in order to
know if an specific `Vda.Connection` support specific features, like role-based
access, checking if it is an `Vda.ConnectionRolebased`, to be added is required.

Incremental features, allows in to quickly add support of new dabases, by mapping
basic features to a `Vda.Connection` interface; then implement more interface
to expose new ones.
